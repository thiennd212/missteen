﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="MyWeb._Default" %>

    <!DOCTYPE HTML>
    <html>

    <head runat="server">
        <title></title>
        <asp:Literal runat="server" ID="ltrMainMeta"></asp:Literal>
        <%--META--%>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <asp:Literal ID="ltrMeta" runat="server"></asp:Literal>

            <%--style import--%>
                <link rel="stylesheet" type="text/css" href="theme/default/dist/css/bootstrap.min.css" />
                <link rel="stylesheet" type="text/css" href="theme/default/css/font-awesome.min.css" />
                <link rel="stylesheet" type="text/css" href="theme/default/css/datepicker.css" />
                <link rel="stylesheet" type="text/css" href="theme/default/css/bootstrap-datepicker.css" />
                <link rel="stylesheet" type="text/css" href="theme/default/plugins/owl-carousel/owl.carousel.css">
                <link rel="stylesheet" type="text/css" href="theme/default/plugins/owl-carousel/owl.theme.css">
                <link rel="stylesheet" type="text/css" href="theme/default/plugins/owl-carousel/owl.transitions.css">
                <link rel="stylesheet" type="text/css" href="theme/default/plugins/cloudzoom/css/cloudzoom.css" />
                <link rel="stylesheet" type="text/css" href="theme/default/plugins/cloudzoom/css/thumbelina.css" />
                <link rel="stylesheet" type="text/css" href="theme/default/css/styles.css" />
                <link type="text/css" rel="stylesheet" href="../../theme/default/plugins/lightbox/colorbox.css" />
                <asp:Literal ID="lrtStyleSheet" runat="server"></asp:Literal>
                <%--style mobile--%>
                    <link rel="stylesheet" type="text/css" href="theme/default/css/icons.css" />
                    <link rel="stylesheet" type="text/css" href="theme/default/plugins/responside/component.css" />

                    <%--javascript import--%>
                        <!--[if lt IE 9]><script src="theme/default/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
                        <script src="theme/default/assets/js/ie-emulation-modes-warning.js"></script>
                        <script src="theme/default/assets/js/ie10-viewport-bug-workaround.js"></script>
                        <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
                        <script type="text/javascript" src="theme/default/js/jquery.min.js"></script>
                        <script type="text/javascript" src="theme/default/dist/js/bootstrap.min.js"></script>
                        <script type="text/javascript" src="theme/default/js/bootstrap-typeahead.js"></script>
                        <script type="text/javascript" src="theme/default/plugins/owl-carousel/owl.carousel.js"></script>
                        <script type="text/javascript" src="theme/default/js/script.js"></script>
                        <script type="text/javascript" src="theme/default/plugins/cloudzoom/js/cloudzoom.js"></script>
                        <script type="text/javascript" src="theme/default/plugins/cloudzoom/js/thumbelina.js"></script>
                        <script type="text/javascript" src="../../theme/default/plugins/lightbox/jquery.colorbox.js"></script>
                        <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

                        <%--javascript mobile--%>
                            <script type="text/javascript" src="theme/default/plugins/responside/modernizr.custom.js"></script>
                            <script type="text/javascript" src="theme/default/plugins/responside/classie.js"></script>
                            <script type="text/javascript" src="theme/default/plugins/responside/mlpushmenu.js"></script>
                            <script type="text/javascript" src="theme/default/js/bootstrap-datepicker.js"></script>

                            <%--lazy load--%>
                                <script type="text/javascript" src="scripts/jquery.lazy/jquery.lazy.min.js"></script>
    </head>

    <body>
        <script>
            window.fbAsyncInit = function () {
                FB.init({
                    appId: '1874508279469250',
                    status: true, // check login status
                    cookie: true, // enable cookies to allow the server to access the session
                    autoLogAppEvents: true,
                    xfbml: true,
                    version: 'v2.9'
                });
            };
        </script>
        <!--onload="_googWcmGet('number', '<%= GlobalClass.conTel %>')"-->
        <form id="MyWebForm" runat="server">
            <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true"></asp:ScriptManager>
            <asp:Literal ID="ltrFacebook" runat="server"></asp:Literal>
            <asp:Literal ID="ltrH1" runat="server"></asp:Literal>
            <asp:Literal ID="ltrH" runat="server"></asp:Literal>
            <asp:PlaceHolder ID="MainPlaceHolder" runat="server"></asp:PlaceHolder>
            <asp:Literal ID="ltrFooterBody" runat="server"></asp:Literal>
            <asp:Literal ID="ltrAdv" runat="server"></asp:Literal>
            <asp:Literal ID="ltrPopup" runat="server"></asp:Literal>
            <asp:Literal ID="ltrBottomLayer" runat="server"></asp:Literal>
            <asp:Literal ID="ltrLiveChat" runat="server"></asp:Literal>

            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div id="loadViewPro"></div>
                        </div>
                    </div>
                </div>
            </div>



            <span id="top-link-block" class="hidden">
            <a href="#top" class="well-sm" onclick="$('html,body').animate({scrollTop:0},'slow');return false;">
                <img src="theme/default/img/icon_gototop.png" />
            </a>
        </span>
            <%--javascript initialize --%>
                <script type="text/javascript">
                    $(document).ready(function() {
                        var listChk = [];
                        $("input[name='checksub']").click(function() {
                            var le = $(this).val()
                            if (this.checked == true) {
                                listChk.push($(this).val());
                                $("input.box" + le).attr("checked", true);
                            } else {
                                $("input.box" + le).attr("checked", false);
                                var removeitem = $(this).val();
                                listChk = $.grep(listChk, function(value) {
                                    return value != removeitem;
                                });
                            }
                            if (listChk == "") {
                                window.location.reload();
                            } else {
                                var cat = window.location.href;
                                cat = cat.substr(cat.lastIndexOf('/') + 1);
                                cat = cat.substr(0, cat.indexOf(".html"));
                                $("#loadFillter").load("/search_results.aspx?attid=" + listChk + "&cat=" + cat);
                            }
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(function() {
                        var Accordion = function(el, multiple) {
                            this.el = el || {};
                            this.multiple = multiple || false;
                            var links = this.el.find('.link');
                            links.on('click', {
                                el: this.el,
                                multiple: this.multiple
                            }, this.dropdown)
                        }

                        Accordion.prototype.dropdown = function(e) {
                            var $el = e.data.el;
                            $this = $(this),
                                $next = $this.next();

                            $next.slideToggle();
                            $this.parent().toggleClass('open');
                            if (!e.data.multiple) {
                                $el.find('.sub-menu-left').not($next).slideUp().parent().removeClass('open');
                            };
                        }
                        var accordion = new Accordion($('#menu_left'), false);
                    });

                    if (($(window).height() + 100) < $(document).height()) {
                        $('#top-link-block').removeClass('hidden').affix({
                            offset: {
                                top: 100
                            }
                        });
                    }

                    function openViewPro(proID) {
                        $("#loadViewPro").load("/view_product.aspx?id=" + proID);
                        $('.bs-example-modal-lg').modal();
                    }

                    function addToCartView(proID) {
                        $("#loadViewPro").load("/add_cart_product.aspx?id=" + proID);
                        $('.bs-example-modal-lg').modal();
                    }
                    $(document).ready(function() {
                        $(".modal .close").click(function() {
                            $("#loadViewPro").html("");
                        });

                        listProductSearch();
                    })
                    var tagsource = []

                    function listProductSearch() {
                        $.ajax({
                            async: false,
                            type: "POST",
                            url: "webService.asmx/getProductList",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function(response) {
                                var tagProSearch = response.d;

                                for (i = 0; i < tagProSearch.length; i++) {
                                    tagsource.push({
                                        "id": tagProSearch[i].id,
                                        "name": tagProSearch[i].strName,
                                        "txtimg": tagProSearch[i].strImg
                                    });
                                }
                            },
                            failure: function(response) {}

                        });
                    }

                    function displayResult(item, val, text) {
                        console.log(item);
                    }

                    $(function() {
                        $('[id*=txtSearch]').typeahead({
                            source: tagsource,
                            itemSelected: displayResult
                        });
                    });
                </script>
                <script type="text/javascript">
                    new mlPushMenu(document.getElementById('mp-menu'), document.getElementById('trigger'), {
                        type: 'cover'
                    });
                </script>
                <script type="text/javascript">
                    document.write("<script type='text/javascript' language='javascript'>MainContentW = 1040;LeftBannerW = 129;RightBannerW = 0;LeftAdjust = 0;RightAdjust = 1;TopAdjust = 145;ShowAdDiv();window.onresize=ShowAdDiv;<\/script>");
                </script>

                <script type="text/javascript">
                    $(window).load(function() {
                        $('#myModalPopUp').modal('show');
                    });
                </script>
                <script type="text/javascript">
                    // ==== img menu top ====

                    $("<a href='http://missteen.vntsc.vn/the-le-cuoc-thi.html' class='img_top_number'><img src='uploads/layout/default/css/images/img_top_1.png' /></a>").appendTo(".menu-top li:eq(0) a");
                    $(".img_top_number").insertBefore(".menu-top li:eq(0) a:eq(0)");

                    $("<a href='http://missteen.vntsc.vn/dang-ky-1195.html' class='img_top_number_1'><img src='uploads/layout/default/css/images/img_top_2.png' /></a>").appendTo(".menu-top li:eq(1) a");
                    $(".img_top_number_1").insertBefore(".menu-top li:eq(1) a:eq(0)");

                    $("<a href='http://missteen.vntsc.vn/danh-sach-thi-sinh.html' class='img_top_number_2'><img src='uploads/layout/default/css/images/img_top_3.png' /></a>").appendTo(".menu-top li:eq(2) a");
                    $(".img_top_number_2").insertBefore(".menu-top li:eq(2) a:eq(0)");

                    $("<a href='http://missteen.vntsc.vn/nha-tai-tro.html' class='img_top_number_3'><img src='uploads/layout/default/css/images/img_top_4.png' /></a>").appendTo(".menu-top li:eq(3) a");
                    $(".img_top_number_3").insertBefore(".menu-top li:eq(3) a:eq(0)");

                    // ==== strike ====
                    $("<a href=''><img src='uploads/layout/default/css/images/strike.png' /></a>").appendTo(".contest-home .cate-header.sub-top > p:eq(0)");
                    $("<div class='strike_cen'><a href=''><img src='uploads/layout/default/css/images/strike.png' /></a></div>").appendTo(".box-news-priority").insertBefore(".box-news-priority .body-news");
                    $("<div class='strike_cen'><a href=''><img src='uploads/layout/default/css/images/strike.png' /></a></div>").appendTo(".box-adv-center").insertBefore(".box-adv-center .body-adv-center");
                    $("<div class='strike_cen'><a href=''><img src='uploads/layout/default/css/images/strike.png' /></a></div>").appendTo(".box-news-sub").insertBefore(".box-news-sub >.box-body");
                    $("<div class='strike_cen'><a href=''><img src='uploads/layout/default/css/images/strike.png' /></a></div>").appendTo(".box-page-sub").insertBefore(".box-page-sub >.box-body");
                    $("<div class='strike_cen'><a href=''><img src='uploads/layout/default/css/images/strike.png' /></a></div>").appendTo(".page_else #listContestant .listTourHeader");
                    $(".box-news-priority .header").html("TIN TỨC NỔI BẬT");
                    $(".box-adv-center .header").html('NHÀ TÀI TRỢ');

                    // ==== login face ====
                    $(".box-social-network .body-network .body-social li:eq(0) a span:contains('Facebook')").html("<img src='uploads/layout/default/css/images/icon-fb-log.png' />");

                    // ==== menu fixed when scrolling ====
                    if ($(window).width() > 992) {
                        var nav = $('.menu_pc_all');

                        $(window).scroll(function() {
                            if ($(this).scrollTop() > 50) {
                                nav.addClass("f-nav");
                                $(".banner .img-logo").css('width', '80%');
                                $("ul.nav-menu> li> a").css('color', '#479729');
                                $(".menu_pc_all").css({
                                    'paddingTop': '5px',
                                    'paddingBottom': '5px',
                                });

                            } else {
                                nav.removeClass("f-nav");
                                $(".banner .img-logo").css('width', '100%');
                                $("ul.nav-menu> li> a").css('color', '#ffffff');
                                $(".menu_pc_all").css({
                                    'paddingTop': '10px',
                                    'paddingBottom': '10px',
                                });
                            }
                        });

                    }

                    // ==== scroll to top ====
                    $('.to-top').click(function() {
                        $("html,body").animate({
                            scrollTop: 0
                        }, 600);
                    });
                    $(window).scroll(function() {
                        if ($(this).scrollTop()) {
                            $('.to-top:hidden').stop(true, true).fadeIn();
                        } else {
                            $('.to-top').stop(true, true).fadeOut();
                        }
                    });

                    // ==== dk ====
                    // $("<i class='fa fa-download' aria-hidden='true'></i>").appendTo(".download_form");
                    $("<img src='uploads/layout/default/css/images/dk_img.png' />").appendTo(".download_form a");

                    // ==== contact ====
                    $(".contact_left").parent(".box-body").css({
                        "paddingLeft": '0px',
                        "paddingRight": '0px',
                    });

                    // ==== chi tiết thí sinh ====
                    $(".page_else .col-md-12 #tourDetail .tourInfo .div-addthis-news").appendTo(".page_else .col-md-12 #tourDetail .tourInfo .tourInfo_right");
                    if ($(window).width() > 1024) {
                        $("#tourDetail").parent(".col-md-12").parent(".row").parent(".page_else").prev(".container.menu_top_all").prev(".clearfix").prev("div").children(".row").children(".bread_crumb").css('paddingTop', '0px');
                    }

                    $("#tourDetail").parent(".col-md-12").parent(".row").parent(".page_else").prev(".container.menu_top_all").css({
                        "display": 'none'
                    });

                    $("#tourDetail").parent(".col-md-12").parent(".row").parent(".page_else").prev(".container.menu_top_all").prev(".clearfix").prev(".header_all").css('paddingTop', '0px');
                    if ($(window).width() > 1023) {
                        $("#tourDetail").parent(".col-md-12").parent(".row").parent(".page_else").parent(".container-fluid.website").prev(".menu_mobile_all").prev(".clearfix").prev(".container-fluid.menu_pc_all").css({
                            "display": 'none',
                        });
                    }
                    // ==== heart ====
                    $(".itemVoted i").removeClass("fa-heart").addClass("fa-heart-o");
                    // ==== click menu ====
                    $(".menu_mobile_all").click(function() {
                        $(".btn-close").addClass("menu-bg");
                        $(".menu_pc_all").toggleClass("menu_pc_all_1");
                        // $("body").animate({
                        //     left: "250px"
                        // }, 500);
                    });
                    $(".btn-close").click(function() {
                        $(this).removeClass("menu-bg");
                        // $("body").animate({
                        //     left: "0px"
                        // }, 500);
                        $(".menu_pc_all").removeClass("menu_pc_all_1");
                    });

                    // ==== detail news ====
                    $("<div class='col-news-right'></div>").appendTo(".box-news-detailt");
                    $(".col-news-right").insertAfter(".box-news-detailt");
                    $(".box-news-detailt").addClass("col-news-left");
                    $(".col-news-right_1").appendTo(".col-news-right");
                </script>
        </form>
    </body>

    </html>