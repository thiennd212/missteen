﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace MyWeb
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class webService : System.Web.Services.WebService
    {
        private string lang = "";
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public void SetSessionLoginCookie(string userName)
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            if (userName.Contains(","))
            {
                string[] arr = userName.Split(',');
                context.Session["UserNameFB"] = arr[0];
                context.Session["UserFBName"] = arr[1];
                context.Session["UserFBAvatar"] = arr[2];
            }else
                context.Session["UserNameFB"] = userName;
        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public ContestantResult GetListContestant(int pageSize, int typesearch, string lstYear, string textSearch, int grId)
        {
            dataAccessDataContext db = new dataAccessDataContext();
            return LoadData(db, pageSize, typesearch, lstYear, textSearch, grId);
        }

        protected ContestantResult LoadData(dataAccessDataContext db, int pageSize, int typesearch, string lstYear, string textSearch, int grId)
        {
            string host = "http://" + HttpContext.Current.Request.Url.Host;
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            var mySessionVar = context.Session["UserNameFB"];
            string userIdFb = "";
            if (Session["UserNameFB"] != null)
                userIdFb = Session["UserNameFB"].ToString();
            int currentPage = 1;
            string strResult = "";
            textSearch = textSearch.Substring(1, textSearch.Length - 2);

            strResult += "<div class='listTourHeader'><a title='Danh sách thí sinh' href='/danh-sach-thi-sinh.html'>Danh sách thí sinh</a></div>";

            var listContestant =
                   db.Contestants.Where(s => s.Active == 1 && s.IdContestantGroup == grId).ToList();

            if (typesearch == 10)
            {
                if (lstYear.Length > 0)
                {
                    lstYear = lstYear.Substring(1, lstYear.Length - 2);
                    char[] c = { ';' };
                    string[] arr = lstYear.Split(c);
                    if (arr.Length > 0)
                    {
                        List<string> lstYearNew = new List<string>();
                        foreach (var item in arr)
                        {
                            if (!string.IsNullOrEmpty(item) && !lstYearNew.Any(s => s == item) && item != "'")
                            {
                                lstYearNew.Add(item);
                            }
                        }

                        listContestant = db.Contestants.Where(s => s.Active == 1 && s.IdContestantGroup == grId &&
                            lstYearNew.Contains(s.BirthYear.ToString())).OrderBy(s => s.BirthYear).ToList();

                    }

                }
            }
            if (typesearch == 1)
            {
                listContestant = db.Contestants.Where(s => s.Active == 1 && s.IdContestantGroup == grId).OrderByDescending(x => x.TotalVoted).ToList();
            }
            if (typesearch == 2)
            {
                listContestant = db.Contestants.Where(s => s.Active == 1 && s.IdContestantGroup == grId).OrderBy(x => x.TotalVoted).ToList();
            }
            if (typesearch == 3)
            {
                listContestant = db.Contestants.Where(s => s.Active == 1 && s.IdContestantGroup == grId).OrderByDescending(x => x.TotalLikeShare).ToList();
            }
            if (typesearch == 4)
            {
                listContestant = db.Contestants.Where(s => s.Active == 1 && s.IdContestantGroup == grId).OrderBy(x => x.TotalLikeShare).ToList();
            }
            if (typesearch == 5)
            {
                listContestant = db.Contestants.Where(s => s.Active == 1 && s.IdContestantGroup == grId).OrderByDescending(x => x.IdentificationNumber).ToList();
            }
            if (typesearch == 6)
            {
                listContestant = db.Contestants.Where(s => s.Active == 1 && s.IdContestantGroup == grId).OrderBy(x => x.IdentificationNumber).ToList();
            }
            if (typesearch == 7)
            {
                listContestant = db.Contestants.Where(s => s.Active == 1 && s.IdContestantGroup == grId &&
                    (s.IdentificationNumber.ToString().Contains(textSearch) ||
                    s.FullName.Contains(textSearch))).ToList();
            }
            if (typesearch == 8)
            {
                listContestant =
                        db.Contestants.Where(s => s.Active == 1 && s.IdContestantGroup == grId &&
                            s.ContestantPlace.Contains(textSearch)).ToList();
            }

            int count = 0;
            if (listContestant.Count > 0)
            {
                if (listContestant.Any())
                {
                    count = listContestant.Count;

                        listContestant = listContestant.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                    int j = 1;
                    foreach (var item in listContestant)
                    {                        
                        string class4 = "contestantItems";
                        if (j % 4 == 0)
                            class4 = "contestantItems contestantItems4";
                        else if(j%2 == 0)
                            class4 = "contestantItems contestantItems2";
                        string urlShare = host + "/" + item.TagName + ".html";
                        strResult += "<div class='" + class4 + "'>";
                        strResult += "  <div class='contestantItems_info1'>";                        
                        strResult += "      <div class='contestantItems_img_wrap'><a class='contestantItems_img' href='/" + item.TagName + ".html' title='" + item.FullName +
                                     "'><img src='" + item.Image1 + "' alt='" + item.FullName + "' /></a><div class = 'contestantItems_button_wrap'>";                        
                        if (!string.IsNullOrEmpty(userIdFb))
                        {
                            var contestantReference = db.ContestantsReferences.FirstOrDefault(x => x.ContestantId == item.Id && x.UserId == userIdFb);
                            if (contestantReference == null)
                            {
                                strResult += "<span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
                                strResult += "      <input type=\"button\" class=\"btn btn-info btn-lg\" onclick=\"vote(" + item.Id.ToString() + ")\" value=\"Vote\" />";
                                strResult += " <input type=\"button\" class=\"btn btn-info btn-lg\" onclick=\"likeshare('" + urlShare + "')\" value=\"Share\" />";

                            }
                            else
                            {
                                strResult += "<span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
                                strResult += " <input type=\"button\" class=\"btn btn-info btn-lg\" onclick=\"likeshare('" + urlShare + "')\" value=\"Share\" />";
                            }
                        }
                        else
                        {
                            strResult += "<span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
                            strResult += "      <input type=\"button\" class=\"btn btn-info btn-lg\" onclick=\"vote(" + item.Id.ToString() + ")\" value=\"Vote\" />";
                            strResult += " <input type=\"button\" class=\"btn btn-info btn-lg\" onclick=\"likeshare('" + urlShare + "')\" value=\"Share\" />";
                        }
                        strResult += "</div></div>";
                        strResult += "      <a class='contestantItems_price' href='/" + item.TagName + ".html'><strong>" + item.FullName + "</strong></a><p>SBD: " + item.IdentificationNumber + "</p>";
                        strResult += "  </div>";
                        strResult += "</div>";
                        j++;
                    }
                }
            }
            else
            {
                strResult += "<p style=\"text-align:center\">Không tìm thấy dữ liệu</p>";
            }

            ContestantResult result = new ContestantResult();
            result.Result = strResult;
            result.Total = count;
            return result;
        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public bool UpdateContestantReference(string contestantId, string fbUserId, string accessToken)
        {
            try
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                context.Session["UserNameFB"] = fbUserId;
                dataAccessDataContext db = new dataAccessDataContext();
                if (contestantId.Contains("'"))
                    contestantId = contestantId.Substring(1, contestantId.Length - 2);
                if (fbUserId.Contains("'"))
                    fbUserId = fbUserId.Substring(1, fbUserId.Length - 2);
                if (accessToken.Contains("'"))
                    accessToken = accessToken.Substring(1, accessToken.Length - 2);

                if (!string.IsNullOrEmpty(contestantId) && !string.IsNullOrEmpty(fbUserId))
                {
                    var contestReference = db.ContestantsReferences.Where(x => x.ContestantId == Convert.ToInt32(contestantId) && x.UserId == fbUserId).FirstOrDefault();
                    if (contestReference == null)
                    {
                        var contest = db.Contestants.Where(x => x.Id == Convert.ToInt32(contestantId)).FirstOrDefault();
                        if (contest != null)
                        {
                            contest.TotalVoted = (contest.TotalVoted != null) ? (contest.TotalVoted + 1) : 1;

                            ContestantsReference cr = new ContestantsReference();
                            cr.ContestantId = Convert.ToInt32(contestantId);
                            cr.UserId = fbUserId;
                            cr.AccessToken = accessToken;

                            db.ContestantsReferences.InsertOnSubmit(cr);
                            db.SubmitChanges();
                        }
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public string RenderTotalVoted(string contestantId)
        {
            try
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                dataAccessDataContext db = new dataAccessDataContext();
                if (contestantId.Contains("'"))
                    contestantId = contestantId.Substring(1, contestantId.Length - 2);

                var contest = db.Contestants.Where(x => x.Id == Convert.ToInt32(contestantId)).FirstOrDefault();

                if (contest != null)
                {                    
                    var mySessionVar = context.Session["UserNameFB"];
                    if (Session["UserNameFB"] != null)
                    {
                        var checkVote = db.ContestantsReferences.FirstOrDefault(x => x.ContestantId == contest.Id && x.UserId == Session["UserNameFB"]);
                        if (checkVote != null)
                        {
                            return "<span class=\"itemVoted\" style=\"float: none; text-align: center; border-radius: 30px; color: rgb(255, 255, 255); background: rgb(236, 0, 140);\">" + contest.TotalVoted + "<i class=\"fa fa-heart-o\"></i></span>";
                        }
                        else
                        {
                            return "<div id=\"btnVoted\"><input type=\"button\" class=\"btn btn-info btn-lg\" value=\"Bình chọn\" onclick=\"vote()\" /></div><span class=\"itemVoted\">" + contest.TotalVoted + "<i class='fa fa-heart'></i></span>";
                        }
                    }
                    else
                    {
                        return "<div id=\"btnVoted\"><input type=\"button\" class=\"btn btn-info btn-lg\" value=\"Bình chọn\" onclick=\"vote()\" /></div><span class=\"itemVoted\">" + contest.TotalVoted + "<i class='fa fa-heart'></i></span>";
                    }
                }
                else
                {
                    return "";
                }
            }
            catch
            {
                return "";
            }
        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public List<entProductSearch> getProductList()
        {
            dataAccessDataContext db = new dataAccessDataContext();
            lang = MyWeb.Global.GetLang();
            if (GlobalClass.pageProducts == false)
            {
                var _ds = db.tbNews.Where(s => s.newActive == 0 && s.newLang == lang).OrderByDescending(s => s.newCreateDate).ToList();
                List<entProductSearch> arrProductSearch = new List<entProductSearch>();

                if (_ds.Count() > 0)
                {
                    for (int i = 0; i < _ds.Count(); i++)
                    {
                        entProductSearch item = new entProductSearch();
                        item.id = int.Parse(_ds[i].newid.ToString());
                        item.strName = _ds[i].newName;

                        if (_ds[i].newImage != null && _ds[i].newImage.Length > 0)
                        {
                            item.strImg = _ds[i].newImage.Split(',')[0];
                        }
                        arrProductSearch.Add(item);

                    }
                }

                return arrProductSearch;
            }
            else
            {
                var _ds = db.tbProducts.Where(s => s.proActive == 1 && s.proLang == lang).OrderByDescending(s => s.proId).ToList();
                List<entProductSearch> arrProductSearch = new List<entProductSearch>();
                if (_ds.Count() > 0)
                {
                    for (int i = 0; i < _ds.Count(); i++)
                    {
                        entProductSearch item = new entProductSearch();
                        item.id = int.Parse(_ds[i].proId.ToString());
                        item.strName = _ds[i].proName;

                        if (_ds[i].proImage != null && _ds[i].proImage.Length > 0)
                        {
                            item.strImg = _ds[i].proImage.Split(',')[0];
                        }
                        arrProductSearch.Add(item);
                    }
                }
                return arrProductSearch;
            }

        }
                
        [System.Web.Services.WebMethod(EnableSession = true)]
        public ResultFacebook SetSessionLogin()
        {
            ResultFacebook rs = new ResultFacebook();
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            var mySessionVar = context.Session["UserNameFB"];
            if (Session["UserNameFB"] != null)
            {
                rs.IsLogin = true;
                rs.Message = Session["UserNameFB"].ToString();
            }
            else
            {
                rs.IsLogin = false;
                rs.Message = "<a href=\"javascript:void(0)\" onClick=\"Login()\" id=\"logInFB\" class=\"facebook\" rel=\"nofollow\"><img src=\"uploads/layout/default/css/images/icon-fb-log.png\" alt=\"\"/></a>";
            }
            return rs;
        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public ResultFacebook SetSessionLogout()
        {
            ResultFacebook rs = new ResultFacebook();
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            var mySessionVar = context.Session["UserNameFB"];
            if (Session["UserNameFB"] != null)
            {
                Session["UserNameFB"] = null;
                Session["UserFBName"] = null;
                Session["UserFBAvatar"] = null;
                rs.IsLogin = false;
                rs.Message = "<a href=\"javascript:void(0)\" onClick=\"Login()\" id=\"logInFB\" class=\"facebook\" rel=\"nofollow\"><img src=\"uploads/layout/default/css/images/icon-fb-log.png\" alt=\"\"/></a>";
            }
            else
            {
                rs.IsLogin = false;
                rs.Message = "<a href=\"javascript:void(0)\" onClick=\"Login()\" id=\"logInFB\" class=\"facebook\" rel=\"nofollow\"><img src=\"uploads/layout/default/css/images/icon-fb-log.png\" alt=\"\"/></a>";
            }
            return rs;
        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public ResultFacebook GetSessionLogin()
        {
            ResultFacebook rs = new ResultFacebook();
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            var mySessionVar = context.Session["UserNameFB"];
            if (Session["UserNameFB"] != null)
            {
                rs.IsLogin = true;
                rs.Message = "<span class=\"avatar\"><img src=\"" + Session["UserFBAvatar"] + "\" alt=\"\"/></span> <span class=\"fullname\">" + Session["UserFBName"] + "</span> (<a href=\"javascript:void(0)\" id=\"logOutFB\" title=\"Thoát\">Thoát</a>)";
            }
            else
            {
                rs.IsLogin = false;
                rs.Message = "<a href=\"javascript:void(0)\" onClick=\"Login()\" id=\"logInFB\" class=\"facebook\" rel=\"nofollow\"><img src=\"uploads/layout/default/css/images/icon-fb-log.png\" alt=\"\"/></a>";
            }
            return rs;
        }

        public class entProductSearch
        {
            private int _id;
            public int id
            {
                get { return _id; }
                set { _id = value; }
            }

            private string _strName;
            public string strName
            {
                get { return _strName; }
                set { _strName = value; }
            }

            private string _strImg;
            public string strImg
            {
                get { return _strImg; }
                set { _strImg = value; }
            }
        }

        public class ContestantResult
        {
            public string Result { get; set; }
            public int Total { get; set; }
        }

        public class ResultFacebook
        {
            public bool IsLogin { get; set; }
            public string Message { get; set; }
        }
    }
}
