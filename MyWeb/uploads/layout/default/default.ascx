﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Register Src="~/views/control/ucLoadControl.ascx" TagPrefix="uc1" TagName="ucLoadControl" %>
<%@ Register Src="~/views/control/ucBanner.ascx" TagPrefix="uc2" TagName="ucBanner" %>
<%@ Register Src="~/views/control/ucSearchBox.ascx" TagPrefix="uc3" TagName="ucSearchBox" %>
<%@ Register Src="~/views/control/ucMenuTop.ascx" TagPrefix="uc4" TagName="ucMenuTop" %>
<%@ Register Src="~/views/control/ucLanguage.ascx" TagPrefix="uc5" TagName="ucLanguage" %>
<%@ Register Src="~/views/control/ucLinkAcount.ascx" TagPrefix="uc6" TagName="ucLinkAcount" %>
<%@ Register Src="~/views/control/ucShopCart.ascx" TagPrefix="uc7" TagName="ucShopCart" %>
<%@ Register Src="~/views/control/ucMenuMain.ascx" TagPrefix="uc8" TagName="ucMenuMain" %>
<%@ Register Src="~/views/control/ucSlides.ascx" TagPrefix="uc9" TagName="ucSlides" %>
<%@ Register Src="~/views/control/ucLogin.ascx" TagPrefix="uc10" TagName="ucLogin" %>
<%@ Register Src="~/views/control/ucListCategory.ascx" TagPrefix="uc11" TagName="ucListCategory" %>
<%@ Register Src="~/views/control/ucOnline.ascx" TagPrefix="uc12" TagName="ucOnline" %>
<%@ Register Src="~/views/news/ucNewsHot.ascx" TagPrefix="uc13" TagName="ucNewsHot" %>
<%@ Register Src="~/views/products/ucProSale.ascx" TagPrefix="uc14" TagName="ucProSale" %>
<%@ Register Src="~/views/control/ucAdvleft.ascx" TagPrefix="uc15" TagName="ucAdvleft" %>
<%@ Register Src="~/views/control/ucBoxface.ascx" TagPrefix="uc16" TagName="ucBoxface" %>
<%@ Register Src="~/views/control/ucOnlineVisit.ascx" TagPrefix="uc17" TagName="ucOnlineVisit" %>
<%@ Register Src="~/views/control/ucAdvright.ascx" TagPrefix="uc18" TagName="ucAdvright" %>
<%@ Register Src="~/views/products/ucProNew.ascx" TagPrefix="uc19" TagName="ucProNew" %>
<%@ Register Src="~/views/news/ucAboutUs.ascx" TagPrefix="uc20" TagName="ucAboutUs" %>
<%@ Register Src="~/views/products/ucProHome.ascx" TagPrefix="uc21" TagName="ucProHome" %>
<%@ Register Src="~/views/products/ucProHomeParent.ascx" TagPrefix="uc22" TagName="ucProHomeParent" %>
<%@ Register Src="~/views/news/ucNewsHomePriority.ascx" TagPrefix="uc23" TagName="ucNewsHomePriority" %>
<%@ Register Src="~/views/news/ucNewsHome.ascx" TagPrefix="uc24" TagName="ucNewsHome" %>
<%@ Register Src="~/views/control/ucMenuFooter.ascx" TagPrefix="uc25" TagName="ucMenuFooter" %>
<%@ Register Src="~/views/control/ucSocialNetwork.ascx" TagPrefix="uc26" TagName="ucSocialNetwork" %>
<%@ Register Src="~/views/control/ucFooter.ascx" TagPrefix="uc27" TagName="ucFooter" %>
<%@ Register Src="~/views/control/ucMenuLeft.ascx" TagPrefix="uc28" TagName="ucMenuLeft" %>
<%@ Register Src="~/views/products/ucFilterAttributes.ascx" TagPrefix="uc29" TagName="ucFilterAttributes" %>
<%@ Register Src="~/views/control/ucAdvCenter.ascx" TagPrefix="uc30" TagName="ucAdvCenter" %>
<%@ Register Src="~/views/library/ucListVideo.ascx" TagPrefix="uc31" TagName="ucListVideo" %>
<%@ Register Src="~/views/products/ucProHot.ascx" TagPrefix="uc32" TagName="ucProHot" %>
<%@ Register Src="~/views/products/ucProMotion.ascx" TagPrefix="uc33" TagName="ucProMotion" %>
<%@ Register Src="~/views/control/ucBreadcrumb.ascx" TagPrefix="uc34" TagName="ucBreadcrumb" %>
<%@ Register Src="~/views/control/ucLinkwebsite.ascx" TagPrefix="uc35" TagName="ucLinkwebsite" %>
<%@ Register Src="~/views/control/ucNewsLetter.ascx" TagPrefix="uc36" TagName="ucNewsLetter" %>
<%@ Register Src="~/views/control/ucCustomerReply.ascx" TagPrefix="uc1004" TagName="ucCustomerReply" %>
<%@ Register Src="~/views/products/ucViewedProducts.ascx" TagPrefix="uc37" TagName="ucViewedProducts" %>
<%@ Register Src="~/views/news/ucPriority.ascx" TagPrefix="uc38" TagName="ucPriority" %>

<%@ Register Src="~/views/tour/ucTourHome.ascx" TagPrefix="uc39" TagName="ucTourHome" %>

<%@ Register Src="~/views/Tour/ucTourNew.ascx" TagPrefix="uc42" TagName="ucTourNew" %>
<%@ Register Src="~/views/Tour/ucTourSale.ascx" TagPrefix="uc43" TagName="ucTourSale" %>
<%@ Register Src="~/views/Tour/ucTourTop.ascx" TagPrefix="uc44" TagName="ucTourTop" %>

<%@ Register Src="~/views/Tour/listTour.ascx" TagPrefix="uc45" TagName="listTour" %>
<%@ Register Src="~/views/Tour/ucTourByTopicHome.ascx" TagPrefix="uc46" TagName="ucTourByTopicHome" %>


<%@ Register Src="~/views/search/ucProSearch.ascx" TagPrefix="uc47" TagName="ucProSearch" %>
<%@ Register Src="~/views/search/ucTourSearch.ascx" TagPrefix="uc48" TagName="ucTourSearch" %>
<%@ Register Src="~/views/Tour/ucCatTourHome.ascx" TagPrefix="uc50" TagName="ucCatTourHome" %>

<%@ Register Src="~/views/products/ucProductByTab.ascx" TagPrefix="uc1" TagName="ucProductByTab" %>
<%@ Register Src="~/views/products/ucProSaleV2.ascx" TagPrefix="uc1" TagName="ucProSaleV2" %>
<%@ Register Src="~/views/pages/ucBookNow.ascx" TagPrefix="uc100" TagName="ucBookNow" %>
<%@ Register Src="~/views/tour/ucTourHot.ascx" TagPrefix="uc1001" TagName="ucTourHot" %>
<%@ Register Src="~/views/tour/ucTourSearchBox.ascx" TagPrefix="uc1002" TagName="ucTourSearchBox" %>
<%@ Register Src="~/views/tour/ucTourSearchBox.ascx" TagPrefix="uc1003" TagName="ucTourSearchBox_1" %>

<!-- Contestant -->
<%@ Register Src="~/views/contestant/contestantDetail.ascx" TagPrefix="uc51" TagName="ucContestDetail" %>
<%@ Register Src="~/views/contestant/ucContestHome.ascx" TagPrefix="uc52" TagName="ucContestHome" %>



   
    
<div class="btn-close"></div>
<!--======== WEBSITE ========-->
<!--==== Menu pc ====-->
<div class="container-fluid menu_pc_all">
    <div class="container">
        <div class="row">
            <div class="scroll_menu">
                <div class="col-md-2 banner_all"><uc2:ucBanner runat="server" ID="ucBanner" /></div>
                <div class="col-md-9 menu_pc"><uc8:ucMenuMain runat="server" ID="ucMenuMain" /></div>
                <div class="col-md-1 login_face"><uc26:ucSocialNetwork runat="server" ID="ucSocialNetwork" /></div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<!--==== End menu pc ====-->
<!--==== menu mobile ====-->
<div class="container-fluid menu_mobile_all">
    <i class="fa fa-bars" aria-hidden="true"></i>Menu
</div>
<!--==== End menu mobile ====-->
<div class="container-fluid website">
    <!--==== Header ====-->
    <div class="container header_all">
        <div class="row">
            <div class="clearfix"></div>
            <%if (Session["home_page"] == null )
            {%>
                <div class="col-md-12 bread_crumb">
                    <uc34:ucBreadcrumb runat="server" ID="ucBreadcrumb" />
                </div>             
            <%} %>
            <%if (Session["home_page"] != null )
            {%>
            <div class="col-md-6">
                <div class="about_home_img">
                    <img src="uploads/layout/default/css/images/about_home_img.png" alt=""/>
                </div>
                <uc20:ucAboutUs runat="server" ID="ucAboutUs" />
            </div>
            <div class="col-md-6">
                <div class="miss_home_img">
                    <a href=""><img src="uploads/layout/default/css/images/miss_home.png" alt=""/></a>
                </div>
            </div>
            <%} %>
        </div>   
    </div>
    <!--==== End Header ====-->
    <div class="clearfix"></div>

    <!--==== Main ====-->
   
    <!--==== Menu top ====-->
    <div class="container menu_top_all">
        <div class="menu_top">
            <div class="row">
                <div class="col-md-12">
                    <uc4:ucMenuTop runat="server" ID="ucMenuTop" />
                </div>
            </div>
        </div>   
    </div>
     <!--==== End Menu top ====-->
    <%if (Session["home_page"] != null )
    {%>
    <!--==== News priority ====-->
    <div class="container news_priority">
        <div class="row">
            <div class="col-md-12">
                <uc38:ucPriority runat="server" ID="ucPriority" />
            </div>
        </div>
    </div>
    <!--==== End News priority ====-->
    <!--==== List of candidates ====-->
    <div class="container list_of_candidates">
        <div class="row">
            <div class="col-md-12">
                <uc52:ucContestHome runat="server" ID="ucContestHome" />
            </div>
        </div>
    </div>
    <!--==== End List of candidates ====-->
    <!--==== Donors ====-->
    <div class="container">
        <div class="donors">
            <div class="row">
                <div class="col-md-4 donors_list_add">
                    <div class="header_donor">ĐƠN VỊ ĐỒNG HÀNH</div>
                    <div class="list_all_donor">
                        <div class="img_donor_list">
                            <div class="list_1"><a href=""><img src="uploads/layout/default/css/images/donors_list_1.png" alt=""/></a></div>
                        </div>
                        <div class="img_donor_list">
                            <div class="list_1"><a href=""><img src="uploads/layout/default/css/images/donors_list_2.png" alt=""/></a></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 donors_list">
                    <div class="header_donor">NHÀ TÀI TRỢ</div>
                    <div class="list_all_donor">
                        <div class="img_donor_list">
                            <div class="list_1"><a href=""><img src="uploads/layout/default/css/images/donors_list_3.png" alt=""/></a></div>
                        </div>
                        <div class="img_donor_list">
                            <div class="list_1"><a href=""><img src="uploads/layout/default/css/images/donors_list_3.png" alt=""/></a></div>
                        </div>
                        <div class="img_donor_list">
                            <div class="list_1"><a href=""><img src="uploads/layout/default/css/images/donors_list_3.png" alt=""/></a></div>
                        </div>
                        <div class="img_donor_list">
                            <div class="list_1"><a href=""><img src="uploads/layout/default/css/images/donors_list_3.png" alt=""/></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--==== End Donors ====-->
    <%} %>

    <!--==== Page else ====-->
    <div class="container page_else">
        <div class="row">
            <div class="col-md-12">
                <uc1:ucLoadControl runat="server" ID="ucLoadControl" />
            </div>
        </div>
    </div>
    <!--==== End Page else ====-->

    <!--==== Footer ====-->
    <footer class="container">
        <div class="row">
            <div class="col-md-6">BTC CUỘC THI : "NGƯỜI ĐẸP XỨ TRÀ" LẦN THỨ IV, NĂM 2017</div>
            <div class="col-md-4">Đơn vị tổ chức: Đài PTTH Thái Nguyên</div>
            <div class="col-md-2 internet_bottom">           
                <div class="zalo">
                    <a href="" target="_blank"><img src="uploads/layout/default/css/images/icon-s3.png" alt=""/></a>
                </div>
                <div class="zalo">
                    <a href="" target="_blank"><img src="uploads/layout/default/css/images/icon-s2.png" alt=""/></a>
                </div>
                <div class="face_book">
                    <a href="" target="_blank"><img src="uploads/layout/default/css/images/icon-s1.png" alt=""/></a>
                </div>
            </div>
        </div>
    </footer>
    
    <!--==== End Main ====-->
</div>
<!--======== END WEBSITE ========-->

<div class="to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></div>

<%if (Session["home_page"] == null )
{%>
    <div class="col-news-right_1">
        <uc16:ucBoxface runat='server' ID='ucBoxface' /> 
        <div class="clearfix"></div>    
        <uc18:ucAdvright runat='server' ID='ucAdvright' /> 
        <div class="clearfix"></div>
        <div class="sponsor_agency">
            
        </div>
    </div> 
<%} %>



 <div class="description">
    <a href="tel:0913050541">
        <div class="phone_animation">
            <div class="phone_animation_circle"></div>
            <div class="phone_animation_circle_1"></div>
            <div class="phone_animation_circle_fill"></div>
            <div class="phone_animation_circle_fill_img"><i class="fa fa-phone" aria-hidden="true"></i></div>
        </div>
    </a>
    <div class="number-phone">Hotline: 0913.050.541 </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#content div").hide(); // Initially hide all content
        $("#tabs li:first").attr("id","current"); // Activate first tab
        $("#content div:first").fadeIn(); // Show first tab content
                
        $('#tabs a').click(function(e) {
            e.preventDefault();
            if ($(this).closest("li").attr("id") == "current"){ //detection for current tab
                return       
            }
            else{             
            $("#content div").hide(); //Hide all content
            $("#tabs li").attr("id",""); //Reset id's
            $(this).parent().attr("id","current"); // Activate this
            $('#' + $(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });
    });



    /*var owl_category_1 = $(".box-adv-center .body-adv-center");
    owl_category_1.owlCarousel({
        items: 4,
        loop:true,
        autoPlay: true,
        transitionStyle: "fade",
        itemsDesktop: [1199, 4],       
        itemsDesktopSmall: [991, 4],
        itemsTablet: [769, 2],
        itemsTablet: [641, 2],
        itemsTablet: [640, 2],
        itemsMobile: [320, 1],
        navigation: false,
        navigationText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
        rewindNav: true,
        scrollPerPage: true,
        slideSpeed: 1500,
        pagination: true,
        paginationNumbers: false,
    });*/
</script>