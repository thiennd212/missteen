﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class tbUserDB
{
    #region[tbUser_Add]
    public static bool tbUser_Add(tbUserDATA _tbUserDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbUser_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@useUid", _tbUserDATA.useUid));
                dbCmd.Parameters.Add(new SqlParameter("@usePid", _tbUserDATA.usePid));
                dbCmd.Parameters.Add(new SqlParameter("@useAdmin", _tbUserDATA.useAdmin));
                dbCmd.Parameters.Add(new SqlParameter("@useName", _tbUserDATA.useName));
                dbCmd.Parameters.Add(new SqlParameter("@useLevel", _tbUserDATA.useLevel));
                dbCmd.Parameters.Add(new SqlParameter("@useOrd", _tbUserDATA.useOrd));
                dbCmd.Parameters.Add(new SqlParameter("@useActive", _tbUserDATA.useActive));
                dbCmd.Parameters.Add(new SqlParameter("@useMail", _tbUserDATA.useMail));
                dbCmd.Parameters.Add(new SqlParameter("@useAuthorities", _tbUserDATA.useAuthorities));
                dbCmd.Parameters.Add(new SqlParameter("@useRole", _tbUserDATA.useRole));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbUser_Update]
    public static bool tbUser_Update(tbUserDATA _tbUserDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbUser_Update", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@useId", _tbUserDATA.useId));
                dbCmd.Parameters.Add(new SqlParameter("@useUid", _tbUserDATA.useUid));
                dbCmd.Parameters.Add(new SqlParameter("@usePid", _tbUserDATA.usePid));
                dbCmd.Parameters.Add(new SqlParameter("@useName", _tbUserDATA.useName));
                dbCmd.Parameters.Add(new SqlParameter("@useAdmin", _tbUserDATA.useAdmin));
                dbCmd.Parameters.Add(new SqlParameter("@useOrd", _tbUserDATA.useOrd));
                dbCmd.Parameters.Add(new SqlParameter("@useActive", _tbUserDATA.useActive));
                dbCmd.Parameters.Add(new SqlParameter("@useMail", _tbUserDATA.useMail));
                dbCmd.Parameters.Add(new SqlParameter("@useAuthorities", _tbUserDATA.useAuthorities));
                dbCmd.Parameters.Add(new SqlParameter("@useRole", _tbUserDATA.useRole));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbusers_Changepass]
    public static bool tbusers_Changepass(tbUserDATA _tbUserDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbusers_Changepass", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@useId", _tbUserDATA.useId));
                dbCmd.Parameters.Add(new SqlParameter("@usePid", _tbUserDATA.usePid));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbUser_Delete]
    public static bool tbUser_Delete(string suseId)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbUser_Delete", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@useId", suseId));
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbUser_GetByAll]
    public static List<tbUserDATA> tbUser_GetByAll()
    {
        List<tbUserDATA> list = new List<tbUserDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbUser_GetByAll", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbUserDATA objtbUserDATA = new tbUserDATA(
                        reader["useId"].ToString(),
                        reader["useUid"].ToString(),
                        reader["usePid"].ToString(),
                        reader["useName"].ToString(),
                        reader["useAdmin"].ToString(),
                        reader["useLevel"].ToString(),
                        reader["useOrd"].ToString(),
                        reader["useActive"].ToString(),
                        reader["useMail"].ToString(),
                        reader["useAuthorities"].ToString(),
                        reader["useRole"].ToString());
                        if (objtbUserDATA.useAdmin != "100") list.Add(objtbUserDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbUser_GetByPhanquyen]
    public static List<tbUserDATA> tbUser_GetByPhanquyen()
    {
        List<tbUserDATA> list = new List<tbUserDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbUser_GetByPhanquyen", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbUserDATA objtbUserDATA = new tbUserDATA(
                        reader["useId"].ToString(),
                        reader["useUid"].ToString(),
                        reader["usePid"].ToString(),
                        reader["useName"].ToString(),
                        reader["useAdmin"].ToString(),
                        reader["useLevel"].ToString(),
                        reader["useOrd"].ToString(),
                        reader["useActive"].ToString(),
                         reader["useMail"].ToString(),
                          reader["useAuthorities"].ToString(),
                        reader["useRole"].ToString());
                        list.Add(objtbUserDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbUser_GetByID]
    public static List<tbUserDATA> tbUser_GetByID(string suseId)
    {
        List<tbUserDATA> list = new List<tbUserDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbUser_GetByID", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@useId", suseId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbUserDATA objtbUserDATA = new tbUserDATA(
                        reader["useId"].ToString(),
                        reader["useUid"].ToString(),
                        reader["usePid"].ToString(),
                        reader["useName"].ToString(),
                        reader["useAdmin"].ToString(),
                        reader["useLevel"].ToString(),
                        reader["useOrd"].ToString(),
                        reader["useActive"].ToString(),
                         reader["useMail"].ToString(),
                          reader["useAuthorities"].ToString(),
                        reader["useRole"].ToString());
                        list.Add(objtbUserDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbUser_GetByAdmin]
    public static List<tbUserDATA> tbUser_GetByAdmin()
    {
        List<tbUserDATA> list = new List<tbUserDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbUser_GetByAdmin", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbUserDATA objtbUserDATA = new tbUserDATA(
                        reader["useId"].ToString(),
                        reader["useUid"].ToString(),
                        reader["usePid"].ToString(),
                        reader["useName"].ToString(),
                        reader["useAdmin"].ToString(),
                        reader["useLevel"].ToString(),
                        reader["useOrd"].ToString(),
                        reader["useActive"].ToString(),
                         reader["useMail"].ToString(),
                         reader["useAuthorities"].ToString(),
                        reader["useRole"].ToString());
                        list.Add(objtbUserDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbUser_GetByLogin]
    public static List<tbUserDATA> tbUser_GetByLogin(string suseUid, string susePid)
    {
        List<tbUserDATA> list = new List<tbUserDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbUser_GetByLogin", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@useUid", suseUid));
                dbCmd.Parameters.Add(new SqlParameter("@usePid", susePid));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbUserDATA objtbUserDATA = new tbUserDATA(
                        reader["useId"].ToString(),
                        reader["useUid"].ToString(),
                        reader["usePid"].ToString(),
                        reader["useName"].ToString(),
                        reader["useAdmin"].ToString(),
                        reader["useLevel"].ToString(),
                        reader["useOrd"].ToString(),
                        reader["useActive"].ToString(),
                         reader["useMail"].ToString(),
                          reader["useAuthorities"].ToString(),
                        reader["useRole"].ToString());
                        list.Add(objtbUserDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbUser_GetByTop]
    public static List<tbUserDATA> tbUser_GetByTop(string Top, string Where, string Order)
    {
        List<tbUserDATA> list = new List<tbUserDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbUser_GetByTop", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@Top", Top));
                dbCmd.Parameters.Add(new SqlParameter("@Where", Where));
                dbCmd.Parameters.Add(new SqlParameter("@Order", Order));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbUserDATA objtbUserDATA = new tbUserDATA(
                        reader["useId"].ToString(),
                        reader["useUid"].ToString(),
                        reader["usePid"].ToString(),
                        reader["useName"].ToString(),
                        reader["useAdmin"].ToString(),
                        reader["useLevel"].ToString(),
                        reader["useOrd"].ToString(),
                        reader["useActive"].ToString(),
                         reader["useMail"].ToString(),
                         reader["useAuthorities"].ToString(),
                        reader["useRole"].ToString());
                        list.Add(objtbUserDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbUser_GetByID]
    public static List<tbUserDATA> tbUser_GetByuseEmail(string useEmail)
    {
        List<tbUserDATA> list = new List<tbUserDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("SP_tbUser_SelectByEmail", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@useMail", useEmail));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbUserDATA objtbUserDATA = new tbUserDATA(
                        reader["useId"].ToString(),
                        reader["useUid"].ToString(),
                        reader["usePid"].ToString(),
                        reader["useName"].ToString(),
                        reader["useAdmin"].ToString(),
                        reader["useLevel"].ToString(),
                        reader["useOrd"].ToString(),
                        reader["useActive"].ToString(),
                         reader["useMail"].ToString(),
                         reader["useAuthorities"].ToString(),
                        reader["useRole"].ToString());
                        list.Add(objtbUserDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbUser_GetByAll]
    public static List<tbUserDATA> tbUser_GetByAdmin(string userId)
    {
        List<tbUserDATA> list = new List<tbUserDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbUser_GetByAll", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbUserDATA objtbUserDATA = new tbUserDATA(
                        reader["useId"].ToString(),
                        reader["useUid"].ToString(),
                        reader["usePid"].ToString(),
                        reader["useName"].ToString(),
                        reader["useAdmin"].ToString(),
                        reader["useLevel"].ToString(),
                        reader["useOrd"].ToString(),
                        reader["useActive"].ToString(),
                         reader["useMail"].ToString(),
                          reader["useAuthorities"].ToString(),
                        reader["useRole"].ToString());
                        if (objtbUserDATA.useAdmin == "100" && userId == reader["useUid"].ToString())
                            list.Add(objtbUserDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
}