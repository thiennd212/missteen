﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class tbMembersDATA
{
    #region[Declare variables]
    private string _ID;
    private string _uID;
    private string _uPas;
    private string _Email;
    private string _Add;
    private string _Tell;
    private string _Fax;
    private string _Counts;
    private string _Active;
    #endregion
    #region[Function]
    public tbMembersDATA() { }
    public tbMembersDATA(string ID_, string uID_, string uPas_, string Email_, string Add_, string Tell_, string Fax_, string Counts_, string Active_)
    {
        _ID = ID_;
        _uID = uID_;
        _uPas = uPas_;
        _Email = Email_;
        _Add = Add_;
        _Tell = Tell_;
        _Fax = Fax_;
        _Counts = Counts_;
    }
    #endregion
    #region[Assigned value]
    public string ID { get { return _ID; } set { _ID = value; } }
    public string uID { get { return _uID; } set { _uID = value; } }
    public string uPas { get { return _uPas; } set { _uPas = value; } }
    public string Email { get { return _Email; } set { _Email = value; } }
    public string Add { get { return _Add; } set { _Add = value; } }
    public string Tell { get { return _Tell; } set { _Tell = value; } }
    public string Fax { get { return _Fax; } set { _Fax = value; } }
    public string Counts { get { return _Counts; } set { _Counts = value; } }
    public string Active { get { return _Active; } set { _Active = value; } }
    #endregion
}