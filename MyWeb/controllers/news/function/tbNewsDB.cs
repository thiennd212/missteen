﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class tbNewsDB
{
    static SqlCommand cmd;
    #region[tbNews_Add]
    public static bool tbNews_Add(tbNewsDATA _tbNewsDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@newName", _tbNewsDATA.newName));
                dbCmd.Parameters.Add(new SqlParameter("@newImage", _tbNewsDATA.newImage));
                dbCmd.Parameters.Add(new SqlParameter("@newimg", _tbNewsDATA.newimg));
                dbCmd.Parameters.Add(new SqlParameter("@skId", _tbNewsDATA.skId));
                dbCmd.Parameters.Add(new SqlParameter("@newFile", _tbNewsDATA.newFile));
                dbCmd.Parameters.Add(new SqlParameter("@newContent", _tbNewsDATA.newContent));
                dbCmd.Parameters.Add(new SqlParameter("@newDetail", _tbNewsDATA.newDetail));
                dbCmd.Parameters.Add(new SqlParameter("@newDate", _tbNewsDATA.newDate));
                dbCmd.Parameters.Add(new SqlParameter("@newTitle", _tbNewsDATA.newTitle));
                dbCmd.Parameters.Add(new SqlParameter("@newDescription", _tbNewsDATA.newDescription));
                dbCmd.Parameters.Add(new SqlParameter("@newKeyword", _tbNewsDATA.newKeyword));
                dbCmd.Parameters.Add(new SqlParameter("@newPriority", _tbNewsDATA.newPriority));
                dbCmd.Parameters.Add(new SqlParameter("@newIndex", _tbNewsDATA.newIndex));
                dbCmd.Parameters.Add(new SqlParameter("@grnId", _tbNewsDATA.grnID));
                dbCmd.Parameters.Add(new SqlParameter("@usrId", _tbNewsDATA.usrID));
                dbCmd.Parameters.Add(new SqlParameter("@newType", _tbNewsDATA.newType));
                dbCmd.Parameters.Add(new SqlParameter("@newActive", _tbNewsDATA.newActive));
                dbCmd.Parameters.Add(new SqlParameter("@newLang", _tbNewsDATA.newLang));
                dbCmd.Parameters.Add(new SqlParameter("@newTagName", _tbNewsDATA.newTagName));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbNews_Update]
    public static bool tbNews_Update(tbNewsDATA _tbNewsDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_Update", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@newId", _tbNewsDATA.newid));
                dbCmd.Parameters.Add(new SqlParameter("@newName", _tbNewsDATA.newName));
                dbCmd.Parameters.Add(new SqlParameter("@newImage", _tbNewsDATA.newImage));
                dbCmd.Parameters.Add(new SqlParameter("@newimg", _tbNewsDATA.newimg));
                dbCmd.Parameters.Add(new SqlParameter("@skId", _tbNewsDATA.skId));
                dbCmd.Parameters.Add(new SqlParameter("@newFile", _tbNewsDATA.newFile));
                dbCmd.Parameters.Add(new SqlParameter("@newContent", _tbNewsDATA.newContent));
                dbCmd.Parameters.Add(new SqlParameter("@newDetail", _tbNewsDATA.newDetail));
                dbCmd.Parameters.Add(new SqlParameter("@newDate", _tbNewsDATA.newDate));
                dbCmd.Parameters.Add(new SqlParameter("@newTitle", _tbNewsDATA.newTitle));
                dbCmd.Parameters.Add(new SqlParameter("@newKeyword", _tbNewsDATA.newKeyword));
                dbCmd.Parameters.Add(new SqlParameter("@newDescription", _tbNewsDATA.newDescription));
                dbCmd.Parameters.Add(new SqlParameter("@newPriority", _tbNewsDATA.newPriority));
                dbCmd.Parameters.Add(new SqlParameter("@newIndex", _tbNewsDATA.newIndex));
                dbCmd.Parameters.Add(new SqlParameter("@grnId", _tbNewsDATA.grnID));
                dbCmd.Parameters.Add(new SqlParameter("@newType", _tbNewsDATA.newType));
                dbCmd.Parameters.Add(new SqlParameter("@newActive", _tbNewsDATA.newActive));
                dbCmd.Parameters.Add(new SqlParameter("@newLang", _tbNewsDATA.newLang));
                dbCmd.Parameters.Add(new SqlParameter("@newTagName", _tbNewsDATA.newTagName));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbNews_UpdateImg]
    public static bool tbNews_UpdateImg(tbNewsDATA _tbNewsDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_UpdateImg", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@newId", _tbNewsDATA.newid));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbNews_Update_newimg]
    public static bool tbNews_Update_newimg(tbNewsDATA _tbNewsDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_Update_newimg", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@newId", _tbNewsDATA.newid));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbNews_UpdatenewFile]
    public static bool tbNews_UpdatenewFile(tbNewsDATA _tbNewsDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_UpdatenewFile", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@newId", _tbNewsDATA.newid));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbNews_UpdatenewHa]
    public static bool tbNews_UpdatenewHa(tbNewsDATA _tbNewsDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_UpdatenewHa", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@newId", _tbNewsDATA.newid));
                dbCmd.Parameters.Add(new SqlParameter("@newActive", _tbNewsDATA.newActive));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbNews_Delete]
    public static bool tbNews_Delete(string snewId)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_Delete", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@newId", snewId));
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbNews_GetByAll]
    public static List<tbNewsDATA> tbNews_GetByAll(string snewLang)
    {
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_GetByAll", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@newLang", snewLang));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                        reader["newid"].ToString(),
                        reader["newName"].ToString(),
                        reader["newImage"].ToString(),
                        reader["newimg"].ToString(),
                        reader["skId"].ToString(),
                        reader["newFile"].ToString(),
                        reader["newContent"].ToString(),
                        reader["newDetail"].ToString(),
                        reader["newDate"].ToString(),
                        reader["newCreateDate"].ToString(),
                        reader["newEndDate"].ToString(),
                        reader["newTitle"].ToString(),
                        reader["newKeyword"].ToString(),
                        reader["newDescription"].ToString(),
                        reader["newPriority"].ToString(),
                        reader["newIndex"].ToString(),
                        reader["newVisit"].ToString(),
                        reader["grnID"].ToString(),
                        reader["usrID"].ToString(),
                        reader["newAuthor"].ToString(),
                        reader["newType"].ToString(),
                        reader["newActive"].ToString(),
                        reader["newLang"].ToString(),
                        reader["ishowdesc"].ToString(),
                        reader["UrlOriginal"].ToString(),
                        reader["vkey"].ToString(),
                        reader["vUrl"].ToString(), reader["newTagName"].ToString());
                        list.Add(objtbNewsDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbNews_GetByID]
    public static List<tbNewsDATA> tbNews_GetByID(string snewId)
    {
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            try
            {
                using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_GetByID", dbConn))
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbConn.Open();
                    dbCmd.Parameters.Add(new SqlParameter("@newId", snewId));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                        reader["newid"].ToString(),
                        reader["newName"].ToString(),
                        reader["newImage"].ToString(),
                        reader["newimg"].ToString(),
                        reader["skId"].ToString(),
                        reader["newFile"].ToString(),
                        reader["newContent"].ToString(),
                        reader["newDetail"].ToString(),
                        reader["newDate"].ToString(),
                        reader["newCreateDate"].ToString(),
                        reader["newEndDate"].ToString(),
                        reader["newTitle"].ToString(),
                        reader["newKeyword"].ToString(),
                        reader["newDescription"].ToString(),
                        reader["newPriority"].ToString(),
                        reader["newIndex"].ToString(),
                        reader["newVisit"].ToString(),
                        reader["grnID"].ToString(),
                        reader["usrID"].ToString(),
                        reader["newAuthor"].ToString(),
                        reader["newType"].ToString(),
                        reader["newActive"].ToString(),
                        reader["newLang"].ToString(),
                        reader["ishowdesc"].ToString(),
                        reader["UrlOriginal"].ToString(),
                        reader["vkey"].ToString(),
                        reader["vUrl"].ToString(), reader["newTagName"].ToString());
                            list.Add(objtbNewsDATA);
                        }
                    }
                    dbConn.Close();
                }
            }
            catch { }
        }
        return list;
    }
    #endregion
    #region [tbNews_getbyTag]
    public static List<tbNewsDATA> sp_tbNews_GetByTag(string newtag, string lang)
    {
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            try
            {
                using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_GetByTag", dbConn))
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbConn.Open();
                    dbCmd.Parameters.Add(new SqlParameter("@newTagName", newtag));
                    dbCmd.Parameters.Add(new SqlParameter("@newLang", lang));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                        reader["newid"].ToString(),
                        reader["newName"].ToString(),
                        reader["newImage"].ToString(),
                        reader["newimg"].ToString(),
                        reader["skId"].ToString(),
                        reader["newFile"].ToString(),
                        reader["newContent"].ToString(),
                        reader["newDetail"].ToString(),
                        reader["newDate"].ToString(),
                        reader["newCreateDate"].ToString(),
                        reader["newEndDate"].ToString(),
                        reader["newTitle"].ToString(),
                        reader["newKeyword"].ToString(),
                        reader["newDescription"].ToString(),
                        reader["newPriority"].ToString(),
                        reader["newIndex"].ToString(),
                        reader["newVisit"].ToString(),
                        reader["grnID"].ToString(),
                        reader["usrID"].ToString(),
                        reader["newAuthor"].ToString(),
                        reader["newType"].ToString(),
                        reader["newActive"].ToString(),
                        reader["newLang"].ToString(),
                        reader["ishowdesc"].ToString(),
                        reader["UrlOriginal"].ToString(),
                        reader["vkey"].ToString(),
                        reader["vUrl"].ToString(), reader["newTagName"].ToString());
                            list.Add(objtbNewsDATA);
                        }
                    }
                    dbConn.Close();
                }
            }
            catch { }
        }
        return list;
    }
    #endregion
    #region[tbNews_GetByID]
    public static List<tbNewsDATA> tbNews_GetByTop(string Top, string Where, string Order)
    {
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            try
            {
                using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_GetByTop", dbConn))
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbConn.Open();
                    dbCmd.Parameters.Add(new SqlParameter("@Top", Top));
                    dbCmd.Parameters.Add(new SqlParameter("@Where", Where));
                    dbCmd.Parameters.Add(new SqlParameter("@Order", Order));
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                        reader["newid"].ToString(),
                        reader["newName"].ToString(),
                        reader["newImage"].ToString(),
                        reader["newimg"].ToString(),
                        reader["skId"].ToString(),
                        reader["newFile"].ToString(),
                        reader["newContent"].ToString(),
                        reader["newDetail"].ToString(),
                        reader["newDate"].ToString(),
                        reader["newCreateDate"].ToString(),
                        reader["newEndDate"].ToString(),
                        reader["newTitle"].ToString(),
                        reader["newKeyword"].ToString(),
                        reader["newDescription"].ToString(),
                        reader["newPriority"].ToString(),
                        reader["newIndex"].ToString(),
                        reader["newVisit"].ToString(),
                        reader["grnID"].ToString(),
                        reader["usrID"].ToString(),
                        reader["newAuthor"].ToString(),
                        reader["newType"].ToString(),
                        reader["newActive"].ToString(),
                        reader["newLang"].ToString(),
                        reader["ishowdesc"].ToString(),
                        reader["UrlOriginal"].ToString(),
                        reader["vkey"].ToString(),
                        reader["vUrl"].ToString(), reader["newTagName"].ToString());
                            list.Add(objtbNewsDATA);
                        }
                    }
                    dbConn.Close();
                }
            }
            catch { }
        }
        return list;
    }
    #endregion
    #region[tbNews_GetBynewPriority]
    public static List<tbNewsDATA> tbNews_GetBynewPriority()
    {
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            try
            {
                using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_GetBynewPriority", dbConn))
                {
                    dbCmd.CommandType = CommandType.StoredProcedure;
                    dbConn.Open();
                    using (SqlDataReader reader = dbCmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                            reader["newid"].ToString(),
                            reader["newName"].ToString(),
                            reader["newImage"].ToString(),
                            reader["newimg"].ToString(),
                            reader["skId"].ToString(),
                            reader["newFile"].ToString(),
                            reader["newContent"].ToString(),
                            reader["newDetail"].ToString(),
                            reader["newDate"].ToString(),
                            reader["newCreateDate"].ToString(),
                            reader["newEndDate"].ToString(),
                            reader["newTitle"].ToString(),
                            reader["newKeyword"].ToString(),
                            reader["newDescription"].ToString(),
                            reader["newPriority"].ToString(),
                            reader["newIndex"].ToString(),
                            reader["newVisit"].ToString(),
                            reader["grnID"].ToString(),
                            reader["usrID"].ToString(),
                            reader["newAuthor"].ToString(),
                            reader["newType"].ToString(),
                            reader["newActive"].ToString(),
                            reader["newLang"].ToString(),
                            reader["ishowdesc"].ToString(),
                            reader["UrlOriginal"].ToString(),
                            reader["vkey"].ToString(),
                            reader["vUrl"].ToString(), reader["newTagName"].ToString());
                            list.Add(objtbNewsDATA);
                        }
                    }
                    dbConn.Close();
                }
            }
            catch { }
        }
        return list;
    }
    #endregion
    #region[tbNews_GetBySearchAdmin]
    public static List<tbNewsDATA> tbNews_GetBySearchAdmin(string top, string chuoi)
    {
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("select top " + top + " * from tbNews where 1=1 " + chuoi + " order by newDate desc", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                        reader["newid"].ToString(),
                        reader["newName"].ToString(),
                        reader["newImage"].ToString(),
                        reader["newimg"].ToString(),
                        reader["skId"].ToString(),
                        reader["newFile"].ToString(),
                        reader["newContent"].ToString(),
                        reader["newDetail"].ToString(),
                        reader["newDate"].ToString(),
                        reader["newCreateDate"].ToString(),
                        reader["newEndDate"].ToString(),
                        reader["newTitle"].ToString(),
                        reader["newKeyword"].ToString(),
                        reader["newDescription"].ToString(),
                        reader["newPriority"].ToString(),
                        reader["newIndex"].ToString(),
                        reader["newVisit"].ToString(),
                        reader["grnID"].ToString(),
                        reader["usrID"].ToString(),
                        reader["newAuthor"].ToString(),
                        reader["newType"].ToString(),
                        reader["newActive"].ToString(),
                        reader["newLang"].ToString(),
                        reader["ishowdesc"].ToString(),
                        reader["UrlOriginal"].ToString(),
                        reader["vkey"].ToString(),
                        reader["vUrl"].ToString(), reader["newTagName"].ToString());
                        list.Add(objtbNewsDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbNews_GetByTopVisit_Vietime]
    public static List<tbNewsDATA> tbNews_GetByTopVisit_Vietime()
    {
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_GetByTopVisit_Vietime", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                        reader["newid"].ToString(),
                        reader["newName"].ToString(),
                        reader["newImage"].ToString(),
                        reader["newimg"].ToString(),
                        reader["skId"].ToString(),
                        reader["newFile"].ToString(),
                        reader["newContent"].ToString(),
                        reader["newDetail"].ToString(),
                        reader["newDate"].ToString(),
                        reader["newCreateDate"].ToString(),
                        reader["newEndDate"].ToString(),
                        reader["newTitle"].ToString(),
                        reader["newKeyword"].ToString(),
                        reader["newDescription"].ToString(),
                        reader["newPriority"].ToString(),
                        reader["newIndex"].ToString(),
                        reader["newVisit"].ToString(),
                        reader["grnID"].ToString(),
                        reader["usrID"].ToString(),
                        reader["newAuthor"].ToString(),
                        reader["newType"].ToString(),
                        reader["newActive"].ToString(),
                        reader["newLang"].ToString(),
                        reader["ishowdesc"].ToString(),
                        reader["UrlOriginal"].ToString(),
                        reader["vkey"].ToString(),
                        reader["vUrl"].ToString(), reader["newTagName"].ToString());
                        list.Add(objtbNewsDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbNews_GetByTop_Hot]
    public static List<tbNewsDATA> tbNews_GetByTop_Hot()
    {
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_GetByTop_Hot", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                        reader["newid"].ToString(),
                        reader["newName"].ToString(),
                        reader["newImage"].ToString(),
                        reader["newimg"].ToString(),
                        reader["skId"].ToString(),
                        reader["newFile"].ToString(),
                        reader["newContent"].ToString(),
                        reader["newDetail"].ToString(),
                        reader["newDate"].ToString(),
                        reader["newCreateDate"].ToString(),
                        reader["newEndDate"].ToString(),
                        reader["newTitle"].ToString(),
                        reader["newKeyword"].ToString(),
                        reader["newDescription"].ToString(),
                        reader["newPriority"].ToString(),
                        reader["newIndex"].ToString(),
                        reader["newVisit"].ToString(),
                        reader["grnID"].ToString(),
                        reader["usrID"].ToString(),
                        reader["newAuthor"].ToString(),
                        reader["newType"].ToString(),
                        reader["newActive"].ToString(),
                        reader["newLang"].ToString(),
                        reader["ishowdesc"].ToString(),
                        reader["UrlOriginal"].ToString(),
                        reader["vkey"].ToString(),
                        reader["vUrl"].ToString(), reader["newTagName"].ToString());
                        list.Add(objtbNewsDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbNews_GetBygrnID_Vietime]
    public static List<tbNewsDATA> tbNews_GetBygrnID_Vietime(string grnid)
    {
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_GetBygrnID_Vietime", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@grnId", grnid));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                        reader["newid"].ToString(),
                        reader["newName"].ToString(),
                        reader["newImage"].ToString(),
                        reader["newimg"].ToString(),
                        reader["skId"].ToString(),
                        reader["newFile"].ToString(),
                        reader["newContent"].ToString(),
                        reader["newDetail"].ToString(),
                        reader["newDate"].ToString(),
                        reader["newCreateDate"].ToString(),
                        reader["newEndDate"].ToString(),
                        reader["newTitle"].ToString(),
                        reader["newKeyword"].ToString(),
                        reader["newDescription"].ToString(),
                        reader["newPriority"].ToString(),
                        reader["newIndex"].ToString(),
                        reader["newVisit"].ToString(),
                        reader["grnID"].ToString(),
                        reader["usrID"].ToString(),
                        reader["newAuthor"].ToString(),
                        reader["newType"].ToString(),
                        reader["newActive"].ToString(),
                        reader["newLang"].ToString(),
                        reader["ishowdesc"].ToString(),
                        reader["UrlOriginal"].ToString(),
                        reader["vkey"].ToString(),
                        reader["vUrl"].ToString(), reader["newTagName"].ToString());
                        list.Add(objtbNewsDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbNews_GetBygrnID_Top_Vietime]
    public static List<tbNewsDATA> tbNews_GetBygrnID_Top_Vietime(string grnid)
    {
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_GetBygrnID_Top_Vietime", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@grnId", grnid));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                        reader["newid"].ToString(),
                        reader["newName"].ToString(),
                        reader["newImage"].ToString(),
                        reader["newimg"].ToString(),
                        reader["skId"].ToString(),
                        reader["newFile"].ToString(),
                        reader["newContent"].ToString(),
                        reader["newDetail"].ToString(),
                        reader["newDate"].ToString(),
                        reader["newCreateDate"].ToString(),
                        reader["newEndDate"].ToString(),
                        reader["newTitle"].ToString(),
                        reader["newKeyword"].ToString(),
                        reader["newDescription"].ToString(),
                        reader["newPriority"].ToString(),
                        reader["newIndex"].ToString(),
                        reader["newVisit"].ToString(),
                        reader["grnID"].ToString(),
                        reader["usrID"].ToString(),
                        reader["newAuthor"].ToString(),
                        reader["newType"].ToString(),
                        reader["newActive"].ToString(),
                        reader["newLang"].ToString(),
                        reader["ishowdesc"].ToString(),
                        reader["UrlOriginal"].ToString(),
                        reader["vkey"].ToString(),
                        reader["vUrl"].ToString(), reader["newTagName"].ToString());
                        list.Add(objtbNewsDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbNews_GetBygrnID_Date_Top_Vietime]
    public static List<tbNewsDATA> sp_tbNews_GetBySearch(string newName)
    {
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("SELECT top 20 * FROM tbNews WHERE newActive = 0 and newName like N'%" + newName + "%' or newContent like N'%" + newName + "%' or newDetail like N'%" + newName + "%' order by newid desc", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                        reader["newid"].ToString(),
                        reader["newName"].ToString(),
                        reader["newImage"].ToString(),
                        reader["newimg"].ToString(),
                        reader["skId"].ToString(),
                        reader["newFile"].ToString(),
                        reader["newContent"].ToString(),
                        reader["newDetail"].ToString(),
                        reader["newDate"].ToString(),
                        reader["newCreateDate"].ToString(),
                        reader["newEndDate"].ToString(),
                        reader["newTitle"].ToString(),
                        reader["newKeyword"].ToString(),
                        reader["newDescription"].ToString(),
                        reader["newPriority"].ToString(),
                        reader["newIndex"].ToString(),
                        reader["newVisit"].ToString(),
                        reader["grnID"].ToString(),
                        reader["usrID"].ToString(),
                        reader["newAuthor"].ToString(),
                        reader["newType"].ToString(),
                        reader["newActive"].ToString(),
                        reader["newLang"].ToString(),
                        reader["ishowdesc"].ToString(),
                        reader["UrlOriginal"].ToString(),
                        reader["vkey"].ToString(),
                        reader["vUrl"].ToString(), reader["newTagName"].ToString());
                        list.Add(objtbNewsDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbNews_GetByTop4_Hot]
    public static List<tbNewsDATA> tbNews_GetByTop4_Hot(string grnid)
    {
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_GetByTop4_Hot", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@grnId", grnid));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                        reader["newid"].ToString(),
                        reader["newName"].ToString(),
                        reader["newImage"].ToString(),
                        reader["newimg"].ToString(),
                        reader["skId"].ToString(),
                        reader["newFile"].ToString(),
                        reader["newContent"].ToString(),
                        reader["newDetail"].ToString(),
                        reader["newDate"].ToString(),
                        reader["newCreateDate"].ToString(),
                        reader["newEndDate"].ToString(),
                        reader["newTitle"].ToString(),
                        reader["newKeyword"].ToString(),
                        reader["newDescription"].ToString(),
                        reader["newPriority"].ToString(),
                        reader["newIndex"].ToString(),
                        reader["newVisit"].ToString(),
                        reader["grnID"].ToString(),
                        reader["usrID"].ToString(),
                        reader["newAuthor"].ToString(),
                        reader["newType"].ToString(),
                        reader["newActive"].ToString(),
                        reader["newLang"].ToString(),
                        reader["ishowdesc"].ToString(),
                        reader["UrlOriginal"].ToString(),
                        reader["vkey"].ToString(),
                        reader["vUrl"].ToString(), reader["newTagName"].ToString());
                        list.Add(objtbNewsDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbNews_GetBygrnID_Date_Top_Vietime]
    public static List<tbNewsDATA> tbNews_GetBygrnID_Date_Top_Vietime(string grnid, string newdate)
    {
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_GetBygrnID_Date_Top_Vietime", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@grnId", grnid));
                dbCmd.Parameters.Add(new SqlParameter("@newdate", newdate));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                        reader["newid"].ToString(),
                        reader["newName"].ToString(),
                        reader["newImage"].ToString(),
                        reader["newimg"].ToString(),
                        reader["skId"].ToString(),
                        reader["newFile"].ToString(),
                        reader["newContent"].ToString(),
                        reader["newDetail"].ToString(),
                        reader["newDate"].ToString(),
                        reader["newCreateDate"].ToString(),
                        reader["newEndDate"].ToString(),
                        reader["newTitle"].ToString(),
                        reader["newKeyword"].ToString(),
                        reader["newDescription"].ToString(),
                        reader["newPriority"].ToString(),
                        reader["newIndex"].ToString(),
                        reader["newVisit"].ToString(),
                        reader["grnID"].ToString(),
                        reader["usrID"].ToString(),
                        reader["newAuthor"].ToString(),
                        reader["newType"].ToString(),
                        reader["newActive"].ToString(),
                        reader["newLang"].ToString(),
                        reader["ishowdesc"].ToString(),
                        reader["UrlOriginal"].ToString(),
                        reader["vkey"].ToString(),
                        reader["vUrl"].ToString(), reader["newTagName"].ToString());
                        list.Add(objtbNewsDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbNews_GetByIDGetDetail_Vietime]
    public static List<tbNewsDATA> tbNews_GetByIDGetDetail_Vietime(string newid)
    {
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_GetByIDGetDetail_Vietime", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@newid", newid));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                        reader["newid"].ToString(),
                        reader["newName"].ToString(),
                        reader["newImage"].ToString(),
                        reader["newimg"].ToString(),
                        reader["skId"].ToString(),
                        reader["newFile"].ToString(),
                        reader["newContent"].ToString(),
                        reader["newDetail"].ToString(),
                        reader["newDate"].ToString(),
                        reader["newCreateDate"].ToString(),
                        reader["newEndDate"].ToString(),
                        reader["newTitle"].ToString(),
                        reader["newKeyword"].ToString(),
                        reader["newDescription"].ToString(),
                        reader["newPriority"].ToString(),
                        reader["newIndex"].ToString(),
                        reader["newVisit"].ToString(),
                        reader["grnID"].ToString(),
                        reader["usrID"].ToString(),
                        reader["newAuthor"].ToString(),
                        reader["newType"].ToString(),
                        reader["newActive"].ToString(),
                        reader["newLang"].ToString(),
                        reader["ishowdesc"].ToString(),
                        reader["UrlOriginal"].ToString(),
                        reader["vkey"].ToString(),
                        reader["vUrl"].ToString(), reader["newTagName"].ToString());
                        list.Add(objtbNewsDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    public static List<tbNewsDATA> tbNews_GetByIDGetDetail_Vietime_TagName(string newTagName)
    {
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_GetByIDGetDetail_Vietime_TagName", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@newTagName", newTagName));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                        reader["newid"].ToString(),
                        reader["newName"].ToString(),
                        reader["newImage"].ToString(),
                        reader["newimg"].ToString(),
                        reader["skId"].ToString(),
                        reader["newFile"].ToString(),
                        reader["newContent"].ToString(),
                        reader["newDetail"].ToString(),
                        reader["newDate"].ToString(),
                        reader["newCreateDate"].ToString(),
                        reader["newEndDate"].ToString(),
                        reader["newTitle"].ToString(),
                        reader["newKeyword"].ToString(),
                        reader["newDescription"].ToString(),
                        reader["newPriority"].ToString(),
                        reader["newIndex"].ToString(),
                        reader["newVisit"].ToString(),
                        reader["grnID"].ToString(),
                        reader["usrID"].ToString(),
                        reader["newAuthor"].ToString(),
                        reader["newType"].ToString(),
                        reader["newActive"].ToString(),
                        reader["newLang"].ToString(),
                        reader["ishowdesc"].ToString(),
                        reader["UrlOriginal"].ToString(),
                        reader["vkey"].ToString(),
                        reader["vUrl"].ToString(), reader["newTagName"].ToString());
                        list.Add(objtbNewsDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbNews_GetByTop10New_Detal_Vietime]
    public static List<tbNewsDATA> tbNews_GetByTop10New_Detal_Vietime(string newid, string date, string grnId)
    {
        string sdate = Convert.ToDateTime(date).ToString("MM/dd/yyyy HH:mm:ss");
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_GetByTop10New_Detal_Vietime", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@newId", newid));
                dbCmd.Parameters.Add(new SqlParameter("@date", sdate));
                dbCmd.Parameters.Add(new SqlParameter("@grnId", grnId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                        reader["newid"].ToString(),
                        reader["newName"].ToString(),
                        reader["newImage"].ToString(),
                        reader["newimg"].ToString(),
                        reader["skId"].ToString(),
                        reader["newFile"].ToString(),
                        reader["newContent"].ToString(),
                        reader["newDetail"].ToString(),
                        reader["newDate"].ToString(),
                        reader["newCreateDate"].ToString(),
                        reader["newEndDate"].ToString(),
                        reader["newTitle"].ToString(),
                        reader["newKeyword"].ToString(),
                        reader["newDescription"].ToString(),
                        reader["newPriority"].ToString(),
                        reader["newIndex"].ToString(),
                        reader["newVisit"].ToString(),
                        reader["grnID"].ToString(),
                        reader["usrID"].ToString(),
                        reader["newAuthor"].ToString(),
                        reader["newType"].ToString(),
                        reader["newActive"].ToString(),
                        reader["newLang"].ToString(),
                        reader["ishowdesc"].ToString(),
                        reader["UrlOriginal"].ToString(),
                        reader["vkey"].ToString(),
                        reader["vUrl"].ToString(), reader["newTagName"].ToString());
                        list.Add(objtbNewsDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbNews_GetByTop10Cu_Detal_Vietime]
    public static List<tbNewsDATA> tbNews_GetByTop10Cu_Detal_Vietime(string newid, string date, string grnId)
    {
        string sdate = Convert.ToDateTime(date).ToString("MM/dd/yyyy HH:mm:ss");
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_GetByTop10Cu_Detal_Vietime", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@newId", newid));
                dbCmd.Parameters.Add(new SqlParameter("@date", sdate));
                dbCmd.Parameters.Add(new SqlParameter("@grnId", grnId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                        reader["newid"].ToString(),
                        reader["newName"].ToString(),
                        reader["newImage"].ToString(),
                        reader["newimg"].ToString(),
                        reader["skId"].ToString(),
                        reader["newFile"].ToString(),
                        reader["newContent"].ToString(),
                        reader["newDetail"].ToString(),
                        reader["newDate"].ToString(),
                        reader["newCreateDate"].ToString(),
                        reader["newEndDate"].ToString(),
                        reader["newTitle"].ToString(),
                        reader["newKeyword"].ToString(),
                        reader["newDescription"].ToString(),
                        reader["newPriority"].ToString(),
                        reader["newIndex"].ToString(),
                        reader["newVisit"].ToString(),
                        reader["grnID"].ToString(),
                        reader["usrID"].ToString(),
                        reader["newAuthor"].ToString(),
                        reader["newType"].ToString(),
                        reader["newActive"].ToString(),
                        reader["newLang"].ToString(),
                        reader["ishowdesc"].ToString(),
                        reader["UrlOriginal"].ToString(),
                        reader["vkey"].ToString(),
                        reader["vUrl"].ToString(), reader["newTagName"].ToString());
                        list.Add(objtbNewsDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[sp_tbNews_Page]
    public static DataTable sp_tbNews_page(int curpage, ref int numofnews)
    {
        cmd = new SqlCommand("sp_tbNews_Page");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@curpage", curpage);
        cmd.Parameters.Add("@return_rows", 10);
        DataSet ds = common.Get_DataSet(cmd);
        numofnews = Convert.ToInt32(ds.Tables[1].Rows[0]["numofrows"].ToString().Trim());
        return ds.Tables[0];
    }
    public static DataTable sp_tbNews_pageQT(int curpage, ref int numofnews)
    {
        cmd = new SqlCommand("sp_tbNews_Page");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@curpage", curpage);
        cmd.Parameters.Add("@return_rows", 50);
        DataSet ds = common.Get_DataSet(cmd);
        numofnews = Convert.ToInt32(ds.Tables[1].Rows[0]["numofrows"].ToString().Trim());
        return ds.Tables[0];
    }
    #endregion
    #region[sp_tbNews_Page_grnId]
    public static DataTable sp_tbNews_Page_grnId(string grnId, int curpage, ref int numofnews)
    {
        cmd = new SqlCommand("sp_tbNews_Page_grnId");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@grnId", grnId);
        cmd.Parameters.Add("@curpage", curpage);
        cmd.Parameters.Add("@return_rows", 10);
        DataSet ds = common.Get_DataSet(cmd);
        numofnews = Convert.ToInt32(ds.Tables[1].Rows[0]["numofrows"].ToString().Trim());
        return ds.Tables[0];
    }
    public static DataTable sp_tbNews_Page_grnIdQT(string grnId, int curpage, ref int numofnews)
    {
        cmd = new SqlCommand("sp_tbNews_Page_grnId");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@grnId", grnId);
        cmd.Parameters.Add("@curpage", curpage);
        cmd.Parameters.Add("@return_rows", 50);
        DataSet ds = common.Get_DataSet(cmd);
        numofnews = Convert.ToInt32(ds.Tables[1].Rows[0]["numofrows"].ToString().Trim());
        return ds.Tables[0];
    }
    #endregion
    #region[tbNews_page_DL]
    public static DataTable tbNews_page_DL(string grnId, int curpage, ref int numofnews)
    {
        cmd = new SqlCommand("sp_tbNews_page_DL");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@grnId", grnId);
        cmd.Parameters.Add("@curpage", curpage);
        cmd.Parameters.Add("@return_rows", 15);
        DataSet ds = common.Get_DataSet(cmd);
        numofnews = Convert.ToInt32(ds.Tables[1].Rows[0]["numofrows"].ToString().Trim());
        return ds.Tables[0];
    }
    #endregion
    #region[tbNews_Paging]
    public static List<tbNewsDATA> tbNews_Paging(string CurentPage, string PageSize, string Fields, string Filter, string Sort)
    {
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_Paging", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add("@CurentPage", CurentPage);
                dbCmd.Parameters.Add("@PageSize", PageSize);
                dbCmd.Parameters.Add("@Fields", Fields);
                dbCmd.Parameters.Add("@Filter", Filter);
                dbCmd.Parameters.Add("@Sort", Sort);
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                        reader["newid"].ToString(),
                        reader["newName"].ToString(),
                        reader["newImage"].ToString(),
                        reader["newimg"].ToString(),
                        reader["skId"].ToString(),
                        reader["newFile"].ToString(),
                        reader["newContent"].ToString(),
                        reader["newDetail"].ToString(),
                        reader["newDate"].ToString(),
                        reader["newCreateDate"].ToString(),
                        reader["newEndDate"].ToString(),
                        reader["newTitle"].ToString(),
                        reader["newKeyword"].ToString(),
                        reader["newDescription"].ToString(),
                        reader["newPriority"].ToString(),
                        reader["newIndex"].ToString(),
                        reader["newVisit"].ToString(),
                        reader["grnID"].ToString(),
                        reader["usrID"].ToString(),
                        reader["newAuthor"].ToString(),
                        reader["newType"].ToString(),
                        reader["newActive"].ToString(),
                        reader["newLang"].ToString(),
                        reader["ishowdesc"].ToString(),
                        reader["UrlOriginal"].ToString(),
                        reader["vkey"].ToString(),
                        reader["vUrl"].ToString(),
                        reader["newTagName"].ToString());
                        list.Add(objtbNewsDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbNews_Paging_Admin]
    public static List<tbNewsDATA> tbNews_Paging_Admin(string CurentPage, string PageSize, string Fields, string Filter, string Sort)
    {
        List<tbNewsDATA> list = new List<tbNewsDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbNews_Paging_Admin", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add("@CurentPage", CurentPage);
                dbCmd.Parameters.Add("@PageSize", PageSize);
                dbCmd.Parameters.Add("@Fields", Fields);
                dbCmd.Parameters.Add("@Filter", Filter);
                dbCmd.Parameters.Add("@Sort", Sort);
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbNewsDATA objtbNewsDATA = new tbNewsDATA(
                        reader["newid"].ToString(),
                        reader["newName"].ToString(),
                        reader["newImage"].ToString(),
                        reader["newimg"].ToString(),
                        reader["skId"].ToString(),
                        reader["newFile"].ToString(),
                        reader["newContent"].ToString(),
                        reader["newDetail"].ToString(),
                        reader["newDate"].ToString(),
                        reader["newCreateDate"].ToString(),
                        reader["newEndDate"].ToString(),
                        reader["newTitle"].ToString(),
                        reader["newKeyword"].ToString(),
                        reader["newDescription"].ToString(),
                        reader["newPriority"].ToString(),
                        reader["newIndex"].ToString(),
                        reader["newVisit"].ToString(),
                        reader["grnID"].ToString(),
                        reader["usrID"].ToString(),
                        reader["newAuthor"].ToString(),
                        reader["newType"].ToString(),
                        reader["newActive"].ToString(),
                        reader["newLang"].ToString(),
                        reader["ishowdesc"].ToString(),
                        reader["UrlOriginal"].ToString(),
                        reader["vkey"].ToString(),
                        reader["vUrl"].ToString(),
                        reader["newTagName"].ToString(),
                        reader["grnName"].ToString());
                        list.Add(objtbNewsDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbNews_Count]
    public static string tbNews_Count(string strQuery)
    {
        string str = "0";
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand(strQuery, dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                try
                {
                    str = dbCmd.ExecuteScalar().ToString();
                }
                catch { }
                dbConn.Close();
            }
        }
        return str;
    }
    #endregion

    internal static void Viewlist()
    {
        throw new NotImplementedException();
    }
}