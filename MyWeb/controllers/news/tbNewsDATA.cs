﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class tbNewsDATA
{
    #region[Declare variables]
    private string _newid;
    private string _newName;
    private string _newImage;
    private string _newimg;
    private string _skId;
    private string _newFile;
    private string _newContent;
    private string _newDetail;
    private string _newDate;
    private string _newCreateDate;
    private string _newEndDate;
    private string _newTitle;
    private string _newKeyword;
    private string _newDescription;
    private string _newPriority;
    private string _newIndex;
    private string _newVisit;
    private string _grnID;
    private string _usrID;
    private string _newAuthor;
    private string _newType;
    private string _newActive;
    private string _newLang;
    private string _ishowdesc;
    private string _UrlOriginal;
    private string _vkey;
    private string _vUrl;
    private string _newTagName;
    private string _grnName;
    #endregion
    #region[Function]
    public tbNewsDATA() { }
    public tbNewsDATA(string newid_, string newName_, string newImage_, string newimg_, string skId_, string newFile_, string newContent_,
        string newDetail_, string newDate_, string newCreateDate_, string newEndDate_, string newTitle_,
        string newKeyword_, string newDescription_, string newPriority_, string newIndex_, string newVisit_,
        string grnID_, string usrID_, string newAuthor_, string newType_, string newActive_, string newLang_,
        string ishowdesc_, string UrlOriginal_, string vkey_, string vUrl_, string newTagName_)
    {
        _newid = newid_;
        _newName = newName_;
        _newImage = newImage_;
        _newimg = newimg_;
        _skId = skId_;
        _newFile = newFile_;
        _newContent = newContent_;
        _newDetail = newDetail_;
        _newDate = newDate_;
        _newCreateDate = newCreateDate_;
        _newEndDate = newEndDate_;
        _newTitle = newTitle_;
        _newKeyword = newKeyword_;
        _newDescription = newDescription_;
        _newPriority = newPriority_;
        _newIndex = newIndex_;
        _newVisit = newVisit_;
        _grnID = grnID_;
        _usrID = usrID_;
        _newAuthor = newAuthor_;
        _newType = newType_;
        _newActive = newActive_;
        _newLang = newLang_;
        _ishowdesc = ishowdesc_;
        _UrlOriginal = UrlOriginal_;
        _vkey = vkey_;
        _vUrl = vUrl_;
        _newTagName = newTagName_;
    }
    public tbNewsDATA(string newid_, string newName_, string newImage_, string newimg_, string skId_, string newFile_, string newContent_,
        string newDetail_, string newDate_, string newCreateDate_, string newEndDate_, string newTitle_,
        string newKeyword_, string newDescription_, string newPriority_, string newIndex_, string newVisit_,
        string grnID_, string usrID_, string newAuthor_, string newType_, string newActive_, string newLang_,
        string ishowdesc_, string UrlOriginal_, string vkey_, string vUrl_, string newTagName_, string grnName_)
    {
        _newid = newid_;
        _newName = newName_;
        _newImage = newImage_;
        _newimg = newimg_;
        _skId = skId_;
        _newFile = newFile_;
        _newContent = newContent_;
        _newDetail = newDetail_;
        _newDate = newDate_;
        _newCreateDate = newCreateDate_;
        _newEndDate = newEndDate_;
        _newTitle = newTitle_;
        _newKeyword = newKeyword_;
        _newDescription = newDescription_;
        _newPriority = newPriority_;
        _newIndex = newIndex_;
        _newVisit = newVisit_;
        _grnID = grnID_;
        _usrID = usrID_;
        _newAuthor = newAuthor_;
        _newType = newType_;
        _newActive = newActive_;
        _newLang = newLang_;
        _ishowdesc = ishowdesc_;
        _UrlOriginal = UrlOriginal_;
        _vkey = vkey_;
        _vUrl = vUrl_;
        _newTagName = newTagName_;
        _grnName = grnName_;
    }
    #endregion
    #region[Assigned value]
    public string newid { get { return _newid; } set { _newid = value; } }
    public string newName { get { return _newName; } set { _newName = value; } }
    public string newImage { get { return _newImage; } set { _newImage = value; } }
    public string newimg { get { return _newimg; } set { _newimg = value; } }
    public string skId { get { return _skId; } set { _skId = value; } }
    public string newFile { get { return _newFile; } set { _newFile = value; } }
    public string newContent { get { return _newContent; } set { _newContent = value; } }
    public string newDetail { get { return _newDetail; } set { _newDetail = value; } }
    public string newDate { get { return _newDate; } set { _newDate = value; } }
    public string newCreateDate { get { return _newCreateDate; } set { _newCreateDate = value; } }
    public string newEndDate { get { return _newEndDate; } set { _newEndDate = value; } }
    public string newTitle { get { return _newTitle; } set { _newTitle = value; } }
    public string newKeyword { get { return _newKeyword; } set { _newKeyword = value; } }
    public string newDescription { get { return _newDescription; } set { _newDescription = value; } }
    public string newPriority { get { return _newPriority; } set { _newPriority = value; } }
    public string newIndex { get { return _newIndex; } set { _newIndex = value; } }
    public string newVisit { get { return _newVisit; } set { _newVisit = value; } }
    public string grnID { get { return _grnID; } set { _grnID = value; } }
    public string usrID { get { return _usrID; } set { _usrID = value; } }
    public string newAuthor { get { return _newAuthor; } set { _newAuthor = value; } }
    public string newType { get { return _newType; } set { _newType = value; } }
    public string newActive { get { return _newActive; } set { _newActive = value; } }
    public string newLang { get { return _newLang; } set { _newLang = value; } }
    public string ishowdesc { get { return _ishowdesc; } set { _ishowdesc = value; } }
    public string UrlOriginal { get { return _UrlOriginal; } set { _UrlOriginal = value; } }
    public string vkey { get { return _vkey; } set { _vkey = value; } }
    public string vUrl { get { return _vUrl; } set { _vUrl = value; } }
    public string newTagName { get { return _newTagName; } set { _newTagName = value; } }
    public string grnName { get { return _grnName; } set { _grnName = value; } }
    #endregion
}