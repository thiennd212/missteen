﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class pageType
{
    public static string GP = "100"; // Nhóm: sản phẩm - grou product
    public static string GN = "200"; // Nhóm: tin tức - group new
    public static string GL = "300"; // Nhóm: thư viện - group library
    public static string LD = "400"; // Nhóm: chi tiết thư viện - library detail
    public static string GM = "500";
    public static string ND = "800"; // Nhóm: chi tiết tin tức - new detail
    public static string PD = "900"; // Nhóm: chi tiết sản phẩm - product detail    
    
    public static string TourCategory = "701"; // Nhóm tour
    public static string TourTopic = "702"; // Chủ đề tour
    public static string TourPlace = "703"; // Địa điểm
    public static string Tour = "704"; // Tour

    public static string Contestant = "705"; // Contestant
    public static string ContestantDetail = "706"; // Chi tiết thí sinh
    public static string ContestantGroup = "707"; // Nhóm thí sinh
    public static string ContestantPlace = "708"; // Địa chỉ thí sinh

    // Nhóm menu
    public static string ML = "1"; // Menu kiểu liên kết trang - menu link
    public static string MC = "2"; // Menu kiểu nội dung - menu
    public static string MU = "3"; // Menu kiểu liên kết URL - menu url

    public static string HP = "4"; // Trang chủ - home page
    public static string MN = "5";// Tin tức - menu new
    public static string MP = "6";// Sản phẩm - menu product
    public static string MPn = "7";// Sản phẩm mới - menu product new
    public static string PC = "8";// Giỏ hàng - product cart
    public static string LP = "9";// Thư viện - library page
    public static string CP = "10";// Liên hệ - contact page
    public static string RG = "11";// Đăng ký thành viên - register
    public static string FP = "12";// Quên mật khẩu - forgot password
    public static string CPw = "13";// Đổi mật khẩu - change password
    public static string UI = "14";// Thông tin tài khoản - user info
    public static string LO = "15";// Đơn hàng - list order
    public static string PM = "16";// Thanh toán - payment
    public static string MPnb = "17";// Sản phẩm noi bat - menu product new
    public static string MPbc = "18";// Sản phẩm ban chay - menu product new
    public static string MPkm = "19";// Sản phẩm km - menu product new
    public static string MM = "20";
    public static string MenuTourCategory = "21"; // Menu liên kết nhóm tour
    public static string MTT = "22"; // Menu liên kết chủ đề
    public static string MTP = "23"; // Menu liên kết địa điểm
    public static string CTG = "24"; // Menu liên kết thí sinh
}