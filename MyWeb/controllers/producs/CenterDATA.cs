﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CenterDATA
{
    #region[Declare variables]
    private string _Id;
    private string _CatId;
    private string _ProId;
    #endregion
    #region[Function]
    public CenterDATA() { }
    public CenterDATA(string Id_, string CatId_, string ProId_)
    {
        _Id = Id_;
        _CatId = CatId_;
        _CatId = ProId_;
    }
    #endregion
    #region[Assigned value]
    public string Id { get { return _Id; } set { _Id = value; } }
    public string CatId { get { return _CatId; } set { _CatId = value; } }
    public string ProId { get { return _ProId; } set { _ProId = value; } }
    #endregion
}