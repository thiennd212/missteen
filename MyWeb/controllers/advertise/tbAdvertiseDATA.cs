﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class tbAdvertiseDATA
{
    #region[Declare variables]
    private string _advId;
    private string _advName;
    private string _advImage;
    private string _advWidth;
    private string _advHeight;
    private string _advLink;
    private string _advTarget;
    private string _advContent;
    private string _advPosition;
    private string _pagId;
    private string _advClick;
    private string _advOrd;
    private string _advActive;
    private string _advLang;
    #endregion
    #region[Function]
    public tbAdvertiseDATA() { }
    public tbAdvertiseDATA(string advId_, string advName_, string advImage_, string advWidth_, string advHeight_,
        string advLink_, string advTarget_, string advContent_, string advPosition_, string pagId_, string advClick_,
        string advOrd_, string advActive_, string advLang_)
    {
        _advId = advId_;
        _advName = advName_;
        _advImage = advImage_;
        _advWidth = advWidth_;
        _advHeight = advHeight_;
        _advLink = advLink_;
        _advTarget = advTarget_;
        _advContent = advContent_;
        _advPosition = advPosition_;
        _pagId = pagId_;
        _advClick = advClick_;
        _advOrd = advOrd_;
        _advActive = advActive_;
        _advLang = advLang_;
    }
    #endregion
    #region[Assigned value]
    public string advId { get { return _advId; } set { _advId = value; } }
    public string advName { get { return _advName; } set { _advName = value; } }
    public string advImage { get { return _advImage; } set { _advImage = value; } }
    public string advWidth { get { return _advWidth; } set { _advWidth = value; } }
    public string advHeight { get { return _advHeight; } set { _advHeight = value; } }
    public string advLink { get { return _advLink; } set { _advLink = value; } }
    public string advTarget { get { return _advTarget; } set { _advTarget = value; } }
    public string advContent { get { return _advContent; } set { _advContent = value; } }
    public string advPosition { get { return _advPosition; } set { _advPosition = value; } }
    public string pagId { get { return _pagId; } set { _pagId = value; } }
    public string advClick { get { return _advClick; } set { _advClick = value; } }
    public string advOrd { get { return _advOrd; } set { _advOrd = value; } }
    public string advActive { get { return _advActive; } set { _advActive = value; } }
    public string advLang { get { return _advLang; } set { _advLang = value; } }
    #endregion
}