﻿using MyWeb;
using MyWeb.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Mail;
using System.Web.UI.WebControls;
using System.Xml;

public class common
{
    static dataAccessDataContext db = new dataAccessDataContext();
    public class TabProduct
    {
        public string field { get; set; }
        public string fvitri { get; set; }
        public string TabThongtin { get; set; }
    }
    public class MovieCollection
    {
        public IEnumerable<TabProduct> movies { get; set; }
    }
    public static string url = "";

    #region[Cat chuoi text de hien thi]
    public static string FormatContentNews(string value, int count)
    {
        string _value = value;
        if (_value.Length >= count)
        {
            string ValueCut = _value.Substring(0, count - 3);
            string[] valuearray = ValueCut.Split(' ');
            string valuereturn = "";
            for (int i = 0; i < valuearray.Length - 1; i++)
            {
                valuereturn = valuereturn + " " + valuearray[i];
            }
            return valuereturn;
        }
        else
        {
            return _value;
        }
    }
    #endregion

    public static string GetHttpURL()
    {
        string path = "";
        if (System.Web.HttpContext.Current != null)
        {
            string[] s = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.Split(new char[] { '/' });
            path = s[0] + "/";
            for (int i = 1; i < s.Length - 1; i++)
            {
                path = path + s[i] + "/";
            }
        }
        return path;
    }
    public static string ReWriteindex()
    {
        string strTitle = url.ToString() + "/index.htm";
        return strTitle;
    }
    public static string ReWriteCategory(object cate, object title)
    {
        string strTitle = vmmsclass.vmmsrewriteurl.ReWriteTitle(title.ToString());
        strTitle = url.ToString() + "?itnews=" + strTitle + "&e=news&c" + cate.ToString();
        //strTitle = url.ToString() + "/cate/" + cate.ToString() + "/" + strTitle + ".htm";
        return strTitle;
    }
    public static string ReWritepage(object pid, object title)
    {
        string strTitle = vmmsclass.vmmsrewriteurl.ReWriteTitle(title.ToString());
        strTitle = url.ToString() + "/page/" + pid.ToString() + "/" + strTitle + ".htm";
        return strTitle;
    }
    public static string ReWritecontact(object title)
    {
        string strTitle = vmmsclass.vmmsrewriteurl.ReWriteTitle(title.ToString());
        strTitle = url.ToString() + "/contact/" + strTitle + ".htm";
        return strTitle;
    }
    public static string ReWritesitemap(object title)
    {
        string strTitle = vmmsclass.vmmsrewriteurl.ReWriteTitle(title.ToString());
        strTitle = url.ToString() + "/sitemap/" + strTitle + ".htm";
        return strTitle;
    }
    public static string ReWriteCategorysub(object cate, object catesub, object title)
    {
        string strTitle = vmmsclass.vmmsrewriteurl.ReWriteTitle(title.ToString());
        strTitle = url.ToString() + "/cates/" + cate.ToString() + "/" + catesub.ToString() + "/" + strTitle + ".htm";
        return strTitle;
    }
    public static string ReWriteCategorydate(object cate, object dates, object title)
    {
        string strTitle = vmmsclass.vmmsrewriteurl.ReWriteTitle(title.ToString());
        //strTitle = url.ToString() + "/catedate/" + cate.ToString() + "/" + dates.ToString() + "/" + strTitle + ".htm";
        strTitle = url.ToString() + "?itnews=" + strTitle + "&e=news&c=" + cate.ToString() + "&d=" + dates.ToString();
        return strTitle;
    }
    public static string ReWriteNewDetail(object cate, object newid, object title)
    {
        string strTitle = vmmsclass.vmmsrewriteurl.ReWriteTitle(title.ToString());
        //strTitle = url.ToString() + "/news/" + cate.ToString() + "/" + newid.ToString() + "/" + strTitle + ".htm";
        //strTitle = url.ToString() + "?namhai="+ strTitle +"&e=newsdetail&c=" + cate.ToString() + "&n=" + newid.ToString();
        strTitle = url.ToString() + strTitle + "_n" + newid.ToString() + "_g" + cate.ToString() + ".aspx";
        return strTitle;
    }

    public static string FormatDate_2_Longdate(DateTime date, string lang)
    {
        string datename = date.DayOfWeek.ToString();
        switch (date.DayOfWeek.ToString())
        {
            case "Saturday":
                if (lang.Equals("vi"))
                {
                    datename = "Thứ bảy";
                    //datename = "Saturday";
                }
                break;
            case "Sunday":

                if (lang.Equals("vi"))
                {
                    datename = "Chủ nhật";
                    //datename = "Sunday";
                    break;
                }
                break;
            case "Monday":

                if (lang.Equals("vi"))
                {
                    datename = "Thứ hai";
                    //datename = "Monday";
                    break;
                }
                break;
            case "Tuesday":

                if (lang.Equals("vi"))
                {
                    datename = "Thứ ba";
                    //datename = "Tuesday";
                    break;
                }
                break;
            case "Wednesday":
                if (lang.Equals("vi"))
                {
                    datename = "Thứ tư";
                    //datename = "Wednesday";
                    break;
                }

                break;
            case "Thursday":
                if (lang.Equals("vi"))
                {
                    datename = "Thứ năm";
                    //datename = "Thursday";
                    break;
                }
                break;
            case "Friday":
                if (lang.Equals("vi"))
                {
                    datename = "Thứ sáu";
                    //datename = "Friday";
                    break;
                }
                break;
        }
        return datename.ToString() + ", Tháng " + date.ToString("M yyyy hh:mm");
    }
    #region[RemoveUnicode]
    public static string RemoveUnicode(string s)
    {
        string stFormD = s.Normalize(NormalizationForm.FormD);
        StringBuilder sb = new StringBuilder();

        for (int ich = 0; ich < stFormD.Length; ich++)
        {
            UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(stFormD[ich]);
            if (uc != UnicodeCategory.NonSpacingMark)
            {
                sb.Append(stFormD[ich]);
            }
        }
        string temp = (sb.ToString().Normalize(NormalizationForm.FormC));
        temp = temp.Replace("đ", "d");
        return temp.Replace("Đ", "D");
    }
    #endregion
    #region[userofline]
    public static void userofline()
    {
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("update tbvisistor set iuseronline=iuseronline - 1 where day(dateonline)=day(getdate()) and month(dateonline)=month(getdate()) and year(dateonline)=year(getdate())", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
            }
        }
    }
    #endregion
    #region Get DataTable
    public static DataTable Get_DataTable(SqlCommand cmd)
    {
        using (DataSet ds = new DataSet())
        {
            using (SqlDataAdapter da = new SqlDataAdapter())
            {
                da.SelectCommand = cmd;
                da.SelectCommand.Connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                da.Fill(ds);
                da.SelectCommand.Connection.Close();
                return ds.Tables[0];
            }
        }
    }
    #endregion
    #region Get_DataSet
    public static DataSet Get_DataSet(SqlCommand cmd)
    {
        using (DataSet ds = new DataSet())
        {
            using (SqlDataAdapter da = new SqlDataAdapter())
            {
                da.SelectCommand = cmd;
                da.SelectCommand.Connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                da.Fill(ds);
                da.SelectCommand.Connection.Close();
                return ds;
            }
        }
    }
    #endregion
    #region[imgdownload]
    public static string imgdownload(string path)
    {
        string _str = "";
        if (path.IndexOf(".doc") > 0 || path.IndexOf(".Doc") > 0)
        {
            _str = "<img src=\"images/word.gif\" style=\"border:0px; width:45px; height:44px\"/>";
        }
        else if (path.IndexOf(".pdf") > 0 || path.IndexOf(".Pdf") > 0)
        {
            _str = "<img src=\"images/pdf.gif\" style=\"border:0px; width:42px; height:42px\"/>";
        }
        else if (path.IndexOf(".zip") > 0 || path.IndexOf(".Zip") > 0 || path.IndexOf(".rar") > 0)
        {
            _str = "<img src=\"images/zip.gif\" style=\"border:0px; width:52px; height:43px\"/>";
        }
        else if (path.IndexOf(".xls") > 0 || path.IndexOf(".Xls") > 0)
        {
            _str = "<img src=\"images/excel.gif\" style=\"border:0px; width:43px; height:43px\"/>";
        }
        return _str;
    }
    #endregion
    public static string Checkpath(string path)
    {
        string _str = "";
        string Duoi = "";
        if (path.Length > 0)
        {
            Duoi = System.IO.Path.GetExtension(path);
        }
        if (Duoi == ".doc" || Duoi == ".xls" || Duoi == ".pdf" || Duoi == ".rar" || Duoi == ".zip")
        {
            _str = path;
        }
        else
        {
            _str = "";
        }
        return _str;
    }
    #region[Lay ngay dau tien cua thang]
    public static DateTime GetFirstDayOfMonth(DateTime dtDate)
    {
        DateTime dtFrom = dtDate;
        dtFrom = dtFrom.AddDays(-(dtFrom.Day - 1));
        return dtFrom;
    }
    #endregion
    #region[Lay ngay ket thuc trong thang]
    public static DateTime GetLastDayOfMonth(DateTime dtDate)
    {
        DateTime dtTo = dtDate;
        dtTo = dtTo.AddMonths(1);
        dtTo = dtTo.AddDays(-(dtTo.Day));
        return dtTo;
    }
    #endregion
    #region[GetStartOfLastWeek]
    public static DateTime GetStartOfLastWeek()
    {
        int DaysToSubtract = (int)DateTime.Now.DayOfWeek - 6;
        DateTime dt = DateTime.Now.Subtract(System.TimeSpan.FromDays(DaysToSubtract));
        return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0);
    }
    #endregion
    #region[GetStartOfCurrentWeek]
    public static DateTime GetStartOfCurrentWeek()
    {
        int DaysToSubtract = (int)DateTime.Now.DayOfWeek - 1;
        DateTime dt = DateTime.Now.Subtract(System.TimeSpan.FromDays(DaysToSubtract));
        return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0);
    }
    #endregion
    #region[Send Mail]
    public static void SendMail(string to, string bbc, string subject, string messages, string smtp, string port, string from, string user, string password)
    {
        try
        {
            System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
            mail.To = to;
            mail.Bcc = bbc;
            mail.From = from;
            mail.Subject = subject;
            mail.BodyEncoding = Encoding.GetEncoding("utf-8");
            mail.BodyFormat = MailFormat.Html;
            mail.Priority = MailPriority.High;
            mail.Body = messages;
            mail.Fields["http://schemas.microsoft.com/cdo/configuration/sendusing"] = 2;
            mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpserver"] = smtp;
            mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpserverport"] = port;
            if (port != "465")
            {
                mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpusessl"] = 0; // "true";
            }
            else
            {
                mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpusessl"] = 1; // "true";
            }
            //mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout"] = 60;
            mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"] = 1;
            mail.Fields["http://schemas.microsoft.com/cdo/configuration/sendusername"] = user;
            mail.Fields["http://schemas.microsoft.com/cdo/configuration/sendpassword"] = password;
            //SmtpMail.SmtpServer = smtp;
            SmtpMail.Send(mail);
        }
        catch (Exception ex)
        {
            throw;
        }
    }
    #endregion
    #region[Remove html]
    public static string RemoveTag(string str)
    {
        string output = str;
        output = Regex.Replace(output, "<[^>]*>", string.Empty);
        output = Regex.Replace(output, @"^\s*$\n", string.Empty, RegexOptions.Multiline);
        output = Regex.Replace(output, @"(\r|\n)+", string.Empty);
        output = Regex.Replace(output, @"&nbsp;+", " ");
        output = Regex.Replace(output, @" +", " ");
        output = output.Trim();
        return output;
    }
    #endregion
    public static void SendMail(string to, string subject, string messages)
    {
        SendMail(to, "", "", "", subject, messages);
    }
    public static void SendMail(string to, string cc, string bbc, string reply, string subject, string messages)
    {
        SendMail(to, cc, bbc, reply, subject, messages, GlobalClass.conMail_Method, GlobalClass.conMail_Port, GlobalClass.conMail_Noreply, GlobalClass.conMail_Noreply, GlobalClass.conMail_Pass);
    }
    public static void SendMail(string to, string cc, string bbc, string reply, string subject, string messages, string smtp, string port, string from, string user, string password)
    {
        try
        {
            MailMessage mail = new MailMessage();
            mail.To = to;
            if (!string.IsNullOrEmpty(cc)) mail.Cc = cc;
            if (!string.IsNullOrEmpty(bbc)) mail.Bcc = bbc;
            if (!string.IsNullOrEmpty(reply)) mail.Headers.Add("Reply-To", reply);
            mail.From = from;
            mail.Subject = subject;
            mail.BodyEncoding = Encoding.GetEncoding("utf-8");
            mail.BodyFormat = MailFormat.Html;
            mail.Body = messages;
            mail.Fields["http://schemas.microsoft.com/cdo/configuration/sendusing"] = 2;
            mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpserver"] = smtp;
            mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpserverport"] = port;
            if (port != "465")
            {
                mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpusessl"] = 0; // "true";
            }
            else
            {
                mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpusessl"] = 1; // "true";
            }
            //mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout"] = 60;
            mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"] = 1;
            mail.Fields["http://schemas.microsoft.com/cdo/configuration/sendusername"] = user;
            mail.Fields["http://schemas.microsoft.com/cdo/configuration/sendpassword"] = password;
            //SmtpMail.SmtpServer = smtp;
            SmtpMail.Send(mail);
        }
        catch { }
    }


    public static string PopulatePager(int recordCount, int currentPage)
    {
        return PopulatePager(recordCount, currentPage, 10);
    }

    public static string LoadPagination(string url, string count, int curpage, int PageSize)
    {
        //if (Request["pg"] != null) { curpage = int.Parse(Request["pg"]); }
        string temp = string.Format("<a href=\"" + url + "/page-1\" title=\"Đầu\">Đầu</a><a href=\"" + url + "/page-{0}\" title=\"Trước\"><<</a>", (curpage - 1).ToString());
        string pagination = string.Format("<div id=\"paginationtech\"><div class=\"postlink\">{0}", curpage == 1 ? "" : temp);

        int maxpage = int.Parse(count) % PageSize != 0 ? int.Parse(count) / PageSize + 1 : int.Parse(count) / PageSize;

        int startPage;

        if (maxpage <= 7) startPage = 1;
        else if (curpage <= 4) startPage = 1;
        else if ((maxpage - curpage) >= 3) startPage = curpage - 3;
        else startPage = maxpage - 6;

        if (startPage > 1) pagination += string.Format("<span class=\"aso\">...</span>");

        for (int i = startPage; i <= (maxpage <= 7 ? maxpage : startPage + 6); i++)
        {
            if (i == curpage)
            {
                pagination += string.Format("<span class=\"activeee\">{0}</span>", i);
            }
            else pagination += string.Format("<a href=\"" + url + "/page-{0}\">{1}</a>", i, i);
        }
        if ((maxpage - curpage > 3) && (maxpage > 7)) pagination += string.Format("<span class=\"aso\">...</span>");
        if (curpage < maxpage) pagination += string.Format("<a href=\"" + url + "/page-{0}\" title=\"Sau\">>></a><a href=\"" + url + "/page-{1}\" title=\"Cuối\">Cuối</a>", (curpage + 1).ToString(), maxpage);
        //pagination += "<a class=\"Counttong\"> Số trang: " + maxpage + " </a>";
        pagination += "</div></div>";
        return pagination;
    }
    public static string PopulatePager(int recordCount, int currentPage, int pageSize)
    {
        return PopulatePager(recordCount, currentPage, pageSize, "First:Last");
    }
    public static string PopulatePager(int recordCount, int currentPage, int pageSize, string Navigation)
    {
        string[] strNati = Navigation.Split(Convert.ToChar(":"));
        double dblPageCount = (double)((decimal)recordCount / (decimal)pageSize);
        int pageCount = (int)Math.Ceiling(dblPageCount);
        string strReturn = string.Empty;
        if (pageCount > 1)
        {
            strReturn += "<ul class=\"pager-nav\">";
            int PageShow = 2;
            int FromPage;
            int ToPage;
            //string pagLink = HttpContext.Current.Request.Url.AbsolutePath;
            string curLink = HttpContext.Current.Request.RawUrl;
            string pagLink = curLink.Split('?')[0];
            string view = GetParameterFromUrl(curLink, "view");
            if (view != "")
            {
                pagLink = AppendUrlParameter(pagLink, "view", view);
            }
            string sort = GetParameterFromUrl(curLink, "sort");
            if (sort != "")
            {
                pagLink = AppendUrlParameter(pagLink, "sort", sort);
            }
            FromPage = currentPage > PageShow ? currentPage - PageShow : 1;
            ToPage = (pageCount - currentPage > PageShow) ? currentPage + PageShow : pageCount;

            if (currentPage > 1) strReturn += string.Format("<li class=\"first_last\"><a href=\"{0}\" rel=\"nofollow\"><img src=\"/uploads/images/first-right.png\" alt=\"{1}\" title=\"{1}\"></a></li><li class=\"prev_next\"><a href=\"{3}\" rel=\"nofollow\"><img src=\"/uploads/images/previous-right.png\" alt=\"{2}\" title=\"{2}\"></a></li>", AppendUrlParameter(pagLink, "page", (1).ToString()), MyWeb.Global.GetLangKey("paging_first"), MyWeb.Global.GetLangKey("paging_previous"), AppendUrlParameter(pagLink, "page", (currentPage - 1).ToString()));

            if (currentPage > PageShow + 1) strReturn += "<li class=\"separator\">...</li>";

            for (int i = FromPage; i <= ToPage; i++)
            {
                string lnk = pagLink;
                strReturn += i == currentPage ? string.Format(" <li class=\"active\"><a href=\"{0}\" rel=\"nofollow\">{1}</a></li> ", AppendUrlParameter(pagLink, "page", i.ToString()), i.ToString()) : string.Format(" <li><a href=\"{0}\" rel=\"nofollow\">{1}</a></li> ", AppendUrlParameter(pagLink, "page", i.ToString()), i.ToString());
            }

            if (currentPage + (PageShow + 2) < pageCount) strReturn += string.Format("<li class=\"separator\">...</li>");

            if (currentPage < pageCount) strReturn += string.Format("<li class=\"prev_next\"><a href=\"{0}\" rel=\"nofollow\"><img src=\"/uploads/images/next-right.png\" alt=\"{2}\" title=\"{2}\"></a></li><li class=\"first_last\"><a href=\"{1}\" rel=\"nofollow\"><img src=\"/uploads/images/last-right.png\" alt=\"{3}\" title=\"{3}\"></a></li>", AppendUrlParameter(pagLink, "page", (currentPage + 1).ToString()), AppendUrlParameter(pagLink, "page", pageCount.ToString()), MyWeb.Global.GetLangKey("paging_next"), MyWeb.Global.GetLangKey("paging_last"));
            strReturn += "</ul>";
        }
        return strReturn;
    }
    public static string GetParameterFromUrl(string URL, string parameterName)
    {
        if ((URL == null) || (URL.Length == 0)) URL = HttpContext.Current.Request.Url.PathAndQuery.ToString();
        string[] urlParts = URL.Split(Convert.ToChar("?"));
        if (urlParts.Length > 1)
        {
            var parameters = urlParts[1].Split(Convert.ToChar("&"));
            for (var i = 0; (i < parameters.Length); i++)
            {
                var parameterParts = parameters[i].Split(Convert.ToChar("="));
                if (parameterParts[0] == parameterName)
                    return parameterParts[1];
            }
        }
        return "";
    }
    public static string AppendUrlParameter(string parameterName, string parameterValue)
    {
        string strCurPage = HttpContext.Current.Request.Path;
        System.Reflection.PropertyInfo isreadonly = typeof(System.Collections.Specialized.NameValueCollection).GetProperty("IsReadOnly", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
        // make collection editable
        isreadonly.SetValue(HttpContext.Current.Request.QueryString, false, null);
        // remove
        HttpContext.Current.Request.QueryString.Remove(parameterName);
        // modify
        HttpContext.Current.Request.QueryString.Set(parameterName, parameterValue);
        //Add page to variable
        string strcurrentPage = HttpContext.Current.Request.QueryString.ToString();
        //Remote paremeter
        HttpContext.Current.Request.QueryString.Remove(parameterName);
        // make collection readonly again
        isreadonly.SetValue(HttpContext.Current.Request.QueryString, true, null);
        return strCurPage + "?" + strcurrentPage;
    }

    public static String AppendUrlParameter(string URL, string parameterName, string parameterValue)
    {
        if ((URL == null) || (URL.Length == 0)) URL = HttpContext.Current.Request.Url.PathAndQuery.ToString();
        string[] urlParts = URL.Split(Convert.ToChar("?"));
        string newQueryString = "";
        try
        {

            if (urlParts.Length > 1)
            {
                var parameters = urlParts[1].Split(Convert.ToChar("&"));
                for (var i = 0; (i < parameters.Length); i++)
                {
                    var parameterParts = parameters[i].Split(Convert.ToChar("="));
                    if (parameterParts[0] != parameterName)
                    {
                        if (newQueryString == "")
                            newQueryString = "?";
                        else
                            newQueryString += "&";
                        newQueryString += parameterParts[0] + "=" + parameterParts[1];
                    }
                }
            }
            if (newQueryString == "")
                newQueryString = "?";
            else
                newQueryString += "&";
            newQueryString += parameterName + "=" + parameterValue;
        }
        catch (Exception)
        { }
        return urlParts[0] + newQueryString;
    }
    public static string LoadProductList(IEnumerable<MyWeb.tbProduct> product)
    {
        return LoadProductList(product, "");
    }
    public static string getCategory_TagName2(string ID)
    {
        string str = "";
        try
        {
            List<tbPageDATA> list = tbPageDB.tbPage_GetByID(ID);
            if (list.Count > 0)
            {
                str += list[0].pagTagName.ToString();
            }
        }
        catch { }
        return str.ToString();
    }
    public static string LoadProductList(IEnumerable<MyWeb.tbProduct> product, string view)
    {
        string _str = "";
        int productInLine = int.Parse(GlobalClass.ProductInline);
        int i = 1;
        foreach (MyWeb.tbProduct pro in product)
        {
            string currency_symbol = "";
            currency_symbol = pro.proUnitprice != null ? pro.proUnitprice : MyWeb.Global.GetLangKey("currency_symbol");
            string alt = pro.proAlt != "" && pro.proAlt != null ? pro.proAlt : pro.proName;


            _str += "<li class=\"item-list\">";
            _str += "<div class=\"frame-box\">";
            if (pro.proImage.ToString().Length > 0)
            {
                _str += "<a title=\"" + pro.proName + "\" href='" + pro.proTagName + ".html' class=\"frame-img\">";
                _str += "<img src=\"" + pro.proImage.Split(Convert.ToChar(","))[0].Replace("uploads", "uploads/_thumbs") + "\" alt=\"" + alt + "\" />";
                _str += "</a>";
            }
            else
            {
                _str += "<a title=\"" + pro.proName + "\" href='" + pro.proTagName + ".html' class=\"frame-img\">";
                _str += "<img src=\"uploads/images/no_image.jpg\" alt=\"" + alt + "\" />";
                _str += "</a>";
            }
            _str += "<div class='itemProContent'>";
            _str += "<a title=\"" + pro.proName + "\" href='" + pro.proTagName + ".html' class=\"pro-name\"><span><h3>" + pro.proName + "</h3></span></a>";

            //_str += "<div class=\"txt-code\">";
            //if (pro.proCode != "")
            //{                
            //    _str += MyWeb.Global.GetLangKey("view_code") + ": <span>" + pro.proCode + "</span>";                
            //}
            //_str += "</div>";

            if (pro.manufacturerId != null)
            {
                var m = db.tbPages.FirstOrDefault(x => x.pagId == pro.manufacturerId);
                if (m != null)
                    _str += "<figcaption class=\"pro-manu\"><b>Hãng sản xuất: </b>" + m.pagName + "</figcaption>";
            }
            if (!string.IsNullOrEmpty(pro.proCode))
                _str += "<figcaption class=\"pro-code\"><b>Mã sản phẩm: </b>" + pro.proCode + "</figcaption>";
            if (!string.IsNullOrEmpty(pro.proWarranty))
                _str += "<figcaption class=\"pro-warranty\"><b>Mô tả ngắn: </b>" + pro.proWarranty + "</figcaption>";
            if (!string.IsNullOrEmpty(pro.proContent))
                _str += "<figcaption class=\"pro-content\"><b>Nội dung tóm tắt: </b>" + pro.proContent + "</figcaption>";
            if (pro.proCount != null)
                _str += "<figcaption class=\"pro-count\"><b>Số lượng: </b>" + pro.proCount + "</figcaption>";
            if (pro.proStatus == 0)
                _str += "<figcaption class=\"pro-status-yes\"><b>Tình trạng: </b>Còn hàng</figcaption>";
            else
                _str += "<figcaption class=\"pro-status-no\"><b>Tình trạng: </b>Hết hàng</figcaption>";
            if (pro.proDate != null)
            {
                DateTime fromDate = Convert.ToDateTime(pro.proDate);
                string dateTu = common.ConvertDate(fromDate);
                _str += "<figcaption class=\"pro-date\"><b>Ngày đăng: </b>" + dateTu + "</figcaption>";
            }
            if (!string.IsNullOrEmpty(pro.proPromotions))
                _str += "<figcaption class=\"pro-content-promotion\"><b>Nội dung khuyến mãi: </b>" + pro.proPromotions + "</figcaption>";
            if (pro.proTungay != null)
            {
                DateTime fromDate = Convert.ToDateTime(pro.proTungay);
                string dateTu = common.ConvertDate(fromDate);
                _str += "<figcaption class=\"pro-promotion-from\"><b>Khuyến mãi từ: </b>" + dateTu + "</figcaption>";
            }
            if (pro.proDenngay != null)
            {
                DateTime fromDate = Convert.ToDateTime(pro.proDenngay);
                string dateTu = common.ConvertDate(fromDate);
                _str += "<figcaption class=\"pro-promotion-to\"><b>Khuyến mãi đến: </b>" + dateTu + "</figcaption>";
            }

            _str += "<div class=\"price\">";
            _str += LoadPrice(pro.proPrice, pro.proOriginalPrice, currency_symbol);
            _str += "</div>";
            _str += "<div class=\"price-new\">";
            _str += LoadSalePrice(pro.proPrice, pro.proOriginalPrice, currency_symbol);
            _str += "</div>";
            _str += "<div class=\"frame-content\">" + pro.proWarranty.ToString() + "</div>";

            if (GlobalClass.showOrderButton)
            {
                _str += "<a onclick=\"addToCartView('" + pro.proId.ToString() + "')\" title=\"" + pro.proName.ToString() + "\" class=\"link-order\">" + MyWeb.Global.GetLangKey("pro_button_order") + "</a>";
            }
            if (GlobalClass.viewProducts13 == "1")
            {
                _str += "<a class=\"link-view-short\" onclick=\"openViewPro('" + pro.proId.ToString() + "')\">" + MyWeb.Global.GetLangKey("view_fast") + "</a>";
            }
            _str += "<a href='" + pro.proTagName + ".html' title=\"" + pro.proName.ToString() + "\" class=\"link-views\">" + MyWeb.Global.GetLangKey("pro_button_detail") + "</a>";

            if (pro.proKM == 1)
            {
                if (Convert.ToInt64(pro.proOriginalPrice) > Convert.ToInt64(pro.proPrice))
                {
                    double intPercent = (double.Parse(pro.proOriginalPrice) - double.Parse(pro.proPrice)) * 100 / double.Parse(pro.proOriginalPrice);
                    _str += "<div class=\"frame-percent\"><span class=\"oil\"> - </span><span class=\"number\">" + intPercent + "</span><span class=\"percent\"> % </span></div>";
                }
            }
            _str += "</div>";
            _str += "</div>";
            _str += "</li>";

            i++;
        }
        return _str;
    }

    public static string LoadProductList(IEnumerable<MyWeb.tbProduct> product, string view, string iCat)
    {
        string _str = "";
        int productInLine = int.Parse(GlobalClass.ProductInline);
        int i = 1;
        foreach (MyWeb.tbProduct pro in product)
        {
            string currency_symbol = "";
            currency_symbol = pro.proUnitprice != null ? pro.proUnitprice : MyWeb.Global.GetLangKey("currency_symbol");
            string alt = pro.proAlt != "" && pro.proAlt != null ? pro.proAlt : pro.proName;

            _str += "<li class=\"item-list view" + iCat + "\">";
            _str += "<div class=\"frame-box\">";
            if (pro.proImage.ToString().Length > 0)
            {
                _str += "<a title=\"" + pro.proName + "\" href='" + pro.proTagName + ".html' class=\"frame-img\">";
                _str += "<img src=\"" + pro.proImage.Split(Convert.ToChar(","))[0].Replace("uploads", "uploads/_thumbs") + "\" alt=\"" + alt + "\" />";
                _str += "</a>";
            }
            else
            {
                _str += "<a title=\"" + pro.proName + "\" href='" + pro.proTagName + ".html' class=\"frame-img\">";
                _str += "<img src=\"uploads/images/no_image.jpg\" alt=\"" + alt + "\" />";
                _str += "</a>";
            }
            _str += "<div class='itemProContent'>";
            _str += "<a title=\"" + pro.proName + "\" href='" + pro.proTagName + ".html' class=\"pro-name\"><span><h3>" + pro.proName + "</h3></span></a>";

            _str += "<div class=\"txt-code\">";
            if (pro.proCode != "")
            {                
                _str += MyWeb.Global.GetLangKey("view_code") + ": <span>" + pro.proCode + "</span>";                
            }
            _str += "</div>";

            _str += "<div class=\"price\">";
            _str += LoadPrice(pro.proPrice, pro.proOriginalPrice, currency_symbol);
            _str += "</div>";
            _str += "<div class=\"price-new\">";
            _str += LoadSalePrice(pro.proPrice, pro.proOriginalPrice, currency_symbol);
            _str += "</div>";
            _str += "<div class=\"frame-content\">" + pro.proWarranty.ToString() + "</div>";
            if (GlobalClass.showOrderButton)
            {
                _str += "<a onclick=\"addToCartView('" + pro.proId.ToString() + "')\" title=\"" + pro.proName.ToString() + "\" class=\"link-order\">" + MyWeb.Global.GetLangKey("pro_button_order") + "</a>";
            }
            if (GlobalClass.viewProducts13 == "1")
            {
                _str += "<a class=\"link-view-short\" onclick=\"openViewPro('" + pro.proId.ToString() + "')\">" + MyWeb.Global.GetLangKey("view_fast") + "</a>";
            }
            _str += "<a href='" + pro.proTagName + ".html' title=\"" + pro.proName.ToString() + "\" class=\"link-views\">" + MyWeb.Global.GetLangKey("pro_button_detail") + "</a>";
            _str += "</div></div>";
            _str += "</li>";
            i++;
        }
        return _str;
    }

    public static string LoadProductListV2(IEnumerable<MyWeb.tbProduct> product)
    {
        string _str = "";
        int productInLine = int.Parse(GlobalClass.ProductInline);
        int i = 1;
        foreach (MyWeb.tbProduct pro in product)
        {
            string currency_symbol = "";
            currency_symbol = pro.proUnitprice != null ? pro.proUnitprice : MyWeb.Global.GetLangKey("currency_symbol");
            string alt = pro.proAlt != "" && pro.proAlt != null ? pro.proAlt : pro.proName;

            _str += "<li class=\"item-list\">";
            _str += "<div class=\"frame-box\">";
            _str += "<a title=\"" + pro.proName + "\" href='" + pro.proTagName + ".html' class=\"frame-img\">";
            _str += pro.proImage.ToString().Length > 0? 
                    "<img src=\"" + pro.proImage.Split(Convert.ToChar(","))[0].Replace("uploads", "uploads/_thumbs") + "\" alt=\"" + alt + "\" />":
                    "<img src=\"uploads/images/no_image.jpg\" alt=\"" + alt + "\" />";               
            _str += "</a>";

            _str += "<div class='itemProContent'>";
            _str += "<a title=\"" + pro.proName + "\" href='" + pro.proTagName + ".html' class=\"pro-name\"><span><h3>" + pro.proName + "</h3></span></a>";
            _str += "<div class=\"txt-code\">";
            if (pro.proCode != "")
            {                
                _str += MyWeb.Global.GetLangKey("view_code") + ": <span>" + pro.proCode + "</span>";                
            }
            _str += "</div>";


            _str += "<div class=\"price\">";
            _str += LoadPrice(pro.proPrice, pro.proOriginalPrice, currency_symbol);
            _str += "</div>";
            _str += "<div class=\"price-new\">";
            _str += LoadSalePrice(pro.proPrice, pro.proOriginalPrice, currency_symbol);
            _str += "</div>";
            _str += "<div class=\"frame-content\">" + pro.proWarranty.ToString() + "</div>";
            if (GlobalClass.showOrderButton)
            {
                _str += "<a onclick=\"addToCartView('" + pro.proId.ToString() + "')\" title=\"" + pro.proName.ToString() + "\" class=\"link-order\">" + MyWeb.Global.GetLangKey("pro_button_order") + "</a>";
            }
            if (GlobalClass.viewProducts13 == "1")
            {
                _str += "<a class=\"link-view-short\" onclick=\"openViewPro('" + pro.proId.ToString() + "')\">" + MyWeb.Global.GetLangKey("view_fast") + "</a>";
            }
            _str += "<a href='" + pro.proTagName + ".html' title=\"" + pro.proName.ToString() + "\" class=\"link-views\">" + MyWeb.Global.GetLangKey("pro_button_detail") + "</a>";
            _str += "</div></div>";
            _str += "</li>";
            i++;
        }
        return _str;
    }

    #region [Load price tooltip]
    private static string LoadPriceTooltip(string Price1, string Price2, string priceType)
    {
        string _str = "";
        try
        {
            string price = !String.IsNullOrEmpty(Price1) && double.Parse(Price1) > 0 ? Price1 : "0";
            price = !String.IsNullOrEmpty(Price2) && double.Parse(price) > double.Parse(Price2) && Price2 != "0" ? Price2 : price;
            if (price != "0")
                _str += string.Format("{0:N0} " + priceType, double.Parse(price));
            else
                _str += "0 " + priceType;
        }
        catch
        {
            _str += "0 " + priceType;
        }
        return _str;
    }
    #endregion
    private static string LoadSalePrice(string Price1, string Price2, string priceType)
    {
        string _str = "";
        try
        {
            string price = !String.IsNullOrEmpty(Price1) && Convert.ToInt64(Price1) > 0 ? Price1 : "0";
            price = !String.IsNullOrEmpty(Price2) && Convert.ToInt64(price) > Convert.ToInt64(Price2) && Price2 != "0" ? Price2 : price;
            if (price != "0")
                _str += "<span>" + MyWeb.Global.GetLangKey("product_price_sale") + ":</span> " + string.Format("{0:N0} " + priceType, Convert.ToInt64(price)) + "";
            else
                _str += "<span>" + MyWeb.Global.GetLangKey("product_price_sale") + ":</span> " + MyWeb.Global.GetLangKey("price_view") + "";
        }
        catch
        {
            _str += "<span>" + MyWeb.Global.GetLangKey("product_price_sale") + ":</span> " + MyWeb.Global.GetLangKey("price_view") + "";
        }
        return _str;
    }

    private static string LoadPrice(string Price1, string Price2, string priceType)
    {
        string _str = "";
        try
        {
            string price = !String.IsNullOrEmpty(Price2) && Convert.ToInt64(Price2) > 0 ? Price2 : "0";
            price = !String.IsNullOrEmpty(Price1) && double.Parse(price) < Convert.ToInt64(Price1) && price != "0" ? Price1 : price;
            if (price != "0" && price != Price1)
                _str += "<span>" + MyWeb.Global.GetLangKey("product_price") + ":</span> <span class=\"value\">" + string.Format("{0:N0} " + priceType, Convert.ToInt64(price)) + "</span>";
        }
        catch { }
        return _str;
    }
    public static string ConvertDate(DateTime Date)
    {
        return ConvertDate(Date, "dd/MM/yyyy");
    }
    public static string ConvertDate(DateTime Date, string DateFormat)
    {
        if (Date != null)
        {
            return Date.ToString(DateFormat);
        }
        else
        {
            return "";
        }
    }

    #region[formatImage]
    protected static string formatImageCT(string img, string alt)
    {
        string _img = img;
        string _imgReturn;
        if ((_img == "") || (_img == "no-image"))
        {
            _imgReturn = "images/no_image.jpg";
        }
        else
        {
            _imgReturn = "<img alt=" + alt + " src=\"" + img + "\" align = \"center\" />";
        }
        return _imgReturn;
    }
    #endregion
    #region Check String
    public static bool NullorEmpty(object String)
    {
        return String != null && String.ToString().Length > 0 ? true : false;
    }
    #endregion
    #region ThumbImage
    public static string ThumbImage(string ImagePath)
    {
        return ImagePath.ToLower().Replace("uploads/", "uploads/_thumbs/");
    }
    #endregion
    #region GetFileExtension
    public static string GetFileExtension(string String)
    {
        return String.Substring(String.LastIndexOf(".") + 1);
    }
    #endregion
    /// <summary>
    /// Create attribute and funtion for textbox
    /// </summary>
    /// <param name="textBox">Textbox to set client attribute</param>
    /// <param name="Event">Event set for textbox</param>
    /// <param name="Funtion">Value of event - funtion javascript</param>
    /// <param name="ScriptFuntion">String funtion script return</param>
    public static void CreateClientScriptAttributes(TextBox textBox, string Event, string Funtion, string ScriptFuntion)
    {
        textBox.Page.ClientScript.RegisterClientScriptBlock(textBox.GetType(), textBox.ID.ToString() + "_" + Event, ScriptFuntion);
        textBox.Attributes.Add(Event, Funtion);
    }

    /// <summary>
    /// Create attribute and funtion for textbox
    /// </summary>
    /// <param name="checkBox">CheckBox to set client attribute</param>
    /// <param name="Event">Event set for checkbox</param>
    /// <param name="Funtion">Value of event - funtion javascript</param>
    /// <param name="ScriptFuntion">String funtion script return</param>
    public static void CreateClientScriptAttributes(CheckBox checkBox, string Event, string Funtion, string ScriptFuntion)
    {
        checkBox.Page.ClientScript.RegisterClientScriptBlock(checkBox.GetType(), checkBox.ID.ToString() + "_" + Event, ScriptFuntion);
        checkBox.Attributes.Add(Event, Funtion);
    }

    public static void CreateReadOnlyAttributes(TextBox textBox)
    {
        textBox.Attributes.Add("readonly", "readonly");
    }
    public static void CreateDisabledAttributes(TextBox textBox)
    {
        textBox.Attributes.Add("disabled", "disabled");
    }

    /// <summary>
    /// Set onkeypress event for textbox,
    /// Call this funtion on load update page
    /// </summary>
    /// <param name="textBox">Textbox only input number</param>
    public static void OnlyInputNumber(TextBox textBox)
    {
        OnlyInputNumber(textBox, "");
    }
    /// <summary>
    /// Set onkeypress event for textbox,
    /// Call this funtion on load update page
    /// </summary>
    /// <param name="textBox">Textbox only input number</param>
    /// <param name="Characters">Skip Characters input</param>
    public static void OnlyInputNumber(TextBox textBox, string Characters)
    {
        CreateClientScriptAttributes(textBox, "onkeypress", "OnlyInputNumber('" + Characters + "')", GetScriptValidatorNumber());
    }
    /// <summary>
    /// Get script validator number
    /// </summary>
    /// <returns>Client javascript function check number</returns>
    protected static string GetScriptValidatorNumber()
    {
        string str;
        str = "<script type=\"text/javascript\">";
        str += @" function OnlyInputNumber(Characters){";
        str += @" var re; ";
        str += @"var ch=String.fromCharCode(event.keyCode);";
        str += @"if (event.keyCode<32)";
        str += @"{";
        str += @"return;";
        str += @"};";
        str += @"if( (event.keyCode<=57)&&(event.keyCode>=48))";
        str += @"{";
        str += @"if (!event.shiftKey)";
        str += @"{";
        str += @"return;";
        str += @"}";
        str += @"}";
        str += @"if (Characters.length >0 && ch==Characters)";
        str += @"{";
        str += @"return;";
        str += @"}";
        str += @"event.returnValue=false;";
        str += "}</script>";
        return str;
    }

    /// <summary>
    /// Get cookie name from cookie
    /// </summary>
    /// <param name="CookieName"></param>
    /// <returns></returns>
    public static string GetCookie(string CookieName)
    {
        HttpCookie cookie = HttpContext.Current.Request.Cookies[CookieName];
        return cookie != null && !string.IsNullOrEmpty(cookie.Value) ? Decode(cookie.Value) : "";
    }
    /// <summary>
    /// Set string to cookie
    /// </summary>
    /// <param name="CookieName"></param>
    /// <param name="CookieValue"></param>
    public static void SetCookie(string CookieName, string CookieValue)
    {
        SetCookie(CookieName, CookieValue, 30);
    }
    public static void SetCookie(string CookieName, string CookieValue, int Date)
    {
        HttpCookie cookie = new HttpCookie(CookieName);
        cookie.Value = Encode(CookieValue);
        cookie.Expires = DateTime.Now.AddDays(Date);
        cookie.Path = "/";
        cookie.HttpOnly = true;
        HttpContext.Current.Response.Cookies.Add(cookie);
    }
    /// <summary>
    /// Ma hoa mot chuoi
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    #region Encode
    public static string Encode(string str)
    {
        byte[] encbuff = Encoding.UTF8.GetBytes(str);
        string strtemp = Convert.ToBase64String(encbuff);
        string strtam = "";
        Int32 i = 0, len = strtemp.Length;
        for (i = 3; i <= len; i += 3)
        {
            strtam = strtam + strtemp.Substring(i - 3, 3) + RandomString(1);
        }
        strtam = strtam + strtemp.Substring(i - 3, len - (i - 3));
        return strtam;
    }
    #endregion
    /// <summary>
    /// Giai ma mot chuoi
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    #region Decode
    public static string Decode(string str)
    {
        string strtam = "";
        Int32 i = 0, len = str.Length;
        for (i = 4; i <= len; i += 4)
        {
            strtam = strtam + str.Substring(i - 4, 3);
        }
        strtam = strtam + str.Substring(i - 4, len - (i - 4));
        byte[] decbuff = Convert.FromBase64String(strtam);
        return System.Text.Encoding.UTF8.GetString(decbuff);
    }
    #endregion

    /// <summary>
    /// Tao mot chuoi ngau nghien
    /// </summary>
    /// <param name="size"></param>
    /// <returns></returns>
    #region Random String
    public static string RandomString(int size)
    {
        return RandomString(size, false);
    }
    public static string RandomString(int size, bool rndNumber)
    {
        Random rnd = new Random();
        string srds = "";
        string[] str = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
        if (rndNumber)
        {
            str = new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
        }
        for (int i = 0; i < size; i++)
        {
            srds = srds + str[rnd.Next(0, str.Length - 1)];
        }
        return srds;
    }
    #endregion

    #region Romove HTML Tag
    public static string RemoveHTMLTags(string content)
    {
        var cleaned = string.Empty;
        try
        {
            StringBuilder textOnly = new StringBuilder();
            using (var reader = XmlNodeReader.Create(new System.IO.StringReader("<xml>" + content + "</xml>")))
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Text)
                        textOnly.Append(reader.ReadContentAsString());
                }
            }
            cleaned = textOnly.ToString();
        }
        catch
        {
            //A tag is probably not closed. fallback to regex string clean.
            string textOnly = string.Empty;
            Regex tagRemove = new Regex(@"<[^>]*(>|$)");
            Regex compressSpaces = new Regex(@"[\s\r\n]+");
            textOnly = tagRemove.Replace(content, string.Empty);
            textOnly = compressSpaces.Replace(textOnly, " ");
            cleaned = textOnly;
        }

        return cleaned;
    }
    #endregion

    public static string RunScriptFile(string fileName, bool blnShowMsg = true)
    {
        if (string.IsNullOrEmpty(fileName))
        {
            //chi chay file script co duoi .txt, cac file co duoi sql va gan ma version duoc chay trong phan Upgrade Version
            return "ERROR";
        }
        string stLog = "";

        // if (blnShowMsg == true)
        // {
        // return "ERROR";
        //}

        StreamReader sr = null;
        try
        {
            sr = new StreamReader(Path.Combine(HttpContext.Current.Server.MapPath("/sql"), fileName));
        }
        catch
        {
            return "ERROR";
            //if (blnShowMsg == true)
            // {
            //file not found
            //  }
            // return "ERROR";
        }

        string line = null;
        StringBuilder sql = new StringBuilder();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            bool hasError = false;
            SqlCommand dbCmd = new SqlCommand();

            dbCmd.CommandType = CommandType.Text;
            dbCmd.Connection = dbConn;
            dbConn.Open();
            do
            {
                if (sr == null)
                {
                    break; // TODO: might not be correct. Was : Exit Do
                }
                line = sr.ReadLine();
                if (line == null)
                {
                    break; // TODO: might not be correct. Was : Exit Do
                }
                //line = line.Trim() & " "
                if (line.Trim().ToUpper() == "GO" & sql.Length > 0)
                {
                    dbCmd.CommandText = sql.ToString();
                    try
                    {
                        stLog = stLog + dbCmd.CommandText + Environment.NewLine;
                        dbCmd.ExecuteNonQuery();
                    }
                    catch
                    {
                        //txtLog.AppendText("Error: " & ex.Message & Environment.NewLine)

                        //hasError = true;
                    }
                    sql.Length = 0;
                }
                else
                {
                    //if (line.Trim().Substring(0,2) != "--")
                    // {
                    sql.AppendLine(line.Trim());
                    //}

                }
            } while (!(line == null));
            //cho lenh cuoi cung
            if (sql.Length > 0)
            {
                try
                {
                    dbCmd.CommandText = sql.ToString();
                    stLog = stLog + dbCmd.CommandText + Environment.NewLine;

                    dbCmd.ExecuteNonQuery();
                }
                catch
                {
                    //hasError = true;
                }
                //sql.Length = 0;
            }
            dbConn.Close();
            if ((sr != null))
            {
                sr.Close();
            }

        }
        return stLog;
    }
    public static string NganXau(string input, int limit) // Only use it here? (Jwendl)
    {
        string output = input;

        // Check if the string is longer than the allowed amount
        // otherwise do nothing
        if (output.Length > limit && limit > 0)
        {

            // cut the string down to the maximum number of characters
            output = output.Substring(0, limit);
            if (input.Substring(output.Length, 1) != " ")
            {
                int LastSpace = output.LastIndexOf(" ");

                // if we found a space then, cut back to that space
                if (LastSpace != -1)
                {
                    output = output.Substring(0, LastSpace);
                }
            }
            // Finally, add the "..."
            output += "...";
        }
        return output;
    }

    public static string killChars(string str)
    {
        var badChars = new string[] {
            "script",
            "language",
            "javascript",
             "select",
             "drop",
             "--",
             "insert",
             "update",
             "delete",
             "xp_",
             "or",
             "tables",
             "colums",
             "schema.tables",
             "schema.colums",
             "where",
             "null",
             "CR",
             "LF",
             "/\\",
             "/\\;",
             "1=1--'",
             "1=1--",
             "'"
         };
        foreach (var gt in badChars)
        {
            if (str.Contains(gt))
            {
                str = str.Replace(gt, "");
            }
        }
        return str;
    }

    public class ConvertVN
    {
        // Phương thức Convert một chuỗi ký tự Có dấu sang Không dấu
        public static string Convert(string chucodau)
        {
            const string FindText = "áàảãạâấầẩẫậăắằẳẵặđéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵÁÀẢÃẠÂẤẦẨẪẬĂẮẰẲẴẶĐÉÈẺẼẸÊẾỀỂỄỆÍÌỈĨỊÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢÚÙỦŨỤƯỨỪỬỮỰÝỲỶỸỴ";
            const string ReplText = "aaaaaaaaaaaaaaaaadeeeeeeeeeeeiiiiiooooooooooooooooouuuuuuuuuuuyyyyyAAAAAAAAAAAAAAAAADEEEEEEEEEEEIIIIIOOOOOOOOOOOOOOOOOUUUUUUUUUUUYYYYY";
            int index = -1;
            char[] arrChar = FindText.ToCharArray();
            while ((index = chucodau.IndexOfAny(arrChar)) != -1)
            {
                int index2 = FindText.IndexOf(chucodau[index]);
                chucodau = chucodau.Replace(chucodau[index], ReplText[index2]);
            }
            return chucodau;
        }
    }

    public static string Exec(string keyWord)
    {
        string[] arrWord = keyWord.Split(' ');
        StringBuilder str = new StringBuilder("%");
        for (int i = 0; i < arrWord.Length; i++)
        {
            str.Append(arrWord[i] + "%");
        }
        return str.ToString();
    }
}