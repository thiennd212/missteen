﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class tbPageDB
{
    #region[tbPage_Add]
    public static bool tbPage_Add(tbPageDATA _tbPageDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbPage_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@pagName", _tbPageDATA.pagName));
                dbCmd.Parameters.Add(new SqlParameter("@pagImage", _tbPageDATA.pagImage));
                dbCmd.Parameters.Add(new SqlParameter("@pagDetail", _tbPageDATA.pagDetail));
                dbCmd.Parameters.Add(new SqlParameter("@paglevel", _tbPageDATA.paglevel));
                dbCmd.Parameters.Add(new SqlParameter("@pagTitle", _tbPageDATA.pagTitle));
                dbCmd.Parameters.Add(new SqlParameter("@pagDescription", _tbPageDATA.pagDescription));
                dbCmd.Parameters.Add(new SqlParameter("@pagKeyword", _tbPageDATA.pagKeyword));
                dbCmd.Parameters.Add(new SqlParameter("@pagType", _tbPageDATA.pagType));
                dbCmd.Parameters.Add(new SqlParameter("@pagLink", _tbPageDATA.pagLink));
                dbCmd.Parameters.Add(new SqlParameter("@pagTarget", _tbPageDATA.pagTarget));
                dbCmd.Parameters.Add(new SqlParameter("@pagPosition", _tbPageDATA.pagPosition));
                dbCmd.Parameters.Add(new SqlParameter("@pagOrd", _tbPageDATA.pagOrd));
                dbCmd.Parameters.Add(new SqlParameter("@pagActive", _tbPageDATA.pagActive));
                dbCmd.Parameters.Add(new SqlParameter("@catId", _tbPageDATA.catId));
                dbCmd.Parameters.Add(new SqlParameter("@grnId", _tbPageDATA.grnId));
                dbCmd.Parameters.Add(new SqlParameter("@pagLang", _tbPageDATA.pagLang));
                dbCmd.Parameters.Add(new SqlParameter("@pagTagName", _tbPageDATA.pagTagName));
                dbCmd.Parameters.Add(new SqlParameter("@check1", _tbPageDATA.check1));
                dbCmd.Parameters.Add(new SqlParameter("@check2", _tbPageDATA.check2));
                dbCmd.Parameters.Add(new SqlParameter("@check3", _tbPageDATA.check3));
                dbCmd.Parameters.Add(new SqlParameter("@check4", _tbPageDATA.check4));
                dbCmd.Parameters.Add(new SqlParameter("@check5", _tbPageDATA.check5));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbPage_Update]
    public static bool tbPage_Update(tbPageDATA _tbPageDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbPage_Update", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@pagId", _tbPageDATA.pagId));
                dbCmd.Parameters.Add(new SqlParameter("@pagName", _tbPageDATA.pagName));
                dbCmd.Parameters.Add(new SqlParameter("@pagImage", _tbPageDATA.pagImage));
                dbCmd.Parameters.Add(new SqlParameter("@pagDetail", _tbPageDATA.pagDetail));
                dbCmd.Parameters.Add(new SqlParameter("@paglevel", _tbPageDATA.paglevel));
                dbCmd.Parameters.Add(new SqlParameter("@pagTitle", _tbPageDATA.pagTitle));
                dbCmd.Parameters.Add(new SqlParameter("@pagDescription", _tbPageDATA.pagDescription));
                dbCmd.Parameters.Add(new SqlParameter("@pagKeyword", _tbPageDATA.pagKeyword));
                dbCmd.Parameters.Add(new SqlParameter("@pagType", _tbPageDATA.pagType));
                dbCmd.Parameters.Add(new SqlParameter("@pagLink", _tbPageDATA.pagLink));
                dbCmd.Parameters.Add(new SqlParameter("@pagTarget", _tbPageDATA.pagTarget));
                dbCmd.Parameters.Add(new SqlParameter("@pagPosition", _tbPageDATA.pagPosition));
                dbCmd.Parameters.Add(new SqlParameter("@pagOrd", _tbPageDATA.pagOrd));
                dbCmd.Parameters.Add(new SqlParameter("@pagActive", _tbPageDATA.pagActive));
                dbCmd.Parameters.Add(new SqlParameter("@catId", _tbPageDATA.catId));
                dbCmd.Parameters.Add(new SqlParameter("@grnId", _tbPageDATA.grnId));
                dbCmd.Parameters.Add(new SqlParameter("@pagLang", _tbPageDATA.pagLang));
                dbCmd.Parameters.Add(new SqlParameter("@pagTagName", _tbPageDATA.pagTagName));
                dbCmd.Parameters.Add(new SqlParameter("@check1", _tbPageDATA.check1));
                dbCmd.Parameters.Add(new SqlParameter("@check2", _tbPageDATA.check2));
                dbCmd.Parameters.Add(new SqlParameter("@check3", _tbPageDATA.check3));
                dbCmd.Parameters.Add(new SqlParameter("@check4", _tbPageDATA.check4));
                dbCmd.Parameters.Add(new SqlParameter("@check5", _tbPageDATA.check5));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbPage_Update_Link]
    public static bool tbPage_Update_Link(tbPageDATA _tbPageDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbPage_Update_Link", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@pagId", _tbPageDATA.pagId));
                dbCmd.Parameters.Add(new SqlParameter("@pagLink", _tbPageDATA.pagLink));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbPage_Update_Ord]
    public static bool tbPage_Update_Order(string pagId, string pagOrd)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("Update tbPage set pagOrd = '" + pagOrd + "' where pagId = '" + pagId + "'", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbPage_UpdatepagImage]
    public static bool tbPage_UpdatepagImage(tbPageDATA _tbPageDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbPage_UpdatepagImage", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@pagId", _tbPageDATA.pagId));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbPage_Delete]
    public static bool tbPage_Delete(string spagId)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbPage_Delete", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@pagId", spagId));
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbPage_GetByAll]
    public static List<tbPageDATA> tbPage_GetByAll(string spagLang)
    {
        List<tbPageDATA> list = new List<tbPageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbPage_GetByAll", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@pagLang", spagLang));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbPageDATA objtbPageDATA = new tbPageDATA(
                        reader["pagId"].ToString(),
                        reader["pagName"].ToString(),
                        reader["pagImage"].ToString(),
                        reader["pagDetail"].ToString(),
                        reader["paglevel"].ToString(),
                        reader["pagTitle"].ToString(),
                        reader["pagDescription"].ToString(),
                        reader["pagKeyword"].ToString(),
                        reader["pagType"].ToString(),
                        reader["pagLink"].ToString(),
                        reader["pagTarget"].ToString(),
                        reader["pagPosition"].ToString(),
                        reader["pagOrd"].ToString(),
                        reader["pagActive"].ToString(),
                        reader["catId"].ToString(),
                        reader["grnId"].ToString(),
                        reader["pagLang"].ToString(),
                        reader["pagTagName"].ToString(),
                        reader["check1"].ToString(),
                        reader["check2"].ToString(),
                        reader["check3"].ToString(),
                        reader["check4"].ToString(),
                        reader["check5"].ToString());
                        list.Add(objtbPageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbPage_GetByID]
    public static List<tbPageDATA> tbPage_GetByID(string spagId)
    {
        List<tbPageDATA> list = new List<tbPageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbPage_GetByID", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@pagId", spagId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbPageDATA objtbPageDATA = new tbPageDATA(
                        reader["pagId"].ToString(),
                        reader["pagName"].ToString(),
                        reader["pagImage"].ToString(),
                        reader["pagDetail"].ToString(),
                        reader["paglevel"].ToString(),
                        reader["pagTitle"].ToString(),
                        reader["pagDescription"].ToString(),
                        reader["pagKeyword"].ToString(),
                        reader["pagType"].ToString(),
                        reader["pagLink"].ToString(),
                        reader["pagTarget"].ToString(),
                        reader["pagPosition"].ToString(),
                        reader["pagOrd"].ToString(),
                        reader["pagActive"].ToString(),
                        reader["catId"].ToString(),
                        reader["grnId"].ToString(),
                        reader["pagLang"].ToString(),
                        reader["pagTagName"].ToString(),
                        reader["check1"].ToString(),
                        reader["check2"].ToString(),
                        reader["check3"].ToString(),
                        reader["check4"].ToString(),
                        reader["check5"].ToString());
                        list.Add(objtbPageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    public static List<tbPageDATA> tbPage_GetByTagName(string spagTag)
    {
        List<tbPageDATA> list = new List<tbPageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbPage_GetByTagName", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@pagTagName", spagTag));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbPageDATA objtbPageDATA = new tbPageDATA(
                        reader["pagId"].ToString(),
                        reader["pagName"].ToString(),
                        reader["pagImage"].ToString(),
                        reader["pagDetail"].ToString(),
                        reader["paglevel"].ToString(),
                        reader["pagTitle"].ToString(),
                        reader["pagDescription"].ToString(),
                        reader["pagKeyword"].ToString(),
                        reader["pagType"].ToString(),
                        reader["pagLink"].ToString(),
                        reader["pagTarget"].ToString(),
                        reader["pagPosition"].ToString(),
                        reader["pagOrd"].ToString(),
                        reader["pagActive"].ToString(),
                        reader["catId"].ToString(),
                        reader["grnId"].ToString(),
                        reader["pagLang"].ToString(),
                        reader["pagTagName"].ToString(),
                        reader["check1"].ToString(),
                        reader["check2"].ToString(),
                        reader["check3"].ToString(),
                        reader["check4"].ToString(),
                        reader["check5"].ToString());
                        list.Add(objtbPageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    public static List<tbPageDATA> tbPage_GetByPageLink(string pagLink)
    {
        List<tbPageDATA> list = new List<tbPageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbPage_GetByPageLink", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@pagLink", pagLink));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbPageDATA objtbPageDATA = new tbPageDATA(
                        reader["pagId"].ToString(),
                        reader["pagName"].ToString(),
                        reader["pagImage"].ToString(),
                        reader["pagDetail"].ToString(),
                        reader["paglevel"].ToString(),
                        reader["pagTitle"].ToString(),
                        reader["pagDescription"].ToString(),
                        reader["pagKeyword"].ToString(),
                        reader["pagType"].ToString(),
                        reader["pagLink"].ToString(),
                        reader["pagTarget"].ToString(),
                        reader["pagPosition"].ToString(),
                        reader["pagOrd"].ToString(),
                        reader["pagActive"].ToString(),
                        reader["catId"].ToString(),
                        reader["grnId"].ToString(),
                        reader["pagLang"].ToString(),
                        reader["pagTagName"].ToString(),
                        reader["check1"].ToString(),
                        reader["check2"].ToString(),
                        reader["check3"].ToString(),
                        reader["check4"].ToString(),
                        reader["check5"].ToString());
                        list.Add(objtbPageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[sp_tbPage_GetByLevel]
    public static List<tbPageDATA> tbPage_GetByLevel(string pagLevel)
    {
        List<tbPageDATA> list = new List<tbPageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbPage_GetByLevel", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@pagLevel", pagLevel));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbPageDATA objtbPageDATA = new tbPageDATA(
                        reader["pagId"].ToString(),
                        reader["pagName"].ToString(),
                        reader["pagImage"].ToString(),
                        reader["pagDetail"].ToString(),
                        reader["paglevel"].ToString(),
                        reader["pagTitle"].ToString(),
                        reader["pagDescription"].ToString(),
                        reader["pagKeyword"].ToString(),
                        reader["pagType"].ToString(),
                        reader["pagLink"].ToString(),
                        reader["pagTarget"].ToString(),
                        reader["pagPosition"].ToString(),
                        reader["pagOrd"].ToString(),
                        reader["pagActive"].ToString(),
                        reader["catId"].ToString(),
                        reader["grnId"].ToString(),
                        reader["pagLang"].ToString(),
                        reader["pagTagName"].ToString(),
                        reader["check1"].ToString(),
                        reader["check2"].ToString(),
                        reader["check3"].ToString(),
                        reader["check4"].ToString(),
                        reader["check5"].ToString());
                        list.Add(objtbPageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbtbPage_GetByMainLui1Cap]
    public static List<tbPageDATA> tbtbPage_GetByMainLui1Cap(string spagId)
    {
        List<tbPageDATA> list = new List<tbPageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbtbPage_GetByMainLui1Cap", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@pagId", spagId));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbPageDATA objtbPageDATA = new tbPageDATA(
                        reader["pagId"].ToString(),
                        reader["pagName"].ToString(),
                        reader["pagImage"].ToString(),
                        reader["pagDetail"].ToString(),
                        reader["paglevel"].ToString(),
                        reader["pagTitle"].ToString(),
                        reader["pagDescription"].ToString(),
                        reader["pagKeyword"].ToString(),
                        reader["pagType"].ToString(),
                        reader["pagLink"].ToString(),
                        reader["pagTarget"].ToString(),
                        reader["pagPosition"].ToString(),
                        reader["pagOrd"].ToString(),
                        reader["pagActive"].ToString(),
                        reader["catId"].ToString(),
                        reader["grnId"].ToString(),
                        reader["pagLang"].ToString(),
                        reader["pagTagName"].ToString(),
                        reader["check1"].ToString(),
                        reader["check2"].ToString(),
                        reader["check3"].ToString(),
                        reader["check4"].ToString(),
                        reader["check5"].ToString());
                        list.Add(objtbPageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbPage_GetByMax]
    public static List<tbPageDATA> tbPage_GetByMax()
    {
        List<tbPageDATA> list = new List<tbPageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbPage_GetByMax", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbPageDATA objtbPageDATA = new tbPageDATA(
                        reader["pagId"].ToString(),
                        reader["pagName"].ToString(),
                        reader["pagImage"].ToString(),
                        reader["pagDetail"].ToString(),
                        reader["paglevel"].ToString(),
                        reader["pagTitle"].ToString(),
                        reader["pagDescription"].ToString(),
                        reader["pagKeyword"].ToString(),
                        reader["pagType"].ToString(),
                        reader["pagLink"].ToString(),
                        reader["pagTarget"].ToString(),
                        reader["pagPosition"].ToString(),
                        reader["pagOrd"].ToString(),
                        reader["pagActive"].ToString(),
                        reader["catId"].ToString(),
                        reader["grnId"].ToString(),
                        reader["pagLang"].ToString(),
                        reader["pagTagName"].ToString(),
                        reader["check1"].ToString(),
                        reader["check2"].ToString(),
                        reader["check3"].ToString(),
                        reader["check4"].ToString(),
                        reader["check5"].ToString());
                        list.Add(objtbPageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbPage_GetBypagPosition]
    public static List<tbPageDATA> tbPage_GetBypagPosition(string spagPosition, string spagLang)
    {
        List<tbPageDATA> list = new List<tbPageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbPage_GetBypagPosition", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@pagPosition", spagPosition));
                dbCmd.Parameters.Add(new SqlParameter("@pagLang", spagLang));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbPageDATA objtbPageDATA = new tbPageDATA(
                        reader["pagId"].ToString(),
                        reader["pagName"].ToString(),
                        reader["pagImage"].ToString(),
                        reader["pagDetail"].ToString(),
                        reader["paglevel"].ToString(),
                        reader["pagTitle"].ToString(),
                        reader["pagDescription"].ToString(),
                        reader["pagKeyword"].ToString(),
                        reader["pagType"].ToString(),
                        reader["pagLink"].ToString(),
                        reader["pagTarget"].ToString(),
                        reader["pagPosition"].ToString(),
                        reader["pagOrd"].ToString(),
                        reader["pagActive"].ToString(),
                        reader["catId"].ToString(),
                        reader["grnId"].ToString(),
                        reader["pagLang"].ToString(),
                        reader["pagTagName"].ToString(),
                        reader["check1"].ToString(),
                        reader["check2"].ToString(),
                        reader["check3"].ToString(),
                        reader["check4"].ToString(),
                        reader["check5"].ToString());
                        list.Add(objtbPageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[sp_tbPage_GetBypagPositionOrder]
    public static List<tbPageDATA> sp_tbPage_GetBypagPositionOrder(string spagPosition, string spagLang)
    {
        List<tbPageDATA> list = new List<tbPageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbPage_GetBypagPositionOrder", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@pagPosition", spagPosition));
                dbCmd.Parameters.Add(new SqlParameter("@pagLang", spagLang));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbPageDATA objtbPageDATA = new tbPageDATA(
                        reader["pagId"].ToString(),
                        reader["pagName"].ToString(),
                        reader["pagImage"].ToString(),
                        reader["pagDetail"].ToString(),
                        reader["paglevel"].ToString(),
                        reader["pagTitle"].ToString(),
                        reader["pagDescription"].ToString(),
                        reader["pagKeyword"].ToString(),
                        reader["pagType"].ToString(),
                        reader["pagLink"].ToString(),
                        reader["pagTarget"].ToString(),
                        reader["pagPosition"].ToString(),
                        reader["pagOrd"].ToString(),
                        reader["pagActive"].ToString(),
                        reader["catId"].ToString(),
                        reader["grnId"].ToString(),
                        reader["pagLang"].ToString(),
                        reader["pagTagName"].ToString(),
                        reader["check1"].ToString(),
                        reader["check2"].ToString(),
                        reader["check3"].ToString(),
                        reader["check4"].ToString(),
                        reader["check5"].ToString());
                        list.Add(objtbPageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbPage_GetBypagPosition5]
    public static List<tbPageDATA> tbPage_GetBypagPosition5(string spagPosition, string spagLang)
    {
        List<tbPageDATA> list = new List<tbPageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbPage_GetBypagPosition5", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@pagPosition", spagPosition));
                dbCmd.Parameters.Add(new SqlParameter("@pagLang", spagLang));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbPageDATA objtbPageDATA = new tbPageDATA(
                        reader["pagId"].ToString(),
                        reader["pagName"].ToString(),
                        reader["pagImage"].ToString(),
                        reader["pagDetail"].ToString(),
                        reader["paglevel"].ToString(),
                        reader["pagTitle"].ToString(),
                        reader["pagDescription"].ToString(),
                        reader["pagKeyword"].ToString(),
                        reader["pagType"].ToString(),
                        reader["pagLink"].ToString(),
                        reader["pagTarget"].ToString(),
                        reader["pagPosition"].ToString(),
                        reader["pagOrd"].ToString(),
                        reader["pagActive"].ToString(),
                        reader["catId"].ToString(),
                        reader["grnId"].ToString(),
                        reader["pagLang"].ToString(),
                        reader["pagTagName"].ToString(),
                        reader["check1"].ToString(),
                        reader["check2"].ToString(),
                        reader["check3"].ToString(),
                        reader["check4"].ToString(),
                        reader["check5"].ToString());
                        list.Add(objtbPageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbPage_GetBypagPositionSub]
    public static List<tbPageDATA> tbPage_GetBypagPositionSub(string spagLevel, string spagPosition, string spagLang)
    {
        List<tbPageDATA> list = new List<tbPageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbPage_GetBypagPositionSub", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@pagLevel", spagLevel));
                dbCmd.Parameters.Add(new SqlParameter("@pagPosition", spagPosition));
                dbCmd.Parameters.Add(new SqlParameter("@pagLang", spagLang));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbPageDATA objtbPageDATA = new tbPageDATA(
                        reader["pagId"].ToString(),
                        reader["pagName"].ToString(),
                        reader["pagImage"].ToString(),
                        reader["pagDetail"].ToString(),
                        reader["paglevel"].ToString(),
                        reader["pagTitle"].ToString(),
                        reader["pagDescription"].ToString(),
                        reader["pagKeyword"].ToString(),
                        reader["pagType"].ToString(),
                        reader["pagLink"].ToString(),
                        reader["pagTarget"].ToString(),
                        reader["pagPosition"].ToString(),
                        reader["pagOrd"].ToString(),
                        reader["pagActive"].ToString(),
                        reader["catId"].ToString(),
                        reader["grnId"].ToString(),
                        reader["pagLang"].ToString(),
                        reader["pagTagName"].ToString(),
                        reader["check1"].ToString(),
                        reader["check2"].ToString(),
                        reader["check3"].ToString(),
                        reader["check4"].ToString(),
                        reader["check5"].ToString());
                        list.Add(objtbPageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbPage_GetBypagSub]
    public static List<tbPageDATA> tbPage_GetBypagSub(string spagLevel, string spagLang)
    {
        List<tbPageDATA> list = new List<tbPageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbPage_GetBypagSub", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@pagLevel", spagLevel));
                dbCmd.Parameters.Add(new SqlParameter("@pagLang", spagLang));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbPageDATA objtbPageDATA = new tbPageDATA(
                        reader["pagId"].ToString(),
                        reader["pagName"].ToString(),
                        reader["pagImage"].ToString(),
                        reader["pagDetail"].ToString(),
                        reader["paglevel"].ToString(),
                        reader["pagTitle"].ToString(),
                        reader["pagDescription"].ToString(),
                        reader["pagKeyword"].ToString(),
                        reader["pagType"].ToString(),
                        reader["pagLink"].ToString(),
                        reader["pagTarget"].ToString(),
                        reader["pagPosition"].ToString(),
                        reader["pagOrd"].ToString(),
                        reader["pagActive"].ToString(),
                        reader["catId"].ToString(),
                        reader["grnId"].ToString(),
                        reader["pagLang"].ToString(),
                        reader["pagTagName"].ToString(),
                        reader["check1"].ToString(),
                        reader["check2"].ToString(),
                        reader["check3"].ToString(),
                        reader["check4"].ToString(),
                        reader["check5"].ToString());
                        list.Add(objtbPageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[sp_tbPage_ExitstByLevel]
    public static List<tbPageDATA> tbPage_ExitstByLevel(string pagLevel)
    {
        int n = pagLevel.Length;
        List<tbPageDATA> list = new List<tbPageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("select * from tbPage where paglevel <> '" + pagLevel + "' and substring(paglevel, 1, " + n.ToString() + ") = '" + pagLevel + "'", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbPageDATA objtbPageDATA = new tbPageDATA(
                        reader["pagId"].ToString(),
                        reader["pagName"].ToString(),
                        reader["pagImage"].ToString(),
                        reader["pagDetail"].ToString(),
                        reader["paglevel"].ToString(),
                        reader["pagTitle"].ToString(),
                        reader["pagDescription"].ToString(),
                        reader["pagKeyword"].ToString(),
                        reader["pagType"].ToString(),
                        reader["pagLink"].ToString(),
                        reader["pagTarget"].ToString(),
                        reader["pagPosition"].ToString(),
                        reader["pagOrd"].ToString(),
                        reader["pagActive"].ToString(),
                        reader["catId"].ToString(),
                        reader["grnId"].ToString(),
                        reader["pagLang"].ToString(),
                        reader["pagTagName"].ToString(),
                        reader["check1"].ToString(),
                        reader["check2"].ToString(),
                        reader["check3"].ToString(),
                        reader["check4"].ToString(),
                        reader["check5"].ToString());
                        list.Add(objtbPageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion

    public static List<tbPageDATA> tbPage_GetByTop(string Top, string Where, string Order)
    {
        List<tbPageDATA> list = new List<tbPageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbPage_GetByTop", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@Top", Top));
                dbCmd.Parameters.Add(new SqlParameter("@Where", Where));
                dbCmd.Parameters.Add(new SqlParameter("@Order", Order));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbPageDATA objtbPageDATA = new tbPageDATA(
                        reader["pagId"].ToString(),
                        reader["pagName"].ToString(),
                        reader["pagImage"].ToString(),
                        reader["pagDetail"].ToString(),
                        reader["paglevel"].ToString(),
                        reader["pagTitle"].ToString(),
                        reader["pagDescription"].ToString(),
                        reader["pagKeyword"].ToString(),
                        reader["pagType"].ToString(),
                        reader["pagLink"].ToString(),
                        reader["pagTarget"].ToString(),
                        reader["pagPosition"].ToString(),
                        reader["pagOrd"].ToString(),
                        reader["pagActive"].ToString(),
                        reader["catId"].ToString(),
                        reader["grnId"].ToString(),
                        reader["pagLang"].ToString(),
                        reader["pagTagName"].ToString(),
                        reader["check1"].ToString(),
                        reader["check2"].ToString(),
                        reader["check3"].ToString(),
                        reader["check4"].ToString(),
                        reader["check5"].ToString());
                        list.Add(objtbPageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    public static List<tbPageDATA> tbPage_GetByArrCatID(string arrCatid)
    {
        List<tbPageDATA> list = new List<tbPageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("select * from tbPage where pagId in (" + arrCatid + ") ", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbPageDATA objtbPageDATA = new tbPageDATA(
                        reader["pagId"].ToString(),
                        reader["pagName"].ToString(),
                        reader["pagImage"].ToString(),
                        reader["pagDetail"].ToString(),
                        reader["paglevel"].ToString(),
                        reader["pagTitle"].ToString(),
                        reader["pagDescription"].ToString(),
                        reader["pagKeyword"].ToString(),
                        reader["pagType"].ToString(),
                        reader["pagLink"].ToString(),
                        reader["pagTarget"].ToString(),
                        reader["pagPosition"].ToString(),
                        reader["pagOrd"].ToString(),
                        reader["pagActive"].ToString(),
                        reader["catId"].ToString(),
                        reader["grnId"].ToString(),
                        reader["pagLang"].ToString(),
                        reader["pagTagName"].ToString(),
                        reader["check1"].ToString(),
                        reader["check2"].ToString(),
                        reader["check3"].ToString(),
                        reader["check4"].ToString(),
                        reader["check5"].ToString());
                        list.Add(objtbPageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    public static List<tbPageDATA> Like_Name_tbPage(string Name)
    {
        List<tbPageDATA> list = new List<tbPageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("	select * from tbPage where dbo.FCCH_ReTextUnicode(pagName)  like N'%" + Name + "%' and pagType='100'", dbConn))
            {
                dbCmd.CommandType = CommandType.Text;
                dbConn.Open();
                // dbCmd.Parameters.Add(new SqlParameter("@Name", Name));                
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbPageDATA objtbPageDATA = new tbPageDATA(
                        reader["pagId"].ToString(),
                        reader["pagName"].ToString(),
                        reader["pagImage"].ToString(),
                        reader["pagDetail"].ToString(),
                        reader["paglevel"].ToString(),
                        reader["pagTitle"].ToString(),
                        reader["pagDescription"].ToString(),
                        reader["pagKeyword"].ToString(),
                        reader["pagType"].ToString(),
                        reader["pagLink"].ToString(),
                        reader["pagTarget"].ToString(),
                        reader["pagPosition"].ToString(),
                        reader["pagOrd"].ToString(),
                        reader["pagActive"].ToString(),
                        reader["catId"].ToString(),
                        reader["grnId"].ToString(),
                        reader["pagLang"].ToString(),
                        reader["pagTagName"].ToString(),
                        reader["check1"].ToString(),
                        reader["check2"].ToString(),
                        reader["check3"].ToString(),
                        reader["check4"].ToString(),
                        reader["check5"].ToString());
                        list.Add(objtbPageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
}