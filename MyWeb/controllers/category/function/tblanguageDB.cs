﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class tblanguageDB
{
    #region[tblanguage_Add]
    public static bool tblanguage_Add(tblanguageDATA _tblanguageDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tblanguage_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@lanid", _tblanguageDATA.lanid));
                dbCmd.Parameters.Add(new SqlParameter("@lanname", _tblanguageDATA.lanname));
                dbCmd.Parameters.Add(new SqlParameter("@lanfolder", _tblanguageDATA.lanfolder));
                dbCmd.Parameters.Add(new SqlParameter("@landefault", _tblanguageDATA.landefault));
                dbCmd.Parameters.Add(new SqlParameter("@lanimage", _tblanguageDATA.lanimage));
                dbCmd.Parameters.Add(new SqlParameter("@lanactive", _tblanguageDATA.lanactive));
                dbCmd.Parameters.Add(new SqlParameter("@lanOrder", _tblanguageDATA.lanOrder));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tblanguage_Update]
    public static bool tblanguage_Update(tblanguageDATA _tblanguageDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tblanguage_Update", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@lanid", _tblanguageDATA.lanid));
                dbCmd.Parameters.Add(new SqlParameter("@lanname", _tblanguageDATA.lanname));
                dbCmd.Parameters.Add(new SqlParameter("@lanfolder", _tblanguageDATA.lanfolder));
                dbCmd.Parameters.Add(new SqlParameter("@landefault", _tblanguageDATA.landefault));
                dbCmd.Parameters.Add(new SqlParameter("@lanimage", _tblanguageDATA.lanimage));
                dbCmd.Parameters.Add(new SqlParameter("@lanactive", _tblanguageDATA.lanactive));
                dbCmd.Parameters.Add(new SqlParameter("@lanOrder", _tblanguageDATA.lanOrder));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tblanguage_Delete]
    public static bool tblanguage_Delete(string slanid)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tblanguage_Delete", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@lanid", slanid));
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tblanguage_GetByAll]
    public static List<tblanguageDATA> tblanguage_GetByAll()
    {
        List<tblanguageDATA> list = new List<tblanguageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tblanguage_GetByAll", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tblanguageDATA objtblanguageDATA = new tblanguageDATA(
                        reader["lanid"].ToString(),
                        reader["lanname"].ToString(),
                        reader["lanfolder"].ToString(),
                        reader["landefault"].ToString(),
                        reader["lanimage"].ToString(),
                        reader["lanactive"].ToString(),
                        reader["lanOrder"].ToString());
                        list.Add(objtblanguageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tblanguage_GetByID]
    public static List<tblanguageDATA> tblanguage_GetByID(string slanid)
    {
        List<tblanguageDATA> list = new List<tblanguageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tblanguage_GetByID", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@lanid", slanid));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tblanguageDATA objtblanguageDATA = new tblanguageDATA(
                        reader["lanid"].ToString(),
                        reader["lanname"].ToString(),
                        reader["lanfolder"].ToString(),
                        reader["landefault"].ToString(),
                        reader["lanimage"].ToString(),
                        reader["lanactive"].ToString(),
                        reader["lanOrder"].ToString());
                        list.Add(objtblanguageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tblanguage_GetBylandefault]
    public static List<tblanguageDATA> tblanguage_GetBylandefault()
    {
        List<tblanguageDATA> list = new List<tblanguageDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tblanguage_GetBylandefault", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tblanguageDATA objtblanguageDATA = new tblanguageDATA(
                        reader["lanid"].ToString(),
                        reader["lanname"].ToString(),
                        reader["lanfolder"].ToString(),
                        reader["landefault"].ToString(),
                        reader["lanimage"].ToString(),
                        reader["lanactive"].ToString(),
                        reader["lanOrder"].ToString());
                        list.Add(objtblanguageDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
}