﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class tblanguageDATA
{
    #region[Declare variables]
    private string _lanid;
    private string _lanname;
    private string _lanfolder;
    private string _landefault;
    private string _lanimage;
    private string _lanactive;
    private string _lanOrder;
    #endregion
    #region[Function]
    public tblanguageDATA() { }
    public tblanguageDATA(string lanid_, string lanname_, string lanfolder_, string landefault_, string lanimage_, string lanactive_, string lanOrder_)
    {
        _lanid = lanid_;
        _lanname = lanname_;
        _lanfolder = lanfolder_;
        _landefault = landefault_;
        _lanimage = lanimage_;
        _lanactive = lanactive_;
        _lanOrder = lanOrder_;
    }
    #endregion
    #region[Assigned value]
    public string lanid { get { return _lanid; } set { _lanid = value; } }
    public string lanname { get { return _lanname; } set { _lanname = value; } }
    public string lanfolder { get { return _lanfolder; } set { _lanfolder = value; } }
    public string landefault { get { return _landefault; } set { _landefault = value; } }
    public string lanimage { get { return _lanimage; } set { _lanimage = value; } }
    public string lanactive { get { return _lanactive; } set { _lanactive = value; } }
    public string lanOrder { get { return _lanOrder; } set { _lanOrder = value; } }
    #endregion
}