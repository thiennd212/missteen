﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Register Src="~/views/control/ucLoadControl.ascx" TagPrefix="uc1" TagName="ucLoadControl" %>
<%@ Register Src="~/views/control/ucBanner.ascx" TagPrefix="uc2" TagName="ucBanner" %>
<%@ Register Src="~/views/control/ucSearchBox.ascx" TagPrefix="uc3" TagName="ucSearchBox" %>
<%@ Register Src="~/views/control/ucMenuTop.ascx" TagPrefix="uc4" TagName="ucMenuTop" %>
<%@ Register Src="~/views/control/ucLanguage.ascx" TagPrefix="uc5" TagName="ucLanguage" %>
<%@ Register Src="~/views/control/ucLinkAcount.ascx" TagPrefix="uc6" TagName="ucLinkAcount" %>
<%@ Register Src="~/views/control/ucShopCart.ascx" TagPrefix="uc7" TagName="ucShopCart" %>
<%@ Register Src="~/views/control/ucMenuMain.ascx" TagPrefix="uc8" TagName="ucMenuMain" %>
<%@ Register Src="~/views/control/ucSlides.ascx" TagPrefix="uc9" TagName="ucSlides" %>
<%@ Register Src="~/views/control/ucLogin.ascx" TagPrefix="uc10" TagName="ucLogin" %>
<%@ Register Src="~/views/control/ucListCategory.ascx" TagPrefix="uc11" TagName="ucListCategory" %>
<%@ Register Src="~/views/control/ucOnline.ascx" TagPrefix="uc12" TagName="ucOnline" %>
<%@ Register Src="~/views/news/ucNewsHot.ascx" TagPrefix="uc13" TagName="ucNewsHot" %>
<%@ Register Src="~/views/products/ucProSale.ascx" TagPrefix="uc14" TagName="ucProSale" %>
<%@ Register Src="~/views/control/ucAdvleft.ascx" TagPrefix="uc15" TagName="ucAdvleft" %>
<%@ Register Src="~/views/control/ucBoxface.ascx" TagPrefix="uc16" TagName="ucBoxface" %>
<%@ Register Src="~/views/control/ucOnlineVisit.ascx" TagPrefix="uc17" TagName="ucOnlineVisit" %>
<%@ Register Src="~/views/control/ucAdvright.ascx" TagPrefix="uc18" TagName="ucAdvright" %>
<%@ Register Src="~/views/products/ucProNew.ascx" TagPrefix="uc19" TagName="ucProNew" %>
<%@ Register Src="~/views/news/ucAboutUs.ascx" TagPrefix="uc20" TagName="ucAboutUs" %>
<%@ Register Src="~/views/products/ucProHome.ascx" TagPrefix="uc21" TagName="ucProHome" %>
<%@ Register Src="~/views/products/ucProHomeParent.ascx" TagPrefix="uc22" TagName="ucProHomeParent" %>
<%@ Register Src="~/views/news/ucNewsHomePriority.ascx" TagPrefix="uc23" TagName="ucNewsHomePriority" %>
<%@ Register Src="~/views/news/ucNewsHome.ascx" TagPrefix="uc24" TagName="ucNewsHome" %>
<%@ Register Src="~/views/control/ucMenuFooter.ascx" TagPrefix="uc25" TagName="ucMenuFooter" %>
<%@ Register Src="~/views/control/ucSocialNetwork.ascx" TagPrefix="uc26" TagName="ucSocialNetwork" %>
<%@ Register Src="~/views/control/ucFooter.ascx" TagPrefix="uc27" TagName="ucFooter" %>
<%@ Register Src="~/views/control/ucMenuLeft.ascx" TagPrefix="uc28" TagName="ucMenuLeft" %>
<%@ Register Src="~/views/products/ucFilterAttributes.ascx" TagPrefix="uc29" TagName="ucFilterAttributes" %>
<%@ Register Src="~/views/control/ucAdvCenter.ascx" TagPrefix="uc30" TagName="ucAdvCenter" %>
<%@ Register Src="~/views/library/ucListVideo.ascx" TagPrefix="uc31" TagName="ucListVideo" %>
<%@ Register Src="~/views/products/ucProHot.ascx" TagPrefix="uc32" TagName="ucProHot" %>
<%@ Register Src="~/views/products/ucProMotion.ascx" TagPrefix="uc33" TagName="ucProMotion" %>
<%@ Register Src="~/views/control/ucBreadcrumb.ascx" TagPrefix="uc34" TagName="ucBreadcrumb" %>
<%@ Register Src="~/views/control/ucLinkwebsite.ascx" TagPrefix="uc35" TagName="ucLinkwebsite" %>
<%@ Register Src="~/views/control/ucNewsLetter.ascx" TagPrefix="uc36" TagName="ucNewsLetter" %>
<%@ Register Src="~/views/control/ucCustomerReply.ascx" TagPrefix="uc1004" TagName="ucCustomerReply" %>
<%@ Register Src="~/views/products/ucViewedProducts.ascx" TagPrefix="uc37" TagName="ucViewedProducts" %>
<%@ Register Src="~/views/news/ucPriority.ascx" TagPrefix="uc38" TagName="ucPriority" %>

<%@ Register Src="~/views/tour/ucTourHome.ascx" TagPrefix="uc39" TagName="ucTourHome" %>

<%@ Register Src="~/views/Tour/ucTourNew.ascx" TagPrefix="uc42" TagName="ucTourNew" %>
<%@ Register Src="~/views/Tour/ucTourSale.ascx" TagPrefix="uc43" TagName="ucTourSale" %>
<%@ Register Src="~/views/Tour/ucTourTop.ascx" TagPrefix="uc44" TagName="ucTourTop" %>

<%@ Register Src="~/views/Tour/listTour.ascx" TagPrefix="uc45" TagName="listTour" %>
<%@ Register Src="~/views/Tour/ucTourByTopicHome.ascx" TagPrefix="uc46" TagName="ucTourByTopicHome" %>


<%@ Register Src="~/views/search/ucProSearch.ascx" TagPrefix="uc47" TagName="ucProSearch" %>
<%@ Register Src="~/views/search/ucTourSearch.ascx" TagPrefix="uc48" TagName="ucTourSearch" %>
<%@ Register Src="~/views/Tour/ucCatTourHome.ascx" TagPrefix="uc50" TagName="ucCatTourHome" %>

<%@ Register Src="~/views/products/ucProductByTab.ascx" TagPrefix="uc1" TagName="ucProductByTab" %>
<%@ Register Src="~/views/products/ucProSaleV2.ascx" TagPrefix="uc1" TagName="ucProSaleV2" %>
<%@ Register Src="~/views/pages/ucBookNow.ascx" TagPrefix="uc100" TagName="ucBookNow" %>
<%@ Register Src="~/views/tour/ucTourHot.ascx" TagPrefix="uc1001" TagName="ucTourHot" %>
<%@ Register Src="~/views/tour/ucTourSearchBox.ascx" TagPrefix="uc1002" TagName="ucTourSearchBox" %>
<%@ Register Src="~/views/tour/ucTourSearchBox.ascx" TagPrefix="uc1003" TagName="ucTourSearchBox_1" %>

<!-- Contestant -->
<%@ Register Src="~/views/contestant/contestantDetail.ascx" TagPrefix="uc51" TagName="ucContestDetail" %>

<div class="container-fluid web-site">
    <!--==== header ====-->
    <div class="container menu-banner-top-all">
        <div class="row">
            <div class="col-md-12 menu-banner">
                <uc4:ucMenuTop runat="server" ID="ucMenuTop" />
                <uc2:ucBanner runat="server" ID="ucBanner" />
            </div>
        </div>
    </div>
    <div class="container menu-all">
        <div class="row">
            <div class="menu-pc col-md-12"><uc8:ucMenuMain runat="server" ID="ucMenuMain" /></div>
            <div class="menu-mobile col-md-12">
                <div class="menu-click"><i class="fa fa-bars" aria-hidden="true"></i></div>
            </div>
        </div>     
    </div> 
    <div class="clearfix"></div>
    <!--==== slider ====-->
    <%if (Session["home_page"] != null )
    {%>
    <div class="container slider-show">
        <div class="row">
            <div class="col-md-12">
                <uc9:ucSlides runat="server" ID="ucSlides" />
            </div>
        </div>
        <div class="box-search-tour">
            <uc1002:ucTourSearchBox runat="server" ID="ucTourSearchBox" />
        </div>
    </div>
    <div class="clearfix"></div>
    
    <!--==== news Priority ====-->
    <div class="container news-priority-all">
        <div class="row">
            <div class="col-md-12">
                <uc38:ucPriority runat="server" ID="ucPriority" />
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    <!--==== news ====-->
    <div class="container news-all">
        <div class="row">    
            <!--<div class="col-md-12">
                <uc45:listTour runat="server" ID="listTour" /> 
            </div>-->
            <uc50:ucCatTourHome runat="server" ID="ucCatTourHome" /> 
                      
            <div class="clearfix"></div>
            <div class="col-md-6">
                <uc1001:ucTourHot runat="server" ID="ucTourHot" /> 
            </div>
            <div class="col-md-6">
                <uc42:ucTourNew runat="server" ID="ucTourNew" />       
            </div>
            <div class="col-md-6">
                <uc43:ucTourSale runat="server" ID="ucTourSale" /> 
            </div>
            <div class="col-md-6">
                <uc44:ucTourTop runat="server" ID="ucTourTop" />       
            </div>
            <div class="clearfix"></div>
            <!--<div class="col-md-12 tour-all">
                <uc39:ucTourHome runat="server" ID="ucTourHome" />       
            </div>-->

            
            <div class="clearfix"></div>
            <div class="col-md-6">
                <uc24:ucNewsHome runat="server" ID="ucNewsHome" />
            </div>
             <div class="col-md-6">
                 <uc30:ucAdvCenter runat="server" ID="ucAdvCenter" />
            </div>   
            <div class="clearfix"></div>
            <div class="col-md-6">
                <uc31:ucListVideo runat="server" ID="ucListVideo" />
            </div>
            <div class="col-md-6">
               <uc13:ucNewsHot runat="server" ID="ucNewsHot" />
            </div>
            <div class="clearfix"></div>
            <div class="col-md-4 facebook-all">
                <uc16:ucBoxface runat="server" ID="ucBoxface" />
            </div>
            <div class="col-md-8" id="custumer-say">
                <uc1004:ucCustomerReply runat="server" ID="ucCustomerReply" />
            </div>
        </div>
    </div>
    <%} %>

    <%if (Session["home_page"] == null )
    {%>
    <div class="container main-page-else">
        <div class="row">
            <div class="col-md-9 col-right-page"><uc1:ucLoadControl runat="server" ID="ucLoadControl" /></div>
            <div class="col-md-3 col-left-page">
                <uc1003:ucTourSearchBox_1 runat="server" ID="ucTourSearchBox_1" />
                <div class="clearfix"></div>
                <uc28:ucMenuLeft runat="server" ID="ucMenuLeft" />
                <div class="clearfix"></div>
                <uc17:ucOnlineVisit runat="server" ID="ucOnlineVisit" />
                <div class="clearfix"></div>
                <uc15:ucAdvleft runat="server" ID="ucAdvleft" />
            </div>
        </div>
    </div>
    <%} %>

    <footer class="container">
        <div class="footer-all row">
            <div class="col-md-6"><uc25:ucMenuFooter runat="server" ID="ucMenuFooter" /></div>
            <div class="col-md-6"><uc27:ucFooter runat="server" ID="ucFooter" /></div>
        </div>
        <div class="row menu-bottom">
            <div class="col-md-12">Copyright © 2016 Goodmorning Travel. All Rights Reserved</div>
            <!--<div class="col-md-3"><uc26:ucSocialNetwork runat="server" ID="ucSocialNetwork" /></div>-->
        </div>

        <div class="call-now"><a href="tel:0984383380">Hotline: 0984.383.380</a></div>

        <div class="to-top">
            <i class="fa fa-angle-up" aria-hidden="true"></i>
        </div>
    </footer>
</div>
  

<script type="text/javascript">
    $(document).ready(function() {
        $("#content div").hide(); // Initially hide all content
        $("#tabs li:first").attr("id","current"); // Activate first tab
        $("#content div:first").fadeIn(); // Show first tab content
                
        $('#tabs a').click(function(e) {
            e.preventDefault();
            if ($(this).closest("li").attr("id") == "current"){ //detection for current tab
                return       
            }
            else{             
            $("#content div").hide(); //Hide all content
            $("#tabs li").attr("id",""); //Reset id's
            $(this).parent().attr("id","current"); // Activate this
            $('#' + $(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });
    });



      var owl_category_1 = $(".box-adv-center .body-adv-center");
    owl_category_1.owlCarousel({
        items: 1,
        loop:true,
        autoPlay: true,
        transitionStyle: "fade",
        itemsDesktop: [1199, 1],       
        itemsDesktopSmall: [991, 1],
        itemsTablet: [769, 1],
        itemsTablet: [641, 1],
        itemsTablet: [640, 1],
        itemsMobile: [320, 1],
        navigation: false,
        navigationText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
        rewindNav: true,
        scrollPerPage: true,
        slideSpeed: 1500,
        pagination: true,
        paginationNumbers: false,
    });

    
      var owl_category_2 = $(".col-md-8 .custumer-say");
    owl_category_2.owlCarousel({
        items: 2,
        loop:true,
        autoPlay: true,
        transitionStyle: "fade",
        itemsDesktop: [1199, 2],       
        itemsDesktopSmall: [991, 2],
        itemsTablet: [769, 2],
        itemsTablet: [641, 1],
        itemsTablet: [640, 1],
        itemsMobile: [320, 1],
        navigation: false,
        navigationText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
        rewindNav: true,
        scrollPerPage: true,
        slideSpeed: 1500,
        pagination: true,
        paginationNumbers: false,
    });
</script>