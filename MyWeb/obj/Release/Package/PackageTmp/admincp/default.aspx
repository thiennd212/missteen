﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="MyWeb.admincp._default" %>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head runat="server">
    <meta charset="utf-8" />
    <title>Đăng nhập hệ thống</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link rel="icon" type="image/x-icon" href="../theme/admin_cms/img/favicon.ico"/>
    <link rel="apple-touch-icon-precomposed" href="../theme/admin_cms/img/logos/72x72.png" sizes="72x72" />
    <link rel="apple-touch-icon-precomposed" href="../theme/admin_cms/img/logos/114x114.png" sizes="144x144" />
    <link rel="apple-touch-icon-precomposed" href="../theme/admin_cms/img/logos/57x57.png" sizes="57x57" />
    <link rel="apple-touch-icon-precomposed" href="../theme/admin_cms/img/logos/114x114.png" sizes="114x114" />
    <link rel="nokia-touch-icon" href="../theme/admin_cms/img/logos/57x57.png" />
    <link rel="apple-touch-icon-precomposed" href="../theme/admin_cms/img/logos/114x114.png" sizes="1x1" />
    <!-- Base css style -->
    <link rel="stylesheet" type="text/css" href="../theme/admin_cms/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="../theme/admin_cms/plugins/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../theme/admin_cms/plugins/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="../theme/admin_cms/css/animate.min.css" />
    <link rel="stylesheet" type="text/css" href="../theme/admin_cms/css/style.min.css" />
    <link rel="stylesheet" type="text/css" href="../theme/admin_cms/css/style-responsive.min.css" />
    <link rel="stylesheet" type="text/css" href="../theme/admin_cms/css/theme/default.css" id="theme" />
    <!-- End base css style -->
    <!-- Base js -->
    <script type="text/javascript" src="../theme/admin_cms/plugins/pace/pace.min.js"></script>
    <!-- End base js -->
</head>
<body class="pace-top">
    <form id="admin_login" runat="server">
        <div id="page-loader" class="fade in"><span class="spinner"></span></div>
        <div id="page-container" class="fade">
            <div class="login bg-black animated fadeInDown">
                <div class="login-header">
                    <div class="brand">
                        <img src="../theme/admin_cms/img/logo_cms.png" />
                    </div>
                </div>

                <div class="login-content">
                    <div class="margin-bottom-0">
                        <div class="form-group m-b-20" id="div_err" runat="server" visible="false" style="color:#ff0000;">
                            <asp:Label id="ltrError" runat="server"></asp:Label>
                        </div>
                        <div class="form-group m-b-20">
                            <asp:TextBox ID="txtUserName" placeholder="Tên đăng nhập" runat="server" class="form-control input-lg no-border"></asp:TextBox>
                        </div>
                        <div class="form-group m-b-20">
                            <asp:TextBox ID="txtPassWord" placeholder="Mật khẩu" runat="server" class="form-control input-lg no-border" TextMode="Password"></asp:TextBox>
                        </div>
                        <div class="form-group m-b-20">
                            <asp:DropDownList ID="drlLang" class="form-control input-lg no-border" runat="server"></asp:DropDownList>
                        </div>
                        <div class="checkbox m-b-20">
                            <label>
                                <input type="checkbox" />Nhớ tài khoản
                            </label>
                        </div>
                        <div class="login-buttons">
                            <asp:Button ID="tbnLogin" runat="server" Text="Đăng nhập" class="btn btn-success btn-block btn-lg" OnClick="tbnLogin_Click"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>
    <script type="text/javascript" src="../theme/admin_cms/plugins/jquery/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="../theme/admin_cms/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
    <script type="text/javascript" src="../theme/admin_cms/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../theme/admin_cms/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!--[if lt IE 9]>
		<script type="text/javascript" src="../theme/admin_cms/crossbrowserjs/html5shiv.js"></script>
		<script type="text/javascript" src="../theme/admin_cms/crossbrowserjs/respond.min.js"></script>
		<script type="text/javascript" src="../theme/admin_cms/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
    <script type="text/javascript" src="../theme/admin_cms/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="../theme/admin_cms/plugins/jquery-cookie/jquery.cookie.js"></script>
    <script src="../theme/admin_cms/js/apps.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            App.init();
        });
    </script>
</body>
</html>
