﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="configuration.ascx.cs" Inherits="MyWeb.control.panel.system.configuration" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<script src="../../scripts/ckfinder/ckfinder.js" type="text/javascript"></script>
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Trang chủ</a></li>
        <li><a href="javascript:;">Cấu hình</a></li>
        <li class="active">Cấu hình chung</li>
    </ol>
    <!-- end breadcrumb -->
    <h1 class="page-header">Cấu hình chung</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse panel-with-tabs" data-sortable-id="ui-unlimited-tabs-1">
                <div class="panel-heading p-0">
                    <div class="panel-heading-btn m-r-10 m-t-10">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    </div>
                    <div class="tab-overflow">
                        <ul class="nav nav-tabs nav-tabs-inverse">
                            <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-inverse"><i class="fa fa-arrow-left"></i></a></li>
                            <asp:HiddenField ID="txtId" runat="server" />
                            <li class="active"><a href="#info-tab" data-toggle="tab">Thông tin chung</a></li>
                            <li class=""><a href="#email-tab" data-toggle="tab">Cấu hình email</a></li>
                            <%if (MyWeb.Common.GlobalClass.GetAppSettingKey("page_products") == "true")
                              {%>
                            <li class=""><a href="#product-tab" data-toggle="tab">Cấu hình sản phẩm</a></li>
                            <%} %>
                            <li class=""><a href="#news-tab" data-toggle="tab">Cấu hình tin tức</a></li>
                            <li class=""><a href="#seo-tab" data-toggle="tab">Cấu hình SEO & Networks</a></li>
                            <li class=""><a href="#other-tab" data-toggle="tab">Cấu hình khác</a></li>
                            <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-inverse"><i class="fa fa-arrow-right"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="tab-content panel-body panel-form">
                    <asp:Panel ID="pnlErr" runat="server" Visible="false">
                        <div class="alert alert-danger fade in" style="border-radius: 0px;">
                            <button class="close" data-dismiss="alert" type="button">
                                <span aria-hidden="true">×</span>
                            </button>
                            <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                        </div>
                    </asp:Panel>
                    <div class="tab-pane fade active in" id="info-tab">
                        <div class="form-horizontal form-bordered">
                            <div class="form-group">
                                <label class="control-label col-md-2">Tên công ty / cơ quan:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtTencoquan" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <span class="vntsc-err">
                                    <asp:RequiredFieldValidator ID="rfvtxtTencoquan" runat="server" ControlToValidate="txtTencoquan" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True" ValidationGroup="CF"></asp:RequiredFieldValidator>
                                </span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Địa chỉ:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtDiachi" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Điện thoại:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtDienthoai" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Fax:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtFax" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Website:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtWebsite" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Ảnh favicon:</label>
                                <div class="col-md-7">
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <asp:TextBox ID="txtImage" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                            <div class="input-group-btn">
                                                <button onclick="BrowseServer('<% =txtImage.ClientID %>','Adv');" class="btn btn-success" type="button" style="margin-left: -12px;">Browse Server</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Dòng chữ chạy:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtdongchay" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Mã livechat:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtlivebox" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Thông tin liên hệ:</label>
                                <div class="col-md-7">
                                    <CKEditor:CKEditorControl ID="fckNguoilienhe" runat="server" Toolbar="Basic"></CKEditor:CKEditorControl>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Thông tin cuối trang:</label>
                                <div class="col-md-7">
                                    <CKEditor:CKEditorControl ID="fckFooter" runat="server" Toolbar="Basic"></CKEditor:CKEditorControl>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="tab-pane fade" id="email-tab">
                        <div class="form-horizontal form-bordered">
                            <div class="form-group">
                                <label class="control-label col-md-2">Máy chủ gửi mail:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtPhuongthucguimail" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Cổng gửi mail:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtCongguimail" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <span class="vntsc-err">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCongguimail" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True" ValidationGroup="CF"></asp:RequiredFieldValidator>
                                </span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Email nhận thông tin:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtEmaillienhe" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <span class="vntsc-err">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmaillienhe" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True" ValidationGroup="CF"></asp:RequiredFieldValidator>
                                </span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Email gửi thông tin:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtEmailgui" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <span class="vntsc-err">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEmailgui" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True" ValidationGroup="CF"></asp:RequiredFieldValidator>
                                </span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Mật khẩu mail gửi:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtPasswordmail" runat="server" class="form-control" TextMode="Password"></asp:TextBox>
                                </div>
                                <span class="vntsc-err">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPasswordmail" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True" ValidationGroup="CF"></asp:RequiredFieldValidator>
                                </span>
                            </div>
                        </div>

                    </div>
                    <%if (MyWeb.Common.GlobalClass.GetAppSettingKey("page_products") == "true")
                      {%>
                    <div class="tab-pane fade" id="product-tab">
                        <div class="form-horizontal form-bordered">
                            <div class="form-group">
                                <label class="control-label col-md-2">Số sản phẩm/ trang:</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtviewProducts1" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Số sản phẩm hiển thị trên 1 trang, mặc định 10
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Số sản phẩm/ dòng:</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtproductInLine" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Số sản phẩm hiển thị trên 1 dòng, mặc định 3
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Sản phẩm bán chạy:</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtviewProducts3" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Số sản phẩm bán chạy hiển thị, mặc định 5
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Sản phẩm mới:</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtviewProducts8" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Số sản phẩm mới hiển thị, mặc định 5
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Sản phẩm nổi bật:</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtviewProductsHot" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Số sản phẩm nổi bật hiển thị, mặc định 5
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Sản phẩm khuyến mại:</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtviewProductsPromo" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Số sản phẩm khuyến mại hiển thị, mặc định 5
                                    </span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-2">Sản phẩm trang chủ:</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtviewProducts2" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Hiển thị theo nhóm ở trang chủ, mặc định 3
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Sản phẩm cùng loại:</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtviewProducts4" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Số sản phẩm cùng loại hiển thị, mặc định 9
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Đăng nhập mua hàng:</label>
                                <div class="col-md-6">
                                    <asp:CheckBox ID="chkLogin" runat="server" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Xem nhanh:</label>
                                <div class="col-md-6">
                                    <asp:CheckBox ID="chkViewFast" runat="server" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Hiển thị trang sản phẩm:</label>
                                <div class="col-md-6">
                                    <asp:DropDownList ID="drlShowProduct" runat="server" class="form-control">
                                        <asp:ListItem Value="1" Text="Tất cả sản phẩm"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Sản phẩm theo nhóm con(nếu có)"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Hiển thị trang chủ:</label>
                                <div class="col-md-6">
                                    <asp:DropDownList ID="drlShowCatelogy" runat="server" class="form-control">
                                        <asp:ListItem Value="1" Text="Sản phẩm theo nhóm"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Nhóm con theo nhóm cha"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Sắp xếp theo:</label>
                                <div class="col-md-6">
                                    <asp:DropDownList ID="ddlSort" runat="server" class="form-control">
                                        <asp:ListItem Value="newest" Text="Mới nhất"></asp:ListItem>
                                        <asp:ListItem Value="name" Text="Tên A - Z"></asp:ListItem>
                                        <asp:ListItem Value="namedesc" Text="Tên Z - A"></asp:ListItem>
                                        <asp:ListItem Value="price" Text="Giá tăng dần"></asp:ListItem>
                                        <asp:ListItem Value="pricedesc" Text="Giá giảm dần"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                            </div>





                        </div>
                    </div>
                    <%} %>
                    <div class="tab-pane fade" id="news-tab">
                        <div class="form-horizontal form-bordered">
                            <div class="form-group">
                                <label class="control-label col-md-2">Số tin / trang:</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtviewNews1" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Số tin hiển thị trên 1 trang, mặc định 10
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Tin trang chủ:</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtviewNews2" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Số tin hiển thị theo nhóm ở trang chủ, mặc định 4
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Tin mới:</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtviewNews3" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Số tin hiển thị phần tin mới, mặc định 6
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Tin nổi bật:</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtviewNews4" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Phần tin nổi bật, mặc định 6
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Tin mới trang chi tiết:</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtviewNews5" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Phần tin mới trong trang tin chi tiết, mặc định 5
                                    </span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-2">Tin cũ hơn trang chi tiết:</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtviewNews6" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Phần tin cũ hơn trong trang tin chi tiết, mặc định 5
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Tin sự kiện:</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtviewNews8" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Số tin hiển thị trong tin sư kiện, mặc định 5
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Tin theo chuyên mục:</label>
                                <div class="col-md-6">
                                    <asp:DropDownList ID="ddlStyleNews" runat="server" class="form-control">
                                        <asp:ListItem Value="1" Text="Tất cả tin"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Tin theo nhóm con (nếu có)"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Tin hiển thị theo dạng 
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Số tin hiển thị theo nhóm:</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtviewNews9" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Số tin hiển thị theo nhóm ở trang tin, mặc định 4
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Hiển thị theo:</label>
                                <div class="col-md-6">
                                    <asp:DropDownList ID="ddlNewsList" runat="server" class="form-control">
                                        <asp:ListItem Selected="True" Value="1">Dạng 1</asp:ListItem>
                                        <asp:ListItem Value="2">Dạng 2</asp:ListItem>
                                        <asp:ListItem Value="3">Dạng 3</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Dạng 1: Hiển thị các tin giống nhau, mặc định<br />
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Dạng 2: Hiển thị 1 tin đầy đủ, các tin còn lại chỉ hiển thị tiêu đề, dưới tin đầu tiên<br />
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Dạng 3: Hiển thị 1 tin đầy đủ, các tin còn lại chỉ hiển thị tiêu đề, bên phải tin đầu tiên
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Hiển thị tin trang chủ:</label>
                                <div class="col-md-6">
                                    <asp:DropDownList ID="ddlNewsIndex" runat="server" class="form-control">
                                        <asp:ListItem Selected="True" Value="1">Dạng 1</asp:ListItem>
                                        <asp:ListItem Value="2">Dạng 2</asp:ListItem>
                                        <asp:ListItem Value="3">Dạng 3</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-4">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Dạng 1: Hiển thị các tin giống nhau, mặc định<br />
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Dạng 2: Hiển thị 1 tin đầy đủ, các tin còn lại chỉ hiển thị tiêu đề, dưới tin đầu tiên<br />
                                        <i class="ion-help-circled fa-1x"></i>&nbsp;Dạng 3: Hiển thị 1 tin đầy đủ, các tin còn lại chỉ hiển thị tiêu đề, bên phải tin đầu tiên
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="tab-pane fade" id="seo-tab">
                        <div class="form-horizontal form-bordered">
                            <div class="form-group">
                                <label class="control-label col-md-2">Meta title:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtTitle" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <span class="vntsc-err">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtTitle" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True" ValidationGroup="CF"></asp:RequiredFieldValidator>
                                </span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Thẻ H1:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txth1" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Meta description:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtDesc" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                </div>
                                <span class="vntsc-err">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtDesc" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True" ValidationGroup="CF"></asp:RequiredFieldValidator>
                                </span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Meta keyword:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtKey" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                </div>
                                <span class="vntsc-err">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtKey" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True" ValidationGroup="CF"></asp:RequiredFieldValidator>
                                </span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Google Analytics Id:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtGoogleAnalytics" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Mã Histats ID:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtMaychugiumail" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <div class="col-md-3">
                                    <span class="vntsc-help">
                                        <i class="ion-help-circled fa-1x"></i>Tạo trên <a target="_blank" href="http://www.histats.com/">www.histats.com</a>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Fanpage Facebook:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtfacebook" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Kích thước facebook comment:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtWidthCB" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Số facebook comment:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtNumCB" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Facebook comment:</label>
                                <div class="col-md-7">
                                    <asp:CheckBox ID="chkCommnetFacebook" runat="server" Text="Kích hoạt cho sản phẩm" />
                                    <asp:CheckBox ID="chkCommentFNews" runat="server" Text="Kích hoạt cho tin tức" />
                                    <asp:CheckBox ID="chkCommentFPage" runat="server" Text="Kích hoạt cho trang nội dung" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Skype size:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtStyleSkype" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Kiểu Yahoo:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtStyleYahoo" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Tweet:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txttweet" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Youtube:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtyoutube" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Zing Me:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtzingme" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Google+:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtgoogle" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">RSS:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtRSS" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>



                        </div>
                    </div>
                    <div class="tab-pane fade" id="other-tab">
                        <div class="form-horizontal form-bordered">
                            <div class="form-group">
                                <label class="control-label col-md-2">Khung soạn thảo trái:</label>
                                <div class="col-md-7">
                                    <CKEditor:CKEditorControl ID="CKEField2" runat="server" Toolbar="Basic"></CKEditor:CKEditorControl>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Khung soạn thảo giữa:</label>
                                <div class="col-md-7">
                                    <CKEditor:CKEditorControl ID="CKEField4" runat="server" Toolbar="Basic"></CKEditor:CKEditorControl>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Trang 404:</label>
                                <div class="col-md-7">
                                    <CKEditor:CKEditorControl ID="ckePageError" runat="server" Toolbar="Basic"></CKEditor:CKEditorControl>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal form-bordered">
                        <div class="form-group" style="border-top: 1px solid #eee;">
                            <label class="control-label col-md-2"></label>
                            <div class="col-md-7">
                                <asp:Button ID="btnSiteMap" runat="server" OnClick="btnSiteMap_Click" Text="Tạo sitemap" class="btn btn-primary" />
                                <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Cập nhật" class="btn btn-primary" ValidationGroup="CF" />
                                <asp:Button ID="btnReset" runat="server" OnClick="btnReset_Click" Text="Hủy" class="btn btn-danger" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
