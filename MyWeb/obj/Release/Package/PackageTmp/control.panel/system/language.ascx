﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="language.ascx.cs" Inherits="MyWeb.control.panel.system.language" %>

<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="/control.panel/">Trang chủ</a></li>
        <li class="active">Quản trị ngôn ngữ</li>
    </ol>
    <!-- end breadcrumb -->
    <h1 class="page-header">Quản trị ngôn ngữ</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse" data-sortable-id="table-basic-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">Danh sách ngôn ngữ</h4>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                                    <thead>
                                        <tr>
                                            <th width="100">Mã ngôn ngữ</th>
                                            <th>Tên ngôn ngữ</th>
                                            <th width="350">Đường dẫn ảnh</th>
                                            <th width="100">Mặc định</th>
                                            <th width="20"></th>
                                            <th width="80"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptFolderList" runat="server" OnItemCommand="rptFolderList_ItemCommand">
                                            <ItemTemplate>
                                                <tr class="even gradeC">
                                                    <td>
                                                        <%#DataBinder.Eval(Container.DataItem, "lanId")%>
                                                    </td>
                                                    <td>
                                                        <%#DataBinder.Eval(Container.DataItem, "lanName")%>
                                                    </td>
                                                    <td>
                                                        <%#DataBinder.Eval(Container.DataItem, "lanImage")%>
                                                    </td>
                                                    <td><%#ShowActive(DataBinder.Eval(Container.DataItem, "lanDefault").ToString())%></td>
                                                    <td>
                                                        <asp:LinkButton ID="btnActive" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"lanId")%>' CommandName="Active" class='<%#ShowActiveClass(DataBinder.Eval(Container.DataItem, "lanActive").ToString())%>' ToolTip="Kích hoạt"><%#ShowActive(DataBinder.Eval(Container.DataItem, "lanActive").ToString())%></asp:LinkButton>
                                                    </td>

                                                    <td>
                                                        <asp:LinkButton class="btn btn-success btn-xs" ID="btnEdit" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"lanId")%>' CommandName="Edit" ToolTip="Mặc định">Set default</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
