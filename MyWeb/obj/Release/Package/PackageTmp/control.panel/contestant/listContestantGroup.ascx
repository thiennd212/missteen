﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="listContestantGroup.ascx.cs" Inherits="MyWeb.control.panel.contestant.listContestantGroup" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<script src="../../scripts/ckfinder/ckfinder.js" type="text/javascript"></script>
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="/control.panel/">Trang chủ</a></li>
        <li class="active">Quản lý nhóm thí sinh</li>
    </ol>
    <!-- end breadcrumb -->
    <asp:Panel ID="pnlListForder" runat="server" Visible="true">
        <h1 class="page-header">Quản lý nhóm thí sinh</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse" data-sortable-id="table-basic-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Danh sách nhóm thí sinh</h4>
                    </div>

                    <div class="alert alert-info fade in" id="pnlErr" runat="server" visible="false">
                        <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                        <button class="close" data-dismiss="alert" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-5">
                                <asp:LinkButton ID="btnAddNew" runat="server" class="btn btn-success btn-sm" OnClick="btnAddNew_Click"><i class="fa fa-plus"></i><span>Thêm mới</span></asp:LinkButton>
                                <asp:LinkButton ID="btnDeleteAll" runat="server" class="btn btn-danger btn-sm" OnClick="btnDeleteAll_Click" OnClientClick="javascript:return confirm('Bạn có muốn xóa thư mục đã chọn?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtSearch" runat="server" class="form-control input-sm" placeholder="Từ khóa tìm kiếm"></asp:TextBox>
                            </div>

                            <div class="col-sm-2">
                                <asp:DropDownList ID="drlForder" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drlForder_SelectedIndexChanged"></asp:DropDownList>
                            </div>

                            <div class="col-sm-1">
                                <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-primary btn-sm" Style="float: right;" OnClick="btnSearch_Click"><i class="fa fa-search"></i><span>Tìm kiếm</span></asp:LinkButton>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                                        <thead>
                                            <tr>
                                                <th width="10">
                                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="False"></asp:CheckBox>
                                                </th>
                                                <th>Tên nhóm</th>
                                                <th width="200">Nhóm cha</th>
                                                <th width="50">Sắp xếp</th>
                                                <th width="20"></th>
                                                <th width="160">Công cụ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptFolderList" runat="server" OnItemCommand="rptFolderList_ItemCommand">
                                                <ItemTemplate>
                                                    <tr class="even gradeC">
                                                        <td>
                                                            <asp:CheckBox ID="chkBox" CssClass="chkBoxSelect" runat="server"></asp:CheckBox>
                                                            <asp:HiddenField ID="hidCatID" Value='<%#DataBinder.Eval(Container.DataItem, "pagId")%>' runat="server" />
                                                            <asp:Label ID="lblID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"pagId")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <%#BindView("", DataBinder.Eval(Container.DataItem, "paglevel").ToString())%>
                                                            <asp:TextBox ID="txtNameForder" runat="server" Text='<%#Eval("pagName").ToString()%>' AutoPostBack="true" OnTextChanged="txtNameForder_TextChanged" class="form-control input-sm" Style="width: 300px !important; float: left;"></asp:TextBox>
                                                        </td>

                                                        <td>
                                                            <%#BindCateName(DataBinder.Eval(Container.DataItem, "paglevel").ToString())%>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtOrder" runat="server" class="form-control input-sm" onkeyup="valid(this,'quotes')" onblur="valid(this,'quotes')" Text='<%#DataBinder.Eval(Container.DataItem, "pagOrd").ToString()%>' OnTextChanged="txtOrder_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="btnActive" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"pagId")%>' CommandName="Active" class='<%#ShowActiveClass(DataBinder.Eval(Container.DataItem, "pagActive").ToString())%>' ToolTip="Kích hoạt"><%#ShowActive(DataBinder.Eval(Container.DataItem, "pagActive").ToString())%></asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton class="btn btn-primary btn-xs" ID="btnAddNew" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"pagId")%>' CommandName="Add" ToolTip="Thêm nhóm thí sinh con"><i class="fa fa-plus"></i>Thêm</asp:LinkButton>
                                                            <asp:LinkButton class="btn btn-success btn-xs" ID="btnEdit" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"pagId")%>' CommandName="Edit" ToolTip="Sửa"><i class="fa fa-pencil-square-o"></i>Sửa</asp:LinkButton>
                                                            <asp:LinkButton class="btn btn-danger btn-xs" ID="btnDel" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"pagId")%>' CommandName="Del" ToolTip="Xóa" OnClientClick="javascript:return confirm('Bạn có muốn xóa?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row dataTables_wrapper">
                            <div class="col-sm-5">
                                <div id="data-table_info" class="dataTables_info" role="status" aria-live="polite">
                                    <asp:Literal ID="ltrStatistic" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div id="data-table_paginate" class="dataTables_paginate paging_simple_numbers">
                                    <ul class="pagination">
                                        <li id="data-table_previous" class="paginate_button previous disabled">
                                            <asp:LinkButton ID="btnPrevious" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Trước</asp:LinkButton>
                                        </li>
                                        <asp:Repeater ID="rptNumberPage" runat="server" OnItemCommand="rptNumberPage_ItemCommand" OnItemDataBound="rptNumberPage_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrLiPage" runat="server"></asp:Literal>
                                                <asp:LinkButton ID="btn" runat="server" CommandArgument='<%# Eval("PageIndex") %>' CommandName="page" Text='<%# Eval("PageText") %> '></asp:LinkButton></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <li id="data-table_next" class="paginate_button next">
                                            <asp:LinkButton ID="btnNext" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Sau</asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAddForder" runat="server" Visible="false">
        <h1 class="page-header">Thêm/sửa nhóm thí sinh</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse" data-sortable-id="table-basic-2">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Thêm/sửa nhóm thí sinh</h4>
                    </div>
                    <div class="panel-body panel-form">
                        <asp:Panel ID="pnlErr2" runat="server" Visible="false">
                            <div class="alert alert-danger fade in" style="border-radius: 0px;">
                                <button class="close" data-dismiss="alert" type="button">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <asp:Literal ID="ltrErr2" runat="server"></asp:Literal>
                            </div>
                        </asp:Panel>
                        <div class="form-horizontal form-bordered">
                            <div class="form-group">
                                <label class="control-label col-md-2">Nhóm cha:</label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="drlChuyenmuc" runat="server" class="form-control"></asp:DropDownList>
                                    <asp:HiddenField ID="hidID" runat="server" />
                                    <asp:HiddenField ID="hidCurLever" runat="server" />
                                    <asp:HiddenField ID="hidLevel" runat="server" />
                                    <asp:HiddenField ID="hidParId" runat="server" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Tên nhóm thí sinh:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtTenchuyenmuc" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Ảnh đại diện:</label>
                                <div class="col-md-7">
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <asp:TextBox ID="txtImage" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                            <div class="input-group-btn">
                                                <button onclick="BrowseServer('<% =txtImage.ClientID %>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                       
                            <div class="form-group">
                                <label class="control-label col-md-2">Meta title:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtTieude" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Meta description:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtDesscription" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Meta keyword:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtKeyword" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Thứ tự:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtThuTu" runat="server" class="form-control" onkeyup="valid(this,'quotes')" onblur="valid(this,'quotes')">1</asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Hiển thị:</label>
                                <div class="col-md-7">
                                    <asp:CheckBox ID="chkKichhoat" runat="server" Checked="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Hiển thị trang chủ:</label>
                                <div class="col-md-7">
                                    <asp:CheckBox ID="chkIndex" runat="server" />
                                </div>
                            </div>  
                            <div class="form-group">
                                <label class="control-label col-md-2">Số lượng tin nhóm trang chủ:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtSoLuongTrangChu" runat="server" class="form-control" onkeyup="valid(this,'quotes')" onblur="valid(this,'quotes')">2</asp:TextBox>
                                </div>
                            </div>                  
                            <div class="form-group">
                                <label class="control-label col-md-2"></label>
                                <div class="col-md-7">
                                    <asp:LinkButton ID="btnUpdate" runat="server" class="btn btn-primary" OnClick="btnUpdate_Click">Cập nhật</asp:LinkButton>
                                    <asp:LinkButton ID="btnReset" runat="server" class="btn btn-danger" OnClick="btnReset_Click">Hủy</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</div>
