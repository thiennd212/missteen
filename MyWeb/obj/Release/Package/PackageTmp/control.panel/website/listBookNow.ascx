﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="listBookNow.ascx.cs" Inherits="MyWeb.control.panel.website.listBookNow" %>

<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="/control.panel/">Trang chủ</a></li>
        <li class="active">Quản lý đặt bàn</li>
    </ol>
    <!-- end breadcrumb -->
    <asp:Panel ID="pnlListForder" runat="server" Visible="true">
        <h1 class="page-header">Quản lý đặt bàn</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse" data-sortable-id="table-basic-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Danh sách đặt bàn</h4>
                    </div>

                    <div class="alert alert-info fade in" id="pnlErr" runat="server" visible="false">
                        <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                        <button class="close" data-dismiss="alert" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <asp:DropDownList ID="drlTrangthai" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drlTrangthai_SelectedIndexChanged" class="form-control input-sm">
                                    <asp:ListItem Value="-1">- Tất cả -</asp:ListItem>
                                    <asp:ListItem Value="0">Chưa xem</asp:ListItem>
                                    <asp:ListItem Value="1">Đã xem</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-3">
                                <asp:Button runat="server" ID="Btn_Export" Text="Kết xuất excel" OnClick="Btn_Export_Click"  class="btn btn-primary btn-sm"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                                        <thead>
                                            <tr>
                                                <th width="10">
                                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="False"></asp:CheckBox>
                                                </th>
                                                <th>Tên người đặt bàn</th>
                                                <th width="180">Điện thoại</th>
                                                <th width="180">Email</th>
                                                <th width="120">Ngày đặt bàn</th>
                                                <th width="80">Trạng thái</th>
                                                <th width="100">Công cụ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptFolderList" runat="server" OnItemCommand="rptFolderList_ItemCommand">
                                                <ItemTemplate>
                                                    <tr class="even gradeC">
                                                        <td>
                                                            <asp:CheckBox ID="chkBox" CssClass="chkBoxSelect" runat="server"></asp:CheckBox>
                                                            <asp:Label ID="lblID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"conId")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "conName")%>
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "conTel")%>
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "conMail")%>
                                                        </td>
                                                        <td><%#FormatDate(DataBinder.Eval(Container.DataItem, "conDate"))%></td>
                                                        <td><%#Actives(DataBinder.Eval(Container.DataItem, "conActive").ToString())%></td>

                                                        <td>
                                                            <asp:LinkButton class="btn btn-success btn-xs" ID="btnEdit" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"conId")%>' CommandName="Edit" ToolTip="Sửa"><i class="fa fa-pencil-square-o"></i>Xem</asp:LinkButton>
                                                            <asp:LinkButton class="btn btn-danger btn-xs" ID="btnDel" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"conId")%>' CommandName="Del" ToolTip="Xóa" OnClientClick="javascript:return confirm('Bạn có muốn xóa?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                        <div class="row dataTables_wrapper">
                            <div class="col-sm-5">
                                <div id="data-table_info" class="dataTables_info" role="status" aria-live="polite">
                                    <asp:Literal ID="ltrStatistic" runat="server"></asp:Literal>
                                </div>
                            </div>

                            <div class="col-sm-7">
                                <div id="data-table_paginate" class="dataTables_paginate paging_simple_numbers">
                                    <ul class="pagination">
                                        <li id="data-table_previous" class="paginate_button previous disabled">
                                            <asp:LinkButton ID="btnPrevious" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Trước</asp:LinkButton>
                                        </li>
                                        <asp:Repeater ID="rptNumberPage" runat="server" OnItemCommand="rptNumberPage_ItemCommand" OnItemDataBound="rptNumberPage_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrLiPage" runat="server"></asp:Literal>
                                                <asp:LinkButton ID="btn" runat="server" CommandArgument='<%# Eval("PageIndex") %>' CommandName="page" Text='<%# Eval("PageText") %> '></asp:LinkButton></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <li id="data-table_next" class="paginate_button next">
                                            <asp:LinkButton ID="btnNext" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Sau</asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>



                </div>
            </div>

        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAddForder" runat="server" Visible="false">
        <h1 class="page-header">Xem đặt bàn</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse" data-sortable-id="table-basic-2">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Xem đặt bàn</h4>
                    </div>

                    <div class="panel-body panel-form">
                        <asp:Panel ID="pnlErr2" runat="server" Visible="false">
                            <div class="alert alert-danger fade in" style="border-radius: 0px;">
                                <button class="close" data-dismiss="alert" type="button">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <asp:Literal ID="ltrErr2" runat="server"></asp:Literal>
                            </div>
                        </asp:Panel>
                        <div class="form-horizontal form-bordered">

                            <div class="form-group">
                                <label class="control-label col-md-2">Người đặt bàn:</label>
                                <div class="col-md-7">
                                    <asp:HiddenField ID="hidID" runat="server" />
                                    <asp:TextBox ID="txtTennguoiLH" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Địa chỉ:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtDiachi" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Điện thoại di động:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtDienthoai" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Điện thoại cố định:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtDienthoaicodinh" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Fax:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtFax" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Email:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtMail" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">File đính kèm:</label>
                                <div class="col-md-7">
                                    <asp:Label ID="lblFIledinhkem" runat="server"></asp:Label>
                                </div>
                            </div>
                            
                            <div class="form-group bookNowNOP">
                                <label class="control-label col-md-3">Số người:</label>                            
                                <div class="col-md-9">
                                    <asp:TextBox ID="txtSoNguoi" runat="server" class="form-control" onkeyup="valid(this,'quotes')" onblur="valid(this,'quotes')">1</asp:TextBox>
                                </div>
                            </div>
                    
                            <div class="form-group bookNowDate">
                                <label class="control-label col-md-3">Thời gian:</label>                          
                                <div class="col-md-9">
                                    <asp:TextBox ID="txtThoiGian" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Nội dung:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtNoidung" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Ngày tạoo:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtNgay" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2"></label>
                                <div class="col-md-7">

                                    <asp:LinkButton ID="btnReset" runat="server" class="btn btn-danger" OnClick="btnReset_Click">Hủy</asp:LinkButton>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</div>
