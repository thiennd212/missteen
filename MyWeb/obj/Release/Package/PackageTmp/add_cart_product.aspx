﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add_cart_product.aspx.cs" EnableEventValidation="false" Inherits="MyWeb.add_cart_product" %>
<!DOCTYPE HTML>
<html>
<head runat="server">
    <title></title>

</head>
<body>
    <form id="add_products" runat="server">
        <asp:ScriptManager EnablePartialRendering="true" ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="udpAdv" runat="server" RenderMode="Block" ChildrenAsTriggers="true">
            <ContentTemplate>
                <div class="box-cart-pop">
                    <div class="row-list-header">
                        Đơn hàng của bạn
                    </div>
                    <div class="row-list">
                        <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                    </div>
                    <div class="row-list">
                        <table class="table table-striped table-bordered dataTable">
                            <thead>
                                <tr>
                                    <th><%= MyWeb.Global.GetLangKey("cart_stt")%></th>
                                    <th><%= MyWeb.Global.GetLangKey("cart_product")%></th>
                                    <th><%= MyWeb.Global.GetLangKey("cart_amount")%></th>
                                    <th><%= MyWeb.Global.GetLangKey("cart_price")%></th>
                                    <th><%= MyWeb.Global.GetLangKey("cart_money")%></th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <asp:Repeater ID="rptDanhsach" runat="server" OnItemCommand="rptDanhsach_ItemCommand">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblstt" runat="server"></asp:Label></td>
                                        <td style="text-align: left;">
                                            <img class="view-img-cart" src="<%#DataBinder.Eval(Container.DataItem, "img")%>" />
                                            <%#DataBinder.Eval(Container.DataItem, "proname")%></td>
                                        <td>
                                            <asp:TextBox ID="txtSL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "number")%>' class="input-card boder-color" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                            <asp:LinkButton class="btn btn-success btn-xs btn-add-card-btn" ID="btnEdit" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"proId")%>' CommandName="Update" ToolTip="Cập nhập lại sản phẩm"><i class="fa fa-pencil-square-o"></i>Cập nhập</asp:LinkButton>
                                        </td>
                                        <td><%#String.Format("{0:N0} đ", double.Parse(Eval("price").ToString()))%></td>
                                        <td><%#String.Format("{0:N0} vnđ", double.Parse(Eval("money").ToString()))%></td>
                                        <td>
                                            <asp:LinkButton class="btn btn-danger btn-xs btn-add-card-btn" ID="btnDel" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"proId")%>' CommandName="Delete" ToolTip="Xóa" OnClientClick="javascript:return confirm('Bạn có muốn xóa?');"><i class="fa fa-trash-o"></i><%= MyWeb.Global.GetLangKey("cart_remove")%></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <tr>
                                <td colspan="6" class="view-price"><%= MyWeb.Global.GetLangKey("cart_total_pay")%><span><asp:Literal ID="lblTongtien" runat="server"></asp:Literal></span></td>
                            </tr>
                        </table>
                    </div>

                    <div class="row-list">
                        <asp:Literal ID="ltrThanhToan" runat="server"></asp:Literal>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
</body>
</html>
