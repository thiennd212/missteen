﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucShopCart.ascx.cs" Inherits="MyWeb.views.control.ucShopCart" %>

<%if(showCart){%>
<div class="header-box-cart">
    <span class="icon-cart"><i class="fa fa-shopping-cart"></i></span>
    <span class="txt-Number"><asp:Literal ID="ltrNumber" runat="server"></asp:Literal></span>
    <span class="view-cart"><a href="/gio-hang.html"><%= MyWeb.Global.GetLangKey("cart_view")%></a></span>
</div>

<%} %>