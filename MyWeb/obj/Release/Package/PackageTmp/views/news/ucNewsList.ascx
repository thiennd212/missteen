﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucNewsList.ascx.cs" Inherits="MyWeb.views.news.ucNewsList" %>

<div class="box-news-sub">
    <div class="cate-header sub-top">
        <div class="txt-name-sub">
            <asp:Literal ID="ltrName" runat="server"></asp:Literal>
        </div>
    </div>
    <asp:Literal runat="server" ID="ltrGroupDes"></asp:Literal>
    <asp:Literal ID="ltrlist" runat="server"></asp:Literal>
    <div class="clearfix">
        <asp:Literal ID="ltrPaging" runat="server"></asp:Literal>
    </div>
    <asp:Literal runat="server" ID="ltrAdv"></asp:Literal>
</div>
