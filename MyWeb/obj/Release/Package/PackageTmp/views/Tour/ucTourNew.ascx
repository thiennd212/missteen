﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucTourNew.ascx.cs" Inherits="MyWeb.views.Tour.ucTourNew" %>
<div id="listTourNew" class="listTour_all">
    <div class="listTour_label">
        <span>Tour mới nhất</span>
    </div>
    <div class="listTour_list">
        <asp:Literal runat="server" ID="ltrListItem"></asp:Literal>
    </div>
</div>
