﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucBookNow2.ascx.cs" Inherits="MyWeb.views.pages.ucBookNow2" %>

<div class="box-sub-book-now bookNow2Form">
    <div class="header bookNow2Title">
        <div class="txt-name-sub"><%= MyWeb.Global.GetLangKey("book-now") %></div>
    </div>
    <asp:UpdatePanel ID="udpBook" runat="server">
        <ContentTemplate>
            <div class="box-body-sub bookNow2Content">
                <div class="row">
                    <div class="form-group">
                        <span class="view-messenger">
                            <asp:Label ID="lblthongbao" runat="server"></asp:Label></span>
                    </div>
                    <div class="form-group bookNow2Name">
                        <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("contact_name")%>*:
                            <asp:RequiredFieldValidator ID="rfvname" runat="server" ControlToValidate="txtname" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True" ValidationGroup="bookNow2"></asp:RequiredFieldValidator></label>
                        <div class="col-md-9">
                            <asp:TextBox ID="txtname" runat="server" class="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group bookNow2Phone">
                        <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("contact_mobilephone")%>:
                            <asp:RequiredFieldValidator ID="rfvDienthoai" runat="server" ControlToValidate="txtDienthoai" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True" ValidationGroup="bookNow2"></asp:RequiredFieldValidator></label>
                        <div class="col-md-9">
                            <asp:TextBox ID="txtDienthoai" runat="server" class="form-control"></asp:TextBox>
                        </div>

                    </div>
                    <div class="form-group bookNow2Email">
                        <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("contact_email")%>:
                            <asp:RegularExpressionValidator ID="revmail" runat="server" ControlToValidate="txtmail" ErrorMessage="(*)" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True" Display="Dynamic" ValidationGroup="bookNow2"></asp:RegularExpressionValidator></label>
                        <div class="col-md-9">
                            <asp:TextBox ID="txtmail" runat="server" class="form-control"></asp:TextBox>
                        </div>

                    </div>
                    
                    <div class="form-group bookNow2NOP">
                        <label class="control-label col-md-3">Số người:</label>                       
                        <div class="col-md-9">
                            <asp:TextBox ID="txtSoNguoi" runat="server" class="form-control" onkeyup="valid(this,'quotes')" onblur="valid(this,'quotes')">1</asp:TextBox>
                        </div>
                    </div>
                    
                    <div class="form-group bookNow2Date">
                        <label class="control-label col-md-3">Thời gian:</label>                           
                        <div class="col-md-9">
                            <asp:TextBox ID="txtThoiGian" runat="server" class="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group bookNow2Content">
                        <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("contact_content")%>:</label>
                        <div class="col-md-9">
                            <asp:TextBox ID="txtnoidung" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group bookNow2Btn">
                        <label class="control-label col-md-3"></label>
                        <div class="col-md-9">
                            <asp:LinkButton ID="btnSend" class="btnSent" runat="server" OnClick="btnSend_Click"  ValidationGroup="bookNow2"><%= MyWeb.Global.GetLangKey("contact_send") %></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<script>
    var r = {
        'special': /[\W]/g,
        'quotes': /[^0-9^]/g,
        'notnumbers': /[^a-zA]/g
    }
    function valid(o, w) {
        o.value = o.value.replace(r[w], '');
    }
</script>