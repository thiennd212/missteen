﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.pages
{
    public partial class ucContactUs : System.Web.UI.UserControl
    {
        private dataAccessDataContext db = new dataAccessDataContext();
        string lang = "vi"; 
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                LoadAdv();
                lblConfig.Text = GlobalClass.conContact1;
            }
        }

        protected void LoadAdv()
        {
            var dsAdv =
                db.tbAdvertises.Where(s => s.advActive == 1 && s.advLang == lang && s.advPosition == 24)
                    .OrderBy(s => s.advOrd)
                    .ToList();
            if (dsAdv.Any())
            {
                string strAdv = "";
                strAdv += "<div class='advByGroup' id='advCat'>";
                for (int i = 0; i < dsAdv.Count(); i++)
                {
                    strAdv += "<div class='advItem'>";
                    strAdv += "<a class='advItemLink' href='" + dsAdv[i].advLink + "' title='" + dsAdv[i].advName + "' target='" + dsAdv[i].advTarget + "'><img class='advItemImg' src='" + dsAdv[i].advImage + "' alt='" + dsAdv[i].advName + "' /></a>";
                    strAdv += "</div>";
                }
                strAdv += "</div>";
                ltrAdv.Text = strAdv;
            }
        }
        protected void btnSend_Click(object sender, EventArgs e)
        {
            imgCaptcha.ValidateCaptcha(txtCaptcha.Text);
            if (!imgCaptcha.UserValidated)
            {
                Session["noidung"] = txtnoidung.Text;
                Session["hoten"] = txtname.Text;
                Session["diachi"] = txtdiachi.Text;
                Session["sdtt"] = txtDienthoai.Text;

                Session["codinh"] = txtDienthoaicodinh.Text;
                Session["faxx"] = txtFax.Text;
                Session["emaill"] = txtmail.Text;           
                lblthongbao.Text = MyWeb.Global.GetLangKey("mess_wrong_code");
                txtCaptcha.Focus();
                return;
            }
            if (Page.IsValid)
            {
                #region[Sendmail]
                List<tbConfigDATA> list = tbConfigDB.tbConfig_GetLang(lang);
                string mailfrom = list[0].conMail_Noreply;
                string mailto = list[0].conMail_Info;
                string pas = list[0].conMail_Pass;
                string host = host = "smtp.gmail.com";
                if (list[0].conMail_Method.Length > 0)
                {
                    host = list[0].conMail_Method;
                }
                int post = 465;
                if (list[0].conMail_Port.Length > 0)
                {
                    post = int.Parse(list[0].conMail_Port);
                }
                string cc = "";
                string Noidung = "";
                Noidung = "<table><tr><td><b>Họ tên:</b></td><td>" + txtname.Text + "</td></tr>";
                Noidung += "<tr><td><b>Địa chỉ:</b></td><td>" + txtdiachi.Text + "</td></tr>";
                Noidung += "<tr><td><b>Điện thoại:</b></td><td>" + txtDienthoai.Text + "</td></tr>";
                Noidung += "<tr><td><b>Email:</b></td><td>" + txtmail.Text + "</td></tr>";
                Noidung += "<tr><td><b>Nội dung:</b></td><td>" + txtnoidung.Text + "</td></tr></table>";

                try
                {
                    common.SendMail(mailto, cc, "", "", "Thông tin liên hệ từ " + txtname.Text, Noidung);
                    common.SendMail(txtmail.Text.Trim(), "", "", "", "Thông tin liên hệ trên website " + Request.Url.Host, Noidung);
                    lblthongbao.Text = "Bạn đã gửi thành công !!";
                }
                catch
                {
                    lblthongbao.Text = "Lỗi không gửi được liên hệ !!";
                }
                #endregion
                string file_path = "";
                tbContactDATA ObjtbContact = new tbContactDATA();
                ObjtbContact.conName = common.killChars(txtname.Text);
                ObjtbContact.conCompany = "";
                ObjtbContact.conAddress = common.killChars(txtdiachi.Text);
                ObjtbContact.conTel = common.killChars(txtDienthoai.Text);
                ObjtbContact.conMail = common.killChars(txtmail.Text);
                ObjtbContact.conDetail = common.killChars(txtnoidung.Text);
                ObjtbContact.conActive = "0";
                ObjtbContact.conLang = lang;
                ObjtbContact.conPositions = common.killChars(txtDienthoaicodinh.Text);
                ObjtbContact.confax = common.killChars(txtFax.Text);
                ObjtbContact.confile = file_path;
                ObjtbContact.conwebsite = "";
                if (tbContactDB.tbContact_Add(ObjtbContact))
                {
                    lblthongbao.Text = MyWeb.Global.GetLangKey("mess_contact");
                    txtname.Text = "";
                    txtDienthoaicodinh.Text = "";
                    txtdiachi.Text = "";
                    txtDienthoai.Text = "";
                    txtFax.Text = "";
                    txtmail.Text = "";
                    txtnoidung.Text = "";
                    txtCaptcha.Text = "";
                }
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtname.Text = "";
            txtDienthoaicodinh.Text = "";
            txtdiachi.Text = "";
            txtDienthoai.Text = "";
            txtFax.Text = "";
            txtmail.Text = "";
            txtnoidung.Text = "";
            txtCaptcha.Text = "";
        }
    }
}