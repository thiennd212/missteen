﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucContestantSearch.ascx.cs" Inherits="MyWeb.views.search.ucContestantSearch" %>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f7e9d12301308fa" async="async"></script>
<%--<%@ Register Src="~/views/pages/ucBookNow.ascx" TagPrefix="uc100" TagName="ucBookNow" %>--%>
<link href="../../uploads/layout/default/css/layout.css" rel="stylesheet" />
<link href="../../theme/default/css/styles.css" rel="stylesheet" />

<div id="listContestant">
    <asp:Literal runat="server" ID="ltrHeader"></asp:Literal>
    <div class="listTourByCat_content">
        <asp:Literal runat="server" ID="ltrContent"></asp:Literal>
    </div>
    <asp:Literal runat="server" ID="ltrPaging"></asp:Literal>
</div>

<div class="block"></div>

<div class="footer-nav" id="footer">
    <div class="row container-footer">
        <div class="col-md-1">
            <p class="input-group text-muted credit">
                <div class="btn-group">
                    <span class="input-group-btn">
                        <button id="btnSearch" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-search"></i>
                            <i class="fa fa-caret-up"></i>
                        </button>
                        <ul class="dropdown-menu drop-up" role="menu">
                            <li class="liFooter" onclick="btnSearchTS1(0);">Tìm theo số báo danh</li>
                            <li class="divider"></li>
                            <li class="liFooter" onclick="btnSearchTS1(1);">Tìm theo Tỉnh/Thành phố</li>
                            <li class="divider"></li>
                        </ul>
                    </span>
                </div>
            </p>
        </div>
        <div class="col-md-3">
            <asp:TextBox ID="txtSearch" runat="server" class="form-control" placeholder="Từ khóa tìm kiếm" onkeypress="return EnterEvent1(event)"></asp:TextBox>
            <asp:Button ID="Button1" runat="server" Style="display: none" Text="Button" />
            <asp:HiddenField runat="server" ID="hfValue" />
        </div>
        <div class="col-md-1">
            <p class="text-muted credit">
                <div class="btn-group">
                    <span class="input-group-btn">
                        <button id="btnFilter" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-filter"></i>
                            <i class="fa fa-caret-up"></i>
                        </button>
                        <ul class="dropdown-menu drop-up" role="menu">
                            <li class="liFooter">Tìm theo Độ tuổi<i class="fa fa-filter"></i>
                                <ul>

                                    <li>
                                        <div class="cus-checkbox">
                                            <input id="n1" type="checkbox" checked="" value="1997,1998">

                                            <label for="n1">Sinh năm 1998</label>
                                        </div>
                                        <!-- div.cus-checkbox -->
                                    </li>
                                    <li>
                                        <div class="cus-checkbox">
                                            <input id="n2" type="checkbox" checked="" value="1999">

                                            <label for="n2">Sinh năm 1999</label>
                                        </div>
                                        <!-- div.cus-checkbox -->
                                    </li>
                                    <li>
                                        <div class="cus-checkbox">
                                            <input id="n3" type="checkbox" checked="" value="2000">

                                            <label for="n3">Sinh năm 2000</label>
                                        </div>
                                        <!-- div.cus-checkbox -->
                                    </li>
                                    <li>
                                        <div class="cus-checkbox">
                                            <input id="n4" type="checkbox" checked="" value="2001">

                                            <label for="n4">Sinh năm 2001</label>
                                        </div>
                                        <!-- div.cus-checkbox -->
                                    </li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                            <li class="liFooter" onclick="btnSearchTS1(6,0);">Xếp theo bình chọn giảm dần<i class="fa fa-filter"></i><i class="fa fa-arrow-down"></i></li>
                            <li class="divider"></li>
                            <li class="liFooter" onclick="btnSearchTS1(6,0);">Xếp theo bình chọn tăng dần<i class="fa fa-filter"></i><i class="fa fa-arrow-up"></i></li>
                            <li class="divider"></li>
                            <li class="liFooter" onclick="btnSearchTS1(6,0);">Xếp theo Like, Share giảm dần<i class="fa fa-filter"></i><i class="fa fa-arrow-down"></i></li>
                            <li class="divider"></li>
                            <li class="liFooter" onclick="btnSearchTS1(6,0);">Xếp theo Like, Share tăng dần<i class="fa fa-filter"></i><i class="fa fa-arrow-up"></i></li>
                            <li class="divider"></li>
                            <li class="liFooter" onclick="btnSearchTS1(6,0);">Xếp theo số báo danh giảm dần<i class="fa fa-filter"></i><i class="fa fa-arrow-down"></i></li>
                            <li class="divider"></li>
                            <li class="liFooter" onclick="btnSearchTS1(6,0);">Xếp theo số báo danh tăng dần<i class="fa fa-filter"></i><i class="fa fa-arrow-up"></i></li>
                        </ul>

                    </span>
                </div>
            </p>
        </div>
        <div class="col-md-3">
            <div class="col-md-3" id="lblFooter">
                <span>Bạn đang xem</span><asp:Label runat="server" ID="lblCount" />
                <span>/ </span>
                <asp:Label runat="server" ID="lblTotal" />
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(window).scroll(function (event) {
            function footer() {
                var scroll = $(window).scrollTop();
                if (scroll > 50) {
                    $(".footer-nav").fadeIn("slow").addClass("show");
                }
                else {
                    $(".footer-nav").fadeOut("slow").removeClass("show");
                }

                clearTimeout($.data(this, 'scrollTimer'));
                $.data(this, 'scrollTimer', setTimeout(function () {
                    if ($('.footer-nav').is(':hover')) {
                        footer();
                    }
                    else {
                        $(".footer-nav").fadeOut("slow");
                    }
                }, 2000));
            }
            footer();
        });

        function EnterEvent1(e) {

            if (e.keyCode == 13) {
                __doPostBack('<%=Button1.UniqueID%>', "");
            }

        }

        function btnSearchTS1(value) {
            var hfValue = document.getElementById('<%= hfValue.ClientID %>');
            hfValue.value = value;
        }
    </script>
