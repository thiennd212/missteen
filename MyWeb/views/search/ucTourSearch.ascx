﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucTourSearch.ascx.cs" Inherits="MyWeb.views.search.ucTourSearch" %>
<div class="box-search-product box-news-sub">
    <div class="cate-header sub-top">
        <div class="txt-name-sub">
            <%= MyWeb.Global.GetLangKey("search_result") %>
        </div>
    </div>
    <div id="listTourByCat">
        <div class="listTourByCat_content">
            <asp:Literal runat="server" ID="ltrContent"></asp:Literal>
        </div>
        <div class="clearfix">
            <asp:Literal runat="server" ID="ltrPaging"></asp:Literal>
        </div>
    </div>
</div>
