﻿using MyWeb.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.search
{
    public partial class ucContestantSearch : System.Web.UI.UserControl
    {
        string lang = "vi";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                LoadData();
            }
        }
        protected void LoadData()
        {
            string type = !String.IsNullOrEmpty(Request.QueryString["type"]) ? Request.QueryString["type"].ToString() : "";
            string input = !String.IsNullOrEmpty(Request.QueryString["input"]) ? Request.QueryString["input"].ToString() : "";
            int currentPage = Request.RawUrl.Contains("page") ? int.Parse(common.GetParameterFromUrl(Request.RawUrl, "page")) : 1;
            string strResult = "";
            int pageSize = 10;

            ltrHeader.Text = "<div class='listTourHeader'><a title='Danh sách thí sinh' href='/danh-sach-thi-sinh.html'>Danh sách thí sinh</a></div>";

            if (type == "0")
            {
                var listContestant =
                    db.Contestants.Where(s => s.Lang == lang && s.Active == 1 && (s.IdentificationNumber.ToString().Contains(input)
                        || s.FullName.Contains(txtSearch.Text))).ToList();

                if (listContestant.Any())
                {
                    int count = listContestant.Count;

                    listContestant = listContestant.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                    foreach (var item in listContestant)
                    {
                        strResult += "<div class='tourItems'>";
                        strResult += "  <div class='contestantItems_info1'>";
                        strResult += "      <span class='tourItems_price'><strong>SBD: " + item.IdentificationNumber + " </strong></span>";
                        strResult += "      <a class='tourItems_img' href='/" + item.TagName + ".html' title='" + item.FullName +
                                     "'><img src='" + item.Image1 + "' alt='" + item.FullName + "' /></a>";
                        strResult += "      <span class='tourItems_price'><strong>" + item.FullName + " </strong></span>";
                        strResult += "      <button class='btn btn-primary'>Bình chọn</button><span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
                        strResult += "  </div>";
                        strResult += "</div>";
                    }

                    ltrContent.Text = strResult;
                    ltrPaging.Text = common.PopulatePager(count, currentPage, pageSize);
                }
            }
            else
            {
                var listContestant =
                    db.Contestants.Where(s => s.Lang == lang && s.Active == 1 && s.ContestantPlace.Contains(input)).ToList();

                if (listContestant.Any())
                {
                    int count = listContestant.Count;

                    listContestant = listContestant.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                    foreach (var item in listContestant)
                    {
                        strResult += "<div class='tourItems'>";
                        strResult += "  <div class='contestantItems_info1'>";
                        strResult += "      <span class='tourItems_price'><strong>SBD: " + item.IdentificationNumber + " </strong></span>";
                        strResult += "      <a class='tourItems_img' href='/" + item.TagName + ".html' title='" + item.FullName +
                                     "'><img src='" + item.Image1 + "' alt='" + item.FullName + "' /></a>";
                        strResult += "      <span class='tourItems_price'><strong>" + item.FullName + " </strong></span>";
                        strResult += "      <button class='btn btn-primary'>Bình chọn</button><span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
                        strResult += "  </div>";
                        strResult += "</div>";
                    }

                    ltrContent.Text = strResult;
                    ltrPaging.Text = common.PopulatePager(count, currentPage, pageSize);
                }
            }
        }

        protected string GetName(int plId)
        {
            var item = db.tbPages.FirstOrDefault(s => s.pagId == plId);

            return item != null ? item.pagName : "";
        }

        protected string GetListPlaceName(int tourId)
        {
            var listPl = db.TourPlaces.Where(s => s.IdTour == tourId).Select(s => s.IdPlace).ToList();
            var listPage = db.tbPages.Where(s => listPl.Contains(s.pagId)).ToList();
            string strResult = "";

            if (listPage.Any())
            {

                foreach (var item in listPage)
                {
                    strResult += strResult != "" ? "</br><span class='itemTours_placeItem'>" + item.pagName + "</span>" : "<span class='itemTours_placeItem'>" + item.pagName + "</span>";
                }
            }

            return strResult;
        }
    }
}