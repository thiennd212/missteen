﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucTourSearchBox : System.Web.UI.UserControl
    {
        string lang = "vi";
        dataAccessDataContext db = new dataAccessDataContext();
        private string inputValue = MyWeb.Global.GetLangKey("search_text");
        protected void Page_Load(object sender, EventArgs e)
        {
            //this.Page.Form.DefaultButton = btnSubmitSearch.UniqueID;
            LoadPlace();
            rdCheckTourIn.Checked = true;
        }

        protected void LoadPlace()
        {
            var listPlace = db.tbPages.Where(s => s.pagActive == 1 && s.pagLang == lang && s.pagType == int.Parse(pageType.TourPlace) && s.pagId != 1025 && s.pagId != 1026).OrderByDescending(s => s.pagOrd).ToList();

            List<string> lstThoiGian = new List<string>();
            lstThoiGian.Add("1 ngày");
            lstThoiGian.Add("2 ngày");
            lstThoiGian.Add("3 ngày");
            lstThoiGian.Add("4 ngày");
            lstThoiGian.Add("5 ngày");
            lstThoiGian.Add("6 ngày");
            lstThoiGian.Add("7 ngày");
            lstThoiGian.Add("8 ngày");
            lstThoiGian.Add("9 ngày");
            lstThoiGian.Add("10 ngày");
            lstThoiGian.Add(">10 ngày");

            drDateTime.Items.Clear();
            drDateTime.Items.Add(new ListItem("- Chọn thời gian", "0"));
            foreach (var item in lstThoiGian)
            {
                drDateTime.Items.Add(new ListItem(item, item));
            }

            drPlaceFrom.Items.Clear();
            drPlaceFrom.Items.Add(new ListItem("- Chọn điểm đi", "0"));
            drPlaceTo.Items.Clear();
            drPlaceTo.Items.Add(new ListItem("- Chọn điểm đến", "0"));

            if (listPlace.Any())
            {
                foreach (var item in listPlace)
                {
                    drPlaceFrom.Items.Add(new ListItem(item.pagName, item.pagId.ToString()));
                    drPlaceTo.Items.Add(new ListItem(item.pagName, item.pagId.ToString()));
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string placeFrom = drPlaceFrom.SelectedItem.Value;
            string placeTo = drPlaceTo.SelectedItem.Value;
            string time = drDateTime.SelectedItem.Value;            
            var catIn = db.tbPages.FirstOrDefault(s => s.pagName.ToLower().Trim() == "tour trong nước" && s.pagLang == lang && s.pagActive == 1 && s.pagType == int.Parse(pageType.TourPlace));
            var catOut = db.tbPages.FirstOrDefault(s => s.pagName.ToLower().Trim() == "tour nước ngoài" && s.pagLang == lang && s.pagActive == 1 && s.pagType == int.Parse(pageType.TourPlace));
            int inOut = (rdCheckTourIn.Checked) ? catIn.pagId : catOut.pagId;
            Response.Redirect("/thi-sinh-tim-kiem.html?inout=" + inOut + "&from=" + placeFrom + "&to=" + placeTo + "&time=" + time);
        }
    }
}