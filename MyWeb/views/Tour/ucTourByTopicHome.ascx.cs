﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyWeb.control.panel.system;

namespace MyWeb.views.Tour
{
    public partial class ucTourByTopicHome : System.Web.UI.UserControl
    {
        private dataAccessDataContext db = new dataAccessDataContext();
        public string lang = "vi";
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                LoadData();    
            }
        }

        protected void LoadData()
        {
            string strResult = "";
            var listTopic =
                db.tbPages.Where(s => s.pagLang == lang && s.pagType == int.Parse(pageType.TourTopic) && s.pagActive == 1).OrderByDescending(s => s.pagOrd).ToList();

            if (listTopic.Any())
            {
                foreach (var topic in listTopic)
                {
                    var listTour =
                        db.Tours.Where(s => s.IdTourTopic == topic.pagId).OrderByDescending(s => s.Id).Take(6).ToList();

                    if (listTour.Any())
                    {
                        strResult += "<div class='listTourByTopic'>";
                        strResult += "<div class='item_header'>" + topic.pagName + "</div>";
                        strResult += "<div class='item_content'>";
                        foreach (var item in listTour)
                        {
                            strResult += "<div class='tourItems'>";
                            strResult += "  <div class='tourItems_info1'>";
                            strResult += "      <a class='tourItems_img' href='/" + item.Tagname + ".html' title='" + item.Name +
                                         "'><img src='" + item.Image1 + "' alt='" + item.Name + "' /></a>";
                            strResult += "      <a class='tourItems_link' href='/" + item.Tagname + ".html' title='" + item.Name + "'>" + item.Name + "</a>";
                            strResult += "      <span class='tourItems_priceOrifin'><strong>Giá: </strong>" + String.Format("{0:0,0}", item.PriceOrigin) +
                                 " " + item.PriceUnit + "</span>";
                            strResult += "      <span class='tourItems_price'><strong>Khuyến mại: </strong>" + String.Format("{0:0,0}", item.Price) + " " +
                                         item.PriceUnit + "</span>";
                            strResult += "      <span class='tourItems_topic'><strong>Chủ đề: </strong>" + GetName(item.IdTourTopic.Value) + "</span>";
                            strResult += "      <span class='tourItems_cat'><strong>Nhóm tour: </strong>" + GetName(item.IdTourCategory.Value) + "</span>";
                            strResult += "      <span class='tourItems_des'>" + MyWeb.Common.StringClass.GetContent(item.Description, 150) + "</span>";
                            strResult += "  </div>";
                            strResult += "  <div class='tourItems_info2'>";
                            strResult += "      <div class='tourItems_placeStart'><span>Điểm đi: </span>" + GetName(item.IdPlace.Value) + "</div>";
                            strResult += "      <div class='tourItems_placeEnd'><span>Điểm đến: </span>" + GetListPlaceName(item.Id) + "</div>";
                            strResult += "  </div>";
                            strResult += "  <div class='tourItems_info3'>";
                            strResult += "      <span class='tourItems_start'><strong>Thời gian khởi hành: </strong>" + item.Start +
                                         "</span>";
                            strResult += "      <span class='tourItems_time'><strong>Số ngày: </strong>" + item.NumberOfDay + "</span>";
                            strResult += "      <span class='tourItems_tran'><strong>Phương tiện: </strong>" + item.Transportation +
                                         "</span>";
                            strResult += "  </div>";
                            strResult += "</div>";
                        }
                        strResult += "</div>";
                        strResult += "</div>";
                    }
                }
            }

            ltrContent.Text = strResult;
        }

        protected string GetName(int plId)
        {
            var item = db.tbPages.FirstOrDefault(s => s.pagId == plId);

            return item != null ? item.pagName : "";
        }

        protected string GetListPlaceName(int tourId)
        {
            var listPl = db.TourPlaces.Where(s => s.IdTour == tourId).Select(s => s.IdPlace).ToList();
            var listPage = db.tbPages.Where(s => listPl.Contains(s.pagId)).ToList();
            string strResult = "";

            if (listPage.Any())
            {

                foreach (var item in listPage)
                {
                    strResult += strResult != "" ? "</br><span class='itemTours_placeItem'>" + item.pagName + "</span>" : "<span class='itemTours_placeItem'>" + item.pagName + "</span>";
                }
            }

            return strResult;
        }
    }
}