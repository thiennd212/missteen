﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyWeb.control.panel.system;

namespace MyWeb.views.Tour
{
    public partial class listTour : System.Web.UI.UserControl
    {
        dataAccessDataContext db =new dataAccessDataContext();
        public string lang = "vi";
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        protected void LoadData()
        {
            var listCat = new List<tbPage>();
            int currentPage = Request.RawUrl.Contains("page") ? int.Parse(common.GetParameterFromUrl(Request.RawUrl, "page")) : 1;
            string strResult = "";
            int pageSize = 10;

            if (Request["hp"] != null)
            {
                var parentCat =
                    db.tbPages.FirstOrDefault(
                        s => s.pagLang == lang && s.pagActive == 1 && s.pagType == int.Parse(pageType.TourCategory) && s.pagTagName == Request["hp"].ToString());
                if (parentCat != null)
                {
                    listCat =
                        db.tbPages.Where(
                            s =>
                                s.pagLang == lang && s.pagActive == 1 && s.pagType == int.Parse(pageType.TourCategory) &&
                                s.paglevel.StartsWith(parentCat.paglevel)).ToList();

                    ltrHeader.Text = "<div class='listTourHeader'><a title='" + parentCat.pagName + "' href='/" +
                                 parentCat.pagTagName + ".html'>" + parentCat.pagName + "</a></div>";
                }                
            }
            else
            {
                listCat =
                    db.tbPages.Where(
                        s => s.pagActive == 1 && s.pagLang == lang && s.pagType == int.Parse(pageType.TourCategory))
                        .ToList();

                ltrHeader.Text = "<div class='listTourHeader'><a title='Danh sách tour' href='/danh-sach-nhom-tour.html'>Danh sách tour</a></div>";
            }

            var arrId = listCat.Select(s => s.pagId).ToArray();
            var listTour =
                db.Tours.Where(s => s.Lang == lang && s.Active == 1 && arrId.Contains(s.IdTourCategory.Value)).ToList();

            if (listTour.Any())
            {
                int count = listTour.Count;

                listTour = listTour.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                foreach (var item in listTour)
                {
                    strResult += "<div class='tourItems'>";
                    strResult += "  <div class='tourItems_info1'>";
                    strResult += "      <a class='tourItems_img' href='/" + item.Tagname + ".html' title='" + item.Name +
                                 "'><img src='" + item.Image1 + "' alt='" + item.Name + "' /></a>";
                    strResult += "      <a class='tourItems_link' href='/" + item.Tagname + ".html' title='" + item.Name + "'>" + item.Name + "</a>";
                    strResult += "      <span class='tourItems_priceOrifin'><strong>Giá: </strong>" + String.Format("{0:0,0}", item.PriceOrigin) +
                                 " " + item.PriceUnit + "</span>";
                    strResult += "      <span class='tourItems_price'><strong>Khuyến mại: </strong>" + String.Format("{0:0,0}", item.Price) + " " +
                                 item.PriceUnit + "</span>";
                    strResult += "      <span class='tourItems_topic'><strong>Chủ đề: </strong>" + GetName(item.IdTourTopic.Value) + "</span>";
                    strResult += "      <span class='tourItems_cat'><strong>Nhóm tour: </strong>" + GetName(item.IdTourCategory.Value) + "</span>";
                    strResult += "      <span class='tourItems_des'>" + MyWeb.Common.StringClass.GetContent(item.Description, 150) + "</span>";
                    strResult += "  </div>";
                    strResult += "  <div class='tourItems_info2'>";
                    strResult += "      <div class='tourItems_placeStart'><span>Điểm đi: </span>" + GetName(item.IdPlace.Value) + "</div>";
                    strResult += "      <div class='tourItems_placeEnd'><span>Điểm đến: </span>" + GetListPlaceName(item.Id) + "</div>";
                    strResult += "  </div>";
                    strResult += "  <div class='tourItems_info3'>";
                    strResult += "      <span class='tourItems_start'><strong>Thời gian khởi hành: </strong>" + item.Start +
                                 "</span>";
                    strResult += "      <span class='tourItems_time'><strong>Số ngày: </strong>" + item.NumberOfDay + "</span>";
                    strResult += "      <span class='tourItems_tran'><strong>Phương tiện: </strong>" + item.Transportation +
                                 "</span>";
                    strResult += "  </div>";
                    strResult += "</div>";
                }
                
                ltrContent.Text = strResult;
                ltrPaging.Text = common.PopulatePager(count, currentPage, pageSize);
            }
        }

        protected string GetName(int plId)
        {
            var item = db.tbPages.FirstOrDefault(s => s.pagId == plId);

            return item != null ? item.pagName : "";
        }

        protected string GetListPlaceName(int tourId)
        {
            var listPl = db.TourPlaces.Where(s => s.IdTour == tourId).Select(s => s.IdPlace).ToList();
            var listPage = db.tbPages.Where(s => listPl.Contains(s.pagId)).ToList();
            string strResult = "";

            if (listPage.Any())
            {

                foreach (var item in listPage)
                {
                    strResult += strResult != "" ? "</br><span class='itemTours_placeItem'>" + item.pagName + "</span>" : "<span class='itemTours_placeItem'>" + item.pagName + "</span>";
                }
            }

            return strResult;
        }
    }
}