﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucTourSearchBox.ascx.cs" Inherits="MyWeb.views.control.ucTourSearchBox" %>
<div id="listTourHot" class="listTour_all">
    <div class="listTour_label">
        <span>Tìm tour</span>
    </div>
    <div class="listTour_list">
        <div class="row">
            <div class="col-md-12 clearfix">
                <asp:RadioButton ID="rdCheckTourIn" runat="server" GroupName="SearchTour" Text="Tour trong nước" />
                <asp:RadioButton ID="rdCheckTourOut" runat="server" GroupName="SearchTour" Text="Tour nước ngoài" />
            </div>
        </div>
        <div class="row clearfix">
            <label class="col-md-3">Khởi hành</label>
            <div class="col-md-9">
                <asp:DropDownList ID="drPlaceFrom" runat="server" class="form-control"></asp:DropDownList>
            </div>
        </div>
        <div class="row clearfix">
            <label class="col-md-3">Điểm đến</label>
            <div class="col-md-9">
                <asp:DropDownList ID="drPlaceTo" runat="server" class="form-control"></asp:DropDownList>
            </div>
        </div>
        <div class="row clearfix">
            <label class="col-md-3">Thời gian</label>
            <div class="col-md-9">
                <asp:DropDownList ID="drDateTime" runat="server" class="form-control"></asp:DropDownList>
            </div>
        </div>
        <div class="row clearfix">
            <label class="col-md-3"></label>
            <div class="col-md-9">
                <div class="col-md-6 padding-none">
                    <%--<asp:Button ID="btnSubmitSearch" runat="server" class="form-control" Text="Tìm kiếm" OnClick="btnSearch_Click" />--%>
                </div>
            </div>
        </div>
    </div>
</div>
