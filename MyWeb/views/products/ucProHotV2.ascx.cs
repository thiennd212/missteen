﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.products
{
    public partial class ucProHotV2 : System.Web.UI.UserControl
    {
        string lang = "vi";
        public bool showAdv = false;
        string viewBy = GlobalClass.viewProducts9;
        private string strNumberView = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();

            if (!IsPostBack)
            {
                BindData();
            }
        }
        private void BindData()
        {
            string _str = "";
            if (GlobalClass.viewProducts11 != "") { strNumberView = GlobalClass.viewProducts11; }
            IEnumerable<tbProduct> objProduct = db.tbProducts.Where(s => s.proActive == 1 && s.proNoibat == 1 && s.proLang.Trim() == lang).OrderByDescending(s => s.proDate).ThenByDescending(s => s.proId);
            objProduct = objProduct.Skip(0).Take(int.Parse(strNumberView));
            if (objProduct.Count() > 0)
            {
                showAdv = true;
                //int i = 0;
                //foreach (tbProduct item in objProduct)
                //{
                //    _str += "<li class=\"item-list\">";
                //    if (item.proImage.Length > 0)
                //    {
                //        _str += "<a class=\"img-view\" href=\"/" + item.proTagName + ".html\" title=\"" + item.proName.ToString() + "\"><img src=\"" + item.proImage.Split(Convert.ToChar(","))[0] + "\" alt=\"" + item.proName.ToString() + "\"/></a>";
                //    }
                //    else
                //    {
                //        _str += "<a class=\"img-view\" title=\"" + item.proName.ToString() + "\" href=\"/" + item.proTagName + ".html\"><img src=\"images/no_image.jpg\" alt=\"" + item.proName.ToString() + "\"/></a>";
                //    }
                //    _str += "<a class=\"link-view\" title=\"" + item.proName.ToString() + "\" href=\"/" + item.proTagName + ".html\"><h3>" + item.proName.ToString() + "</h3></a>";
                //    _str += "</li>";
                //    i++;
                //}
                //ltrProduct.Text = _str;
                ltrProduct.Text = common.LoadProductListV2(objProduct);
            }
            
        }
    }
}