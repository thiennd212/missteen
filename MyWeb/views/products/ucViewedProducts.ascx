﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucViewedProducts.ascx.cs" Inherits="MyWeb.views.products.ucViewedProducts" %>

<%if (showAdv)
  {%>
<div class="box-product-new box-product-viewed">
    <div class="header">
        <%=MyWeb.Global.GetLangKey("viewed_products") %>
    </div>
    <ul class="body-pro">
        <asp:Literal ID="ltrProduct" runat="server"></asp:Literal>
    </ul>
</div>
<%} %>