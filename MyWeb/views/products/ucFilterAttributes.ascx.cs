﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.products
{
    public partial class ucFilterAttributes : System.Web.UI.UserControl
    {
        private string lang = "";
        public string catId = "";
        string strCatID = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = Global.GetLang();
            if (Request["cat"] != null)
            {
                try { catId = Request["cat"].ToString(); }
                catch { }
            }

            try
            {
                if (Request["hp"] != null) { strCatID = db.tbPages.Where(s => s.pagType == int.Parse(pageType.GP) && s.pagTagName == Request["hp"].ToString()).FirstOrDefault().pagId.ToString(); }
                string iCat = strCatID;
            }
            catch { }

            if (!IsPostBack)
            {
                BindData();
            }
        }
        private void BindData()
        {
            StringBuilder str = new StringBuilder();

            if (strCatID != "")
            {
                tbPage category = db.tbPages.FirstOrDefault(s => s.pagId == int.Parse(strCatID));
                if (category.paglevel.Length > 0)
                {
                    var pagelistid = db.tbPages.Where(u => u.paglevel.StartsWith(category.paglevel) && u.pagType == int.Parse(pageType.GP) && u.pagActive == 1 && u.pagLang == lang).ToList();
                    var arrCatId = pagelistid.Select(s => s.pagId).ToArray();
                    var listpro = db.tbProducts.Where(s => s.proActive == 1 && s.proLang == lang && arrCatId.Contains(Convert.ToInt32(s.catId))).ToList();

                    string[] _strProID = listpro.Select(s=>s.proId.ToString()).ToArray();

                    string[] obj = (from a in db.tbAttributes join  b in db.tbAttPros on a.Id equals b.attId 
                              where _strProID.Contains(b.proId.ToString()) select a.Id.ToString()).ToArray();

                    IEnumerable<tbAttribute> listAtt = db.tbAttributes.Where(a => a.Lang == lang && a.Active == 1 && a.Level.Length == 5);

                    foreach (tbAttribute item in listAtt)
                    {
                        IEnumerable<tbAttribute> listAttsub = (IEnumerable<tbAttribute>)from a in db.tbAttributes
                                                                                        where a.Level.StartsWith(item.Level) && a.Level.Length > 5
                                                                                        select a;
                        listAttsub = listAttsub.ToList();
                        listAttsub = listAttsub.Distinct();

                        listAttsub = listAttsub.Where(s => obj.Contains(s.Id.ToString()));
                        if (listAttsub.Count() > 0)
                        {
                            str.Append("<div class=\"header\">" + item.Name + "</div><ul class=\"box-body-fillter\">");
                            foreach (tbAttribute item2 in listAttsub)
                            {
                                if (item2.Image.Length > 0)
                                    str.Append("<li class=\"att_type2_chk\"><input type=\"checkbox\" name=\"checksub\" value=\"" + item2.Level + "\" class=\"att_type2_chkbox box" + item2.Level + "\" /><img src=\"" + item2.Image + "\" width=\"12px\" height=\"12px\" style=\"margin: 1px 4px 0 0px;\"/>" + item2.Name + "</li>");
                                else
                                    str.Append("<li class=\"att_type2_chk\"><input type=\"checkbox\" name=\"checksub\" value=\"" + item2.Level + "\" class=\"att_type2_chkbox box" + item2.Level + "\" />" + item2.Name + "</li>");
                            }
                            str.Append("</ul>");
                        }
                    }


                }

                ltrData.Text = str.ToString();
            }
            else
            {
                IEnumerable<tbAttribute> listAtt = db.tbAttributes.Where(a => a.Lang == lang && a.Active == 1 && a.Level.Length == 5);
                foreach (tbAttribute item in listAtt)
                {
                    IEnumerable<tbAttribute> listAttsub = (IEnumerable<tbAttribute>)from a in db.tbAttributes
                                                                                    where a.Level.StartsWith(item.Level) && a.Level.Length > 5
                                                                                    select a;
                    listAttsub = listAttsub.ToList();
                    listAttsub = listAttsub.Distinct();
                    if (listAttsub.Count() > 0)
                    {
                        str.Append("<div class=\"header\">" + item.Name + "</div><ul class=\"box-body-fillter\">");
                        foreach (tbAttribute item2 in listAttsub)
                        {
                            if (item2.Image.Length > 0)
                                str.Append("<li class=\"att_type2_chk\"><input type=\"checkbox\" name=\"checksub\" value=\"" + item2.Level + "\" class=\"att_type2_chkbox box" + item2.Level + "\" /><img src=\"" + item2.Image + "\" width=\"12px\" height=\"12px\" style=\"margin: 1px 4px 0 0px;\"/>" + item2.Name + "</li>");
                            else
                                str.Append("<li class=\"att_type2_chk\"><input type=\"checkbox\" name=\"checksub\" value=\"" + item2.Level + "\" class=\"att_type2_chkbox box" + item2.Level + "\" />" + item2.Name + "</li>");
                        }
                        str.Append("</ul>");
                    }
                }
                ltrData.Text = str.ToString();
            }
        }
    }
}