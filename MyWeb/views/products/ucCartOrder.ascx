﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCartOrder.ascx.cs" Inherits="MyWeb.views.products.ucCartOrder" %>
<div class="box-oder-pay">
    <div class="cate-header sub-top">
        <div class="txt-name-sub">
            <asp:Literal ID="ltrHead" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="body-cart">
        <div class="row">
            <div class="form-group">
                <span class="view-cate">Thông tin khách hàng</span>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Họ và tên*:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtname" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="rfvname" runat="server" ControlToValidate="txtname" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Email:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RegularExpressionValidator ID="revmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="*" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True" Display="Dynamic"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3">Số di động:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtPhone" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2"><asp:RequiredFieldValidator ID="rfvDienthoai" runat="server" ControlToValidate="txtPhone" Display="Dynamic" ErrorMessage="(*)" SetFocusOnError="True"></asp:RequiredFieldValidator></div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3">Số cố định:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtPhoneCodinh" runat="server" class="form-control"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3">Hình thức thanh toán:</label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlthanhtoan" runat="server" class="form-control"></asp:DropDownList>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3">Địa chỉ:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAddress" runat="server" class="form-control"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3">Nội dung:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtContent" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"></label>
                <div class="col-md-7">
                    <asp:LinkButton ID="btnSend" runat="server" OnClick="btnSend_Click"><%= MyWeb.Global.GetLangKey("forgot_email_button") %></asp:LinkButton>
                    <asp:LinkButton ID="btnReset" runat="server" OnClick="btnReset_Click"><%= MyWeb.Global.GetLangKey("cart_remove")%></asp:LinkButton>
                </div>
            </div>

    </div>
</div>
