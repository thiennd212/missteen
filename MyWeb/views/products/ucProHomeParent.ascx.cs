﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.products
{
    public partial class ucProHomeParent : System.Web.UI.UserControl
    {
        string lang = "vi";
        private string strNumberView = GlobalClass.viewProducts2;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                BindData();
            }
        }

        private void BindData()
        {
            string _str = "";
            var lst = db.tbPages.Where(s => s.paglevel.Length == 5 && s.pagActive == 1 && s.check1.ToString() == "1" && s.pagLang.Trim() == lang).OrderBy(s => s.pagOrd).ToList();
            foreach (tbPage list in lst)
            {
                if (list != null)
                {
                    string link = "/" + MyWeb.Common.StringClass.NameToTag(list.pagTagName) + ".html";
                    _str += "<div class=\"group-product\"><div class=\"header\"><a href='/" + MyWeb.Common.StringClass.NameToTag(list.pagTagName) + ".html'>" + list.pagName + "</a></div>";
                    _str += "<div class=\"box-body\">";
                    string alt = list.pagName;
                    _str += "<div class=\"pro-view\">";
                    _str += "<div class=\"frame-img\">";
                    _str += String.Format("<a class=\"pro-img\" href=\"{0}\"><img src=\"{1}\" alt=\"{2}\"/></a>", link, list.pagImage, alt);
                    _str += "</div>";
                    _str += String.Format("<a class=\"pro-name\" href=\"{0}\">{1}</a>", link, list.pagName);
                    _str += "</div>";
                    _str += "</div></div>";
                }
            }
            ltrGroupPro.Text = _str;
        }
    }
}