﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProductByTab.ascx.cs" Inherits="MyWeb.views.products.ucProductByTab" %>
<div id="pro-soter">
    <div class="pro-store-content">
        <asp:Literal runat="server" ID="ltrTabName"></asp:Literal>
        <asp:Literal runat="server" ID="ltrTabContent"></asp:Literal>                
    </div>
</div>