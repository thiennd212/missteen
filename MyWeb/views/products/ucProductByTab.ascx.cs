﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.products
{
    public partial class ucProductByTab : System.Web.UI.UserControl
    {
        protected string lang = "vi";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        protected void LoadData()
        {
            string strName = "", strContent = "";
            var lstCat = db.tbPages.Where(s => s.pagLang == lang && s.pagActive == 1 && s.pagType == int.Parse(pageType.GP) && s.check1.ToString() == "1").ToList();
            if (lstCat.Count() > 0)
            {
                strName += "<ul id='tabs'>";
                strContent += "<div id='content'>";
                for (int i = 0; i < lstCat.Count(); i++)
                {
                    // Tab name
                    strName += "<li><a href='#' name='tab" + (i + 1) + "'>";
                    strName += "<div class='img-hover'><img src='" + lstCat[i].pagImage + "' alt='" + lstCat[i].pagName + "'></div>";
                    strName += "<span>" + lstCat[i].pagName + "</span>";
                    string catDes = "";
                    catDes = lstCat[i].check5.Length > 65 ? MyWeb.Common.StringClass.GetContent(lstCat[i].check5, 65) : lstCat[i].check5;
                    strName += "<p class='mota-nhom'>" + catDes + "</p>";
                    strName += "</a></li>";
                    // Tab content
                    strContent += "<div id='tab" + (i + 1) + "'>";
                    strContent += "<ul class='product-list'>";
                    int numberOfPro = 8;
                    try { numberOfPro = int.Parse(GlobalClass.viewProducts1); }
                    catch { numberOfPro = 8; }
                    var lstPro = db.tbProducts.Where(s => s.proActive == 1 && s.proLang == lang && Convert.ToDouble(s.catId) == lstCat[i].pagId).OrderByDescending(s => s.proId).Take(numberOfPro).ToList();
                    if (lstPro.Count() > 0)
                    {
                        for (int j = 0; j < lstPro.Count(); j++)
                        {
                            strContent += "<li class='item-list'><div class='frame-box'>";
                            strContent += "<a title='" + lstPro[j].proName + "' href='/" + lstPro[j].proTagName + ".html' class='frame-img'><img src='" + lstPro[j].proImage.Split(',')[0] + "' alt='" + lstPro[j].proName + "'></a>";
                            strContent += "<a title='" + lstPro[j].proName + "' href='/" + lstPro[j].proTagName + ".html' class='pro-name'><h3>" + lstPro[j].proName + "</h3></a>";
                            if (!String.IsNullOrEmpty(lstPro[j].proOriginalPrice) && Convert.ToDouble(lstPro[j].proOriginalPrice) != 0)
                                strContent += "<div class='price'><span>" + MyWeb.Global.GetLangKey("product_price") + ":</span> <span class='value'>" + Convert.ToDouble(lstPro[j].proOriginalPrice).ToString("N0") + "</span></div>";
                            else
                                strContent += "<div class='price'></div>";
                            strContent += "<div class='price-new'><span>" + MyWeb.Global.GetLangKey("product_price_sale") + ":</span> " + Convert.ToDouble(lstPro[j].proPrice).ToString("N0") + " </div>";
                            string strRecontent = "";
                            strRecontent = lstPro[j].proWarranty.Length > 200 ? MyWeb.Common.StringClass.GetContent(lstPro[j].proWarranty, 200) : lstPro[j].proWarranty;
                            strContent += "<div class='frame-content'>" + strRecontent + "</div>";
                            strContent += "<a onclick=\"addToCartView('" + lstPro[j].proId + "')\" title='" + lstPro[j].proName + "' class='link-order'>" + MyWeb.Global.GetLangKey("pro_button_order") + "</a><a class='link-view-short' onclick=\"openViewPro('" + lstPro[j].proId + "')\">" + MyWeb.Global.GetLangKey("view_fast") + "</a><a href='/" + lstPro[j].proTagName + ".html' title='" + lstPro[j].proName + "' class='link-views'>Chi tiết</a>";
                            if (!String.IsNullOrEmpty(lstPro[j].proOriginalPrice) && Convert.ToDouble(lstPro[j].proOriginalPrice) != 0)
                                strContent += "<div class='frame-percent'><span class='oil'>- </span><span class='number'>" + getPercent(lstPro[j].proOriginalPrice, lstPro[j].proPrice) + "</span><span class='percent'> % </span></div>";
                            strContent += "</div></li>";
                        }
                    }
                    strContent += "</ul>";
                    strContent += "</div>";
                }
                strContent += "</div>";
                strName += "</ul>";
            }
            ltrTabName.Text = strName;
            ltrTabContent.Text = strContent;
        }

        protected double getPercent(string priceOld, string priceNew)
        {
            return Math.Round((1 - (float.Parse(priceOld) / float.Parse(priceNew))) * 100);
        }
    }
}