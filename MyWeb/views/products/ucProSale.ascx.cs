﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.products
{
    public partial class ucProSale : System.Web.UI.UserControl
    {
        string lang = "vi";
        public bool showAdv = false;
        private string strNumberView = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                BindData();
            }
        }
        private void BindData()
        {
            string _str = "";
            if (GlobalClass.viewProducts3 != "") { strNumberView = GlobalClass.viewProducts3; }
            if (strNumberView == "")
            {
                strNumberView = "5";
            }
            IEnumerable<tbProduct> product = db.tbProducts.OrderByDescending(s => s.proDate).ThenByDescending(s => s.proId).Where(s => s.proActive == 1 && s.proBanchay == 1 && s.proLang.Trim() == lang);
            product = product.Skip(0).Take(int.Parse(strNumberView));
            if (product.Count() > 0)
            {
                int countss = product.Count();
                showAdv = true;
                int i = 0;
                foreach (tbProduct item in product)
                {

                    _str += "<li class=\"item-list\"><figure>";
                    if (item.proImage.Length > 0)
                    {
                        _str += "<a class=\"img-view\" href=\"/" + item.proTagName + ".html\" title=\"" + item.proName.ToString() + "\"><img src=\"" + item.proImage.Split(Convert.ToChar(","))[0] + "\" alt=\"" + item.proName.ToString() + "\"/></a>";
                    }
                    else
                    {
                        _str += "<a class=\"img-view\" title=\"" + item.proName.ToString() + "\" href=\"/" + item.proTagName + ".html\"><img src=\"images/no_image.jpg\" alt=\"" + item.proName.ToString() + "\"/></a>";
                    }
                    _str += "<a class=\"link-view\" title=\"" + item.proName.ToString() + "\" href=\"/" + item.proTagName + ".html\"><h3>" + item.proName.ToString() + "</h3></a>";
                    if (item.manufacturerId != null)
                    {
                        var m = db.tbPages.FirstOrDefault(x => x.pagId == item.manufacturerId);
                        if (m != null)
                            _str += "<figcaption class=\"pro-manu\"><b>Hãng sản xuất: </b>" + m.pagName + "</figcaption>";
                    }
                    if (!string.IsNullOrEmpty(item.proCode))
                        _str += "<figcaption class=\"pro-code\"><b>Mã sản phẩm: </b>" + item.proCode + "</figcaption>";
                    if (!string.IsNullOrEmpty(item.proWarranty))
                        _str += "<figcaption class=\"pro-warranty\"><b>Mô tả ngắn: </b>" + item.proWarranty + "</figcaption>";
                    if (!string.IsNullOrEmpty(item.proContent))
                        _str += "<figcaption class=\"pro-content\"><b>Nội dung tóm tắt: </b>" + item.proContent + "</figcaption>";
                    if (item.proCount != null)
                        _str += "<figcaption class=\"pro-count\"><b>Số lượng: </b>" + item.proCount + "</figcaption>";
                    if (item.proStatus == 0)
                        _str += "<figcaption class=\"pro-status-yes\"><b>Tình trạng: </b>Còn hàng</figcaption>";
                    else
                        _str += "<figcaption class=\"pro-status-no\"><b>Tình trạng: </b>Hết hàng</figcaption>";
                    if (item.proDate != null)
                    {
                        DateTime fromDate = Convert.ToDateTime(item.proDate);
                        string dateTu = common.ConvertDate(fromDate);
                        _str += "<figcaption class=\"pro-date\"><b>Ngày đăng: </b>" + dateTu + "</figcaption>";
                    }
                    _str += "<figcaption class=\"pro-price-no\"><b>Giá niêm yết: </b>" + String.Format("{0:0,0}", item.proOriginalPrice) + " " + item.proUnitprice + "</figcaption>";
                    _str += "<figcaption class=\"pro-price-yes\"><b>Giá bán: </b>" + String.Format("{0:0,0}", item.proPrice) + " " + item.proUnitprice + "</figcaption>";
                    if (!string.IsNullOrEmpty(item.proPromotions))
                        _str += "<figcaption class=\"pro-content-promotion\"><b>Nội dung khuyến mãi: </b>" + item.proPromotions + "</figcaption>";
                    if (item.proTungay != null)
                    {
                        DateTime fromDate = Convert.ToDateTime(item.proTungay);
                        string dateTu = common.ConvertDate(fromDate);
                        _str += "<figcaption class=\"pro-promotion-from\"><b>Khuyến mãi từ: </b>" + dateTu + "</figcaption>";
                    }
                    if (item.proDenngay != null)
                    {
                        DateTime fromDate = Convert.ToDateTime(item.proDenngay);
                        string dateTu = common.ConvertDate(fromDate);
                        _str += "<figcaption class=\"pro-promotion-to\"><b>Khuyến mãi đến: </b>" + dateTu + "</figcaption>";
                    }
                    _str += "</figure></li>";
                    i++;
                }
            }
            ltrProduct.Text = _str;
        }
    }
}