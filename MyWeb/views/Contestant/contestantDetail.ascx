﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="contestantDetail.ascx.cs" Inherits="MyWeb.views.Contestant.contestantDetail" %>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f8b7e455a803d6f" async="async"></script>
<div id="tourDetail">
    <div class="tourInfo">
        <input type="button" class="btn btn-info btn-lg" value="X" onclick="CloseToList()" />
        <div class="clearfix"></div>
        <div class="tourInfo_left">
            <asp:Literal runat="server" ID="ltrInfoLeft"></asp:Literal>
        </div>
        <div class="tourInfo_right">
            <div class="contestantItems_button_wrap_detail" id="wrap-detail-button">
                <asp:Literal runat="server" ID="ltrVote"></asp:Literal>
                <%--<div id="btnVoted">
                    <%if (Session["UserNameFB"] == null) {%>
                    <input type="button" class="btn btn-info btn-lg" value="Bình chọn" onclick="vote()" />
                    <%} %>
                </div>--%>
            </div>
            <asp:Literal runat="server" ID="ltrInfoRight"></asp:Literal>
            <div class="div-addthis-news">
                <div class="addthis_native_toolbox"></div>
            </div>
            <asp:HiddenField ID="hdTourName" runat="server" />
            <asp:HiddenField ID="hdContestantId" runat="server" />
            <asp:HiddenField ID="FBUserId" runat="server" />
            <asp:HiddenField ID="hfAccessToken" runat="server" />
            <asp:Button ID="btnhVote" runat="server" Style="display: none" Text="Button" OnClick="btnhVote_Click" />
            <asp:Button ID="btnhReload" runat="server" Style="display: none" Text="Button" OnClick="btnhReload_Click" />
        </div>
        <%if (GlobalClass.commentFNews.Contains("1"))
          {%><div class="shares">
              <div class="fb-comments" data-href="<%=strUrlFace %>" data-colorscheme="<%=cf %>" data-width="<%=wf %>" data-numposts="<%=nf %>"></div>
          </div>
        <%} %>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        var sync1 = $("#sync1");
        var sync2 = $("#sync2");

        sync1.owlCarousel({
            singleItem: true,
            slideSpeed: 1000,
            navigation: false,
            pagination: false,
            afterAction: syncPosition,
            responsiveRefreshRate: 200,
        });

        sync2.owlCarousel({
            items: 5,
            itemsDesktop: [1199, 5],
            itemsDesktopSmall: [979, 10],
            itemsTablet: [768, 8],
            itemsMobile: [479, 4],
            navigation: true,
            navigationText: ["«", "»"],
            rewindNav: false,
            scrollPerPage: false,
            slideSpeed: 1500,
            pagination: false,
            paginationNumbers: false,
            autoPlay: false,
            afterInit: function (el) {
                el.find(".owl-item").eq(0).addClass("synced");
            }
        });

        var r = {
            'special': /[\W]/g,
            'quotes': /[^0-9^]/g,
            'notnumbers': /[^a-zA]/g
        }
        function valid(o, w) {
            o.value = o.value.replace(r[w], '');
        }

        function MyFunction() {
            $("#myModal").css("display", "none");
        }

        function syncPosition(el) {
            var current = this.currentItem;
            $("#sync2")
              .find(".owl-item")
              .removeClass("synced")
              .eq(current)
              .addClass("synced")
            if ($("#sync2").data("owlCarousel") !== undefined) {
                center(current)
            }
        }

        $("#sync2").on("click", ".owl-item", function (e) {
            e.preventDefault();
            var number = $(this).data("owlItem");
            sync1.trigger("owl.goTo", number);
        });

        function center(number) {
            var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
            var num = number;
            var found = false;
            for (var i in sync2visible) {
                if (num === sync2visible[i]) {
                    var found = true;
                }
            }

            if (found === false) {
                if (num > sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                } else {
                    if (num - 1 === -1) {
                        num = 0;
                    }
                    sync2.trigger("owl.goTo", num);
                }
            } else if (num === sync2visible[sync2visible.length - 1]) {
                sync2.trigger("owl.goTo", sync2visible[1])
            } else if (num === sync2visible[0]) {
                sync2.trigger("owl.goTo", num - 1)
            }

        }

    });
</script>

<script type="text/javascript">
    CloudZoom.quickStart();
    $('#txtThoiGian').datepicker({
        todayBtn: "linked",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        startDate: '-3d',
    });
</script>
<script type="text/javascript">
    function SetUserName(value) {
        //PageMethods.SetUserName(value);
        $.ajax({
            async: false,
            type: "POST",
            url: "webService.asmx/SetSessionLoginCookie",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ userName: value }),
            success: function (response) {
                console.log(response.d);
            },
            failure: function (response) {
            }

        });
    }

    function LogoutInfoSS() {
        $.ajax({
            async: false,
            type: "POST",
            url: "webService.asmx/GetSessionLogin",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d != "" && response.d != null) {
                    if (response.d.IsLogin) {
                        $("#lgFacebook").html(response.d.Message)
                    }
                    else
                        $("#lgFacebook").html(response.d.Message);
                }
            },
            failure: function (response) {
            }

        });
    }

    function GetInfoUserFb(uid) {
        FB.api(
            "/" + uid + "",
            function (response) {
                if (response && !response.error) {
                    FB.api(
                        "/" + uid + "/picture",
                        function (responsex) {
                            if (responsex && !responsex.error) {
                                var para = uid + "," + response.name + "," + responsex.data.url;
                                var str = "<span class=\"avatar\"><img src=\"" + responsex.data.url + "\" alt=\"\"/></span> <span class=\"fullname\">" + response.name + "</span> (<a onclick=\"LogoutFb()\" href=\"javascript:{}\" title=\"Thoát\">Thoát</a>)";
                                $("#lgFacebook").html(str);
                            } else {
                                var para = uid;
                                var str = "<a href='javscript:{}' onclick=\"Login()\" class=\"facebook\" rel=\"nofollow\"><img src=\"uploads/layout/default/css/images/icon-fb-log.png\" alt=\"\"/></a>";
                                $("#lgFacebook").html(str);
                            }
                            SetUserName(para);
                        }
                    );
                } else {
                    LogoutInfoSS();
                }
            }
        );
    }

    function vote() {
        FB.getLoginStatus(function (response) {
            if (response.status === 'not_authorized' || response.status === 'unknown') {
                FB.login(function (response) {
                    if (response.authResponse) {
                        var uid = response.authResponse.userID;
                        GetInfoUserFb(uid);
                    }
                });
            } else if (response.status === 'connected') {
                var uid = response.authResponse.userID;
                var accessToken = response.authResponse.accessToken;

                var FBUserId = document.getElementById('<%= FBUserId.ClientID %>');
                FBUserId.value = uid;
                GetInfoUserFb(uid);

                var hfAccessToken = document.getElementById('<%= hfAccessToken.ClientID %>');
                hfAccessToken.value = accessToken;
                btnVoteYes();
            }
        }, true);
}

function RenderTotalVote() {
    var contestantId = document.getElementById('<%=hdContestantId.ClientID%>').value;
    $.ajax({
        async: false,
        type: "POST",
        url: "webService.asmx/RenderTotalVoted",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ contestantId: contestantId }),
        success: function (response) {
            $("#wrap-detail-button").html(response.d);
        },
        failure: function (response) {
        }

    });
};

function btnVoteYes() {
    var fbUserId = document.getElementById('<%=FBUserId.ClientID%>').value;
    var contestantId = document.getElementById('<%=hdContestantId.ClientID%>').value;
    var accessToken = document.getElementById('<%=hfAccessToken.ClientID%>').value;
    $.ajax({
        async: false,
        type: "POST",
        url: "webService.asmx/UpdateContestantReference",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ contestantId: contestantId, fbUserId: fbUserId, accessToken: accessToken }),
        success: function (response) {
            if (response.d === true)
            {
                $("#btnVoted").html("");
                $("#wrap-detail-button").addClass("wrap-detail-no-button-vote");
                RenderTotalVote();
            }
        },
        failure: function (response) {
        }

    });
};

    $(document).ready(function () {
        RenderTotalVote();
    });

function btnVote() {
    __doPostBack('<%=btnhVote.UniqueID%>', "");
}
function btnReload() {
    __doPostBack('<%=btnhReload.UniqueID%>', "");
}
function CloseToList() {
    window.location.assign("/danh-sach-thi-sinh.html");
}
</script>