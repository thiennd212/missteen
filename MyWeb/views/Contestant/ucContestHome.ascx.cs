﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.Contestant
{
    public partial class ucContestHome : System.Web.UI.UserControl
    {
        string lang = "vi";
        int currentPage = 1;
        int recordCount = 0;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                BindData();
            }
        }       

        private void BindData()
        {
            try
            {
                string host = "http://" + HttpContext.Current.Request.Url.Host;
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                var mySessionVar = context.Session["UserNameFB"];
                if (Session["UserNameFB"] != null)
                    FBUserId.Value = Session["UserNameFB"].ToString();
                string _str = "";
                var lstContestHome = db.Contestants.Where(x => x.IsHome == true).OrderByDescending(x => x.Id).Take(4).ToList();
                if (lstContestHome.Count() > 0)
                {
                    _str += "<div class=\"cate-header sub-top\">";
                    _str += "<p>DANH SÁCH THI SINH</p>";
                    _str += "<ul class=\"contest-list\">";
                    foreach (var item in lstContestHome)
                    {
                        string urlShare = host + "/" + item.TagName + ".html";
                        _str += "<li class=\"item-home\"><figure>";                        
                        _str += "<figcaption class = 'contestantItems_img_wrap'>";
                        if (item.Image1.Length > 0)
                        {
                            _str += "<a class=\"img-view\" href=\"/" + item.TagName + ".html\" title=\"" + item.FullName.ToString() + "\"><img src=\"" + item.Image1.Split(Convert.ToChar(","))[0] + "\" alt=\"" + item.FullName.ToString() + "\"/></a>";
                        }
                        else
                        {
                            _str += "<a class=\"img-view\" title=\"" + item.FullName.ToString() + "\" href=\"/" + item.TagName + ".html\"><img src=\"images/no_image.jpg\" alt=\"" + item.FullName.ToString() + "\"/></a>";
                        }
                        _str += "<figcaption class = 'contestantItems_button_wrap'>";
                        if (!string.IsNullOrEmpty(FBUserId.Value))
                        {
                            var contestantReference = db.ContestantsReferences.FirstOrDefault(x => x.ContestantId == item.Id && x.UserId == FBUserId.Value);
                            if (contestantReference == null)
                            {                                
                                _str += "<span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
                                _str += "      <input type=\"button\" class=\"btn btn-info btn-lg\" onclick=\"vote(" + item.Id.ToString() + ")\" value=\"Vote\" />";
                                _str += " <input type=\"button\" class=\"btn btn-info btn-lg\" onclick=\"likeshare('" + urlShare + "')\" value=\"Share\" />";
                            }
                            else
                            {
                                _str += "<span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
                            }
                        }
                        else
                        {                            
                            _str += "<span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
                            _str += "      <input type=\"button\" class=\"btn btn-info btn-lg\" onclick=\"vote(" + item.Id.ToString() + ")\" value=\"Vote\" />";
                            _str += " <input type=\"button\" class=\"btn btn-info btn-lg\" onclick=\"likeshare('" + urlShare + "')\" value=\"Share\" />";
                        }
                        _str += "</figcaption></figcaption>";
                        _str += "<a class=\"link-view\" title=\"" + item.FullName.ToString() + "\" href=\"/" + item.TagName + ".html\"><h3>" + item.FullName.ToString() + "</h3></a><p>SBD: " + item.IdentificationNumber + "</p>";
                        _str += "</figure></li>";
                    }
                    _str += "</ul>";
                    _str += "<p><a href=\"/danh-sach-thi-sinh.html\" title=\"Xem tất cả thí sinh\">Xem tất cả</a></p>";
                    _str += "</div>";
                }
                ltrHomeContest.Text = _str;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void btnhVote_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(hfContestantId.Value) && !string.IsNullOrEmpty(FBUserId.Value))
                {
                    var contestReference = db.ContestantsReferences.Where(x => x.ContestantId == Convert.ToInt32(hfContestantId.Value) && x.UserId == FBUserId.Value).FirstOrDefault();
                    if (contestReference == null)
                    {
                        var contest = db.Contestants.Where(x => x.Id == Convert.ToInt32(hfContestantId.Value)).FirstOrDefault();
                        if (contest != null)
                        {
                            contest.TotalVoted = (contest.TotalVoted != null) ? (contest.TotalVoted + 1) : 1;

                            ContestantsReference cr = new ContestantsReference();
                            cr.ContestantId = Convert.ToInt32(hfContestantId.Value);
                            cr.UserId = FBUserId.Value;
                            cr.AccessToken = hfAccessToken.Value;

                            db.ContestantsReferences.InsertOnSubmit(cr);
                            db.SubmitChanges();
                        }
                    }
                }
                BindData();
            }
            catch
            {

            }
        }

        public void btnhReload_Click(object sender, EventArgs e)
        {
            string ccUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            if (ccUrl.Contains("?"))
            {
                string[] arrUrl = ccUrl.Split('?');
                ccUrl = arrUrl[0];
            }
            Response.Redirect(ccUrl);
        }
    }
}