﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MyWeb.views.Contestant
{
    public partial class contestantDetail : System.Web.UI.UserControl
    {
        public string strUrlFace = "";
        public string wf = "770";
        public string nf = "5";
        public string cf = "light";
        int currentPage;
        dataAccessDataContext db = new dataAccessDataContext();
        protected string lang = "vi";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (GlobalClass.widthfkComment != null) { wf = GlobalClass.widthfkComment; }
            if (GlobalClass.numfkComment != null) { nf = GlobalClass.numfkComment; }
            if (GlobalClass.colorfkComment != null) { cf = GlobalClass.colorfkComment; }
            lang = MyWeb.Global.GetLang();
            currentPage = Request.QueryString["page"] != null ? int.Parse(Request.QueryString["page"]) : 1;
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        protected void LoadData()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            var mySessionVar = context.Session["UserNameFB"];
            if (Session["UserNameFB"] != null)
                FBUserId.Value = Session["UserNameFB"].ToString();
            strUrlFace = Request.Url.ToString();
            if (Request["hp"] != null)
            {
                string tagName = Request["hp"].ToString();
                var curContestant = db.Contestants.FirstOrDefault(s => s.TagName == tagName);

                if (curContestant != null)
                {
                    string img1 = "", img2 = "", strInfoRight = "", strContent1 = "", strContent2 = "";

                    // Info left
                    if (curContestant.Image1 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curContestant.Image1 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curContestant.Image1 + "\" /></div>";
                    }
                    if (curContestant.Image2 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curContestant.Image2 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curContestant.Image2 + "\" /></div>";
                    }
                    if (curContestant.Image3 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curContestant.Image3 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curContestant.Image3 + "\" /></div>";
                    }
                    if (curContestant.Image4 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curContestant.Image4 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curContestant.Image4 + "\" /></div>";
                    }
                    if (curContestant.Image5 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curContestant.Image5 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curContestant.Image5 + "\" /></div>";
                    }
                    if (curContestant.Image6 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curContestant.Image6 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curContestant.Image6 + "\" /></div>";
                    }
                    if (curContestant.Image7 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curContestant.Image7 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curContestant.Image7 + "\" /></div>";
                    }
                    if (curContestant.Image8 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curContestant.Image8 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curContestant.Image8 + "\" /></div>";
                    }
                    if (curContestant.Image9 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curContestant.Image9 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curContestant.Image9 + "\" /></div>";
                    }
                    if (curContestant.Image10 != "")
                    {
                        img1 += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + curContestant.Image10 + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        img2 += "<div class=\"item\"><img src=\"" + curContestant.Image10 + "\" /></div>";
                    }

                    ltrInfoLeft.Text = "<div id='sync1' class='owl-carousel'>" + img1 + "</div><div id='sync2' class='owl-carousel'>" + img2 + "</div>";
                    hdTourName.Value = curContestant.FullName;
                    hdContestantId.Value = curContestant.Id.ToString();
                    ltrVote.Text = "<span class=\"itemVoted\">" + curContestant.TotalVoted + "<i class='fa fa-heart'></i></span>";
                    // Info right
                    strInfoRight += "<div class='contestant_detail_info'>";
                    strInfoRight += "   <div class='contestant_detail_info_top'>";
                    strInfoRight += "       <div class = 'contestant_detail_info_top_name'>" + curContestant.FullName + "</div>";
                    strInfoRight += "       <div class = 'contestant_detail_info_top_sbd'><strong>SBD: </strong>" + curContestant.IdentificationNumber + "</div>";                    
                    strInfoRight += "       <hr />";
                    strInfoRight += "       <div class = 'contestant_detail_info_bottom_ContestantPlace'>" + curContestant.ContestantPlace + "</div>";
                    strInfoRight += "       <div class = 'contestant_detail_info_bottom_age'><strong>Tuổi: </strong>" + curContestant.Age + "</div>";
                    strInfoRight += "       <div class = 'contestant_detail_info_bottom_height'><strong>Chiều cao: </strong>" + curContestant.Height + "</div>";
                    strInfoRight += "       <div class = 'contestant_detail_info_bottom_PersonalAchievements'>";
                    strInfoRight += "           <strong>Thành tích cá nhân</strong>";
                    strInfoRight += "           <p>" + curContestant.PersonalAchievements + "</p>";
                    strInfoRight += "       </div>";
                    strInfoRight += "       <div class = 'contestant_detail_info_bottom_Gifted'><strong>Năng khiếu: </strong>" + curContestant.Gifted + "</div>";
                    strInfoRight += "       <div class = 'contestant_detail_info_bottom_Hobby'><strong>Sở thích: </strong>" + curContestant.Hobby + "</div>";
                    strInfoRight += "       <div class = 'contestant_detail_info_bottom_LifeMotto'><strong>Châm ngôn: </strong>" + curContestant.LifeMotto + "</div>";
                    strInfoRight += "       <div class = 'contestant_detail_info_bottom_PointOfView'>";
                    strInfoRight += "           <strong>Quan điểm</strong>";
                    strInfoRight += "           <p>" + curContestant.PointOfView + "</p>";
                    strInfoRight += "       </div>";
                    strInfoRight += "   </div>";
                    strInfoRight += "</div>";


                    ltrInfoRight.Text = strInfoRight;

                    // Tab
                    var listTab = db.TourTags.Where(s => s.IdTour == curContestant.Id).OrderBy(s => s.Order).ToList();
                    if (listTab.Count > 0)
                    {
                        strContent1 += "<ul class=\"tabs-pro\">";
                        for (int i = 0; i < listTab.Count; i++)
                        {
                            if (i == 0)
                            {
                                strContent1 += "<li class=\"active\" rel=\"tab" + i + "\">" + listTab[i].Name + "</li>";
                            }
                            else
                            {
                                strContent1 += "<li rel=\"tab" + i + "\">" + listTab[i].Name + "</li>";
                            }
                            strContent2 += "<div id=\"tab" + i + "\" class=\"tab_content\">" + listTab[i].Text + "</div>";

                        }
                        strContent1 += "</ul>";
                    }

                    GetLikeShareAndSaveToDB();

                }
                else
                {
                    Response.Redirect("/error404.html");
                }
            }
            else
            {
                Response.Redirect("/error404.html");
            }
        }

        public void GetLikeShareAndSaveToDB()
        {
            try
            {
                using (var client = new System.Net.Http.HttpClient())
                {
                    string ccUrl = HttpContext.Current.Request.Url.AbsoluteUri;                    
                    if (ccUrl.Contains("?"))
                    {
                        string[] arrUrl = ccUrl.Split('?');
                        ccUrl = arrUrl[0];
                    }
                    client.BaseAddress = new Uri("http://graph.facebook.com/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync("?ids=" + ccUrl + "").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        JObject json = JObject.Parse(responseString);
                        var countLike = json[ccUrl]["share"]["share_count"];
                        var contest = db.Contestants.FirstOrDefault(x => x.Id == Convert.ToInt32(hdContestantId.Value));
                        if (contest != null)
                        {
                            contest.TotalLikeShare = Convert.ToInt32(countLike);
                            db.SubmitChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class share
        {
            public int comment_count { get; set; }
            public int share_count { get; set; }
        }

        protected string GetName(int plId)
        {
            var item = db.tbPages.FirstOrDefault(s => s.pagId == plId);

            return item != null ? item.pagName : "";
        }

        protected string GetListPlaceName(int tourId)
        {
            var listPl = db.TourPlaces.Where(s => s.IdTour == tourId).Select(s => s.IdPlace).ToList();
            var listPage = db.tbPages.Where(s => listPl.Contains(s.pagId)).ToList();
            string strInfoRight = "";

            if (listPage.Any())
            {

                foreach (var item in listPage)
                {
                    strInfoRight += strInfoRight != "" ? "</br><span class='itemTours_placeItem'>" + item.pagName + "</span>" : "<span class='itemTours_placeItem'>" + item.pagName + "</span>";
                }
            }

            return strInfoRight;
        }

        protected string getCommentPaging(int cur, int size, int total)
        {
            string strResult = "";
            var numberOfPage = total % size == 0 ? total / size : (total / size) + 1;
            strResult += "<ul class='paging'>";
            var curUrl = Request.RawUrl;
            curUrl = curUrl.IndexOf("?") > 0 ? curUrl.Substring(0, curUrl.IndexOf("?")) : curUrl;
            for (int i = 1; i <= numberOfPage; i++)
            {
                strResult += i == cur ?
                    "<li class='active'>" + i + "</li>" :
                    "<li class='item'><a href='" + curUrl + "?page=" + i + "' title='Trang " + i + "'>" + i + "</a></li>";
            }
            strResult += "</ul>";
            return strResult;
        }

        public void btnhVote_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(hdContestantId.Value) && !string.IsNullOrEmpty(FBUserId.Value))
                {
                    var contestReference = db.ContestantsReferences.Where(x => x.ContestantId == Convert.ToInt32(hdContestantId.Value) && x.UserId == FBUserId.Value).FirstOrDefault();
                    if (contestReference == null)
                    {
                        var contest = db.Contestants.Where(x => x.Id == Convert.ToInt32(hdContestantId.Value)).FirstOrDefault();
                        if (contest != null)
                        {
                            contest.TotalVoted = (contest.TotalVoted != null) ? (contest.TotalVoted + 1) : 1;

                            ContestantsReference cr = new ContestantsReference();
                            cr.ContestantId = Convert.ToInt32(hdContestantId.Value);
                            cr.UserId = FBUserId.Value;
                            cr.AccessToken = hfAccessToken.Value;

                            db.ContestantsReferences.InsertOnSubmit(cr);
                            db.SubmitChanges();
                            LoadData();
                        }
                        else
                        {

                        }
                    }
                }
            }
            catch
            {

            }
        }

        public void btnhReload_Click(object sender, EventArgs e)
        {
            string ccUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            if (ccUrl.Contains("?"))
            {
                string[] arrUrl = ccUrl.Split('?');
                ccUrl = arrUrl[0];
            }
            Response.Redirect(ccUrl);
        }
    }
}