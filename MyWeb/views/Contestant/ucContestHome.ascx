﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucContestHome.ascx.cs" Inherits="MyWeb.views.Contestant.ucContestHome" %>
<div class="contest-home">
    <asp:Literal ID="ltrHomeContest" runat="server"></asp:Literal>
    <asp:HiddenField id="session_obejct" runat="server" />
    <asp:HiddenField ID="FBUserId" runat="server" />
    <asp:HiddenField ID="hfAccessToken" runat="server" />
    <asp:HiddenField ID="hfContestantId" runat="server" />
    <asp:Button ID="btnhVote" runat="server" Style="display: none" Text="Button" OnClick="btnhVote_Click" />
    <asp:Button ID="btnhReload" runat="server" Style="display: none" Text="Button" OnClick="btnhReload_Click" />
</div>
<script type="text/javascript">
    function SetUserName(value) {
        //PageMethods.SetUserName(value);
        $.ajax({
            async: false,
            type: "POST",
            url: "webService.asmx/SetSessionLoginCookie",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ userName: value }),
            success: function (response) {
                console.log(response.d);
            },
            failure: function (response) {
            }

        });
    }

    function LogoutInfoSS() {
        $.ajax({
            async: false,
            type: "POST",
            url: "webService.asmx/GetSessionLogin",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d != "" && response.d != null) {
                    if (response.d.IsLogin) {
                        $("#lgFacebook").html(response.d.Message)
                    }
                    else
                        $("#lgFacebook").html(response.d.Message);
                }
            },
            failure: function (response) {
            }

        });
    }

    function GetInfoUserFb(uid) {
        FB.api(
            "/" + uid + "",
            function (response) {
                if (response && !response.error) {
                    FB.api(
                        "/" + uid + "/picture",
                        function (responsex) {
                            if (responsex && !responsex.error) {
                                var para = uid + "," + response.name + "," + responsex.data.url;
                                var str = "<span class=\"avatar\"><img src=\"" + responsex.data.url + "\" alt=\"\"/></span> <span class=\"fullname\">" + response.name + "</span> (<a onclick=\"LogoutFb()\" href=\"javascript:{}\" title=\"Thoát\">Thoát</a>)";
                                $("#lgFacebook").html(str);
                            } else {
                                var para = uid;
                                var str = "<a href='javscript:{}' onclick=\"Login()\" class=\"facebook\" rel=\"nofollow\"><img src=\"uploads/layout/default/css/images/icon-fb-log.png\" alt=\"\"/></a>";
                                $("#lgFacebook").html(str);
                            }
                            SetUserName(para);
                        }
                    );
                } else {
                    LogoutInfoSS();
                }
            }
        );
    }

    function vote(id) {        
        FB.getLoginStatus(function (response) {
            if (response.status === 'not_authorized' || response.status === 'unknown') {
                FB.login(function (response) {
                    if (response.authResponse) {
                        var uid = response.authResponse.userID;
                        GetInfoUserFb(uid);
                    }
                });
            } else if (response.status === 'connected') {
                var uid = response.authResponse.userID;
                var accessToken = response.authResponse.accessToken;

                var FBUserId = document.getElementById('<%= FBUserId.ClientID %>');
                FBUserId.value = uid;
                GetInfoUserFb(uid);

                var hfAccessToken = document.getElementById('<%= hfAccessToken.ClientID %>');
                hfAccessToken.value = accessToken;

                var hfContestantId = document.getElementById('<%= hfContestantId.ClientID %>');
                hfContestantId.value = id;
                btnVote();
            }
        }, true);
    };

    function likeshare(url) {
        FB.ui(
                {
                    method: 'share',
                    href: url,
                },
                // callback
                function (response) {
                    if (response && !response.error_message) {
                        //alert('Posting completed.');
                    } else {
                        //alert('Error while posting.');
                    }
                });
    };

    function btnVote() {
        __doPostBack('<%=btnhVote.UniqueID%>', "");
    };
    function btnReload() {
        __doPostBack('<%=btnhReload.UniqueID%>', "");
    };
</script>
