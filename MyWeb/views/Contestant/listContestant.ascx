﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="listContestant.ascx.cs" Inherits="MyWeb.views.Contestant.listContestant" %>
<div id="listContestant">
</div>

<div id="listContestant2">
    <asp:Literal runat="server" ID="ltrHeader"></asp:Literal>
    <div class="listcontestantByCat_content">
        <asp:Literal runat="server" ID="ltrContent"></asp:Literal>
    </div>
    <asp:Literal runat="server" ID="ltrPaging"></asp:Literal>
</div>

<asp:HiddenField ID="FBUserId" runat="server" />
<asp:HiddenField ID="hfAccessToken" runat="server" />
<asp:HiddenField ID="hfContestantId" runat="server" />
<asp:HiddenField ID="hfContestantGroupId" runat="server" />
<asp:Button ID="btnhVote" runat="server" Style="display: none" Text="Button" OnClick="btnhVote_Click" />
<asp:Button ID="btnhReload" runat="server" Style="display: none" Text="Button" OnClick="btnhReload_Click" />

<div class="footer-nav" id="footer">
    <div class="row container-footer">
        <div class="wrap-search">
            <div class="col-md-1">
                <div class="input-group text-muted credit">
                    <div class="btn-group">
                        <div class="input-group-btn">
                            <button id="btnSearch" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-search"></i>
                                <i class="fa fa-caret-up"></i>
                            </button>
                            <ul class="dropdown-menu drop-up" role="menu">
                                <li class="liFooter" id="searchSBD" onclick="btnSearchTS1(7);">Tìm theo số báo danh</li>
                                <li class="divider"></li>
                                <li class="liFooter" id="searchTP" onclick="btnSearchTS1(8);">Tìm theo Tỉnh/Thành phố</li>
                                <li class="divider"></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <asp:TextBox ID="txtSearch" runat="server" class="form-control" placeholder="Từ khóa tìm kiếm" onkeypress="return EnterEvent1(event)"></asp:TextBox>
                <%--<asp:Button ID="Button1" runat="server" Style="display: none" Text="Button" OnClick="Button1_Click" />
                <asp:Button ID="Button2" runat="server" Style="display: none" Text="Button" OnClick="Button2_Click" />--%>
                <asp:HiddenField runat="server" ID="hfValue" />
                <asp:HiddenField runat="server" ID="hfValueFilter" />
            </div>
        </div>
        <div class="col-md-1">
            <div class="text-muted credit">
                <div class="btn-group">
                    <div class="input-group-btn">
                        <button id="btnFilter" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-filter"></i>
                            <i class="fa fa-caret-up"></i>
                        </button>
                        <div class="dropdown-menu drop-up" role="menu">
                            <div class="liFooter">
                                <p onclick="btnSearchTS2(10);">Tìm theo Độ tuổi</p>
                                <i class="fa fa-filter"></i>
                                <ul id="searchByAge">
                                    <li>
                                        <asp:Repeater ID="rptBirthYearList" runat="server">
                                            <ItemTemplate>
                                                <div class="cus-checkbox">
                                                    <asp:Label ID="lblBirthYear" runat="server">Sinh năm <%#Eval("BirthYear").ToString()%></asp:Label>
                                                    <input type="checkbox" id="cbBirthYear2" checked="checked" value='<%# Eval("BirthYear").ToString() %>' />
                                                    <%--<asp:CheckBox runat="server" ID="cbBirthYear" Checked="true" value='<%#DataBinder.Eval(Container.DataItem, "BirthYear")%>' />--%>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <!-- div.cus-checkbox -->
                                    </li>
                                </ul>
                            </div>
                            <div class="divider"></div>
                            <div class="liFooter" onclick="btnSearchTS2(1);">Xếp theo bình chọn giảm dần<i class="fa fa-filter"></i><i class="fa fa-arrow-down"></i></div>
                            <div class="divider"></div>
                            <div class="liFooter" onclick="btnSearchTS2(2);">Xếp theo bình chọn tăng dần<i class="fa fa-filter"></i><i class="fa fa-arrow-up"></i></div>
                            <div class="divider"></div>
                            <div class="liFooter" onclick="btnSearchTS2(3);">Xếp theo Like, Share giảm dần<i class="fa fa-filter"></i><i class="fa fa-arrow-down"></i></div>
                            <div class="divider"></div>
                            <div class="liFooter" onclick="btnSearchTS2(4);">Xếp theo Like, Share tăng dần<i class="fa fa-filter"></i><i class="fa fa-arrow-up"></i></div>
                            <div class="divider"></div>
                            <div class="liFooter" onclick="btnSearchTS2(5);">Xếp theo số báo danh giảm dần<i class="fa fa-filter"></i><i class="fa fa-arrow-down"></i></div>
                            <div class="divider"></div>
                            <div class="liFooter" onclick="btnSearchTS2(6);">Xếp theo số báo danh tăng dần<i class="fa fa-filter"></i><i class="fa fa-arrow-up"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="col-md-12" id="lblFooter">
                <span>Bạn đang xem </span>
                <label id="lblCount2"></label>
                <span>/ </span>
                <label id="lblTotal2"></label>
                <%-- <asp:Label runat="server" ID="lblCount" />
                <asp:Label runat="server" ID="lblTotal" />--%>
            </div>
        </div>
    </div>
</div>
<div id="fb-root"></div>
<script type="text/javascript">
    function SetUserName(value) {
        //PageMethods.SetUserName(value);
        $.ajax({
            async: false,
            type: "POST",
            url: "webService.asmx/SetSessionLoginCookie",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ userName: value }),
            success: function (response) {
                console.log(response.d);
            },
            failure: function (response) {
            }

        });
    }

    function LogoutInfoSS() {
        $.ajax({
            async: false,
            type: "POST",
            url: "webService.asmx/GetSessionLogin",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d != "" && response.d != null) {
                    if (response.d.IsLogin) {
                        $("#lgFacebook").html(response.d.Message)
                    }
                    else
                        $("#lgFacebook").html(response.d.Message);
                }
            },
            failure: function (response) {
            }

        });
    }

    function GetInfoUserFb(uid) {
        FB.api(
            "/" + uid + "",
            function (response) {
                if (response && !response.error) {
                    FB.api(
                        "/" + uid + "/picture",
                        function (responsex) {
                            if (responsex && !responsex.error) {
                                var para = uid + "," + response.name + "," + responsex.data.url;
                                var str = "<span class=\"avatar\"><img src=\"" + responsex.data.url + "\" alt=\"\"/></span> <span class=\"fullname\">" + response.name + "</span> (<a onclick=\"LogoutFb()\" href=\"javascript:{}\" title=\"Thoát\">Thoát</a>)";
                                $("#lgFacebook").html(str);
                            } else {
                                var para = uid;
                                var str = "<a href='javscript:{}' onclick=\"Login()\" class=\"facebook\" rel=\"nofollow\"><img src=\"uploads/layout/default/css/images/icon-fb-log.png\" alt=\"\"/></a>";
                                $("#lgFacebook").html(str);
                            }
                            SetUserName(para);
                        }
                    );
                } else {
                    LogoutInfoSS();
                }
            }
        );
    }

    function vote(id) {
        FB.getLoginStatus(function (response) {
            if (response.status === 'not_authorized' || response.status === 'unknown') {
                FB.login(function (response) {
                    if (response.authResponse) {
                        var uid = response.authResponse.userID;
                        GetInfoUserFb(uid);
                    }
                });
            } else if (response.status === 'connected') {
                var uid = response.authResponse.userID;
                var accessToken = response.authResponse.accessToken;

                var FBUserId = document.getElementById('<%= FBUserId.ClientID %>');
                FBUserId.value = uid;
                GetInfoUserFb(uid);

                var hfAccessToken = document.getElementById('<%= hfAccessToken.ClientID %>');
                hfAccessToken.value = accessToken;

                var hfContestantId = document.getElementById('<%= hfContestantId.ClientID %>');
                hfContestantId.value = id;
                btnVote();
            }
        }, true);
};

function likeshare(url) {
    FB.ui(
                    {
                        method: 'share',
                        href: url,
                    },
                    // callback
                    function (response) {
                        if (response && !response.error_message) {
                            FB.getLoginStatus(function (response) {
                                if (response.status === 'not_authorized' || response.status === 'unknown') {
                                    FB.login(function (response) {
                                        if (response.authResponse) {
                                            var uid = response.authResponse.userID;
                                            GetInfoUserFb(uid);
                                        }
                                    });
                                } else if (response.status === 'connected') {
                                    var uid = response.authResponse.userID;
                                    var accessToken = response.authResponse.accessToken;

                                    var FBUserId = document.getElementById('<%= FBUserId.ClientID %>');
                                    FBUserId.value = uid;
                                    GetInfoUserFb(uid);

                                    var hfAccessToken = document.getElementById('<%= hfAccessToken.ClientID %>');
                                    hfAccessToken.value = accessToken;

                                    var hfContestantId = document.getElementById('<%= hfContestantId.ClientID %>');
                                        hfContestantId.value = id;


                                    }
                            }, true);
                            //alert('Posting completed.');
                        } else {
                            //alert('Error while posting.');
                        }
                    });

                };

                function btnVote() {
                    var fbUserId = document.getElementById('<%=FBUserId.ClientID%>').value;
    var contestantId = document.getElementById('<%=hfContestantId.ClientID%>').value;
    var accessToken = document.getElementById('<%=hfAccessToken.ClientID%>').value;
    var grId = document.getElementById('<%=hfContestantGroupId.ClientID%>').value;
    $.ajax({
        async: false,
        type: "POST",
        url: "webService.asmx/UpdateContestantReference",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ contestantId: contestantId, fbUserId: fbUserId, accessToken: accessToken }),
        success: function (response) {
            if (response.d === true)
                LoadContestantFromAjax(12, 9, "", "", grId);
        },
        failure: function (response) {
        }

    });
};
function btnReload() {
    __doPostBack('<%=btnhReload.UniqueID%>', "");
};
</script>

<script type="text/javascript">
    var grId = document.getElementById('<%=hfContestantGroupId.ClientID%>').value;
    function footer(scroll) {
        if (scroll > 50) {
            $(".footer-nav").addClass("show");
        }
        else {
            $(".footer-nav").removeClass("show");
        }
    }

    function btnLazyload(scroll, typesearch, lstYear, textSearch) {
        var pagezise = 12;
        if (scroll != 0 && scroll % 50 == 0) {
            pagezise += pagezise;
            LoadContestantFromAjax(pagezise, typesearch, lstYear, textSearch, grId);
        }
    }

    function LoadContestantFromAjax(pz, typesearch, lstYear, textSearch, grId) {
        $.ajax({
            async: false,
            type: "POST",
            url: "webService.asmx/GetListContestant",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ pageSize: pz, typesearch: typesearch, lstYear: "'" + lstYear + "'", textSearch: "'" + textSearch + "'", grId: grId }),
            success: function (response) {
                if (pz > response.d.Total)
                    $("#lblCount2").html(response.d.Total);
                else
                    $("#lblCount2").html(pz);
                $("#lblTotal2").html(response.d.Total);
                $("#listContestant").html(response.d.Result);
            },
            failure: function (response) {
            }

        });
    }

    var valueSearch = '';
    var valueSearchFilter = '';
    var textSearch = '';

    $(window).load(function () {
        if (valueSearch === '' && valueSearchFilter === '') {
            LoadContestantFromAjax(12, 9, "", "", grId);
        } else if (valueSearch !== '' && valueSearchFilter === '') {
            LoadContestantFromAjax(12, valueSearch, lstBirthYear, textsearch, grId);
        } else if (valueSearch === '' && valueSearchFilter !== '') {
            LoadContestantFromAjax(12, valueSearchFilter, lstBirthYear, textsearch, grId);
        }
    });

    $(window).scroll(function (event) {
        var scroll = $(window).scrollTop();
        footer(scroll);

        var typesearch = valueSearch;
        var typesearch2 = valueSearchFilter;
        var textsearch = textSearch;

        if (typesearch2 !== undefined && typesearch2 !== null && typesearch2 !== '') {
            LoadContestantFromAjax(12, typesearch2, lstBirthYear, textsearch, grId);
        }
        else if (typesearch !== undefined && typesearch !== null && typesearch !== '') {
            LoadContestantFromAjax(12, typesearch, lstBirthYear, textsearch, grId);
        }
        else {
            btnLazyload(scroll, 9, "", "");
        }
    });

    function EnterEvent1(e) {
        var typeSearch = valueSearch;
        textSearch = document.getElementById('<%= txtSearch.ClientID %>').value;
        valueSearchFilter = '';

        if (e.keyCode == 13) {
            if (typeSearch === "") {
                typeSearch = 7;
                valueSearch = 7;
            }
            LoadContestantFromAjax(12, typeSearch, lstBirthYear, textSearch, grId);
            $("body").scrollTop(0);
            return false;
        }
    }

    function btnSearchTS1(value) {
        valueSearch = value;
        if (value == 7) {
            $("#searchSBD").addClass("active");
            $("#searchTP").removeClass("active");
            $("#ctl18_ucLoadControl_ctl00_txtSearch").attr("placeholder", "Họ tên, số báo danh");
        } else {
            $("#searchSBD").removeClass("active");
            $("#searchTP").addClass("active");
            $("#ctl18_ucLoadControl_ctl00_txtSearch").attr("placeholder", "Tỉnh, thành phố");
        }
    }

    function btnSearchTS2(value) {
        valueSearch = '';
        valueSearchFilter = value;        

        if (value === 10) {
            getCheckboxValues();
        }

        LoadContestantFromAjax(12, value, lstBirthYear, '', grId);
        $("body").scrollTop(0);
    }

    var lstBirthYear = '';
    function getCheckboxValues() {
        lstBirthYear = '';
        $('#searchByAge input[type="checkbox"]').each(function () {
            if ($(this).prop('checked') == true) {
                lstBirthYear = lstBirthYear + $(this).val() + ";";
            }
        });
    }
</script>
