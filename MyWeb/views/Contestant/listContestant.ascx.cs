﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Facebook;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace MyWeb.views.Contestant
{
    public partial class listContestant : System.Web.UI.UserControl
    {
        public string strUrlFace = "";
        public string wf = "770";
        public string nf = "5";
        public string cf = "light";
        int currentPage;
        dataAccessDataContext db = new dataAccessDataContext();
        protected string lang = "vi";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (GlobalClass.widthfkComment != null) { wf = GlobalClass.widthfkComment; }
            if (GlobalClass.numfkComment != null) { nf = GlobalClass.numfkComment; }
            if (GlobalClass.colorfkComment != null) { cf = GlobalClass.colorfkComment; }
            lang = MyWeb.Global.GetLang();
            currentPage = Request.QueryString["page"] != null ? int.Parse(Request.QueryString["page"]) : 1;
            if (Request["hp"] != null)
            {
                string idgr = db.tbPages.Where(s => s.pagType == 707 && s.pagTagName == Request["hp"].ToString().Split('?')[0]).FirstOrDefault().pagId.ToString();
                hfContestantGroupId.Value = idgr;
            }
            if (!IsPostBack)
            {
                //LoadData(12);
                LoadBirthYear();
            }
        }

        protected void LoadBirthYear()
        {
            try
            {
                var _listBirthYear = db.Contestants.Where(s => s.Lang == lang && s.Active == 1 && s.BirthYear != null)
                    .OrderBy(s => s.BirthYear).GroupBy(s => s.BirthYear).Select(g => new { Key = g.Key, BirthYear = g.First().BirthYear, Id = g.First().Id }).ToList();

                if (_listBirthYear.Count() > 0)
                {
                    rptBirthYearList.DataSource = _listBirthYear;
                    rptBirthYearList.DataBind();
                }
            }
            catch
            {

            }
        }

        protected void LoadData(int pageSize)
        {

            System.Web.HttpContext context = System.Web.HttpContext.Current;
            var mySessionVar = context.Session["UserNameFB"];
            if (Session["UserNameFB"] != null)
                FBUserId.Value = Session["UserNameFB"].ToString();
            int currentPage = Request.RawUrl.Contains("page") ? int.Parse(common.GetParameterFromUrl(Request.RawUrl, "page")) : 1;
            string strResult = "";

            ltrHeader.Text = "<div class='listTourHeader'><a title='Danh sách thí sinh' href='/danh-sach-thi-sinh.html'>Danh sách thí sinh</a></div>";

            var listContestant =
                db.Contestants.Where(s => s.Lang == lang && s.Active == 1).ToList();

            if (listContestant.Any())
            {
                int count = listContestant.Count;

                listContestant = listContestant.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                int j = 0;
                foreach (var item in listContestant)
                {
                    strResult += "<div class='contestantItems'>";
                    strResult += "  <div class='contestantItems_info1'>";
                    strResult += "      <span class='contestantItems_price'><strong>SBD: " + item.IdentificationNumber + " </strong></span>";
                    strResult += "      <a class='contestantItems_img' href='/" + item.TagName + ".html' title='" + item.FullName +
                                 "'><img src='" + item.Image1 + "' alt='" + item.FullName + "' /></a>";
                    strResult += "      <a class='contestantItems_price' href='/" + item.TagName + ".html'><strong>" + item.FullName + " </strong></a>";
                    if (!string.IsNullOrEmpty(FBUserId.Value))
                    {
                        var contestantReference = db.ContestantsReferences.FirstOrDefault(x => x.ContestantId == item.Id && x.UserId == FBUserId.Value);
                        if (contestantReference == null)
                        {
                            strResult += "      <input type=\"button\" class=\"btn btn-info btn-lg\" onclick=\"vote(" + item.Id.ToString() + ")\" value=\"Bình chọn\" />";
                            strResult += "<span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
                        }
                        else
                        {
                            strResult += "<span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
                        }
                    }
                    else
                    {
                        strResult += "      <input type=\"button\" class=\"btn btn-info btn-lg\" onclick=\"vote(" + item.Id.ToString() + ")\" value=\"Bình chọn\" />";
                        strResult += "<span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
                    }

                    strResult += "  </div>";
                    strResult += "</div>";
                }

                ltrContent.Text = strResult;
                //ltrPaging.Text = common.PopulatePager(count, currentPage, pageSize);
                //lblCount.Text = listContestant.Count.ToString();
                //lblTotal.Text = count.ToString();
            }
        }

        protected string GetName(int plId)
        {
            var item = db.tbPages.FirstOrDefault(s => s.pagId == plId);

            return item != null ? item.pagName : "";
        }

        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        int currentPage = Request.RawUrl.Contains("page") ? int.Parse(common.GetParameterFromUrl(Request.RawUrl, "page")) : 1;
        //        string strResult = "";
        //        int pageSize = 12;
        //        var value = hfValue.Value;
        //        if ((string.IsNullOrEmpty(value) || value == "0") && !string.IsNullOrEmpty(txtSearch.Text))
        //        {
        //            ltrHeader.Text = "<div class='listTourHeader'><a title='Danh sách thí sinh' href='/danh-sach-thi-sinh.html'>Danh sách thí sinh</a></div>";

        //            var listContestant =
        //                db.Contestants.Where(s => s.Lang == lang && s.Active == 1 && (s.IdentificationNumber.ToString().Contains(txtSearch.Text)
        //                    || s.FullName.Contains(txtSearch.Text))).ToList();

        //            if (listContestant.Any())
        //            {
        //                int count = listContestant.Count;

        //                listContestant = listContestant.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

        //                foreach (var item in listContestant)
        //                {
        //                    strResult += "<div class='contestantItems'>";
        //                    strResult += "  <div class='contestantItems_info1'>";
        //                    strResult += "      <span class='contestantItems_price'><strong>SBD: " + item.IdentificationNumber + " </strong></span>";
        //                    strResult += "      <a class='contestantItems_img' href='/" + item.TagName + ".html' title='" + item.FullName +
        //                                 "'><img src='" + item.Image1 + "' alt='" + item.FullName + "' /></a>";
        //                    strResult += "      <a class='contestantItems_price' href='/" + item.TagName + ".html'><strong>" + item.FullName + " </strong></a>";
        //                    if (!string.IsNullOrEmpty(FBUserId.Value))
        //                    {
        //                        var contestantReference = db.ContestantsReferences.FirstOrDefault(x => x.ContestantId == item.Id && x.UserId == FBUserId.Value);
        //                        if (contestantReference == null)
        //                        {
        //                            strResult += "      <input type=\"button\" class=\"btn btn-info btn-lg\" onclick=\"vote(" + item.Id.ToString() + ")\" value=\"Bình chọn\" />";
        //                            strResult += "<span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
        //                        }
        //                        else
        //                        {
        //                            strResult += "<span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
        //                        }
        //                    }
        //                    else
        //                    {
        //                        strResult += "      <input type=\"button\" class=\"btn btn-info btn-lg\" onclick=\"vote(" + item.Id.ToString() + ")\" value=\"Bình chọn\" />";
        //                        strResult += "<span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
        //                    }

        //                    strResult += "  </div>";
        //                    strResult += "</div>";
        //                }

        //                ltrContent.Text = strResult;
        //                lblCount.Text = listContestant.Count.ToString();
        //                lblTotal.Text = count.ToString();
        //            }
        //        }


        //        else if (value == "1" && !string.IsNullOrEmpty(txtSearch.Text))
        //        {
        //            ltrHeader.Text = "<div class='listTourHeader'><a title='Danh sách thí sinh' href='/danh-sach-thi-sinh.html'>Danh sách thí sinh</a></div>";

        //            var listContestant =
        //                db.Contestants.Where(s => s.Lang == lang && s.Active == 1 && s.ContestantPlace.Contains(txtSearch.Text)).ToList();

        //            if (listContestant.Any())
        //            {
        //                int count = listContestant.Count;

        //                listContestant = listContestant.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

        //                foreach (var item in listContestant)
        //                {
        //                    strResult += "<div class='contestantItems'>";
        //                    strResult += "  <div class='contestantItems_info1'>";
        //                    strResult += "      <span class='contestantItems_price'><strong>SBD: " + item.IdentificationNumber + " </strong></span>";
        //                    strResult += "      <a class='contestantItems_img' href='/" + item.TagName + ".html' title='" + item.FullName +
        //                                 "'><img src='" + item.Image1 + "' alt='" + item.FullName + "' /></a>";
        //                    strResult += "      <a class='contestantItems_price' href='/" + item.TagName + ".html'><strong>" + item.FullName + " </strong></a>";
        //                    strResult += "      <input type=\"button\" class=\"btn btn-info btn-lg\" onclick=\"vote(" + item.Id.ToString() + ")\" value=\"Bình chọn\" ";

        //                    if (!string.IsNullOrEmpty(hfAccessToken.Value) && !string.IsNullOrEmpty(FBUserId.Value))
        //                    {
        //                        var contestantReference = db.ContestantsReferences.FirstOrDefault(x => x.ContestantId == item.Id && x.UserId == FBUserId.Value);
        //                        if (contestantReference == null)
        //                        {
        //                            strResult += " /><span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
        //                        }
        //                        else
        //                        {
        //                            strResult += " style=\"display:none\" /><span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
        //                        }
        //                    }
        //                    else
        //                        strResult += " /><span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";

        //                    strResult += "  </div>";
        //                    strResult += "</div>";
        //                }

        //                ltrContent.Text = strResult;
        //                lblCount.Text = listContestant.Count.ToString();
        //                lblTotal.Text = count.ToString();
        //            }
        //        }
        //        else
        //        {
        //            LoadData(12);
        //        }

        //    }
        //    catch
        //    {

        //    }
        //}

        //protected void Button2_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        int currentPage = Request.RawUrl.Contains("page") ? int.Parse(common.GetParameterFromUrl(Request.RawUrl, "page")) : 1;
        //        string strResult = "";
        //        int pageSize = 12;
        //        var valueFilter = hfValueFilter.Value;

        //        ltrHeader.Text = "<div class='listTourHeader'><a title='Danh sách thí sinh' href='/danh-sach-thi-sinh.html'>Danh sách thí sinh</a></div>";

        //        var listContestant = db.Contestants.Where(s => s.Lang == lang && s.Active == 1).ToList();

        //        if (!string.IsNullOrEmpty(valueFilter))
        //        {
        //            if (valueFilter == "0")
        //            {
        //                List<string> lstBirthYear = new List<string>();

        //                foreach (RepeaterItem item in rptBirthYearList.Items)
        //                {
        //                    CheckBox cbBirthYear = (CheckBox)item.FindControl("cbBirthYear");
        //                    if (cbBirthYear != null)
        //                    {
        //                        if (cbBirthYear.Checked)
        //                        {
        //                            lstBirthYear.Add(cbBirthYear.Attributes["value"]);
        //                        }
        //                    }
        //                }

        //                if (lstBirthYear.Count > 0)
        //                {
        //                    listContestant = db.Contestants.Where(s => s.Lang == lang && s.Active == 1 && s.BirthYear != null &&
        //                        lstBirthYear.Contains(s.BirthYear.ToString())).OrderBy(s => s.BirthYear).ToList();
        //                }
        //                else
        //                {
        //                    LoadData(12);
        //                }
        //            }
        //            else if (valueFilter == "1")
        //            {
        //                listContestant = db.Contestants.Where(s => s.Lang == lang && s.Active == 1).OrderByDescending(x => x.TotalVoted).ToList();
        //            }
        //            else if (valueFilter == "2")
        //            {
        //                listContestant = db.Contestants.Where(s => s.Lang == lang && s.Active == 1).OrderBy(x => x.TotalVoted).ToList();
        //            }
        //            else if (valueFilter == "3")
        //            {
        //                listContestant = db.Contestants.Where(s => s.Lang == lang && s.Active == 1).OrderByDescending(x => x.TotalLikeShare).ToList();
        //            }
        //            else if (valueFilter == "4")
        //            {
        //                listContestant = db.Contestants.Where(s => s.Lang == lang && s.Active == 1).OrderBy(x => x.TotalLikeShare).ToList();
        //            }
        //            else if (valueFilter == "5")
        //            {
        //                listContestant = db.Contestants.Where(s => s.Lang == lang && s.Active == 1).OrderByDescending(x => x.IdentificationNumber).ToList();
        //            }
        //            else if (valueFilter == "6")
        //            {
        //                listContestant = db.Contestants.Where(s => s.Lang == lang && s.Active == 1).OrderBy(x => x.IdentificationNumber).ToList();
        //            }
        //        }

        //        if (listContestant.Any())
        //        {
        //            int count = listContestant.Count;

        //            listContestant = listContestant.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

        //            foreach (var item in listContestant)
        //            {
        //                strResult += "<div class='contestantItems'>";
        //                strResult += "  <div class='contestantItems_info1'>";
        //                strResult += "      <span class='contestantItems_price'><strong>SBD: " + item.IdentificationNumber + " </strong></span>";
        //                strResult += "      <a class='contestantItems_img' href='/" + item.TagName + ".html' title='" + item.FullName +
        //                             "'><img src='" + item.Image1 + "' alt='" + item.FullName + "' /></a>";
        //                strResult += "      <a class='contestantItems_price'  href='/" + item.TagName + ".html'><strong>" + item.FullName + " </strong></a>";
        //                if (!string.IsNullOrEmpty(FBUserId.Value))
        //                {
        //                    var contestantReference = db.ContestantsReferences.FirstOrDefault(x => x.ContestantId == item.Id && x.UserId == FBUserId.Value);
        //                    if (contestantReference == null)
        //                    {
        //                        strResult += "      <input type=\"button\" class=\"btn btn-info btn-lg\" onclick=\"vote(" + item.Id.ToString() + ")\" value=\"Bình chọn\" />";
        //                        strResult += "<span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
        //                    }
        //                    else
        //                    {
        //                        strResult += "<span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
        //                    }
        //                }
        //                else
        //                {
        //                    strResult += "      <input type=\"button\" class=\"btn btn-info btn-lg\" onclick=\"vote(" + item.Id.ToString() + ")\" value=\"Bình chọn\" />";
        //                    strResult += "<span class='itemVoted'>" + item.TotalVoted + "<i class='fa fa-heart'></i></span>";
        //                }

        //                strResult += "  </div>";
        //                strResult += "</div>";
        //            }

        //            ltrContent.Text = strResult;
        //            lblCount.Text = listContestant.Count.ToString();
        //            lblTotal.Text = count.ToString();
        //        }


        //    }
        //    catch
        //    {

        //    }
        //}

        public void btnhVote_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(hfContestantId.Value) && !string.IsNullOrEmpty(FBUserId.Value))
                {
                    var contestReference = db.ContestantsReferences.Where(x => x.ContestantId == Convert.ToInt32(hfContestantId.Value) && x.UserId == FBUserId.Value).FirstOrDefault();
                    if (contestReference == null)
                    {
                        var contest = db.Contestants.Where(x => x.Id == Convert.ToInt32(hfContestantId.Value)).FirstOrDefault();
                        if (contest != null)
                        {
                            contest.TotalVoted = (contest.TotalVoted != null) ? (contest.TotalVoted + 1) : 1;

                            ContestantsReference cr = new ContestantsReference();
                            cr.ContestantId = Convert.ToInt32(hfContestantId.Value);
                            cr.UserId = FBUserId.Value;
                            cr.AccessToken = hfAccessToken.Value;

                            db.ContestantsReferences.InsertOnSubmit(cr);
                            db.SubmitChanges();
                        }
                    }
                }
                LoadData(12);
            }
            catch
            {

            }
        }

        public void btnhReload_Click(object sender, EventArgs e)
        {
            string ccUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            if (ccUrl.Contains("?"))
            {
                string[] arrUrl = ccUrl.Split('?');
                ccUrl = arrUrl[0];
            }
            Response.Redirect(ccUrl);
        }

        public void btnhReloadLazy_Click(object sender, EventArgs e)
        {
            Parameter para = JsonConvert.DeserializeObject<Parameter>(Request["__EVENTARGUMENT"]);

            LoadData(para.PageSize);
        }

        public class Parameter
        {
            public int PageSize { get; set; }
        }
    }
}