﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.library
{
    public partial class ucLibrary : System.Web.UI.UserControl
    {
        string GroupLibraryId = "";
        string Id = "";
        string lang = "vi";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["hp"] != null)
            {
                var isDetail = db.tbPages.Where(s => s.pagTagName == Request["hp"].ToString() && s.pagType == 400).FirstOrDefault();
                if (isDetail != null)
                {
                    Id = db.tbPages.Where(s => s.pagTagName == Request["hp"].ToString() && s.pagType == 400).FirstOrDefault().pagTagName.ToString();
                    GroupLibraryId = db.tbPages.Where(s => s.pagType == 400 && s.paglevel == isDetail.paglevel).FirstOrDefault().pagTagName.ToString();
                }
                else
                {
                    try
                    {
                        GroupLibraryId = db.tbPages.Where(s => s.pagTagName == Request["hp"].ToString() && s.pagType == 300).FirstOrDefault().pagTagName.ToString();
                    }
                    catch { }
                }
            }
            if (Request["l"] != null) { Id = Request["l"].ToString(); }
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                BindData();
            }
        }
        void BindData()
        {
            var glist = db.tbPages.Where(g => g.pagLang == lang && g.paglevel.Length == 5 && g.pagActive == 1 && g.pagType == 300).ToList();
            Library listd = db.Libraries.FirstOrDefault(l => l.Lang == lang && l.TagName == Id);
            IEnumerable<Library> list = db.Libraries.Where(l => l.Lang == lang).OrderBy(s => s.Ord).ThenByDescending(s => s.Id);
            list = null;
            string strReturn = "";

            if (common.NullorEmpty(GroupLibraryId))
            {
                tbPage dbgrlib = db.tbPages.FirstOrDefault(s => s.pagTagName == GroupLibraryId);
                glist =
                    db.tbPages.Where(
                        g =>
                            g.paglevel.StartsWith(dbgrlib.paglevel) && g.paglevel.Length >= dbgrlib.paglevel.Length &&
                            g.pagLang == lang && g.pagActive == 1).ToList();
            }
            else
            {
                glist =
                    db.tbPages.Where(
                        g => g.pagLang == lang && g.paglevel.Length == 5 && g.pagActive == 1 && g.pagType == 300)
                        .ToList();
            }
            foreach (tbPage lst in glist)
            {
                var listAdv = db.tbAdvertise_categories.Where(s => s.CatID == lst.pagId).ToList();
                if (listAdv.Count() > 0)
                {
                    string strAdv = "";
                    var arrId = listAdv.Select(s => s.advertisID).ToArray();
                    var dsAdv = db.tbAdvertises.Where(s => arrId.Contains(s.advId) && s.advLang == lang && s.advActive == 1).OrderBy(s => s.advOrd).ToList();
                    strAdv += "<div class='advByGroup' id='advCat'>";
                    for (int i = 0; i < dsAdv.Count(); i++)
                    {
                        strAdv += "<div class='advItem'>";
                        strAdv += "<a class='advItemLink' href='" + dsAdv[i].advLink + "' title='" + dsAdv[i].advName + "' target='" + dsAdv[i].advTarget + "'><img class='advItemImg' src='" + dsAdv[i].advImage + "' alt='" + dsAdv[i].advName + "' /></a>";
                        strAdv += "</div>";
                    }
                    strAdv += "</div>";
                    ltrAdv.Text = strAdv;
                }
            }
            if (common.NullorEmpty(GroupLibraryId))
            {
                tbPage dbgrlib = db.tbPages.FirstOrDefault(s => s.pagTagName == GroupLibraryId);
                list = db.Libraries.Where(s => s.GroupLibraryId == dbgrlib.pagId && s.Lang == lang && s.Active == 1).ToList();
                strReturn += "<div class=\"cate-header sub-top\"><div class=\"txt-name-sub\">" + dbgrlib.pagName + "</div><div class='libraryDes'>" + dbgrlib.pagDetail + "</div></div>";
                glist = db.tbPages.Where(g => g.paglevel.StartsWith(dbgrlib.paglevel) && g.paglevel.Length >= dbgrlib.paglevel.Length && g.pagLang == lang && g.pagActive == 1).ToList();
            }
            else
            {
                glist = db.tbPages.Where(g => g.pagLang == lang && g.paglevel.Length == 5 && g.pagActive == 1 && g.pagType == 300).ToList();
                string img = "";
                if (glist.Count() > 0)
                {
                    for (int i = 0; i < glist.Count(); i++)
                    {                        
                        if (glist[i].pagImage != "")
                        {
                            img += "<a href=\"/" + glist[i].pagTagName + ".html\" ><img src=\"" + glist[i].pagImage + "\" width=\"357\" height=\"300\" style=\"margin-left:-8px; margin-top:4px\" /></a>";
                        }
                    }
                }
                strReturn += "<div class=\"cate-header sub-top\"><div class=\"txt-name-sub\">" + MyWeb.Global.GetLangKey("library") + "</div></div>";
            }            
            strReturn += "<div class=\"box-body clearfix\">";
            foreach (tbPage lst in glist)
            {
                strReturn += "<div class=\"box-library-list\">";
                string strSrc = common.NullorEmpty(lst.pagImage) ? common.ThumbImage(lst.pagImage) : "/images/no_image.png";
                string link = "/" + lst.pagTagName + ".html";
                strReturn += string.Format("", link, lst.pagName, strSrc);
                strReturn += "<a class=\"frame-img\" href='" + link + "' title='" + lst.pagName + "'>";
                strReturn += "<img src='" + strSrc + "' alt='" + lst.pagName + "' />";
                strReturn += "</a>";
                strReturn += String.IsNullOrEmpty(lst.pagDetail) ? "" : "<div class='libDes'>" + lst.pagDetail + "</div>";
                strReturn += "<a class=\"txt-name\" href='" + link + "' title='" + lst.pagName + "'>" + lst.pagName + "</a>";                
                strReturn += "</div>";
            }
            if (Id != "")
            {
                list = db.Libraries.Where(l => l.Active == 1 && l.GroupLibraryId == listd.GroupLibraryId && l.TagName != Id).Skip(0).Take(20);

                var firstVideo = listd.File;
                firstVideo = firstVideo.Replace("&feature=youtu.be", "");
                firstVideo = firstVideo.Replace("watch?v=", "embed/");
                strReturn += "<div class=\"Library_Detail\">";
                strReturn += "<iframe width=\"560\" height=\"340\" src=\"" + firstVideo + "\" frameborder=\"0\" allowfullscreen></iframe><div class=\"lbrname\">" + listd.Name + "</div>";
                strReturn += "</div>";
            }
            if (list != null)
            {             
                foreach (Library dblib in list)
                {
                    string strFile = dblib.File;
                    string strLink = "/" + dblib.TagName + ".html";
                    //Neu co file dinh kem
                    strReturn += "<div class=\"box-library-list\">";
                    if (common.NullorEmpty(strFile))
                    {                        
                        string strSrc = common.NullorEmpty(dblib.Image) ? common.ThumbImage(dblib.Image) : "/images/icon-video.png";
                        //Adv image
                        if (strFile.Contains(".jpg") || strFile.Contains(".jpeg") || strFile.Contains(".gif") || strFile.Contains(".png") || strFile.Contains(".bmp"))
                        {
                            strReturn += string.Format("<a class=\"frame-img group-box\" class=\"slideImg\" href=\"{0}\" title=\"{1}\" ><img src=\"{2}\" alt=\"{1}\"/></a><a class=\"txt-name\" href=\"{0}\" title=\"{1}\">{1}</a>", strFile, dblib.Name, strSrc);
                        }
                        //Video
                        else if (strFile.Contains(".flv") || strFile.Contains("youtube") || strFile.Contains("youtu.be"))
                        {                            
                            string strURL = dblib.File;
                            strURL = strURL.Replace("&feature=youtu.be", "");
                            strURL = strURL.Replace("watch?v=", "embed/");
                            strURL = strURL.IndexOf("?rel=0") > 0 ? strURL : strURL + "?rel=0";
                            strReturn += "<div class='frame-img'><iframe src='" + strURL + "' frameborder=\"0\" allowfullscreen></iframe><div class=\"lbrname\">" + dblib.Name + "</div></div>";
                        }
                        else if (strFile.Contains(".doc") || strFile.Contains(".docx") || strFile.Contains(".xls") || strFile.Contains(".xlsx") || strFile.Contains(".pdf") || strFile.Contains(".rar") || strFile.Contains(".zip") || strFile.Contains(".7z"))
                        {
                            strReturn += string.Format("<a class=\"frame-img\" href=\"{0}\" title=\"{1}\"><img src=\"{2}\" alt=\"{1}\"/></a><a class=\"txt-name\" href=\"{0}\" title=\"{1}\">{1}</a><a href=\"{0}\" title=\"{1}\">Download <img src=\"/images/" + common.GetFileExtension(dblib.File) + ".gif\" alt=\"{1}\" width=\"16\"/></a>", dblib.File, dblib.Name, strSrc);
                        }
                        else
                        {
                            strReturn += string.Format("<a class=\"frame-img group-box\" href=\"{0}\" title=\"{1}\"><img src=\"{2}\" alt=\"{1}\"/></a><a class=\"txt-name\" href=\"{0}\" title=\"{1}\">{1}</a>", strLink, dblib.Name, strSrc);
                        }
                    }
                    else if (common.NullorEmpty(dblib.Image))
                    {                                                                        
                        string strSrc = common.NullorEmpty(dblib.Image) ? common.ThumbImage(dblib.Image) : "/images/no_image.gif";
                        strReturn += string.Format("<a class=\"frame-img group-box\" href=\"{0}\" title=\"{1}\"><img src=\"{2}\" alt=\"{1}\"/></a><a class=\"txt-name\" href=\"{0}\" title=\"{1}\">{1}</a>", dblib.Image, dblib.Name, strSrc);                        
                    }
                    strReturn += "</div>";
                }
            }
            
            strReturn += "</div>";
            ltrLibrary.Text = strReturn;
        }
    }
}