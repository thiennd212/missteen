﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucListVideo.ascx.cs" Inherits="MyWeb.views.library.ucListVideo" %>

<div class="box-top-video">
    <div class="header">Video</div>
    <div class="body-video">
        <div id="myElement">
            <asp:Literal runat="server" ID="ltrIframeVideo"></asp:Literal>
        </div>
        <asp:Literal ID="ltrListVideo" runat="server"></asp:Literal>
    </div>
</div>

<script type="text/javascript">
    $(".videoItems").on("click", function () {
        var strURL = this.name;
        strURL = strURL.replace("&feature=youtu.be", "");
        strURL = strURL.replace("watch?v=", "embed/");
        strURL = strURL.indexOf("?rel=0") > 0 ? strURL : strURL + "?rel=0";
        strURL = strURL.indexOf("&autoplay=1") > 0 ? strURL : strURL + "&autoplay=1";
        $("#myElement").find("iframe")[0].src = strURL;
    })
</script>
