﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.news
{
    public partial class ucNewsHomePriority : System.Web.UI.UserControl
    {
        string lang = "vi";
        public bool slideNews = false;
        string viewNews3 = "3";
        string viewNews7 = "5";
        string viewNews8 = "5";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            ltrDate.Text = common.ConvertDate(DateTime.Now);
            lang = MyWeb.Global.GetLang();
            if (GlobalClass.viewNews3 != "") { viewNews3 = GlobalClass.viewNews3; }
            if (GlobalClass.viewNews8 != "") { viewNews8 = GlobalClass.viewNews8; }
            if (GlobalClass.viewNews8 != "") { viewNews7 = GlobalClass.viewNews8; }
            if (!IsPostBack)
            {
                BindEvent();
                BindLastNews();
                BindTopical();
            }
        }

        private void BindEvent()
        {
            string _str = "";
            List<tbNewsDATA> list = new List<tbNewsDATA>();
            list = tbNewsDB.tbNews_GetByTop(viewNews7, "newimg=1 and len(newImage) > 0 and newActive=0 and newLang='" + lang + "'", "newDate desc,newId desc");
            int i = 0;
            if (list.Count > 0)
            {
                slideNews = true;
                _str += " <div class=\"views-priority\">";

                _str += "<div id='owl-news-home' class='event-views '>";
                for (i = 0; i < list.Count; i++)
                {
                    tbGroupNew gn = db.tbGroupNews.Where(s => s.grnId == int.Parse(list[i].grnID)).FirstOrDefault();
                    string link = "/" + list[i].newTagName + ".html";
                    _str += "<div class=\"item\">";
                    _str += "<a title=\"" + list[i].newName + "\" href=\"" + link + "\"><img src=\"" + list[i].newImage + "\" alt=\"" + list[i].newName + "\"  /></a>";
                    _str += "<div class=\"view-face\">";
                    _str += string.Format("<a href=\"{0}\" title=\"{1}\">{1}</a><p class=\"view-p\">{2}</p>", link, MyWeb.Common.StringClass.GetContent(list[i].newName, 96), list[i].newContent);
                    _str += "</div>";
                    _str += "</div>";
                }
                _str += "</div>";

                _str += "</div>";
            }

            list.Clear();
            list = null;
            ltrEvent.Text = _str;
        }

        private void BindLastNews()
        {
            string _str = "";
            List<tbNewsDATA> list = new List<tbNewsDATA>();
            list = tbNewsDB.tbNews_GetByTop(viewNews3, "newActive=0 and newLang='" + lang + "'", "newDate desc,newId desc");
            for (int i = 0; i < list.Count; i++)
            {
                tbGroupNew gn = db.tbGroupNews.Where(s => s.grnId == int.Parse(list[i].grnID)).FirstOrDefault();
                if (i == 0)
                {
                    _str += "<ul class=\"ul-last-news\">";
                }
                try
                {
                    string strLast = i == list.Count - 1 ? " class=\"last\"" : string.Empty;
                    string link = "/" + list[i].newTagName + ".html";
                    _str += string.Format("<li" + strLast + "><a href=\"{0}\" title=\"{1}\">{1}</a></li>", link, MyWeb.Common.StringClass.GetContent(list[i].newName, 96));
                }
                catch { }
                if (i == list.Count - 1)
                {
                    _str += "</ul>";
                }
            }
            list.Clear();
            list = null;
            ltrLastNews.Text = _str;
        }

        private void BindTopical()
        {
            string _str = "";
            List<tbNewsDATA> list = new List<tbNewsDATA>();
            list = tbNewsDB.tbNews_GetByTop(viewNews7, "newType=1 and newActive=0 and newLang='" + lang + "'", "newDate desc,newid desc");
            int i = 0;
            if (list.Count > 0)
            {
                for (i = 0; i < list.Count; i++)
                {
                    tbGroupNew gn = db.tbGroupNews.Where(s => s.grnId == int.Parse(list[i].grnID)).FirstOrDefault();
                    _str += "<div class='frame-top'>";
                    string link = "/" + list[i].newTagName + ".html";
                    if (list[i].newImage.Length > 0)
                    {
                        _str += string.Format("<a class=\"view-img\" href=\"{0}\" title=\"{2}\"><img src=\"{1}\" alt=\"{2}\" title=\"{2}\" /></a>", link, list[i].newImage, list[i].newName);
                    }
                    _str += string.Format("<a class=\"view-link\" href=\"{0}\" title=\"{1}\">{1}</a>", link, MyWeb.Common.StringClass.GetContent(list[i].newName), MyWeb.Common.StringClass.GetContent(list[i].newName));
                    _str += "</div>";
                }
            }
            list.Clear();
            list = null;
            ltrTopical.Text = _str;
        }
    }
}