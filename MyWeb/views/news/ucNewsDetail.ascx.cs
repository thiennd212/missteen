﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.news
{
    public partial class ucNewsDetail : System.Web.UI.UserControl
    {
        string lang = "vi";
        string strNewTag = "";
        public string strLink = "";
        public string strUrl = "";
        int iEmptyIndex = 0;
        int currentPage;
        public string strUrlFace = "";
        public string wf = "770";
        public string nf = "5";
        public string cf = "light";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            strUrl = HttpContext.Current.Request.RawUrl;
            if (Request["hp"] != null) { strNewTag = Request["hp"].ToString(); }
            iEmptyIndex = strNewTag.IndexOf("?");
            if (iEmptyIndex != -1)
            {
                strNewTag = strNewTag.Substring(0, iEmptyIndex);
            }
            currentPage = Request.QueryString["page"] != null ? int.Parse(Request.QueryString["page"]) : 1;          
            if (GlobalClass.widthfkComment != null) { wf = GlobalClass.widthfkComment; }
            if (GlobalClass.numfkComment != null) { nf = GlobalClass.numfkComment; }
            if (GlobalClass.colorfkComment != null) { cf = GlobalClass.colorfkComment; }
            lbAlter.Visible = false;

            if (!IsPostBack)
            {                
                BindData();
                BindAttachFile();
            }
        }
        private void BindData()
        {
            tbNew objNews = db.tbNews.FirstOrDefault(s => s.newTagName == strNewTag && s.newLang == lang);
            List<tbNewsDATA> _objNewsOld = tbNewsDB.tbNews_Paging("1", GlobalClass.viewNews6, "*", "newTagName != '" + strNewTag + "' AND newDate >= '" + Convert.ToDateTime(objNews.newDate).ToString("MM/dd/yyyy HH:mm:ss") + "' AND grnId = " + objNews.grnID + " AND newActive = 0", "newDate desc, newId desc");
            List<tbNewsDATA> _objNewsOrther = tbNewsDB.tbNews_Paging("1", GlobalClass.viewNews5, "*", "newTagName != '" + strNewTag + "' AND newDate <= '" + Convert.ToDateTime(objNews.newDate).ToString("MM/dd/yyyy HH:mm:ss") + "' AND grnId = " + objNews.grnID + " AND newActive = 0", "newDate desc, newId desc");            
            strUrlFace = Request.Url.ToString();
            if (objNews != null)
            {
                // Load detail
                ltrNewName.Text = objNews.newName;
                hdNewId.Value = objNews.newid.ToString();
                string strNewDetail = "";
                strNewDetail += "<div class='news-img-detailt'><img alt='" + objNews.newName + "' src='" + objNews.newImage + "' /></div>";
                strNewDetail += "<div class='news-container'>" + objNews.newContent + "</div>";
                strNewDetail += "<div class='news-detailt'>" + objNews.newDetail + "</div>";
                ltrNewDetail.Text = strNewDetail;

                if (objNews.newTag != null && objNews.newTag != "")
                {
                    string strt = "<b>Tag</b>: ";
                    string tag = objNews.newTag;
                    tag = tag.Trim();
                    string[] arrTag = tag.Split(Convert.ToChar(";"));
                    if (arrTag.Count() > 1)
                    {
                        for (int i = 0; i < arrTag.Count(); i++)
                        {
                            string t = arrTag[i].Replace(" ", "-");
                            strt += "<a rel='tagn' href='/tagn_" + t + ".html'>" + t + "</a>";
                        }
                    }
                    else
                    {
                        tag = tag.Replace(",", "");
                        strt += "<a rel='tagn' href='/tagn_" + tag + ".html'>" + tag + "</a>";
                    }
                    ltrTag.Text = strt;
                }
                else
                {
                    ltrTag.Text = "";
                }

                if (_objNewsOld.Count > 0)
                {
                    rptNewsOld.DataSource = _objNewsOld;
                    rptNewsOld.DataBind();
                    pnlNewsOld.Visible = true;
                }
                if (_objNewsOrther.Count > 0)
                {
                    rptNewsOrther.DataSource = _objNewsOrther;
                    rptNewsOrther.DataBind();
                    pnNewsOrther.Visible = true;
                }
                var preNews = db.tbNews.Where(x => x.newid < objNews.newid && x.grnID == objNews.grnID && x.newActive == 0).OrderByDescending(x => x.newid).FirstOrDefault();
                if (preNews != null)
                {
                    lrtTinLienQuanKhac.Text += "<div class=\"col-md-3 left\"><a href=\"/" + preNews.newTagName + ".html\" title=\"" + preNews.newName + "\">Tin cũ hơn</a></div>";
                }
                var nextNews = db.tbNews.Where(x => x.newid > objNews.newid && x.grnID == objNews.grnID && x.newActive == 0).OrderBy(x => x.newid).FirstOrDefault();
                if (nextNews != null)
                {
                    lrtTinLienQuanKhac.Text += "<div class=\"col-md-3 right\"><a href=\"/" + nextNews.newTagName + ".html\" title=\"" + nextNews.newName + "\">Tin mới hơn</a></div>";
                }
            }

            // Load list comment
            var listComment = db.tbComments.Where(s => s.comActive == 1 && s.comNewId == objNews.newid).OrderByDescending(s => s.comId).ToList();
            int sizeComment = 10;
            if (listComment.Count() > 0) {
                if (listComment.Count > sizeComment)
                    ltrPagingComment.Text = getCommentPaging(currentPage, sizeComment, listComment.Count);
                listComment = listComment.Skip((currentPage - 1) * sizeComment).Take(sizeComment).ToList();
                string strComment = "";
                strComment += "<div id='listComment'>";
                for (int c = 0; c < listComment.Count(); c++) {
                    strComment += "<div class='commentNewItem'>";
                    strComment += "<span class='itemName'>" + listComment[c].comName + "</span>";
                    strComment += "<span class='itemEmail'>" + listComment[c].comEmail + "</span>";
                    strComment += "<span class='itemContent'>" + listComment[c].comComment + "</span>";
                    strComment += "<span class='itemCreatedDate'>" + listComment[c].comDate + "</span>";
                    strComment += "</div>";
                }
                strComment += "</div>";
                ltrListComment.Text = strComment;                
            }
        }
        protected string getCommentPaging(int cur, int size, int total)
        {
            string strResult = "";
            var numberOfPage = total % size == 0 ? total / size : (total / size) + 1;
            strResult += "<ul class='paging'>";
            var curUrl = Request.RawUrl;
            curUrl = curUrl.IndexOf("?") > 0 ? curUrl.Substring(0, curUrl.IndexOf("?")) : curUrl;
            for (int i = 1; i <= numberOfPage; i++) {
                strResult += i == cur ?
                    "<li class='active'>" + i + "</li>" :
                    "<li class='item'><a href='" + curUrl + "?page=" + i + "' title='Trang " + i + "'>" + i + "</a></li>";
            }
            strResult += "</ul>";
            return strResult;
        }
        void BindAttachFile()
        {
            List<tbNewsDATA> list = tbNewsDB.sp_tbNews_GetByTag(strNewTag, lang);
            if (list.Count > 0)
            {
                if (list[0].newFile == null || list[0].newFile == "")
                {
                    btnDownload.Visible = false; pnDownload.Visible = false; ltrVideo.Text = ""; return;
                }
                string typefile = list[0].newFile.Substring(list[0].newFile.LastIndexOf('.') + 1);
                if (typefile == "swf" || typefile == "flv" || typefile == "wma" || typefile == "wmv" || typefile == "avi" || typefile == "mp3" || typefile == "wav" || typefile == "dat")
                {
                    btnDownload.Visible = false;
                    ltrVideo.Text = string.Format("<script type=\"text/javascript\"> playfile('{0}', '{1}', '{2}', 'false', '{3}', '', '')</script>", list[0].newFile, "420", "340", list[0].newImage);
                }
                else
                {
                    btnDownload.Visible = true;
                }
            }


        }
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            List<tbNewsDATA> list = tbNewsDB.sp_tbNews_GetByTag(strNewTag, lang);
            DownloadFile(list[0].newFile);
        }
        private void DownloadFile(string strFile)
        {
            try
            {
                strFile = Server.MapPath(strFile);
                WebClient req = new WebClient();
                HttpResponse response = HttpContext.Current.Response;
                response.Clear();
                response.ClearContent();
                response.ClearHeaders();
                response.Buffer = true;
                FileInfo file = new FileInfo(strFile);
                response.AddHeader("Content-Disposition", "attachment;filename=\"" + file.Name + "\"");
                byte[] data = req.DownloadData(strFile);
                response.BinaryWrite(data);
                response.End();
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "alert", "alert('" + MyWeb.Global.GetLangKey("file_download_notfound") + "');", true);
            }
        }

        protected void lbtSendComment_Click(object sender, EventArgs e)
        {
            try
            {
                tbComment obj = new tbComment();
                obj.comName = txtCommentName.Text;
                obj.comEmail = txtCommentEmail.Text;
                obj.comComment = txtCommentContent.Text;
                obj.comDate = DateTime.Now;
                obj.comNewId = Convert.ToInt64(hdNewId.Value.ToString());
                obj.comActive = 0;
                db.tbComments.InsertOnSubmit(obj);
                db.SubmitChanges();
                lbAlter.Visible = true;
                lbAlter.Text = "Gửi bình luận thành công !";
                txtCommentName.Text = "";
                txtCommentEmail.Text = "";
                txtCommentContent.Text = "";
            }
            catch (Exception)
            {
                lbAlter.Visible = true;
                lbAlter.Text = "Có lỗi xảy ra, vui lòng thử lại sau !";
            }            
        }
    }
}