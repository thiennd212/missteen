﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucAboutUs.ascx.cs" Inherits="MyWeb.views.news.ucAboutUs" %>

<%if (showNews)
  {%>
<div class="box-about-us">
    <div class="header">
        <asp:Literal ID="ltrName" runat="server"></asp:Literal></div>
    <div class="body-news">
        <asp:Literal ID="ltrContent" runat="server"></asp:Literal>
    </div>
</div>

<%} %>