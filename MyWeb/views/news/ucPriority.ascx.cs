﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.news
{
    public partial class ucPriority : System.Web.UI.UserControl
    {
        string lang = "vi";
        public bool showAdv = true;
        private string strNumberView = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                BindData();
            }
        }
        private void BindData()
        {
            string _str = "";
            strNumberView = GlobalClass.viewNews4 != "" ? GlobalClass.viewNews4 : "10";
            List<tbNewsDATA> list = tbNewsDB.tbNews_GetByTop(strNumberView, "newActive=0 and newPriority=1 and newLang ='" + lang + "'", "newDate DESC,newId desc");
            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    tbPage gn = db.tbPages.Where(s => s.pagId == int.Parse(list[i].grnID)).FirstOrDefault();
                    string link = gn != null ? "/" + list[i].newTagName + ".html" : "";
                    _str += "<li class=\"item-list\">";
                    if (list[i].newImage.ToString().Length > 0)
                    {
                        _str += "<a class=\"img-view\" title=\"" + list[i].newName.ToString() + "\" href=\"" + link + "\"><img src=\"" + list[i].newImage.ToString() + "\" alt=\"" + list[i].newName.ToString() + "\" /></a>";
                    }
                    else
                    {
                        _str += "<a class=\"img-view\" title=\"" + list[i].newName.ToString() + "\" href=\"" + link + "\"><img src=\"images/no_image.jpg\" alt=\"" + list[i].newName.ToString() + "\" /></a>";
                    }
                    _str += "<h5><a class=\"link-view\" title=\"" + list[i].newName.ToString() + "\" href=\"" + link + "\">" + list[i].newName.ToString() + "</a></h5>";
                    _str += "<div class=\"news-more\" ><a href='" + link + "' rel=\"nofollow\">" + MyWeb.Global.GetLangKey("news_more") + "</a></div>";
                    _str += "<div class=\"div-news-container\" style=\"display:none;\">" + list[i].newContent.ToString() + "</div>";
                    _str += "</li>";
                }
            }
            else
                showAdv = false;
            ltrNew.Text = _str;
            list.Clear();
            list = null;
        }
    }
}