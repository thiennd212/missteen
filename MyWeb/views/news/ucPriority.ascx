﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucPriority.ascx.cs" Inherits="MyWeb.views.news.ucPriority" %>

<%if (showAdv)
  {%>
<div class="box-news-priority">
    <div class="header"><%= MyWeb.Global.GetLangKey("news_priority")%></div>
    <ul class="body-news">
        <asp:Literal ID="ltrNew" runat="server"></asp:Literal>
    </ul>
</div>

<%} %>