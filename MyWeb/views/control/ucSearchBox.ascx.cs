﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucSearchBox : System.Web.UI.UserControl
    {
        private string inputValue = MyWeb.Global.GetLangKey("search_text");
        protected void Page_Load(object sender, EventArgs e)
        {
            txtSearch.Attributes.Add("placeholder", inputValue);
            this.Page.Form.DefaultButton = btnSearch.UniqueID;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            var strSearch = txtSearch.Text;
            Session["txtSearch"] = txtSearch.Text;
            Response.Redirect("/tim-kiem.html");
        }
    }
}