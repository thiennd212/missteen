﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucMenuFooter : System.Web.UI.UserControl
    {
        string lang = "vi";
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                BindData();
            }
        }
        private void BindData()
        {
            string strMenu = "";
            int i = 0;
            List<tbPageDATA> lists = tbPageDB.tbPage_GetBypagPosition5("2", lang).Where(s => s.pagActive == "1").ToList();
            foreach (tbPageDATA list in lists)
            {
                strMenu += "<li class=\"parent" + (i == 0 ? " first" : "") + "\"><h3>" + list.pagName + "</h3>";
                List<tbPageDATA> list2 = tbPageDB.tbPage_GetBypagSub(list.paglevel, lang).Where(s => s.pagActive == "1").ToList();
                if (list2.Count > 0)
                {
                    strMenu += "<ul>";
                    for (int j = 0; j < list2.Count; j++)
                    {
                        string lienket = "";
                        if (list2[j].pagType == "3")
                        {
                            lienket = MyWeb.Common.StringClass.GetContent(list2[j].pagLink, 7);
                            if (!(lienket == "http://..."))
                            {
                                lienket = "http://";
                            }
                            else
                            {
                                lienket = "";
                            }
                        }
                        string strLink = list2[j].pagType == "3" ? list2[j].pagLink : list2[j].pagLink;
                        strMenu += "<li><a href=\"" + strLink + "\" target=\"" + list2[j].pagTarget + "\" title=\"" + list2[j].pagName + "\">" + list2[j].pagName + "</a></li>";
                    }
                    strMenu += "</ul>";
                }
                strMenu += "</li>";
                i++;
            }
            lists.Clear();
            lists = null;
            ltrMenuFooter.Text = strMenu;
        }
    }
}