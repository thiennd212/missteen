﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucAdvCenter.ascx.cs" Inherits="MyWeb.views.control.ucAdvCenter" %>
<%if (showAdv)
  {%>
<div class="box-adv-center">
    <div class="header"><%= MyWeb.Global.GetLangKey("advertise")%></div>
    <ul class="body-adv-center">
        <asp:Literal ID="ltradv" runat="server"></asp:Literal>
    </ul>
</div>
<%} %>