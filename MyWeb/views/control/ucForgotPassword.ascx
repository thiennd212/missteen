﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucForgotPassword.ascx.cs" Inherits="MyWeb.views.control.ucForgotPassword" %>

<div class="box-forgot-password">
    <div class="cate-header">
        <div class="txt-name-sub"><%= MyWeb.Global.GetLangKey("user_forgot_password") %></div>
    </div>
    <div class="box-body-sub">
        <div class="row">
            <div class="info"><%=MyWeb.Global.GetLangKey("user_forgot_pass_enter_email_account")%></div>
            <asp:Label ID="lblConfig" runat="server"></asp:Label>
            <asp:HiddenField ID="hdPass" runat="server" />
        </div>
        <div class="row">
            <div class="form-group">
                <span class="view-messenger">
                    <asp:Label ID="lblthongbao" runat="server"></asp:Label></span>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3"><%=MyWeb.Global.GetLangKey("user_username")%>*:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtUser" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="rfvname" runat="server" ControlToValidate="txtUser" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3">Email:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtmail" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RegularExpressionValidator ID="revmail" runat="server" ControlToValidate="txtmail" ErrorMessage="*" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True" Display="Dynamic"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"></label>
                <div class="col-md-7">
                    <asp:LinkButton ID="btnSend" runat="server" OnClick="btnSend_Click"><%= MyWeb.Global.GetLangKey("contact_send") %></asp:LinkButton>
                    <asp:LinkButton ID="btnReset" runat="server" OnClick="btnReset_Click"><%= MyWeb.Global.GetLangKey("contact_cancel")%></asp:LinkButton>
                </div>
            </div>


        </div>
    </div>
</div>
