﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucForgotPassword : System.Web.UI.UserControl
    {
        string lang = "vi";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            List<tbMember> lstCK = db.tbMembers.Where(s => s.uID == txtUser.Text && s.Email == txtmail.Text).ToList();
            if (lstCK.Count > 0)
            {
                hdPass.Value = randomPass();
                #region[Sendmail]
                List<tbConfigDATA> list = tbConfigDB.tbConfig_GetLang(lang);
                string mailfrom = list[0].conMail_Noreply;
                string mailto = list[0].conMail_Info;
                string pas = list[0].conMail_Pass;
                string host = host = "smtp.gmail.com";
                if (list[0].conMail_Method.Length > 0)
                {
                    host = list[0].conMail_Method;
                }
                int post = 465;
                if (list[0].conMail_Port.Length > 0)
                {
                    post = int.Parse(list[0].conMail_Port);
                }
                string cc = "";
                string Noidung = "";
                Noidung = "<table><tr><td><b>Xin chào:</b></td><td>" + txtUser.Text + "</td></tr>";
                Noidung += "<tr><td><b>Bạn đã sử dụng chức năng quên mật khẩu trên website :</b></td><td>http://" + Request.Url.Host + "</td></tr>";
                Noidung += "<tr><td><b>Mật khẩu mới của bạn là:</b></td><td>" + hdPass.Value + "</td></tr>";
                try
                {
                    //common.SendMail(mailto, cc, "", "", "Thông tin liên hệ từ " + txtname.Text, Noidung);
                    common.SendMail(txtmail.Text.Trim(), "", "", "", "Thông báo đổi mật khẩu từ " + Request.Url.Host, Noidung);
                    List<tbMembersDATA> lists = tbMembersDB.tbMembers_GetByUser(txtUser.Text);
                    lists[0].ID = lists[0].ID;
                    lists[0].uID = common.killChars(txtUser.Text);
                    lists[0].uPas = vmmsclass.Encodingvmms.Encode(hdPass.Value);
                    lists[0].Email = lists[0].Email;
                    lists[0].Add = lists[0].Add;
                    lists[0].Tell = lists[0].Tell;
                    lists[0].Fax = "";
                    if (tbMembersDB.tbMembers_Update(lists[0]))
                    {
                        Response.Write("<script>alert('" + MyWeb.Global.GetLangKey("user_forgot_pass_success") + "'); window.location.assign('http://" + Request.Url.Host + "')</script>");
                    }
                }
                catch
                {
                    lblthongbao.Text = "Lỗi không gửi được email !!";
                }
                #endregion
            }
            else
            {
                lblthongbao.Text = MyWeb.Global.GetLangKey("user_forgot_pass_error");
            }
        }

        protected string randomPass()
        {
            Random randomNumber = new Random();
            int generatedNo = randomNumber.Next(100, int.MaxValue);
            return generatedNo.ToString();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtUser.Text = "";
            txtmail.Text = "";
            Response.Redirect("/quen-mat-khau.html");
        }
    }
}