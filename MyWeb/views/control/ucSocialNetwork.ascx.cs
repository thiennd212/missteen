﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucSocialNetwork : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public void btnhReload_Click(object sender, EventArgs e)
        {
            string ccUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            if (ccUrl.Contains("?"))
            {
                string[] arrUrl = ccUrl.Split('?');
                ccUrl = arrUrl[0];
            }            
            Response.Redirect(ccUrl);
        }
    }
}