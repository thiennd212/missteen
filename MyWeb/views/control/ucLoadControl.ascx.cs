﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucLoadControl : System.Web.UI.UserControl
    {
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["e"] != null)
            {
                if (Request["e"].ToString() == "load")
                {
                    string _request = Request["hp"] != null ? Request["hp"].ToString() : Request.Path;
                    string _strLink = Request["hp"].ToString() + ".html";
                    if (!_request.ToLower().Contains("default.aspx"))
                    {
                        try
                        {
                            string _strType = "";
                            var _curTypeLink = db.tbPages.Where(s => s.pagLink == _strLink).ToList();
                            if (_curTypeLink.Count > 0)
                            {
                                _strType = _curTypeLink[0].pagType.ToString();
                            }
                            else
                            {
                                var _curTypeRequest = db.tbPages.Where(s => s.pagTagName == Request["hp"].ToString()).ToList();
                                if (_curTypeRequest != null && _curTypeRequest.Count > 0)
                                {
                                    var _curTypeTag = db.tbPages.Where(s => s.pagTagName == _curTypeRequest[0].pagTagName).FirstOrDefault();
                                    _strType = _curTypeTag != null ? _curTypeTag.pagType.ToString() : _curTypeRequest[0].pagType.ToString();
                                }
                            }
                            if (string.IsNullOrEmpty(_strType))
                            {
                                var chkTour = db.Tours.FirstOrDefault(x => x.Tagname == _request);
                                if (chkTour != null)
                                    _strType = "704";
                            }
                            Session["offslide"] = _strType;

                            switch (_strType)
                            {
                                case "10":
                                    Controls.Add(LoadControl("~/views/pages/ucContactUs.ascx"));
                                    break;
                                case "6":
                                    Session["home_pro_list"] = "true";
                                    string cat = db.tbPages.Where(s => s.pagTagName == Request["hp"].ToString()).FirstOrDefault().pagId.ToString();
                                    if (GlobalClass.viewProducts7 == "1" || HasChild(cat) == false)
                                        Controls.Add(LoadControl("~/views/products/ucProList.ascx"));
                                    else
                                        Controls.Add(LoadControl("~/views/products/ucProHome.ascx"));
                                    break;
                                case "100":
                                    Session["home_pro_list"] = "true";
                                    string catid = db.tbPages.Where(s => s.pagTagName == Request["hp"].ToString()).FirstOrDefault().pagId.ToString();
                                    if (GlobalClass.viewProducts7 == "1" || HasChild(catid) == false)
                                        Controls.Add(LoadControl("~/views/products/ucProList.ascx"));
                                    else
                                        Controls.Add(LoadControl("~/views/products/ucProHome.ascx"));
                                    break;
                                case "20":
                                    Controls.Add(LoadControl("~/views/products/ucProListByMan.ascx"));
                                    break;
                                case "500":
                                    Controls.Add(LoadControl("~/views/products/ucProListByMan.ascx"));
                                    break;
                                case "7":
                                    Session["home_pro_list"] = "true";
                                    Controls.Add(LoadControl("~/views/products/ucProNew.ascx"));
                                    break;
                                case "900":
                                    Session["home_pro_view"] = "true";
                                    Controls.Add(LoadControl("~/views/products/ucProDetail.ascx"));
                                    break;
                                case "404":
                                    Controls.Add(LoadControl("~/views/pages/ucPage404.ascx"));
                                    break;
                                case "2":
                                    Controls.Add(LoadControl("~/views/pages/ucPageDetail.ascx"));
                                    break;
                                case "5":
                                    Controls.Add(LoadControl("~/views/news/ucNewsList.ascx"));
                                    break;
                                case "200":
                                    Controls.Add(LoadControl("~/views/news/ucNewsList.ascx"));
                                    break;
                                case "800":
                                    Controls.Add(LoadControl("~/views/news/ucNewsDetail.ascx"));
                                    break;
                                case "15":
                                    Controls.Add(LoadControl("~/views/products/ucCartList.ascx"));
                                    break;
                                case "16":
                                    Controls.Add(LoadControl("~/views/products/ucCartOrder.ascx"));
                                    break;
                                case "11":
                                    Controls.Add(LoadControl("~/views/control/ucRegister.ascx"));
                                    break;
                                case "12":
                                    Controls.Add(LoadControl("~/views/control/ucForgotPassword.ascx"));
                                    break;
                                case "13":
                                    Controls.Add(LoadControl("~/views/control/ucChangePassword.ascx"));
                                    break;
                                case "14":
                                    Controls.Add(LoadControl("~/views/control/ucChangeUser.ascx"));
                                    break;
                                case "9":
                                    Controls.Add(LoadControl("~/views/library/ucLibrary.ascx"));
                                    break;
                                case "300":
                                    Controls.Add(LoadControl("~/views/library/ucLibrary.ascx"));
                                    break;
                                case "400":
                                    Controls.Add(LoadControl("~/views/library/ucLibrary.ascx"));
                                    break;
                                case "21":
                                    Controls.Add(LoadControl("~/views/Tour/listTour.ascx"));
                                    break;
                                case "701":
                                    Controls.Add(LoadControl("~/views/Tour/listTour.ascx"));
                                    break;
                                case "704":
                                    Controls.Add(LoadControl("~/views/Tour/tourDetail.ascx"));
                                    break;
                                case "705":
                                    Controls.Add(LoadControl("~/views/Contestant/listContestant.ascx"));
                                    break;
                                case "706":
                                    Controls.Add(LoadControl("~/views/Contestant/contestantDetail.ascx"));
                                    break;
                                case "707":
                                    Controls.Add(LoadControl("~/views/Contestant/listContestant.ascx"));
                                    break;
                                case "24":
                                    Controls.Add(LoadControl("~/views/Contestant/listContestant.ascx"));
                                    break;
                            }

                        }
                        catch
                        {
                            Response.Redirect("/error404.html");
                        }
                    }
                }
                if (Request["e"].ToString() == "listpro")
                {
                    Session["home_pro_list"] = "true";
                    if (GlobalClass.viewProducts7 == "1")
                        Controls.Add(LoadControl("~/views/products/ucProList.ascx"));
                    else
                        Controls.Add(LoadControl("~/views/products/ucProHome.ascx"));
                }
                if (Request["e"].ToString() == "listnews")
                {
                    Controls.Add(LoadControl("~/views/news/ucNewsList.ascx"));
                }
                if (Request["e"].ToString() == "pronew")
                {
                    Session["home_pro_list"] = "true";
                    Controls.Add(LoadControl("~/views/products/ucProNew.ascx"));
                }
                if (Request["e"].ToString() == "cart")
                {
                    Controls.Add(LoadControl("~/views/products/ucCartList.ascx"));
                }
                if (Request["e"].ToString() == "cartcus")
                {
                    Controls.Add(LoadControl("~/views/products/ucCartOrder.ascx"));
                }
                if (Request["e"].ToString() == "dangnhap")
                {
                    Controls.Add(LoadControl("~/views/control/ucLogin.ascx"));
                }
                if (Request["e"].ToString() == "dangkythanhvien")
                {
                    Controls.Add(LoadControl("~/views/control/ucRegister.ascx"));
                }
                if (Request["e"].ToString() == "quenmatkhau")
                {
                    Controls.Add(LoadControl("~/views/control/ucForgotPassword.ascx"));
                }
                if (Request["e"].ToString() == "doimatkhau")
                {
                    Controls.Add(LoadControl("~/views/control/ucChangePassword.ascx"));
                }
                if (Request["e"].ToString() == "thongtintv")
                {
                    Controls.Add(LoadControl("~/views/control/ucChangeUser.ascx"));
                }
                if (Request["e"].ToString() == "historyorder")
                {
                    Controls.Add(LoadControl("~/views/control/ucOrderHistory.ascx"));
                }
                if (Request["e"].ToString() == "listUCControls")
                {
                    Controls.Add(LoadControl("~/views/listUC/listUCControls.ascx"));
                }
                if (Request["e"].ToString() == "listUCLibrarys")
                {
                    Controls.Add(LoadControl("~/views/listUC/listUCLibrarys.ascx"));
                }
                if (Request["e"].ToString() == "listUCNews")
                {
                    Controls.Add(LoadControl("~/views/listUC/listUCNews.ascx"));
                }
                if (Request["e"].ToString() == "listUCPages")
                {
                    Controls.Add(LoadControl("~/views/listUC/listUCPages.ascx"));
                }
                if (Request["e"].ToString() == "listUCProducts")
                {
                    Controls.Add(LoadControl("~/views/listUC/listUCProducts.ascx"));
                }
                if (Request["e"].ToString() == "tourByCat")
                {
                    Controls.Add(LoadControl("~/views/Tour/listTour.ascx"));
                }
                if (Request["e"].ToString() == "search")
                {
                    if (GlobalClass.pageProducts == true)
                    {
                        Session["home_pro_list"] = "true";
                        Controls.Add(LoadControl("~/views/search/ucProSearch.ascx"));
                    }
                    else
                    {
                        Controls.Add(LoadControl("~/views/search/ucNewsSearch.ascx"));
                    }
                }
                if (Request["e"].ToString() == "searchTour")
                {
                    Controls.Add(LoadControl("~/views/search/ucTourSearch.ascx"));
                }
                if (Request["e"].ToString() == "searchContestant")
                {
                    Controls.Add(LoadControl("~/views/search/ucContestantSearch.ascx"));
                }
            }
        }

        private bool HasChild(string catId)
        {
            bool b = false;
            if (catId == "")
                return b;
            tbPage cat = db.tbPages.FirstOrDefault(s => s.pagId == int.Parse(catId));
            if (cat != null)
            {
                b = db.tbPages.Any(s => s.pagType == 100 && s.paglevel.StartsWith(cat.paglevel) && s.paglevel.Length > cat.paglevel.Length);
            }
            return b;
        }
    }
}