﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucAdvright : System.Web.UI.UserControl
    {
        string lang = "vi";
        public bool showAdv = true;
        private string strNumberView = "3";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                BindData();
            }
        }

        private void BindData()
        {
            string _str = "";
            List<tbAdvertiseDATA> list = tbAdvertiseDB.tbAdvertise_GetByPosition_Vietime(strNumberView);
            list = list = list.Where(s => s.advLang.Trim().ToLower() == lang).ToList();
            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    string path = "";
                    path = list[i].advImage;
                    _str += "<div class='advItem'>";                    
                    if (path.IndexOf(".swf") > 0)
                    {
                        _str += "<li class=\"item-list\"><script language='javascript'>playfile('" + path + "', '" + list[i].advWidth + "', '" + list[i].advHeight + "', true, '', '', 'link=" + list[i].advLink + "');</script></li>";
                    }
                    else
                    {
                        _str += "<li class=\"item-list\"><a class=\"img-view\" title=\"" + list[i].advName + "\" href='" + list[i].advLink + "' target='" + list[i].advTarget + "'><img alt=\"" + list[i].advName + "\" src='" + path + "?width=100' width='" + list[i].advWidth + "' height='" + list[i].advHeight + "' /></a></li>";
                    }
                    //_str += "<span class='advItemName'>" + list[i].advName + "</span>";
                    _str += "/<div>";
                }
            }
            else
                showAdv = false;
            ltradv.Text = _str;
            list.Clear();
            list = null;
        }
    }
}