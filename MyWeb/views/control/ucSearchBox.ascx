﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucSearchBox.ascx.cs" Inherits="MyWeb.views.control.ucSearchBox" %>

<div class="box-search">
    <asp:TextBox ID="txtSearch" runat="server" class="input-text" placeholder="Sản phẩm muốn tìm kiếm?"></asp:TextBox>
    <asp:LinkButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" class="btn-search color-backgroud"><i class="fa fa-search"></i></asp:LinkButton>
</div>
