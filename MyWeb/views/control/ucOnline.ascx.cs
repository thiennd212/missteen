﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucOnline : System.Web.UI.UserControl
    {
        string lang = "vi";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                BindData();
            }
        }

        private void BindData()
        {
            string _str = "";
            string t = "2";
            string sk = "16";
            if (GlobalClass.styleYahoo != "") { t = GlobalClass.styleYahoo; }
            if (GlobalClass.styleSkype != "") { sk = GlobalClass.styleSkype; }
            List<tbGroupOnline> list = db.tbGroupOnlines.OrderByDescending(s => s.gOrd).ToList(); ;
            if (list.Count > 0)
            {
                _str += "<div class=\"header\">" + MyWeb.Global.GetLangKey("support_online") + "</div>";
                _str += "<div class=\"box-body\">";
                foreach (var item in list)
                {
                    IEnumerable<tbOnline> onlines = db.tbOnlines.Where(s => s.onLang.Trim() == lang && s.onType == item.ID && s.onActive == 1).OrderBy(s => s.onOrd);

                    if (onlines.Count() > 0)
                    {
                        _str += "<div class=\"list-cate-online\">";
                        _str += "<div class=\"header-cate\">" + item.gName + "</div>";
                        _str += "<div class=\"list-online\">";
                        foreach (tbOnline online in onlines)
                        {
                            _str += "<div class=\"header-support\">" + online.onName1 + "</div>";
                            _str += "<div class=\"list-item\">";
                            if (online.onYahoo != "")
                            {
                                _str += "<div class=\"nickZalo\"><a href=\"" + online.onYahoo + "\" target='_blank' style=\"text-decoration: none\"><img src='/theme/default/img/zalo.png' alt=\"" + online.onYahoo + "\" border=\"0\" align=\"middle\" /></a></div>";
                            }
                            if (online.onSkype != "")
                            {

                                _str += "<div class=\"skype-style\" id=\"SkypeButton_Call_" + online.onSkype + "_1\">";
                                _str += "<script type=\"text/javascript\">";
                                _str += "Skype.ui({";
                                _str += "\"name\": \"dropdown\",";
                                _str += "\"element\": \"SkypeButton_Call_" + online.onSkype + "_1\",";
                                _str += "\"participants\": [\"" + online.onSkype + "\"],";
                                _str += "\"imageSize\": " + sk + "";
                                _str += "});";
                                _str += "</script>";
                                _str += "</div>";

                            }
                            if (online.onEmail != "")
                            {
                                _str += "<div class=\"nickEmail\">Email :<a href=\"" + online.onEmail + "\">" + online.onEmail + "</a></div>";
                            }
                            if (online.onTell != "")
                            {
                                _str += "<div class=\"nickTell\">" + online.onTell + "</div>";
                            }
                            _str += "</div>";
                        }
                        _str += "</div></div>";                        
                    }

                }
                _str += "</div>";
            }
            else
            {
                _str = "";
            }
            ltrSupport.Text = _str;
        }
    }
}