﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucBoxface : System.Web.UI.UserControl
    {
        string lang = "vi";
        public string u = "https://www.facebook.com/pages/VMMS/168951059963937";
        public string w = "222";
        public string h = "250";
        public string c = "light";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = Global.GetLang();
            if (GlobalClass.widthfbLike != null) { w = GlobalClass.widthfbLike; }
            if (GlobalClass.heightfbLike != null) { h = GlobalClass.heightfbLike; }
            if (GlobalClass.colorfkComment != null) { c = GlobalClass.colorfkComment; }
            if (!IsPostBack)
            {
                tbConfig c = db.tbConfigs.FirstOrDefault(s => s.conLang == lang);
                string[] arrSocial = c.viewSocial.Split(Convert.ToChar(","));
                u = arrSocial[0];
            }
        }
    }
}