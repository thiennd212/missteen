﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucNewsLetter.ascx.cs" Inherits="MyWeb.views.control.ucNewsLetter" %>

<div class="news-letter">
    <div class="txt-header-letter color-text-head"><%= MyWeb.Global.GetLangKey("news_letter_header")%></div>
    <div class="txt-letter">
        <%= MyWeb.Global.GetLangKey("news_letter_info")%>
    </div>
    <div class="box-input boder-color">
        <asp:UpdatePanel ID="udpNewsLetter" runat="server">
            <ContentTemplate>
                <asp:TextBox ID="txtEmail" runat="server" class="input-text txtEmailNewsLetter" placeholder="Nhập địa chỉ email của bạn"></asp:TextBox>
                <asp:LinkButton ID="btnNewsLetter" runat="server" class="btn-letter color-backgroud" OnClick="btnNewsLetter_Click" OnClientClick="return checkReg()">Đăng ký</asp:LinkButton>
                <asp:Literal ID="btnErr" runat="server" Visible="false"></asp:Literal>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
<!--end letter-->
<script type="text/javascript">
    function checkReg() {
        var e = $(".txtEmailNewsLetter");
        var filterEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (e.val() == "") {
            alert("Vui lòng nhập email !");
            e.focus();
            return false;
        } else {
            if (!filterEmail.test(e.val())) {
                alert("Email không đúng định dạng !");
                e.focus();
                return false;
            } else {
                return true;
            }
        }
    }
</script>