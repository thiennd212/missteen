﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucOnlineVisit.ascx.cs" Inherits="MyWeb.views.control.ucOnlineVisit" %>

<div class="box-online-visit">
    <div class="header"><%= MyWeb.Global.GetLangKey("online_visit")%></div>
    <div class="body-online-visit">
        <div id='histats_counter'></div>
        <script type='text/javascript'>var _Hasync = _Hasync || []; _Hasync.push(['Histats.start', '<%= GlobalClass.conMail_Smtp%>']); _Hasync.push(['Histats.fasi', '1']); _Hasync.push(['Histats.track_hits', '']); (function () { var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true; hs.src = ('http://s10.histats.com/js15_as.js'); (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs); })();</script>
    </div>
</div>
