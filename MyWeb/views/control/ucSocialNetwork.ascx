﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucSocialNetwork.ascx.cs" Inherits="MyWeb.views.control.ucSocialNetwork" %>

<div class="box-social-network">
    <asp:Button ID="btnhReload" runat="server" Style="display: none" Text="Button" OnClick="btnhReload_Click" />
    <button id="load" style="display:none">Load data</button>
    <div class="body-network">
        <div id="lgFacebook"><a href="javascript:void(0)" onClick="Login()" class="facebook" rel="nofollow"><span>Facebook</span></a></div>
        <div class="header"><%= MyWeb.Global.GetLangKey("connect_with_us")%></div>
        <ul class="body-social">
            <!--GlobalClass.viewSocial1-->
            <%--<asp:Literal ID="ltrLoginFB" runat="server"></asp:Literal>--%>
            <%if (GlobalClass.viewSocial1 != "")                  
              {%><%--<li><a target="_blank" href='javscript:{}' onclick="Login()" class="facebook" rel="nofollow"><span>Facebook</span></a></li>--%>
            <%} %>
            <%if (GlobalClass.viewSocial2 != "")
              {%><li><a target="_blank" href='<%= GlobalClass.viewSocial2 %>' class="tweet" rel="nofollow"><span>Tweet</span></a></li>
            <%} %>
            <%if (GlobalClass.viewSocial3 != "")
              {%><li><a target="_blank" href='<%= GlobalClass.viewSocial3 %>' class="youtube" rel="nofollow"><span>Youtube</span></a></li>
            <%} %>
            <%if (GlobalClass.viewSocial4 != "")
              {%><li><a target="_blank" href='<%= GlobalClass.viewSocial4 %>' class="zing" rel="nofollow"><span>Zing Me</span></a></li>
            <%} %>
            <%if (GlobalClass.viewSocial5 != "")
              {%><li><a target="_blank" href='<%= GlobalClass.viewSocial5 %>' class="googleplus" rel="nofollow"><span>Google+</span></a></li>
            <%} %>
            <%if (GlobalClass.viewSocial6 != "")
              {%><li><a target="_blank" href='<%= GlobalClass.viewSocial6 %>' class="rss" rel="nofollow"><span>RSS</span></a></li>
            <%} %>
        </ul>
    </div>
</div>
<script type="text/javascript">
    function SetUserName(value) {
        //PageMethods.SetUserName(value);
        $.ajax({
            async: false,
            type: "POST",
            url: "webService.asmx/SetSessionLoginCookie",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ userName: value }),
            success: function (response) {
                console.log(response.d);
            },
            failure: function (response) {
            }

        });
    }

    function Login()
    {
        FB.getLoginStatus(function (response) {
            if (response.status === 'not_authorized' || response.status === 'unknown') {
                FB.login(function (response) {
                    if (response.authResponse) {
                        var uid = response.authResponse.userID
                        SetUserName(uid);
                        btnSetLogin();
                    }
                });
            } else if (response.status === 'connected') {
                var uid = response.authResponse.userID;
                var accessToken = response.authResponse.accessToken;
                SetUserName(uid);
                btnSetLogin();
            }
        }, true);
    }

    function LogoutFb()
    {
        $.ajax({
            async: false,
            type: "POST",
            url: "webService.asmx/SetSessionLogout",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d != "" && response.d != null) {
                    if (response.d.IsLogin) {
                        $("#lgFacebook").html("<span>Loading...</span>")
                        GetInfoUserFb(response.d.Message);
                    }
                    else
                        $("#lgFacebook").html(response.d.Message);
                }
            },
            failure: function (response) {
            }

        });
    }

    function LogoutInfoSS() {
        $.ajax({
            async: false,
            type: "POST",
            url: "webService.asmx/GetSessionLogin",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d != "" && response.d != null) {
                    if (response.d.IsLogin) {
                        $("#lgFacebook").html(response.d.Message)
                    }
                    else
                        $("#lgFacebook").html(response.d.Message);
                }
            },
            failure: function (response) {
            }

        });
    }

    function GetInfoUserFb(uid) {
        FB.api(
            "/" + uid + "",
            function (response) {
                if (response && !response.error) {
                    FB.api(
                        "/" + uid + "/picture",
                        function (responsex) {
                            if (responsex && !responsex.error) {
                                var para = uid + "," + response.name + "," + responsex.data.url;                                
                                var str = "<span class=\"avatar\"><img src=\"" + responsex.data.url + "\" alt=\"\"/></span> <span class=\"fullname\">" + response.name + "</span> (<a onclick=\"LogoutFb()\" href=\"javascript:{}\" title=\"Thoát\">Thoát</a>)";
                                $("#lgFacebook").html(str);
                            } else {
                                var para = uid;
                                var str = "<a href='javscript:{}' onclick=\"Login()\" class=\"facebook\" rel=\"nofollow\"><img src=\"uploads/layout/default/css/images/icon-fb-log.png\" alt=\"\"/></a>";
                                $("#lgFacebook").html(str);
                            }
                            SetUserName(para);
                        }
                    );
                } else {
                    LogoutInfoSS();
                }
            }
        );
    }
      
    function btnSetLogin() {
        $.ajax({
            async: false,
            type: "POST",
            url: "webService.asmx/SetSessionLogin",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d != "" && response.d != null) {
                    if (response.d.IsLogin) {
                        $("#lgFacebook").html("<span>Loading...</span>")
                        GetInfoUserFb(response.d.Message);
                    }
                    else
                        $("#lgFacebook").html(response.d.Message);
                }
            },
            failure: function (response) {
            }

        });
    };

    $(window).load(function () {
        btnSetLogin();
    });    

    //$(document).ready(function(){
    //    $("#logOutFB").on("click", function () {
    //        LogoutFb();
    //    });

    //    var a = $(this).find('a#logInFB')[0];
    //    var evObj = document.createEvent('MouseEvents');
    //    evObj.initMouseEvent('click', true, true, window);
    //    a.dispatchEvent(evObj);

    //    $("#logInFB").on("click", function () {            
    //        Login();
    //    });
    //});

    function btnReload() {
        __doPostBack('<%=btnhReload.UniqueID%>', "");
    };
</script>
