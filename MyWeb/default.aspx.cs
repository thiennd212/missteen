﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MyWeb
{
    public partial class _Default : System.Web.UI.Page
    {
        string strUrl = "";
        private string lang = "";
        string strTagID = "";
        int intEmptyHome = 0;
        public bool showPoupup = true, showSlide = true;
        dataAccessDataContext db = new dataAccessDataContext();
        public tbConfig objConfig;
        protected void Page_Load(object sender, EventArgs e)
        {
            checkCodeService.checkCodeWebsiteSoapClient svCheck = new checkCodeService.checkCodeWebsiteSoapClient();
            string webUrl = Request.Url.Host;            
            string webIP = GetIPAddress();                 
            if (Session["checkCode"] == null) {
                Session["checkCode"] = svCheck.checkLicenseWebsite(webUrl, webIP);
            }
            int check = Convert.ToInt32(Session["checkCode"].ToString());
            if (check == 2)
            {
                Response.Redirect("/error404.html");
            }
            else if (check == 3) {
                Response.Redirect("/error404.html");
            }
            else
            {
                #region Load data
                strUrl = HttpContext.Current.Request.RawUrl;
                lang = MyWeb.Global.GetLang();
                if (Request["hp"] != null) { strTagID = Request["hp"].ToString(); }

                if (intEmptyHome != -1)
                {
                    strTagID = strTagID.Substring(0, intEmptyHome);
                }

                objConfig = db.tbConfigs.Where(s => s.conLang == lang).FirstOrDefault();

                #region layout + css
                if (MyWeb.Common.StringClass.Check(Request["layout"]))
                {
                    DirectoryInfo strPath = new DirectoryInfo(MyWeb.Common.GlobalClass.PhysicalApplicationPath("uploads/layout/" + Request["layout"].ToString().Trim()));
                    if (strPath.Exists)
                    {
                        Session["layout"] = Request["layout"].ToString().Trim();
                    }
                }
                string _layout = MyWeb.Common.StringClass.Check(Session["layout"]) ? Session["layout"].ToString() : GlobalClass.usingLayout;
                _layout = MyWeb.Common.StringClass.Check(_layout) ? _layout : "default";
                Control _layoutControl = (Control)Page.LoadControl("/uploads/layout/" + _layout + "/default.ascx");
                MainPlaceHolder.Controls.Add(_layoutControl);
                #endregion

                Session["home_page"] = null;
                if (Request["e"] == null)
                {
                    Session["home_page"] = "true";
                }

                Session["home_pro_view"] = null;
                Session["home_pro_list"] = null;

                if (objConfig.livechat != null)
                {
                    ltrLiveChat.Text = objConfig.livechat;
                }

                if (!IsPostBack)
                {
                    try
                    {                        
                        Session["strCurrentUrl"] = Request.RawUrl;
                        BindData(_layout);
                        BindSEO();
                        BindAdvertise();
                    }
                    catch
                    {
                        Response.Redirect("/error404.html");
                    }

                }
                #endregion
            }
            MyWebForm.Action = Request.RawUrl;
        }

        [System.Web.Services.WebMethod]
        public static void SetUserName(string userName)
        {
            Page objPage = new Page();
            if (userName.Contains(","))
            {
                string[] arr = userName.Split(',');
                objPage.Session["UserNameFB"] = arr[0];
                objPage.Session["UserFBName"] = arr[1];
                objPage.Session["UserFBAvatar"] = arr[2];
            }else
                objPage.Session["UserNameFB"] = userName;
            
        }

        public string GetIPAddress()
        {
            //IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            //IPAddress ipAddress = ipHostInfo.AddressList[0];
            //return ipAddress.ToString();
            return Request.ServerVariables["LOCAL_ADDR"];
        }

        void BindData(string layout)
        {
            string _style = "", _strMeta="";
            string style = MyWeb.Common.StringClass.Check(Session["style"]) ? Session["style"].ToString() : GlobalClass.usingStyle;
            if (layout != "" && style != "")
            {
                string _favicon = "";
                if (GlobalClass.faviconIMG != "")
                {
                    _favicon = "<link rel=\"icon\" type=\"image/x-icon\" href=\"" + GlobalClass.faviconIMG + "\"/>";
                }
                _style += String.Format("<link href=\"" + common.GetHttpURL() + "uploads/layout/{0}/layout.css\" rel=\"stylesheet\" type=\"text/css\" />{1}", layout + "/" + style, _favicon);

                lrtStyleSheet.Text = _style;
            }

            if (GlobalClass.viewBanner5 == "0" && !(Request.Url.AbsoluteUri.ToLower().Contains("default.aspx") || Request.Url.AbsoluteUri.ToLower() == "/"))
                showPoupup = false;

            #region Mate Share

            if (Session["home_page"] != null)
            {
                _strMeta += "<meta property=\"og:site_name\" content=\"" + Request.Url.Host + "\" />";
                _strMeta += "<meta property=\"og:url\" content=\"http://" + Request.Url.Host + "\" />";
                _strMeta += "<meta property=\"article:author\" content=\"" + Request.Url.Host + "\" />";
                _strMeta += "<meta property=\"og:type\" content=\"article\" />";
                _strMeta += "<link rel=\"image_src\" href=\"http://" + Request.Url.Host + "" + objConfig.conHeader + "\" />";
                _strMeta += "<meta property=\"og:image\" content=\"http://" + Request.Url.Host + "" + objConfig.conHeader + "\" />";
                _strMeta += "<meta property=\"og:title\" content=\"" + objConfig.conTitle + "\" />";
                _strMeta += "<meta property=\"og:description\" content=\"" + objConfig.conDescription + "\" />";
                _strMeta += "<meta property=\"og:image:width\" content=\"1500\" />";
                _strMeta += "<meta property=\"og:image:height\" content=\"1500\" />";
            }

            if (Request["e"] != null)
            {
                if (Request["e"].ToString() == "load")
                {
                    string _request = Request["hp"] != null ? Request["hp"].ToString() : Request.Path;
                    string _pageLink = Request["hp"].ToString() + ".html";
                    if (!_request.ToLower().Contains("default.aspx"))
                    {
                        string _strCur = "";
                        var _curTypeLink = db.tbPages.Where(s => s.pagLink == _pageLink).ToList();
                        if (_curTypeLink.Count > 0)
                        {
                            _strCur = _curTypeLink[0].pagType.ToString();
                        }
                        else
                        {
                            var _curTypeRequest = db.tbPages.Where(s => s.pagTagName == Request["hp"].ToString()).ToList();
                            if (_curTypeRequest != null && _curTypeRequest.Count > 0)
                            {
                                var _curTypeTag = db.tbPages.Where(s => s.pagTagName == _curTypeRequest[0].pagTagName).FirstOrDefault();
                                _strCur = _curTypeTag != null ? _curTypeTag.pagType.ToString() : _curTypeRequest[0].pagType.ToString();
                            }
                        }

                        if (_strCur == "800")
                        {
                            tbNew objNews = db.tbNews.Where(s => s.newTagName == strTagID).FirstOrDefault();
                            if (objNews != null)
                            {
                                string _strLink = "/" + objNews.newTagName + ".html";
                                string _strDesp = MyWeb.Common.StringClass.GetContent(common.RemoveHTMLTags(objNews.newContent.ToString()), 185);
                                if (objNews.newDescription != "") { _strDesp = objNews.newDescription; }

                                if (objNews.newImage != "")
                                {
                                    string images = "";
                                    if (images.Contains("http://") || images.Contains("https://"))
                                    {
                                        images = objNews.newImage;
                                    }
                                    else
                                    {
                                        images = "http://" + Request.Url.Host + objNews.newImage;
                                    }
                                    _strMeta = "<meta property=\"og:site_name\" content=\"" + Request.Url.Host + "\" />";
                                    _strMeta += "<meta property=\"og:url\" content=\"http://" + Request.Url.Host + "" + _strLink + "\" />";
                                    _strMeta += "<meta property=\"article:author\" content=\"" + Request.Url.Host + "\" />";
                                    _strMeta += "<meta property=\"og:type\" content=\"article\" />";
                                    _strMeta += "<link rel=\"image_src\" href=\"" + images + "\" />";
                                    _strMeta += "<meta property=\"og:image\" content=\"" + images + "\" />";
                                    _strMeta += "<meta property=\"og:title\" content=\"" + objNews.newName + "\" />";
                                    _strMeta += "<meta property=\"og:description\" content=\"" + _strDesp + "\" />";
                                    _strMeta += "<meta property=\"og:image:width\" content=\"1500\" />";
                                    _strMeta += "<meta property=\"og:image:height\" content=\"1500\" />";
                                }
                            }
                        }
                        else if (_strCur == "900")
                        {
                            tbProduct objPro = db.tbProducts.Where(s => s.proTagName == strTagID).FirstOrDefault();
                            if (objPro != null)
                            {
                                string[] arrImage = new string[10];
                                if (objPro.proImage != null && objPro.proImage != "")
                                    arrImage = objPro.proImage.Split(Convert.ToChar(","));
                                arrImage = arrImage.Where(s => s != null && s.Length > 0).ToArray();
                                string _strDesp = MyWeb.Common.StringClass.GetContent(common.RemoveHTMLTags(objPro.proDetail.ToString()), 185);
                                if (objPro.proDescription != "") { _strDesp = objPro.proDescription; }
                                string catId = objPro.catId.Split(Convert.ToChar(","))[0];
                                string _strLink = "/" + objPro.proTagName + ".html";
                                if (arrImage.Count() > 0)
                                {
                                    string images = "";
                                    if (arrImage[0].Contains("http://") || arrImage[0].Contains("https://"))
                                    {
                                        images = arrImage[0];
                                    }
                                    else
                                    {
                                        images = "http://" + Request.Url.Host + arrImage[0];

                                    }
                                    _strMeta = "<link rel=\"image_src\" href=\"" + images + "\" /><meta property=\"og:image\" itemprop=\"thumbnailUrl\" content=\"" + images + "\" /><meta property=\"og:title\" content=\"" + objPro.proName + "\" /><meta property=\"og:description\" content=\"" + _strDesp + "\" /><meta property=\"og:url\" content=\"http://" + Request.Url.Host + "" + _strLink + "\" />";
                                }
                            }
                        }
                    }
                    
                }
            }
            else
            {
                _strMeta = "<meta property=\"og:site_name\" content=\"" + Request.Url.Host + "\" />";
                _strMeta += "<meta property=\"og:url\" content=\"http://" + Request.Url.Host + "\" />";
                _strMeta += "<meta property=\"article:author\" content=\"" + Request.Url.Host + "\" />";
                _strMeta += "<meta property=\"og:type\" content=\"article\" />";
                List<tbAdvertiseDATA> list = tbAdvertiseDB.tbAdvertise_GetByPosition_Vietime("20");
                list = list = list.Where(s => s.advLang.Trim().ToLower() == lang).ToList();
                if (list.Count > 0)
                {
                    string images = "";
                    if (images.Contains("http://") || images.Contains("https://"))
                    {
                        images = list[0].advImage;
                    }
                    else
                    {
                        images = "http://" + Request.Url.Host + list[0].advImage; ;
                    }
                    _strMeta += "<meta property=\"og:image\" content=\"" + images + "\" />";
                    _strMeta += "<meta property=\"og:image\" content=\"" + images + "\" />";

                }
                _strMeta += "<meta property=\"og:title\" content=\"" + GlobalClass.conTitle + "\" />";
                _strMeta += "<meta property=\"og:description\" content=\"" + GlobalClass.conDescription.ToString() + "\" />";
                _strMeta += "<meta property=\"og:image:width\" content=\"1500\" />";
                _strMeta += "<meta property=\"og:image:height\" content=\"1500\" />";
            }

            ltrMeta.Text = _strMeta;
            #endregion

            if (GlobalClass.viewBanner1 == "0" && !(Request.Url.AbsoluteUri.ToLower().Contains("default.aspx") || Request.Url.AbsoluteUri.ToLower() == "/"))
                showSlide = false;

            if (GlobalClass.Google_Analytics_Id != "")
            {
                string strAnalytics = "";
                strAnalytics += "<script type=\"text/javascript\">var _gaq=_gaq||[];_gaq.push(['_setAccount','" + GlobalClass.Google_Analytics_Id + "']);_gaq.push(['_trackPageview']);(function () {var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);})();</script>";
                ltrScript.Text = strAnalytics;
            }
            if (GlobalClass.conId == null)
                Global.LoadConfig(Global.GetLang());
            if (GlobalClass.commentFacebook.Contains("1") || GlobalClass.commentFNews.Contains("1") || GlobalClass.commentFPage.Contains("1"))
            {
                ltrFacebook.Text = "<div id=\"fb-root\"></div><script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = \"//connect.facebook.net/vi_VN/all.js#xfbml=1&version=v2.9\"; fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>";
            }
        }

        void BindSEO()
        {
            if (Request["e"] != null)
            {
                if (Request["e"].ToString() == "load")
                {
                    string request = Request["hp"] != null ? Request["hp"].ToString() : Request.Path;
                    if (!request.ToLower().Contains("default.aspx"))
                    {
                        var curType = db.tbPages.Where(s => s.pagTagName == Request["hp"].ToString()).ToList();
                        if (curType.Count > 0)
                        {
                            if (curType[0].pagType == 900)
                            {
                                tbProduct pro = db.tbProducts.FirstOrDefault(s => s.proTagName == curType[0].pagTagName);
                                LoadMetaConfig(pro.proName, pro.proTitle, pro.proDescription, pro.proKeyword);
                            }
                            else
                            {
                                LoadMetaConfig(curType[0].pagName, curType[0].pagTitle, curType[0].pagDescription, curType[0].pagKeyword);
                            }
                        }
                        else
                        {
                            var curTour = db.Tours.FirstOrDefault(x => x.Tagname == Request["hp"].ToString());
                            if (curTour != null)
                                LoadMetaConfig(curTour.Name, curTour.SEOTitle, curTour.SEODescription, curTour.SEOKeyword);
                            else
                            {
                                var contest = db.Contestants.FirstOrDefault(x => x.TagName == Request["hp"].ToString());
                                if (contest != null)
                                    LoadMetaConfig(contest.FullName, curTour.SEOTitle, curTour.SEODescription, curTour.SEOKeyword);
                            }
                        }

                    }
                }
                if (Request["e"].ToString() == "listpro")
                {
                    var curP = db.tbPages.FirstOrDefault(s => s.pagActive == 1 && s.pagLang == lang && s.pagLink == "san-pham.html");
                    if (curP != null) LoadMetaConfig(curP.pagName, curP.pagTitle, curP.pagDescription, curP.pagKeyword);
                    else LoadMetaConfig("Sản phẩm", "", "", "");
                }
                else if (Request["e"].ToString() == "listnews")
                {
                    var curP = db.tbPages.FirstOrDefault(s => s.pagActive == 1 && s.pagLang == lang && s.pagLink == "tin-tuc.html");
                    if (curP != null) LoadMetaConfig(curP.pagName, curP.pagTitle, curP.pagDescription, curP.pagKeyword);
                    else LoadMetaConfig("Tin tức", "", "", "");
                }
                else if (Request["e"].ToString() == "pronew")
                {
                    if (Request["key"].ToString() == "noibat")
                    {
                        var curP = db.tbPages.FirstOrDefault(s => s.pagActive == 1 && s.pagLang == lang && s.pagLink == "san-pham-noi-bat.html");
                        if (curP != null) LoadMetaConfig(curP.pagName, curP.pagTitle, curP.pagDescription, curP.pagKeyword);
                        else LoadMetaConfig("Sản phẩm nổi bật", "", "", "");
                    }
                    else if (Request["key"].ToString() == "banchay")
                    {
                        var curP = db.tbPages.FirstOrDefault(s => s.pagActive == 1 && s.pagLang == lang && s.pagLink == "san-pham-ban-chay.html");
                        if (curP != null) LoadMetaConfig(curP.pagName, curP.pagTitle, curP.pagDescription, curP.pagKeyword);
                        else LoadMetaConfig("Sản phẩm bán chạy", "", "", "");
                    }
                    else if (Request["key"].ToString() == "khuyenmai")
                    {
                        var curP = db.tbPages.FirstOrDefault(s => s.pagActive == 1 && s.pagLang == lang && s.pagLink == "san-pham-khuyen-mai.html");
                        if (curP != null) LoadMetaConfig(curP.pagName, curP.pagTitle, curP.pagDescription, curP.pagKeyword);
                        else LoadMetaConfig("Sản phẩm khuyến mại", "", "", "");
                    }
                    else
                    {
                        var curP = db.tbPages.FirstOrDefault(s => s.pagActive == 1 && s.pagLang == lang && s.pagLink == "san-pham-moi.html");
                        if (curP != null) LoadMetaConfig(curP.pagName, curP.pagTitle, curP.pagDescription, curP.pagKeyword);
                        else LoadMetaConfig("Sản phẩm mới", "", "", "");
                    }
                }
                else if (Request["e"].ToString() == "cart")
                {
                    var curP = db.tbPages.FirstOrDefault(s => s.pagActive == 1 && s.pagLang == lang && s.pagLink == "gio-hang.html");
                    if (curP != null) LoadMetaConfig(curP.pagName, curP.pagTitle, curP.pagDescription, curP.pagKeyword);
                    else LoadMetaConfig("Giỏ hàng", "", "", "");
                }
                else if (Request["e"].ToString() == "cartcus")
                {
                    var curP = db.tbPages.FirstOrDefault(s => s.pagActive == 1 && s.pagLang == lang && s.pagLink == "thanh-toan.html");
                    if (curP != null) LoadMetaConfig(curP.pagName, curP.pagTitle, curP.pagDescription, curP.pagKeyword);
                    else LoadMetaConfig("Thanh toán", "", "", "");
                }
                else if (Request["e"].ToString() == "dangnhap")
                {
                    LoadMetaConfig("Đăng nhập", "", "", "");
                }
                else if (Request["e"].ToString() == "dangkythanhvien")
                {
                    LoadMetaConfig("Đăng ký thành viên", "", "", "");
                }
                else if (Request["e"].ToString() == "quenmatkhau")
                {
                    LoadMetaConfig("Quên mật khẩu", "", "", "");
                }
                else if (Request["e"].ToString() == "doimatkhau")
                {
                    LoadMetaConfig("Thay đổi mật khẩu", "", "", "");
                }
                else if (Request["e"].ToString() == "thongtintv")
                {
                    LoadMetaConfig("Thay đổi thông tin cá nhân", "", "", "");
                }
                else if (Request["e"].ToString() == "search")
                {
                    LoadMetaConfig("Kết quả tìm kiếm", "", "", "");
                }
                else if (Request["e"].ToString() == "searchTour")
                {
                    LoadMetaConfig("Kết quả tìm kiếm tour", "", "", "");
                }
            }
            else
            {
                string link = Request.Url.ToString().ToLower();
                if (link.Contains("lien-he.html"))
                {
                    var curP = db.tbPages.FirstOrDefault(s => s.pagActive == 1 && s.pagLang == lang && s.pagLink == "lien-he.html");
                    if (curP != null) LoadMetaConfig(curP.pagName, curP.pagTitle, curP.pagDescription, curP.pagKeyword);
                    else
                    {
                        if (lang == "vi")
                            LoadMetaConfig("Liên hệ", "", "", "");
                        else
                            LoadMetaConfig("Contact", "", "", "");
                    }
                }
                else
                {
                    LoadMetaConfigDefault();
                }
            }

            if (Session["home_page"] != null)
            {
                ltrH1.Text = "<h1 style=\"position:absolute;z-index:-100000;visibility:hidden;width:1px\">" + GlobalClass.Field1 + "</h1>";
                ltrH.Text = "<h2 style=\"position:absolute;z-index:-100000;visibility:hidden;width:1px\">" + GlobalClass.conDescription + "</h2><h3 style=\"position:absolute;z-index:-100000;visibility:hidden;width:1px\">" + GlobalClass.conKeywords  + "</h3>";
            }
        }

        private void BindAdvertise()
        {
            //Load adv left
            string strReturn = "";
            List<tbAdvertiseDATA> list = tbAdvertiseDB.tbAdvertise_GetByPosition_Vietime("6");
            list = list.Where(s => s.advLang.ToLower().Trim() == lang).ToList();
            strReturn += "<div id=\"divAdvLeft\">";
            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    string path = "";
                    path = list[i].advImage;
                    if (path.IndexOf(".swf") > 0)
                    {
                        strReturn += "<script language='javascript'>playfile('" + path + "', '" + list[i].advWidth + "', '" + list[i].advHeight + "', true, '', '', 'link=" + list[i].advLink + "');</script>";
                    }
                    else
                    {
                        strReturn += "<a href=\"" + list[i].advLink + "\" target=\"" + list[i].advTarget + "\"><img src=\"" + path + "\" width=\"" + list[i].advWidth + "\" height=\"" + list[i].advHeight + "\" alt=\"" + list[i].advName + "\" /></a>";
                    }
                }
            }
            strReturn += "</div>";
            list.Clear();
            list = null;

            //Load adv right
            List<tbAdvertiseDATA> list2 = tbAdvertiseDB.tbAdvertise_GetByPosition_Vietime("7");
            list2 = list2.Where(s => s.advLang.ToLower().Trim() == lang).ToList();
            strReturn += "<div id=\"divAdvRight\">";
            if (list2.Count > 0)
            {
                for (int i = 0; i < list2.Count; i++)
                {
                    string path = "";
                    path = list2[i].advImage;
                    if (path.IndexOf(".swf") > 0)
                    {
                        strReturn += "<script language='javascript'>playfile('" + path + "', '" + list2[i].advWidth + "', '" + list2[i].advHeight + "', true, '', '', 'link=" + list2[i].advLink + "');</script>";
                    }
                    else
                    {
                        strReturn += "<a href=\"" + list2[i].advLink + "\" target=\"" + list2[i].advTarget + "\"><img src=\"" + path + "\" width=\"" + list2[i].advWidth + "\" height=\"" + list2[i].advHeight + "\" alt=\"" + list2[i].advName + "\" /></a>";
                    }
                }
            }
            strReturn += "</div>";
            list2.Clear();
            list2 = null;
            ltrAdv.Text = strReturn;

            //Load popup banner 
            if (GlobalClass.viewBanner5 == "0" && !Request.Url.AbsoluteUri.ToLower().Contains("default.aspx"))
                showPoupup = false;
            if (GlobalClass.viewBanner6 == "1" && common.GetCookie("popupBanner") != "")
                showPoupup = false;
            if(common.GetCookie("popupBanner") != "")
                showPoupup = false;

            List<tbAdvertiseDATA> list3 = tbAdvertiseDB.tbAdvertise_GetByPosition_Vietime("8");
           
            list3 = list3.Where(s => s.advLang.ToLower().Trim() == lang).ToList();
            if (list3.Count > 0 && showPoupup && Session["home_page"] != null)
            {
                common.SetCookie("popupBanner", "1", 1);
                Session["Is_Popup"] = "yes";
                string _strPop = "";
                _strPop += "<div id=\"myModalPopUp\" class=\"modal fade\" role=\"dialog\">";
                _strPop += "<div class=\"modal-dialog\">";
                _strPop += "<div class=\"modal-content\">";
                _strPop += "<div class=\"modal-header\">";
                _strPop += "<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>";
                _strPop += "</div>";
                _strPop += "<div class=\"modal-body\">";
                _strPop += "<a title=\"" + list3[0].advName + "\" href=\"" + list3[0].advLink + "\" target=\"" + list3[0].advTarget + "\"><img class=\"div_pop_home\" alt=\"" + list3[0].advName + "\" src=\"" + list3[0].advImage + "\"></a>";
                _strPop += "</div>";
                _strPop += "</div>";
                _strPop += "</div>";
                _strPop += "</div>";
                ltrPopup.Text = _strPop;
            }
            list3.Clear();
            list3 = null;

            //Load bottom layer
            bool showBottomLayer = true;
            if (GlobalClass.viewBanner7 == "0" && !Request.Url.AbsoluteUri.ToLower().Contains("default.aspx"))
                showBottomLayer = false;
            if (GlobalClass.viewBanner8 == "1" && common.GetCookie("BottomLayer") != "")
                showBottomLayer = false;
            List<tbAdvertiseDATA> list4 = tbAdvertiseDB.tbAdvertise_GetByPosition_Vietime("9");
            list4 = list4.Where(s => s.advLang.ToLower().Trim() == lang).ToList();
            if (list4.Count > 0 && showBottomLayer)
            {
                string strBottom = "";
                common.SetCookie("BottomLayer", "1", 1);
                strBottom = "<div id=\"BottomLayer\">";
                strBottom += "<div class=\"bb_open clearfix\" style=\"width:" + (int.Parse(list4[0].advWidth) - 7) + "px; display: none;\"><marquee behavior=\"alternate\" style=\"width:" + (int.Parse(list4[0].advWidth) - 46) + "px;\">" + list4[0].advName + "</marquee> <a href=\"javascript://\" onclick=\"toggle_bb();\" class=\"max\">Hiện</a></div>";
                strBottom += "<div class=\"bb_close clearfix\" style=\"width:" + (int.Parse(list4[0].advWidth) - 7) + "px;\">" + list4[0].advName + " <a href=\"javascript://\" onclick=\"toggle_bb();\" class=\"min\">Ẩn</a></div>";
                strBottom += "<div class=\"BottomLayer\" style=\"width:" + (int.Parse(list4[0].advWidth) - 7) + "px;height:" + (int.Parse(list4[0].advHeight)) + "px\"><a title=\"" + list4[0].advName + "\" href=\"" + list4[0].advLink + "\" target=\"" + list4[0].advTarget + "\"><img style=\"width:" + (int.Parse(list4[0].advWidth) - 7) + "px;height:" + (int.Parse(list4[0].advHeight)) + "px\" src=\"" + list4[0].advImage + "\" alt=\"" + list4[0].advName + "\" /></a></div>";
                strBottom += "</div>";
                ltrBottomLayer.Text = strBottom;
            }
            list4.Clear();
            list4 = null;
        }

        void LoadMetaConfigDefault()
        {
            Page.Title = GlobalClass.conTitle.ToString();
            #region
            //if (GlobalClass.conDescription != "")
            //{
            //    SetMetaTag("description", GlobalClass.conDescription.ToString());
            //}
            //if (GlobalClass.conKeywords != "")
            //{
            //    SetMetaTag("keywords", GlobalClass.conKeywords.ToString());
            //}
            #endregion

            string sDes = GlobalClass.conDescription != "" ? GlobalClass.conDescription : "";
            string sKey = GlobalClass.conKeywords != "" ? GlobalClass.conKeywords : "";
            SetMetaTag2("description", sDes, "keywords", sKey);
        }

        void LoadMetaConfig(string strName, string strTitle, string strDescription, string strKeyword)
        {
            if (strTitle != "")
                ltrH1.Text = "<h1 style=\"position:absolute;z-index:-100000;visibility:hidden;width:1px\">" + strTitle + "</h1>";
            else
                ltrH1.Text = "<h1 style=\"position:absolute;z-index:-100000;visibility:hidden;width:1px\">" + strName + "</h1>";

            ltrH.Text = "<h2 style=\"position:absolute;z-index:-100000;visibility:hidden;width:1px\">" + strDescription + "</h2><h3 style=\"position:absolute;z-index:-100000;visibility:hidden;width:1px\">" + strKeyword + "</h3>";
            if (GlobalClass.showSEO.Equals("True"))
            {
                if (strDescription != "")
                {
                    strDescription = strDescription + " | " + GlobalClass.conDescription.ToString();
                }
                else
                {
                    strDescription = strTitle != "" ? strTitle : strName;
                    strDescription = strDescription + " | " + GlobalClass.conDescription.ToString();
                }
                if (strKeyword != "")
                {
                    strKeyword = strKeyword + " | " + GlobalClass.conKeywords.ToString();
                }
                else
                {
                    strKeyword = strTitle != "" ? strTitle : strName;
                    strKeyword = strKeyword + " | " + GlobalClass.conKeywords.ToString();
                }
                strTitle = strTitle != "" ? strTitle : strName;
            }
            Page.Title = strTitle != "" ? strTitle : strName;
            #region
            //if (strDescription != "")
            //{
            //    SetMetaTag("description", strDescription);
            //}
            //else
            //{
            //    strDescription = strTitle != "" ? strTitle : strName;
            //    SetMetaTag("description", strDescription);
            //}
            //if (strKeyword != "")
            //{
            //    SetMetaTag("keywords", strKeyword);
            //}
            //else
            //{
            //    strKeyword = strTitle != "" ? strTitle : strName;
            //    SetMetaTag("keywords", strKeyword);
            //}
            #endregion            
            SetMetaTag2("description", strDescription, "keywords", strKeyword);
        }

        private void SetMetaTag(string strName, string strValue)
        {
            HtmlMeta _meta = new HtmlMeta();
            _meta.Name = strName;
            _meta.Content = strValue;
            Page.Header.Controls.Add(_meta);
        }

        private void SetMetaTag2(string sName, string sValue, string sName2, string sValue2) {
            string strKq = "";
            strKq += "<meta name=\"" + sName + "\" content=\"" + sValue + "\" />";
            strKq += "<meta name=\"" + sName2 + "\" content=\"" + sValue2 + "\" />";
            ltrMainMeta.Text = strKq;
        }
    }
}