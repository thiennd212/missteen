﻿using MyWeb.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel
{
    public partial class _default : System.Web.UI.Page
    {
        static dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        [WebMethod]
        public static int saveData(string name, string id)
        {
            int status = 200;
            List<tbProTag> proTags = new List<tbProTag>();
            try
            {
                id = id.Replace("form-control input-sm", "");
                if (id.Contains("_"))
                {
                    var intID = id.Substring(0, id.LastIndexOf('_'));
                    intID = intID.Replace("_txt", "");
                    var objPro = db.tbProducts.FirstOrDefault(s => s.proId + "" == intID + "");
                    if (objPro != null)
                    {
                        if (name.Contains("price-j"))
                        {
                            objPro.proPrice = name.Replace("price-j", "");
                        }
                        else if (name.Contains("ord-j"))
                        {
                            objPro.proOrd = Convert.ToInt32(name.Replace("ord-j", ""));
                            status = 400;
                        }
                        else
                        {

                            string namepage = name;
                            string tagpage = objPro.proTagName;
                            objPro.proName = name;

                            var detN = db.tbProducts.FirstOrDefault(s => s.proName == name);
                            if (detN != null)
                            {
                                objPro.proTagName = StringClass.NameToTag(name) + "-" + intID;
                            }
                            else { objPro.proTagName = StringClass.NameToTag(name); }

                            var objPage = db.tbPages.FirstOrDefault(s => s.pagTagName == tagpage);
                            if (objPage != null)
                            {
                                if (objPage.pagName == namepage)
                                {
                                    objPage.pagTagName = StringClass.NameToTag(name) + "-" + intID;
                                }
                                else
                                {
                                    objPage.pagTagName = StringClass.NameToTag(name);
                                }
                                objPage.pagName = namepage;
                                db.SubmitChanges();
                            }
                            status = 500;
                        }
                        db.SubmitChanges();
                    }
                }
                {
                    if (!string.IsNullOrEmpty(name) && name.Contains("field"))
                    {
                        var str = "{\"movies\":" + name.TrimEnd(',') + "]}";
                        var strList = "";
                        if (!string.IsNullOrEmpty(str) && str.Contains("TabThongtin"))
                        {
                            if (IsNumeric(id))
                            {
                                strList += "delete from tbProTag where proId=" + id + "  ";

                                var index = Regex.Matches(str, "TabThongtin-").Count;
                                if (index > 0)
                                {
                                    #region
                                    for (int i = 0; i < index; i++)
                                    {
                                        var checkUndifine = str.Substring(str.IndexOf("\"field-", StringComparison.Ordinal),
                                            str.IndexOf("\",\"fvitri-", StringComparison.Ordinal) -
                                            str.IndexOf("\"field-", StringComparison.Ordinal));
                                        if (!checkUndifine.Contains("undefined"))
                                        {
                                            var tag = "";
                                            var val = "";
                                            var vt = "";
                                            tag = str.Substring(str.IndexOf("-\":\"", StringComparison.Ordinal) + 4, str.IndexOf("\",\"fvitri", StringComparison.Ordinal) - (str.IndexOf("-\":\"", StringComparison.Ordinal) + 4));
                                            str = str.Remove(0, str.IndexOf("\",\"fvitri", StringComparison.Ordinal));
                                            vt = str.Substring(str.IndexOf("-\":\"", StringComparison.Ordinal) + 4,
                                                str.IndexOf("\",\"TabThongtin", StringComparison.Ordinal) - (str.IndexOf("-\":\"", StringComparison.Ordinal) + 4));
                                            str = str.Remove(0, str.IndexOf("\",\"TabThongtin", StringComparison.Ordinal));
                                            if (i < index - 1)
                                            {
                                                val = str.Substring(str.IndexOf("-\":\"", StringComparison.Ordinal) + 4,
                                                    str.IndexOf("\"}, {\"field-", StringComparison.Ordinal) -
                                                    (str.IndexOf("-\":\"", StringComparison.Ordinal) + 4));
                                                str = str.Remove(0, str.IndexOf("\"}, {\"field-", StringComparison.Ordinal));
                                            }
                                            else
                                            {
                                                str = str.Remove(0, str.IndexOf("-\":\"", StringComparison.Ordinal) + 4);
                                                val = str.Substring(0, str.LastIndexOf("\"}]}", StringComparison.Ordinal));
                                            }
                                            var ent = new tbProTag();
                                            if (IsNumeric(vt))
                                            {
                                                if (IsNumeric(id))
                                                {
                                                    ent.proId = Convert.ToInt32(id);
                                                }
                                                ent.TagName = tag;
                                                ent.TagValue = val;
                                                ent.Active = 1;
                                                ent.Ord = Convert.ToInt32(vt);
                                                proTags.Add(ent);


                                            }
                                            else
                                            {
                                                if (IsNumeric(id))
                                                {
                                                    ent.proId = Convert.ToInt32(id);
                                                }
                                                ent.TagName = tag;
                                                ent.TagValue = val;
                                                ent.Active = 1;
                                                ent.Ord = Convert.ToInt32(vt);
                                                proTags.Add(ent);

                                            }
                                        }
                                    }

                                    #endregion
                                }
                                else
                                {
                                    int idproductss = Convert.ToInt32(id);
                                    var data = db.tbProTags.Where(u => u.proId == idproductss).ToList();
                                    if (data.Count > 0)
                                    {
                                        for (int i = 0; i < data.Count; i++)
                                        {
                                            db.tbProTags.DeleteOnSubmit(data[i]);
                                        }
                                        db.SubmitChanges();
                                    }
                                }
                            }
                            else if (id=="")
                            {
                                #region Them tab khi them moi san pham

                                var _objLast = db.tbProducts.OrderByDescending(s => s.proId).FirstOrDefault();

                                var index = Regex.Matches(str, "TabThongtin-").Count;
                                if (index > 0)
                                {
                                    for (int i = 0; i < index; i++)
                                    {
                                        var checkUndifine = str.Substring(str.IndexOf("\"field-", StringComparison.Ordinal),
                                            str.IndexOf("\",\"fvitri-", StringComparison.Ordinal) -
                                            str.IndexOf("\"field-", StringComparison.Ordinal));
                                        if (!checkUndifine.Contains("undefined"))
                                        {
                                            var tag = "";
                                            var val = "";
                                            var vt = "";
                                            tag = str.Substring(str.IndexOf("-\":\"", StringComparison.Ordinal) + 4, str.IndexOf("\",\"fvitri", StringComparison.Ordinal) - (str.IndexOf("-\":\"", StringComparison.Ordinal) + 4));
                                            str = str.Remove(0, str.IndexOf("\",\"fvitri", StringComparison.Ordinal));
                                            vt = str.Substring(str.IndexOf("-\":\"", StringComparison.Ordinal) + 4,
                                                str.IndexOf("\",\"TabThongtin", StringComparison.Ordinal) - (str.IndexOf("-\":\"", StringComparison.Ordinal) + 4));
                                            str = str.Remove(0, str.IndexOf("\",\"TabThongtin", StringComparison.Ordinal));
                                            if (i < index - 1)
                                            {
                                                val = str.Substring(str.IndexOf("-\":\"", StringComparison.Ordinal) + 4,
                                                    str.IndexOf("\"}, {\"field-", StringComparison.Ordinal) -
                                                    (str.IndexOf("-\":\"", StringComparison.Ordinal) + 4));
                                                str = str.Remove(0, str.IndexOf("\"}, {\"field-", StringComparison.Ordinal));
                                            }
                                            else
                                            {
                                                str = str.Remove(0, str.IndexOf("-\":\"", StringComparison.Ordinal) + 4);
                                                val = str.Substring(0, str.LastIndexOf("\"}]}", StringComparison.Ordinal));
                                            }

                                            var ent = new tbProTag();
                                            if (IsNumeric(vt))
                                            {
                                                ent.proId = _objLast.proId;
                                                ent.TagName = tag;
                                                ent.TagValue = val;
                                                ent.Active = 1;
                                                ent.Ord = Convert.ToInt32(vt);
                                                proTags.Add(ent);
                                            }
                                            else
                                            {
                                                ent.proId = _objLast.proId;
                                                ent.TagName = tag;
                                                ent.TagValue = val;
                                                ent.Active = 1;
                                                ent.Ord = Convert.ToInt32(vt);
                                                proTags.Add(ent);

                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                        }

                        if (tbProductDB.tbProductTag(strList))
                        {
                            if (proTags.Any())
                            {
                                db.tbProTags.InsertAllOnSubmit(proTags);
                                db.SubmitChanges();
                            }
                            status = 300;
                            return status;
                        }
                        else
                        {
                            return -1;
                        }
                    }
                }
                return status;
            }
            catch
            {
                return -1;
            }
        }

        [WebMethod]
        public static int clearData(string name, string id)
        {
            var data = db.tbProTags.Where(u => u.proId == null).ToList();
            if (data.Count > 0)
            {
                for (int i = 0; i < data.Count; i++)
                {
                    db.tbProTags.DeleteOnSubmit(data[i]);
                }
                db.SubmitChanges();
            }
            return 1;
        }

        public static bool IsNumeric(object Expression)
        {
            double retNum;

            bool isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }
    }
}