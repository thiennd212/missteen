﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.tour
{
    public partial class listTour : System.Web.UI.UserControl
    {
        string _Admin = "";
        private string lang = "vi";
        int pageSize = 15;
        string userid = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); }
            if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); }
            if (Session["uid"] != null) userid = Session["uid"].ToString();

            if (Request["pages"] != null && !Request["pages"].Equals(""))
            {
                pageSize = int.Parse(Request["pages"].ToString());
            }
            if (!IsPostBack)
            {
                BindForder();
                BindForder2();
                BindData();
            }
        }

        void BindData()
        {
            var _listForder = db.Tours.Where(s => s.Lang == lang).OrderByDescending(s=>s.Id).ToList();

            if (txtSearch.Text != "")
            {
                _listForder = _listForder.Where(s => s.Name.ToLower().Contains(txtSearch.Text.ToLower())).ToList();
            }

            if (drlForder.SelectedValue != "0")
            {
                _listForder = _listForder.Where(s => s.IdTourCategory == int.Parse(drlForder.SelectedValue)).ToList();
            }

            if (drlForder1.SelectedValue != "0")
            {
                _listForder = _listForder.Where(s => s.IdTourTopic == int.Parse(drlForder.SelectedValue)).ToList();
            }

            if (_listForder.Count() > 0)
            {
                recordCount = _listForder.Count();
                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion

                var result = _listForder.Skip(currentPage * pageSize).Take(pageSize);
                rptFolderList.DataSource = result;
                rptFolderList.DataBind();
                BindPaging();
            }
            else
            {
                rptFolderList.DataSource = null;
                rptFolderList.DataBind();
            }
        }
    

        void BindForder()
        {
            string strForder = "";
            List<tbPageDATA> list = tbPageDB.tbPage_GetByAll(lang);
            list = list.Where(s => s.pagType == pageType.TourCategory).ToList();
            drlForder.Items.Clear();
            drlForder.Items.Add(new ListItem("- Nhóm tour -", "0"));

            ddlCategory.Items.Clear();
            ddlCategory.Items.Add(new ListItem("- Nhóm cha -", "0"));
            for (int i = 0; i < list.Count; i++)
            {
                strForder = "";
                for (int j = 1; j < list[i].paglevel.Length / 5; j++)
                {
                    strForder = strForder + "---";
                }
                drlForder.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].pagId));
                ddlCategory.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].pagId));
            }
            list.Clear();
            list = null;
        }

        void BindForder2()
        {
            string strForder = "";
            List<tbPageDATA> list = tbPageDB.tbPage_GetByAll(lang);
            list = list.Where(s => s.pagType == pageType.TourTopic).ToList();
            drlForder1.Items.Clear();
            drlForder1.Items.Add(new ListItem("- Chhủ đề -", "0"));

            ddlTopic.Items.Clear();
            ddlTopic.Items.Add(new ListItem("- Nhóm cha -", ""));
            for (int i = 0; i < list.Count; i++)
            {
                strForder = "";
                for (int j = 1; j < list[i].paglevel.Length / 5; j++)
                {
                    strForder = strForder + "---";
                }
                drlForder1.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].pagId));
                ddlTopic.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].pagId));
            }
            list.Clear();
            list = null;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            RepeaterItem item = default(RepeaterItem);
            for (int i = 0; i < rptFolderList.Items.Count; i++)
            {
                item = rptFolderList.Items[i];
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    if (((CheckBox)item.FindControl("chkBox")).Checked)
                    {
                        try
                        {
                            HiddenField hidID = (HiddenField)item.FindControl("hidCatID");
                            var itemDel = db.tbPages.Where(u => u.pagId == Convert.ToInt32(hidID.Value)).ToList();
                            if (itemDel.Count() > 0)
                            {
                                string levelss = itemDel[0].paglevel;
                                var procap1 = db.Tours.Where(u => u.IdTourCategory.Value == int.Parse(hidID.Value)).ToList();
                                if (procap1.Count > 0)
                                {
                                    db.Tours.DeleteAllOnSubmit(procap1);
                                }

                                var pagecap1 = db.tbPages.Where(s => s.paglevel.Substring(0, levelss.Length) == levelss).ToList();
                                if (pagecap1.Count > 0)
                                {
                                    db.tbPages.DeleteAllOnSubmit(pagecap1);
                                }
                                db.tbPages.DeleteAllOnSubmit(itemDel);
                                db.SubmitChanges();
                            }
                        }
                        catch { }
                    }
                }
            }
            pnlErr.Visible = true;
            ltrErr.Text = "Xóa thành công nhóm tour !";
            BindData();
            chkSelectAll.Checked = false;
        }

        protected void drlForder_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }
        protected void drlForder1_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {                
                case "Del":
                    var itemDel = db.Tours.FirstOrDefault(s => s.Id == int.Parse(strID));

                    if (itemDel != null)
                    {
                        db.Tours.DeleteOnSubmit(itemDel);
                        db.SubmitChanges();
                    }

                    pnlErr.Visible = true;
                    ltrErr.Text = "Xóa thành công nhóm tour !";
                    BindData();

                    break;
                case "Edit":
                    var itemEdit = db.Tours.FirstOrDefault(s => s.Id == int.Parse(strID));

                    if (itemEdit != null)
                    {
                        BindForder();
                        BindForder2();
                        LoadPlace();

                        if (!string.IsNullOrEmpty(itemEdit.IdTourCategory.ToString()))
                            ddlCategory.SelectedValue = itemEdit.IdTourCategory.ToString();
                        if (!string.IsNullOrEmpty(itemEdit.IdTourTopic.ToString()))
                            ddlTopic.SelectedValue = itemEdit.IdTourTopic.ToString();
                        if (!string.IsNullOrEmpty(itemEdit.IdPlace.ToString()))
                        {
                            var itemPlace = db.tbPages.Where(s => s.pagActive == 1 && s.pagLang == lang && s.pagType == int.Parse(pageType.TourPlace) && s.pagId == itemEdit.IdPlace).FirstOrDefault();
                            if (itemPlace != null)
                                ddlPlaceStart.SelectedValue = itemEdit.IdPlace.ToString();
                        }
                        txtName.Text = itemEdit.Name;
                        txtNumberOfDay.Text = itemEdit.NumberOfDay;
                        txtStart.Text = itemEdit.Start;
                        txtTransportation.Text = itemEdit.Transportation;
                        txtDescription.Text = itemEdit.Description;
                        if (itemEdit.IsHome != null) ckbHome.Checked = itemEdit.IsHome.Value;
                        ckbActive.Checked = itemEdit.Active == 1;

                        var listTab = db.TourTags.Where(s => s.IdTour == itemEdit.Id).OrderBy(s => s.Id).ToList();
                        var listPlace =
                            db.TourPlaces.Where(s => s.IdTour == itemEdit.Id).Select(s => s.IdPlace).ToArray();

                        foreach (ListItem item in cblPlaceEnd.Items)
                        {
                            item.Selected = listPlace.Contains(int.Parse(item.Value));
                        }

                        if (listTab.Any())
                        {
                            txtNameTab1.Text = listTab[0].Name;
                            txtOrderTab1.Text = listTab[0].Order.ToString();
                            txtContentTab1.Text = listTab[0].Text;

                            if (listTab.Count > 1)
                            {
                                pnTab2.Visible = true;
                                txtNameTab2.Text = listTab[1].Name;
                                txtOrderTab2.Text = listTab[1].Order.ToString();
                                txtContentTab2.Text = listTab[1].Text;
                            }

                            if (listTab.Count > 2)
                            {
                                pnTab3.Visible = true;
                                txtNameTab3.Text = listTab[2].Name;
                                txtOrderTab3.Text = listTab[2].Order.ToString();
                                txtContentTab3.Text = listTab[2].Text;
                            }

                            if (listTab.Count > 3)
                            {
                                pnTab4.Visible = true;
                                txtNameTab4.Text = listTab[3].Name;
                                txtOrderTab4.Text = listTab[3].Order.ToString();
                                txtContentTab4.Text = listTab[3].Text;
                            }

                            if (listTab.Count > 4)
                            {
                                pnTab5.Visible = true;
                                txtNameTab5.Text = listTab[4].Name;
                                txtOrderTab5.Text = listTab[4].Order.ToString();
                                txtContentTab5.Text = listTab[4].Text;
                            }

                            txtImage1.Text = itemEdit.Image1;
                            txtImage2.Text = itemEdit.Image2;
                            txtImage3.Text = itemEdit.Image3;
                            txtImage4.Text = itemEdit.Image4;
                            txtImage5.Text = itemEdit.Image5;
                            txtImage6.Text = itemEdit.Image6;
                            txtImage7.Text = itemEdit.Image7;
                            txtImage8.Text = itemEdit.Image8;
                            txtImage9.Text = itemEdit.Image9;
                            txtImage10.Text = itemEdit.Image10;
                            txtPrice.Text = itemEdit.Price.ToString();
                            txtPriceOrigin.Text = itemEdit.PriceOrigin.ToString();
                            txtPriceUnit.Text = itemEdit.PriceUnit;

                            if (itemEdit.IsNew != null) ckbNew.Checked = itemEdit.IsNew.Value;
                            if (itemEdit.IsHot != null) ckbHot.Checked = itemEdit.IsHot.Value;
                            if (itemEdit.IsSale != null) ckbSale.Checked = itemEdit.IsSale.Value;
                            if (itemEdit.IsTop != null) ckbTop.Checked = itemEdit.IsTop.Value;

                            txtSEOTitle.Text = itemEdit.SEOTitle;
                            txtSEODescription.Text = itemEdit.SEODescription;
                            txtSEOKeyword.Text = itemEdit.SEOKeyword;
                        }
                        else
                        {
                            ddlTabs.SelectedValue = "1";
                        }
                    }                    

                    hidID.Value = strID;                                       
                    pnlErr.Visible = false;
                    ltrErr.Text = "";
                    ltrErr2.Text = "";
                    pnlErr2.Visible = false;
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;

                    break;
                case "Add":
                    BindForder();
                    BindForder2();
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    Resetcontrol();                   
                    ltrErr2.Text = "";
                    pnlErr2.Visible = false;
                    pnlErr.Visible = false;
                    ltrErr.Text = "";
                    break;
            }
        }

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

       
        public static string ShowActive(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = false;
            pnlAddForder.Visible = true;
            Resetcontrol();
            LoadPlace();
        }

        protected void LoadPlace()
        {
            var listPlace = db.tbPages.Where(s => s.pagActive == 1 && s.pagLang == lang && s.pagType == int.Parse(pageType.TourPlace)).OrderByDescending(s => s.pagOrd).ToList();

            ddlPlaceStart.Items.Clear();
            ddlPlaceStart.Items.Add(new ListItem("- Chọn điểm đi", "0"));
            cblPlaceEnd.Items.Clear();

            if (listPlace.Any())
            {                
                foreach(var item in listPlace)
                {
                    ddlPlaceStart.Items.Add(new ListItem(item.pagName, item.pagId.ToString()));
                    cblPlaceEnd.Items.Add(new ListItem(item.pagName, item.pagId.ToString()));
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Validation() == true)
            {
                Tour itemTour = new Tour();
                tbPage itemPage = new tbPage();
                var curId = hidID.Value;

                if(curId.Length > 0)
                {
                    itemTour = db.Tours.FirstOrDefault(s => s.Id == int.Parse(curId));
                    if (itemTour != null)
                    {
                        itemPage = db.tbPages.FirstOrDefault(s => s.pagTagName == itemTour.Tagname);
                    }
                }

                itemTour.IdTourCategory = int.Parse(ddlCategory.SelectedValue);
                itemTour.IdTourTopic = int.Parse(ddlTopic.SelectedValue);
                itemTour.IdPlace = int.Parse(ddlPlaceStart.SelectedValue);
                itemTour.Name = txtName.Text.Trim();
                itemTour.NumberOfDay = txtNumberOfDay.Text;
                itemTour.Start = txtStart.Text;
                itemTour.Transportation = txtTransportation.Text;
                itemTour.Description = txtDescription.Text;
                itemTour.Image1 = txtImage1.Text;
                itemTour.Image2 = txtImage2.Text;
                itemTour.Image3 = txtImage3.Text;
                itemTour.Image4 = txtImage4.Text;
                itemTour.Image5 = txtImage5.Text;
                itemTour.Image6 = txtImage6.Text;
                itemTour.Image7 = txtImage7.Text;
                itemTour.Image8 = txtImage8.Text;
                itemTour.Image9 = txtImage9.Text;
                itemTour.Image10 = txtImage10.Text;
                itemTour.PriceOrigin = Convert.ToDecimal(txtPriceOrigin.Text);
                itemTour.Price = Convert.ToDecimal(txtPrice.Text);
                itemTour.PriceUnit = txtPriceUnit.Text;
                itemTour.IsNew = ckbNew.Checked;
                itemTour.IsHot = ckbHot.Checked;
                itemTour.IsSale = ckbSale.Checked;
                itemTour.IsTop = ckbTop.Checked;
                itemTour.IsHome = ckbHome.Checked;
                itemTour.SEOTitle = txtSEOTitle.Text;
                itemTour.SEODescription = txtSEODescription.Text;
                itemTour.SEOKeyword = txtSEOKeyword.Text;
                itemTour.Lang = lang;
                itemTour.Active = ckbActive.Checked ? 1 : 0;
                
                string tagName = MyWeb.Common.StringClass.NameToTag(txtName.Text);                

                tbPage hasTagName = null;
                if (itemPage != null)
                {
                    itemPage.pagType = int.Parse(pageType.Tour);
                    itemPage.paglevel = "9988800000";
                    itemPage.pagTitle = txtName.Text;
                    itemPage.pagTagName = tagName;
                    hasTagName = db.tbPages.FirstOrDefault(s => s.pagTagName == tagName && s.pagId != itemPage.pagId);
                }

                if (curId.Length == 0)
                {
                    if (itemPage != null)
                        db.tbPages.InsertOnSubmit(itemPage);
                    db.SubmitChanges();
                    
                    if (hasTagName != null)
                    {
                        var lastPage = db.tbPages.OrderByDescending(s => s.pagId).FirstOrDefault();

                        if (lastPage != null)
                        {
                            lastPage.pagTagName = tagName + "-" + lastPage.pagId;
                            itemTour.Tagname = lastPage.pagTagName;
                        }
                    }
                    else
                    {
                        var lastPage = db.tbPages.OrderByDescending(s => s.pagId).FirstOrDefault();

                        if (lastPage != null)
                        {
                            lastPage.pagTagName = tagName + "-" + lastPage.pagId;                            
                        }

                        itemTour.Tagname = tagName;          
                    }
                    
                    db.Tours.InsertOnSubmit(itemTour);
                    db.SubmitChanges();

                    var lastTour = db.Tours.OrderByDescending(s => s.Id).FirstOrDefault();
                    if (lastTour != null)
                    {
                        addPlace(lastTour.Id);
                        addTab(lastTour.Id);
                    }
                }
                else
                {
                    if (hasTagName != null)
                    {
                        if (itemPage != null)
                            itemPage.pagTagName = tagName + "-" + itemPage.pagId;
                        itemTour.Tagname = itemPage.pagTagName;
                    }
                    else
                    {
                        if (itemPage != null)
                            itemPage.pagTagName = tagName;
                        itemTour.Tagname = tagName;
                    }

                    addPlace(itemTour.Id);
                    addTab(itemTour.Id);

                    db.SubmitChanges();
                }

                

                pnlErr.Visible = true;
                ltrErr.Text = curId.Length == 0 ? "Thêm mới tour thành công" : "Cập nhật tour thành công !";
                Resetcontrol();
                BindData();
                hidID.Value = "";
                pnlListForder.Visible = true;
                pnlAddForder.Visible = false;
            }
        }

        protected void addPlace(int tourId)
        {
            var listPlace = db.TourPlaces.Where(s => s.IdTour == tourId);

            db.TourPlaces.DeleteAllOnSubmit(listPlace);

            foreach(ListItem item in cblPlaceEnd.Items)
            {
                if(item.Selected)
                {
                    TourPlace itemPL = new TourPlace();
                    itemPL.IdTour = tourId;
                    itemPL.IdPlace = int.Parse(item.Value);
                    db.TourPlaces.InsertOnSubmit(itemPL);
                }
            }

            db.SubmitChanges();
        }

        protected void addTab(int tourId)
        {
            var listTab = db.TourTags.Where(s => s.IdTour == tourId);

            db.TourTags.DeleteAllOnSubmit(listTab);

            int count = int.Parse(ddlTabs.SelectedValue);

            if (count > 0)
            {
                TourTag itemT = new TourTag();
                if(txtNameTab1.Text != "" && txtOrderTab1.Text != "")
                {

                }
                itemT.IdTour = tourId;
                itemT.Name = txtNameTab1.Text;
                itemT.Order = int.Parse(txtOrderTab1.Text);
                itemT.Text = txtContentTab1.Text;
                db.TourTags.InsertOnSubmit(itemT);

                if(count > 1)
                {
                    TourTag itemT2 = new TourTag();
                    if (txtNameTab2.Text != "" && txtOrderTab2.Text != "")
                    {
                        itemT2.IdTour = tourId;
                        itemT2.Name = txtNameTab2.Text;
                        itemT2.Order = int.Parse(txtOrderTab2.Text);
                        itemT2.Text = txtContentTab2.Text;
                        db.TourTags.InsertOnSubmit(itemT2);
                    }                    

                    if (count > 2)
                    {
                        TourTag itemT3 = new TourTag();
                        if (txtNameTab3.Text != "" && txtOrderTab3.Text != "")
                        {
                            itemT3.IdTour = tourId;
                            itemT3.Name = txtNameTab3.Text;
                            itemT3.Order = int.Parse(txtOrderTab3.Text);
                            itemT3.Text = txtContentTab3.Text;
                            db.TourTags.InsertOnSubmit(itemT3);
                        }                        

                        if (count > 3)
                        {
                            TourTag itemT4 = new TourTag();
                            if (txtNameTab4.Text != "" && txtOrderTab4.Text != "")
                            {
                                itemT4.IdTour = tourId;
                                itemT4.Name = txtNameTab4.Text;
                                itemT4.Order = int.Parse(txtOrderTab4.Text);
                                itemT4.Text = txtContentTab4.Text;
                                db.TourTags.InsertOnSubmit(itemT4);
                            }                            

                            if (count > 4)
                            {
                                TourTag itemT5 = new TourTag();
                                if (txtNameTab5.Text != "" && txtOrderTab5.Text != "")
                                {
                                    itemT5.IdTour = tourId;
                                    itemT5.Name = txtNameTab5.Text;
                                    itemT5.Order = int.Parse(txtOrderTab5.Text);
                                    itemT5.Text = txtContentTab5.Text;
                                    db.TourTags.InsertOnSubmit(itemT5);
                                }                                
                            }
                        }
                    }
                }
            }

            db.SubmitChanges();
        }

        protected bool Validation()
        {            
            if (ddlTopic.SelectedValue == "0")
            {
                ltrErr2.Text = "Yêu cầu chọn chủ đề tour !";
                ddlCategory.Focus();
                pnlErr2.Visible = true;
                return false;
            }
            else if (ddlCategory.SelectedValue == "0" )
            {                
                ltrErr2.Text = "Yêu cầu chọn nhóm tour !";
                ddlCategory.Focus();
                pnlErr2.Visible = true;
                return false;                
            }
            else if (txtName.Text == "")
            {
                ltrErr2.Text = "Chưa nhập tên tour !";
                txtName.Focus();
                pnlErr2.Visible = true;
                return false;
            }

            return true;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            Session["insert"] = "false";           
            hidID.Value = "";
        }

        void Resetcontrol()
        {
            txtName.Text = "";
            txtNumberOfDay.Text = "";
            txtStart.Text = "";
            txtTransportation.Text = "";
            txtDescription.Text = "";
            ckbHome.Checked = false;
            ckbActive.Checked = false;
            ddlTabs.SelectedValue = "1";
            pnTab2.Visible = false;
            pnTab3.Visible = false;
            pnTab4.Visible = false;
            pnTab5.Visible = false;
            txtOrderTab1.Text = "";
            txtNameTab1.Text = "";
            txtContentTab1.Text = "";
            txtOrderTab2.Text = "";
            txtNameTab2.Text = "";
            txtContentTab2.Text = "";
            txtOrderTab3.Text = "";
            txtNameTab3.Text = "";
            txtContentTab3.Text = "";
            txtOrderTab4.Text = "";
            txtNameTab4.Text = "";
            txtContentTab4.Text = "";
            txtOrderTab5.Text = "";
            txtNameTab5.Text = "";
            txtContentTab5.Text = "";
            txtImage1.Text = "";
            txtImage2.Text = "";
            txtImage3.Text = "";
            txtImage4.Text = "";
            txtImage5.Text = "";
            txtImage6.Text = "";
            txtImage7.Text = "";
            txtImage8.Text = "";
            txtImage9.Text = "";
            txtImage10.Text = "";
            txtPriceOrigin.Text = "";
            txtPrice.Text = "";
            txtPriceUnit.Text = "";
            ckbNew.Checked = false;
            ckbHot.Checked = false;
            ckbSale.Checked = false;
            ckbTop.Checked = false;
            txtSEODescription.Text = "";
            txtSEOKeyword.Text = "";
            txtSEOTitle.Text = "";
            hidID.Value = "";
            ddlTabs.SelectedValue = "1";
        }

        public static string ShowActiveClass(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "btn btn-primary btn-xs" : "btn btn-danger btn-xs";
        }

        protected void ddlTabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTabs.SelectedValue == "1")
            {
                pnTab2.Visible = false;
                pnTab3.Visible = false;
                pnTab4.Visible = false;
                pnTab5.Visible = false;
            }
            else if (ddlTabs.SelectedValue == "2")
            {
                pnTab2.Visible = true;
                pnTab3.Visible = false;
                pnTab4.Visible = false;
                pnTab5.Visible = false;
            }
            else if(ddlTabs.SelectedValue == "3")
            {
                pnTab2.Visible = true;
                pnTab3.Visible = true;
                pnTab4.Visible = false;
                pnTab5.Visible = false;
            }
            else if (ddlTabs.SelectedValue == "4")
            {
                pnTab2.Visible = true;
                pnTab3.Visible = true;
                pnTab4.Visible = true;
                pnTab5.Visible = false;
            }
            else if (ddlTabs.SelectedValue == "5")
            {
                pnTab2.Visible = true;
                pnTab3.Visible = true;
                pnTab4.Visible = true;
                pnTab5.Visible = true;
            }
        }

        protected string GetCategoryName(string id)
        {
            var item = db.tbPages.FirstOrDefault(s => s.pagId == int.Parse(id));

            return item != null ? item.pagName : "";
        }

        protected string GetListPlace(string id)
        {
            var item = db.Tours.FirstOrDefault(s => s.Id == int.Parse(id));
            string strResult = "";

            if (item != null)
            {
                var listPlace = db.TourPlaces.Where(s => s.IdTour == item.Id).ToList();

                if (listPlace.Any())
                {
                    foreach (var itemPL in listPlace)
                    {
                        var place = db.tbPages.FirstOrDefault(s => s.pagId == itemPL.IdPlace);

                        if (place != null)
                        {
                            strResult += strResult != "" ? "</br>" + place.pagName : place.pagName;
                        }                        
                    }
                }
            }

            return strResult;
        }
    }
}