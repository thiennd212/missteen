﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="listTour.ascx.cs" Inherits="MyWeb.control.panel.tour.listTour" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<script src="../../scripts/ckfinder/ckfinder.js" type="text/javascript"></script>
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="/control.panel/">Trang chủ</a></li>
        <li class="active">Quản lý tour</li>
    </ol>
    <!-- end breadcrumb -->
    <asp:Panel ID="pnlListForder" runat="server" Visible="true">
        <h1 class="page-header">Quản lý tour</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse" data-sortable-id="table-basic-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Danh sách tour</h4>
                    </div>

                    <div class="alert alert-info fade in" id="pnlErr" runat="server" visible="false">
                        <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                        <button class="close" data-dismiss="alert" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <asp:LinkButton ID="btnAddNew" runat="server" class="btn btn-success btn-sm" OnClick="btnAddNew_Click"><i class="fa fa-plus"></i><span>Thêm mới</span></asp:LinkButton>
                                <asp:LinkButton ID="btnDeleteAll" runat="server" class="btn btn-danger btn-sm" OnClick="btnDeleteAll_Click" OnClientClick="javascript:return confirm('Bạn có muốn xóa thư mục đã chọn?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtSearch" runat="server" class="form-control input-sm" placeholder="Từ khóa tìm kiếm"></asp:TextBox>
                            </div>

                            <div class="col-sm-2">
                                <asp:DropDownList ID="drlForder" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drlForder_SelectedIndexChanged"></asp:DropDownList>                                
                            </div>
                            <div class="col-sm-2">
                                <asp:DropDownList ID="drlForder1" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drlForder1_SelectedIndexChanged"></asp:DropDownList>
                            </div>

                            <div class="col-sm-1">
                                <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-primary btn-sm" Style="float: right;" OnClick="btnSearch_Click"><i class="fa fa-search"></i><span>Tìm kiếm</span></asp:LinkButton>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                                        <thead>
                                            <tr>
                                                <th width="10">
                                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="False"></asp:CheckBox>
                                                </th>
                                                <th>Tên tour</th>
                                                <th>Nhóm</th>
                                                <th>Chủ đề</th>
                                                <th>Điểm đi</th>
                                                <th>Điểm đến</th>        
                                                <th width="160">Công cụ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptFolderList" runat="server" OnItemCommand="rptFolderList_ItemCommand">
                                                <ItemTemplate>
                                                    <tr class="even gradeC">
                                                        <td>
                                                            <asp:CheckBox ID="chkBox" CssClass="chkBoxSelect" runat="server"></asp:CheckBox>
                                                            <asp:HiddenField ID="hidCatID" Value='<%#DataBinder.Eval(Container.DataItem, "Id")%>' runat="server" />
                                                            <asp:Label ID="lblID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Id")%>'></asp:Label>
                                                        </td>
                                                        <td>                                                            
                                                            <%#Eval("Name").ToString() %>
                                                        </td>
                                                        <td>
                                                            <%#GetCategoryName(Eval("IdTourCategory").ToString()) %>    
                                                        </td>
                                                        <td>
                                                            <%#GetCategoryName(Eval("IdTourTopic").ToString()) %>    
                                                        </td>
                                                        <td>
                                                            <%#GetCategoryName(Eval("IdPlace").ToString()) %>    
                                                        </td>
                                                        <td>
                                                            <%#GetListPlace(Eval("Id").ToString()) %>    
                                                        </td>                                                     
                                                        <td>                                                            
                                                            <asp:LinkButton class="btn btn-success btn-xs" ID="btnEdit" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Id")%>' CommandName="Edit" ToolTip="Sửa"><i class="fa fa-pencil-square-o"></i>Sửa</asp:LinkButton>
                                                            <asp:LinkButton class="btn btn-danger btn-xs" ID="btnDel" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Id")%>' CommandName="Del" ToolTip="Xóa" OnClientClick="javascript:return confirm('Bạn có muốn xóa?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row dataTables_wrapper">
                            <div class="col-sm-5">
                                <div id="data-table_info" class="dataTables_info" role="status" aria-live="polite">
                                    <asp:Literal ID="ltrStatistic" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div id="data-table_paginate" class="dataTables_paginate paging_simple_numbers">
                                    <ul class="pagination">
                                        <li id="data-table_previous" class="paginate_button previous disabled">
                                            <asp:LinkButton ID="btnPrevious" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Trước</asp:LinkButton>
                                        </li>
                                        <asp:Repeater ID="rptNumberPage" runat="server" OnItemCommand="rptNumberPage_ItemCommand" OnItemDataBound="rptNumberPage_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrLiPage" runat="server"></asp:Literal>
                                                <asp:LinkButton ID="btn" runat="server" CommandArgument='<%# Eval("PageIndex") %>' CommandName="page" Text='<%# Eval("PageText") %> '></asp:LinkButton></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <li id="data-table_next" class="paginate_button next">
                                            <asp:LinkButton ID="btnNext" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Sau</asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAddForder" runat="server" Visible="false">
        <h1 class="page-header">Thêm/sửa tour</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse" data-sortable-id="table-basic-2">
                    <div class="panel panel-inverse panel-with-tabs" data-sortable-id="ui-unlimited-tabs-1">
                        <div class="panel-heading p-0">
                            <div class="panel-heading-btn m-r-10 m-t-10">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-expand"><i class="fa fa-expand"></i></a>                                
                            </div>
                            <div class="tab-overflow">
                                <ul class="nav nav-tabs nav-tabs-inverse">
                                    <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-inverse"><i class="fa fa-arrow-left"></i></a></li>
                                    <li class="active"><a href="#info-tab" data-toggle="tab">Thông tin tour</a></li>
                                    <li class=""><a href="#contain-tab" data-toggle="tab">Chi tiết tour</a></li>
                                    <li class=""><a href="#img-tab" data-toggle="tab">Ảnh đại diện</a></li>
                                    <li class=""><a href="#price-tab" data-toggle="tab">Giá, thuộc tính tour</a></li>
                                    <li class=""><a href="#seo-tab" data-toggle="tab">SEO</a></li>
                                    <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-inverse"><i class="fa fa-arrow-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-content panel-body panel-form">
                            <asp:Panel ID="pnlErr2" runat="server" Visible="false">
                                <div class="alert alert-danger fade in" style="border-radius: 0px;">
                                    <button class="close" data-dismiss="alert" type="button">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <asp:Literal ID="ltrErr2" runat="server"></asp:Literal>
                                </div>
                            </asp:Panel>
                            <div class="tab-pane fade active in" id="info-tab">
                                <div class="form-horizontal form-bordered">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Chủ đề:</label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlTopic" runat="server" class="form-control"></asp:DropDownList>                                   
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Nhóm tour:</label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCategory" runat="server" class="form-control"></asp:DropDownList>
                                            <asp:HiddenField ID="hidID" runat="server" />
                                        </div>
                                    </div>      
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Địa điểm:</label>
                                        <div class="col-md-7">
                                            <div class="rows">
                                                <div class="col-sm-6">
                                                    <label class="control-label">Điểm đi:</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="control-label">Điểm đến:</label>
                                                </div>                                                                                                
                                            </div>
                                            <div class="rows">
                                                <div class="col-sm-6">                                                 
                                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlPlaceStart">
                                                        <asp:ListItem Value="0">- Chọn địa điểm -</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-6" style=" max-height: 150px; overflow: auto;">                                                
                                                    <asp:CheckBoxList runat="server" ID="cblPlaceEnd"></asp:CheckBoxList>
                                                </div> 
                                            </div>                                             
                                        </div>                                                                              
                                    </div>                      
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Tên tour:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtName" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>                                                
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Số ngày:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtNumberOfDay" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Thời gian khởi hành:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtStart" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Phương tiện:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtTransportation" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Nội dung tóm tắt:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtDescription" runat="server" class="form-control" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                        </div>
                                    </div>          
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Hiển thị trang chủ:</label>
                                        <div class="col-md-7">
                                            <asp:CheckBox ID="ckbHome" runat="server" Checked="true" />
                                        </div>
                                    </div>                                                                                                            
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Hiển thị:</label>
                                        <div class="col-md-7">
                                            <asp:CheckBox ID="ckbActive" runat="server" Checked="true" />
                                        </div>
                                    </div>                                
                                </div>                                   
                            </div>
                            <div class="tab-pane fade" id="contain-tab">
                                <div class="form-horizontal form-bordered">
                                    <asp:UpdatePanel runat="server" ID="upTab">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Số lượng tab:</label>
                                                <div class="col-md-10">
                                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlTabs" OnSelectedIndexChanged="ddlTabs_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="1">1</asp:ListItem>
                                                        <asp:ListItem Value="2">2</asp:ListItem>
                                                        <asp:ListItem Value="3">3</asp:ListItem>
                                                        <asp:ListItem Value="4">4</asp:ListItem>
                                                        <asp:ListItem Value="5">5</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>  
                                            <div class="form-group">
                                                <div class="col-md-2"></div>
                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-2">Tên Tab hiển thị:</label>
                                                        <div class="col-md-7">
                                                            <asp:TextBox CssClass="col-md-12 form-control" runat="server" ID="txtNameTab1"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <asp:TextBox runat="server" CssClass="form-control" ID="txtOrderTab1" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <CKEditor:CKEditorControl ID="txtContentTab1" runat="server"></CKEditor:CKEditorControl>
                                                    </div>
                                                </div>                                        
                                            </div>
                                            <asp:Panel runat="server" ID="pnTab2">
                                                <div class="col-md-2"></div>
                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-2">Tên Tab hiển thị:</label>
                                                            <div class="col-md-7">
                                                                <asp:TextBox runat="server" CssClass="col-md-12 form-control" ID="txtNameTab2"></asp:TextBox>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <asp:TextBox runat="server" CssClass="form-control" ID="txtOrderTab2" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <CKEditor:CKEditorControl ID="txtContentTab2" runat="server"></CKEditor:CKEditorControl>
                                                        </div>
                                                    </div>
                                                </div>                                        
                                            </asp:Panel>
                                            <asp:Panel runat="server" ID="pnTab3">
                                                <div class="col-md-2"></div>
                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-2">Tên Tab hiển thị:</label>
                                                            <div class="col-md-7">
                                                                <asp:TextBox runat="server" CssClass="col-md-12 form-control" ID="txtNameTab3"></asp:TextBox>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <asp:TextBox runat="server" CssClass="form-control" ID="txtOrderTab3" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <CKEditor:CKEditorControl ID="txtContentTab3" runat="server"></CKEditor:CKEditorControl>
                                                        </div>
                                                    </div>
                                                </div>                                        
                                            </asp:Panel>
                                            <asp:Panel runat="server" ID="pnTab4">
                                                <div class="col-md-2"></div>
                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-2">Tên Tab hiển thị:</label>
                                                            <div class="col-md-7">
                                                                <asp:TextBox runat="server" CssClass="col-md-12 form-control" ID="txtNameTab4"></asp:TextBox>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <asp:TextBox runat="server" CssClass="form-control" ID="txtOrderTab4" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <CKEditor:CKEditorControl ID="txtContentTab4" runat="server"></CKEditor:CKEditorControl>
                                                        </div>
                                                    </div>
                                                </div>                                        
                                            </asp:Panel>
                                            <asp:Panel runat="server" ID="pnTab5">
                                                <div class="col-md-2"></div>
                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-2">Tên Tab hiển thị:</label>
                                                            <div class="col-md-7">
                                                                <asp:TextBox runat="server" CssClass="col-md-12 form-control" ID="txtNameTab5"></asp:TextBox>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <asp:TextBox runat="server" CssClass="form-control" ID="txtOrderTab5" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <CKEditor:CKEditorControl ID="txtContentTab5" runat="server"></CKEditor:CKEditorControl>
                                                        </div>
                                                    </div>
                                                </div>                                        
                                            </asp:Panel>            
                                        </ContentTemplate>
                                    </asp:UpdatePanel>                                                                                                                                                                        
                                </div>
                            </div>
                            <div class="tab-pane fade" id="img-tab">
                                <div class="form-horizontal form-bordered">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 1:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage1" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage1.ClientID %>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 2:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage2" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage2.ClientID %>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 3:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage3" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage3.ClientID %>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 4:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage4" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage4.ClientID %>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 5:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage5" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage5.ClientID %>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 6:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage6" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage6.ClientID %>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 7:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage7" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage7.ClientID %>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 8:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage8" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage8.ClientID %>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 9:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage9" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage9.ClientID %>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 10:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage10" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage10.ClientID %>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="price-tab">
                                <div class="form-horizontal form-bordered">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Giá niêm yết:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtPriceOrigin" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Giá bán:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtPrice" runat="server" class="form-control" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Loại tiền:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtPriceUnit" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Tùy chọn:</label>
                                        <div class="col-md-7">
                                            <asp:CheckBox runat="server" ID="ckbNew" Text="Mới" />                                         
                                            <asp:CheckBox ID="ckbHot" runat="server" Text="Nổi bật" />                                            
                                            <asp:CheckBox ID="ckbSale" runat="server" Text="Khuyến mại" />                                            
                                            <asp:CheckBox ID="ckbTop" runat="server" Text="Xem nhiều" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="seo-tab">
                                <div class="form-horizontal form-bordered">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Meta title:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtSEOTitle" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Meta description:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtSEODescription" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Meta keyword:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtSEOKeyword" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="form-horizontal form-bordered">                                           
                                <div class="form-group">
                                    <label class="control-label col-md-2"></label>
                                    <div class="col-md-7">
                                        <asp:LinkButton ID="btnUpdate" runat="server" class="btn btn-primary" OnClick="btnUpdate_Click">Cập nhật</asp:LinkButton>
                                        <asp:LinkButton ID="btnReset" runat="server" class="btn btn-danger" OnClick="btnReset_Click">Hủy</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                                    
                </div>
            </div>
        </div>
    </asp:Panel>
</div>