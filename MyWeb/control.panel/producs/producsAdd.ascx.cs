﻿using MyWeb.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.producs
{
    public partial class producsAdd : System.Web.UI.UserControl
    {
        string _Admin = "";
        private string lang = "vi";
        string userid = "";
        string _strID = "";
        public bool attr = true;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            _strID = PageHelper.GetQueryString("idpro");
            if (!IsPostBack)
            {
                if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); }
                if (Session["uid"] != null) userid = Session["uid"].ToString();
                ddlImage.Attributes.Add("onchange", "LoadImageList();");
                BindImages();
                LoadFormSize(10);
                BindForder();
                BindListMan();
                BindAttributes();
                BindTags();
                hidID.Value = "";
                if (_strID != "")
                {
                    BindDataProduct(_strID);
                }
                else
                {
                    BindData();
                }
            }
        }

        void BindData()
        {
            var strTabs = "";
            strTabs += "<div id='TabsAppend'>";
            strTabs += "<div class=\"item_tabs\">";
            strTabs += "<div class=\"form-group\">";
            strTabs += "<label class=\"control-label col-md-2\">Tên Tab hiển thị:</label>";
            strTabs += "<div class=\"col-md-7\">";
            strTabs += "<input id=\"field0\" type=\"text\" class=\"form-control target\" value=\"Thông tin chi tiết\"/>";
            strTabs += "</div>";
            strTabs += "<div class=\"col-md-3\">";
            strTabs += "<input id=\"fvitri0\" type=\"text\" class=\"form-control target\" onblur=\"valid(this,'quotes')\" onkeyup=\"valid(this,'quotes')\" placeholder=\"Thứ tự\" value=\"1\"/>";
            strTabs += "</div>";
            strTabs += "</div>";

            strTabs += "<div class=\"form-group\">";
            strTabs += "<label class=\"control-label col-md-2\">Nội dung:</label>";
            strTabs += "<div class=\"col-md-10\">";
            strTabs += "<textarea id=\"TabThongtin0\" name=\"TabThongtin\" class=\"form-control target\"></textarea>";
            strTabs += "</div>";
            strTabs += "</div>";
            strTabs += "</div>";
            strTabs += "</div>";
            ltrTabBind.Text = strTabs;


            trTextKM.Attributes.Add("style", "display:none");
            trThoigianKM.Attributes.Add("style", "display:none");
            txtNewsDate.Text = string.Format("{0:MM/dd/yyyy}", DateTime.Now);
        }

        void BindDataProduct(string intID)
        {
            hidID.Value = intID;
            tbProduct dbPro = db.tbProducts.FirstOrDefault(s => s.proId == Int32.Parse(intID));
            List<tbProTag> lstProTag = db.tbProTags.Where(s => s.proId == Int32.Parse(intID)).ToList();
            txtTen.Text = dbPro.proName;
            BindAttChecked(intID);
            string[] arrImage = new string[10];
            if (dbPro.proImage != null && dbPro.proImage.Length > 0)
                arrImage = dbPro.proImage.Split(Convert.ToChar(","));
            ddlImage.SelectedValue = arrImage.Count().ToString();
            for (int i = 0; i < arrImage.Count(); i++)
            {
                TextBox txtMImage = (TextBox)rptImages.Items[i].FindControl("txtMImage");
                txtMImage.Text = arrImage[i];
            }
            txtVideo.Text = dbPro.proVideo;
            txtCount.Text = dbPro.proCount.ToString();

            txtGiagoc.Text = dbPro.proOriginalPrice;
            txtGia.Text = dbPro.proPrice;
            txtLoaitien.Text = dbPro.proUnitprice;
            txtTieude.Text = dbPro.proTitle;
            txtDesscription.Text = dbPro.proDescription;
            txtKeyword.Text = dbPro.proKeyword;
            txtThutu.Text = dbPro.proOrd.ToString();
            txtNewsDate.Text = string.Format("{0:MM/dd/yyyy}", dbPro.proDate);
            fckTomtat.Text = dbPro.proContent;
            txtMa.Text = dbPro.proCode;
            txtproWarranty.Text = dbPro.proWarranty;
            txtPromotions.Text = dbPro.proPromotions;
            if (dbPro.proActive == 1) { chkKichhoat.Checked = true; } else { chkKichhoat.Checked = false; }
            if (dbPro.proIndex == 1) { chkSanphammoi.Checked = true; } else { chkSanphammoi.Checked = false; }
            if (dbPro.proNoibat == 1) { chkNoibat.Checked = true; } else { chkNoibat.Checked = false; }
            if (dbPro.proBanchay == 1) { chkSpbanchay.Checked = true; } else { chkSpbanchay.Checked = false; }
            if (dbPro.proKM == 1)
            {
                chkKM.Checked = true;
            }
            else
            {
                chkKM.Checked = false;
                trTextKM.Attributes.Add("style", "display:none");
                trThoigianKM.Attributes.Add("style", "display:none");
            }

            if (!string.IsNullOrEmpty(dbPro.proTungay + ""))
            {
                txtTungay.Text = string.Format("{0:MM/dd/yyyy}", dbPro.proTungay);
            }
            if (!string.IsNullOrEmpty(dbPro.proDenngay + ""))
            {
                txtDenngay.Text = string.Format("{0:MM/dd/yyyy}", dbPro.proDenngay);
            }
            if (!string.IsNullOrEmpty(dbPro.catId))
            {
                ddlForder.SelectedValue = dbPro.catId;
            }
            if (dbPro.manufacturerId != null)
            {
                try
                {
                    ddlMan.SelectedValue = dbPro.manufacturerId.ToString();
                }
                catch
                {
                    ddlMan.SelectedValue = "";
                }
            }

            dbPro.proNoidungKM = txtPromotions.Text;
            var status = "0";
            if (!string.IsNullOrEmpty(dbPro.proStatus + ""))
            {
                status = dbPro.proStatus + "";
            }

            foreach (ListItem item in rbtStatus.Items)
            {
                {
                    if (item.Value == status)
                    {
                        item.Selected = true;
                    }
                }
            }

            var strTabs = "";
            if (lstProTag.Any())
            {
                int i = 0;
                strTabs += "<div id='TabsAppend'>";
                foreach (var item in lstProTag)
                {
                    var ntnDel = "";
                    if (i > 0)
                    {
                        ntnDel = "<input class=\"btn btn-danger btn-sm ibtnDel\" type=\"button\" value=\"Xóa tab\" onclick=\"javascript:return confirm('Bạn có muốn xóa?');\" />";
                    }
                    strTabs += "<span class='span_tab'>";
                    strTabs += "<div class=\"item_tabs\">";
                    strTabs += "<div class=\"form-group\">";
                    strTabs += "<label class=\"control-label col-md-2\">Tên Tab hiển thị:</label>";
                    strTabs += "<div class=\"col-md-7\">";
                    strTabs += "<input id=\"field" + i + "\" type=\"text\" value=\"" + item.TagName + "\" type=\"text\" class=\"form-control target\" />";
                    strTabs += "</div>";
                    strTabs += "<div class=\"col-md-2\">";
                    strTabs += "<input id=\"fvitri" + i + "\" type=\"text\" value=\"" + item.Ord + "\" type=\"text\" class=\"form-control target\" onblur=\"valid(this,'quotes')\" onkeyup=\"valid(this,'quotes')\" placeholder=\"Thứ tự\" />";
                    strTabs += "</div>";

                    strTabs += "<div class=\"col-md-1\">";
                    strTabs += ntnDel;
                    strTabs += "</div>";

                    strTabs += "</div>";

                    strTabs += "<div class=\"form-group\">";
                    strTabs += "<label class=\"control-label col-md-2\">Nội dung:</label>";
                    strTabs += "<div class=\"col-md-10\">";
                    strTabs += "<textarea id=\"TabThongtin" + i + "\" name=\"TabThongtin" + i + "\" class=\"form-control target\">" + item.TagValue + "</textarea>";
                    strTabs += "</div>";
                    strTabs += "</div>";
                    strTabs += "</div>";
                    strTabs += "</span>";

                    i++;
                }
                strTabs += "</div>";
            }
            else
            {
                
                strTabs += "<div id='TabsAppend'>";
                strTabs += "<div class=\"item_tabs\">";
                strTabs += "<div class=\"form-group\">";
                strTabs += "<label class=\"control-label col-md-2\">Tên Tab hiển thị:</label>";
                strTabs += "<div class=\"col-md-7\">";
                strTabs += "<input id=\"field0\" type=\"text\" class=\"form-control target\" />";
                strTabs += "</div>";
                strTabs += "<div class=\"col-md-3\">";
                strTabs += "<input id=\"fvitri0\" type=\"text\" class=\"form-control target\" onblur=\"valid(this,'quotes')\" onkeyup=\"valid(this,'quotes')\" placeholder=\"Thứ tự\" />";
                strTabs += "</div>";
                strTabs += "</div>";

                strTabs += "<div class=\"form-group\">";
                strTabs += "<label class=\"control-label col-md-2\">Nội dung:</label>";
                strTabs += "<div class=\"col-md-10\">";
                strTabs += "<textarea id=\"TabThongtin0\" name=\"TabThongtin\" class=\"form-control target\"></textarea>";
                strTabs += "</div>";
                strTabs += "</div>";
                strTabs += "</div>";
                strTabs += "</div>";
            }
            ltrTabBind.Text = strTabs;

            lstNewRelate.Items.Clear();
            if (dbPro.proTag != null)
            {
                int i = 0;
                string strMaTin = null;
                strMaTin = dbPro.proTag;
                string[] MaTin = null;
                MaTin = strMaTin.Split(new Char[] { ';' });
                string k = null;
                lstB.Items.Clear();
                foreach (string k_loopVariable in MaTin)
                {
                    k = k_loopVariable;
                    if (!string.IsNullOrEmpty(k.Trim()))
                    {
                        lstB.Items.Add(new ListItem(k, k));
                    }
                }

                for (i = 0; i < lstB.Items.Count; i++)
                {

                    IEnumerable<tbTag> list2 = db.tbTags.Where(s => s.Lang == lang && s.Active == 1 && s.Type == 1);
                    foreach (tbTag n in list2)
                    {
                        if (lstB.Items[i].Text == n.Name.ToString())
                        {
                            lstNewRelate.Items.Add(new ListItem(n.Name.ToString(), n.Id.ToString()));
                        }
                    }
                }
            }

        }

        private void BindImages()
        {
            ddlImage.Items.Clear();
            for (int i = 1; i <= 10; i++)
            {
                ddlImage.Items.Add(new ListItem(i.ToString() + " ảnh", i.ToString()));
            }
            ddlImage.DataBind();
        }

        protected void LoadFormSize(int intSize)
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Columns.Add("Id");
            for (int i = 0; i < intSize; i++)
            {
                System.Data.DataRow dr = dt.NewRow();
                dr["Id"] = i + 1;
                dt.Rows.Add(dr);
            }
            rptImages.DataSource = dt.DefaultView;
            rptImages.DataBind();
        }

        protected DateTime convertDate(string date)
        {
            DateTime datetime = DateTime.Now;
            if (date != "")
            {
                var arrDate = date.Split(' ');
                string strNewTime = arrDate[1];
                if (arrDate[2] == "PM")
                {
                    var hour = int.Parse(arrDate[1].Split(':')[0].ToString());
                    hour = hour + 12;
                    strNewTime = hour + ":" + arrDate[1].Split(':')[1];
                }
                string strNewDate = arrDate[0] + " " + strNewTime + ":00";
                datetime = DateTime.ParseExact(strNewDate, "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat);
            }
            return datetime;
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Validation() == true)
            {
                DateTime datetime =DateTime.Now;
                if (txtNewsDate.Text != "")
                {
                    datetime = convertDate(txtNewsDate.Text);
                }


                string sproIndex = "0";
                string sproActive = "0";
                int iImage = int.Parse(ddlImage.SelectedValue);
                string[] arrImage = new string[iImage];
                for (int i = 0; i < iImage; i++)
                {
                    TextBox txtMImage = (TextBox)rptImages.Items[i].FindControl("txtMImage");
                    if (!String.IsNullOrEmpty(txtMImage.Text))
                        arrImage[i] = txtMImage.Text;
                }

                arrImage = arrImage.Where(s => s != null && s.Length > 0).ToArray();
                if (chkSanphammoi.Checked == true) { sproIndex = "1"; }
                if (chkKichhoat.Checked == true) { sproActive = "1"; } else { sproActive = "0"; }
                string proID = hidID.Value;
                if (proID.Length == 0)
                {
                    var ObjtbProductDATA = new tbProduct();
                    ObjtbProductDATA.proCode = txtMa.Text.Trim();
                    ObjtbProductDATA.proName = txtTen.Text.Trim();
                    ObjtbProductDATA.proImage = string.Join(",", arrImage);
                    ObjtbProductDATA.proFiles = "";
                    ObjtbProductDATA.proContent = fckTomtat.Text;
                    ObjtbProductDATA.proDetail = "";
                    ObjtbProductDATA.proDate = datetime;
                    ObjtbProductDATA.proTitle = txtTieude.Text;
                    ObjtbProductDATA.proDescription = txtDesscription.Text;
                    ObjtbProductDATA.proKeyword = txtKeyword.Text;
                    ObjtbProductDATA.proPriority = 0;
                    ObjtbProductDATA.proIndex = Convert.ToInt32(sproIndex);
                    ObjtbProductDATA.proType = 1;
                    ObjtbProductDATA.proActive = Convert.ToInt32(sproActive);
                    ObjtbProductDATA.proOriginalPrice = txtGiagoc.Text.Trim() == "" ? "0" : txtGiagoc.Text;
                    ObjtbProductDATA.proPrice = txtGia.Text.Trim() == "" ? "0" : txtGia.Text;
                    ObjtbProductDATA.proUnitprice = txtLoaitien.Text;
                    ObjtbProductDATA.proPromotions = txtPromotions.Text;
                    ObjtbProductDATA.proWeight = "0";
                    ObjtbProductDATA.proUnits = "";
                    ObjtbProductDATA.proOrd = txtThutu.Text != "" ? Convert.ToInt32(txtThutu.Text) : 1;
                    ObjtbProductDATA.proLang = lang;
                    ObjtbProductDATA.proWarranty = txtproWarranty.Text;
                    ObjtbProductDATA.proTagName = "";
                    ObjtbProductDATA.proVideo = txtVideo.Text;
                    ObjtbProductDATA.proCount = txtCount.Text != "" ? Convert.ToDouble(txtCount.Text) : 0;
                    ObjtbProductDATA.proSpLienQuan = "";
                    ObjtbProductDATA.proSpLienQuan = "";
                    ObjtbProductDATA.proUrl = "";
                    ObjtbProductDATA.proKieuspdaban = 0;
                    ObjtbProductDATA.proNoibat = chkNoibat.Checked ? 1 : 0;
                    ObjtbProductDATA.proBanchay = chkSpbanchay.Checked ? 1 : 0;
                    ObjtbProductDATA.proKM = chkKM.Checked ? 1 : 0;

                    if (!string.IsNullOrEmpty(txtTungay.Text))
                    {                        
                        ObjtbProductDATA.proTungay = convertDate(txtTungay.Text);
                    }
                    if (!string.IsNullOrEmpty(txtDenngay.Text))
                    {
                        ObjtbProductDATA.proDenngay = convertDate(txtDenngay.Text);
                    }
                    ObjtbProductDATA.proNoidungKM = txtPromotions.Text;

                    foreach (ListItem item in rbtStatus.Items)
                    {
                        if (item.Selected)
                        {
                            ObjtbProductDATA.proStatus = Convert.ToInt32(item.Value);
                        }
                    }

                    ObjtbProductDATA.catId = ddlForder.SelectedValue;
                    if (!string.IsNullOrEmpty(ddlMan.SelectedValue))
                        ObjtbProductDATA.manufacturerId = int.Parse(ddlMan.SelectedValue);
                    else
                        ObjtbProductDATA.manufacturerId = null;

                    #region Them vao tbPage + tbProduct
                    tbPageDATA objNewMenu = new tbPageDATA();
                    objNewMenu.pagName = txtTen.Text;
                    objNewMenu.pagImage = string.Join(",", arrImage); ;
                    objNewMenu.pagType = pageType.PD;
                    var curNewType = db.tbPages.Where(s => s.pagId == int.Parse(ddlForder.SelectedValue)).FirstOrDefault();
                    objNewMenu.paglevel = curNewType.paglevel + "00000";
                    objNewMenu.pagTitle = txtTen.Text;
                    objNewMenu.pagDescription = txtDesscription.Text;
                    objNewMenu.pagKeyword = txtKeyword.Text;
                    objNewMenu.pagOrd = "0";
                    objNewMenu.pagActive = chkKichhoat.Checked ? "0" : "1";
                    objNewMenu.pagLang = lang;
                    objNewMenu.pagDetail = "";
                    objNewMenu.pagLink = "";
                    objNewMenu.pagTarget = "";
                    objNewMenu.pagPosition = "0";
                    objNewMenu.catId = "";
                    objNewMenu.grnId = "";
                    objNewMenu.pagTagName = "";
                    objNewMenu.check1 = "";
                    objNewMenu.check2 = "";
                    objNewMenu.check3 = "";
                    objNewMenu.check4 = "";
                    objNewMenu.check5 = "";
                    string tagName = MyWeb.Common.StringClass.NameToTag(txtTen.Text);
                    if (tbPageDB.tbPage_Add(objNewMenu))
                    {
                        List<tbPageDATA> curItem = tbPageDB.tbPage_GetByMax();
                        var hasTagName = db.tbPages.Where(s => s.pagTagName == tagName).FirstOrDefault();
                        curItem[0].pagId = curItem[0].pagId;
                        curItem[0].pagTagName = hasTagName != null ? tagName + "-" + curItem[0].pagId : tagName;
                        tbPageDB.tbPage_Update(curItem[0]);

                        ObjtbProductDATA.proTagName = curItem[0].pagTagName;

                        if (tbProductDB.tbProduct_Add(ObjtbProductDATA))
                        {
                            List<tbProductDATA> listp = tbProductDB.tbProduct_GetByTop("1", "", "proDate DESC");
                            try
                            {
                                string proid = listp[0].proId;

                                tbProductDB.tbProductTag("update tbProTag set proId=" + listp[0].proId + " where proId is null");
                            }
                            catch
                            {
                                pnlErr.Visible = true;
                                ltrErr.Text = "Đã xảy ra lỗi ! Xin vui lòng kiểm tra dữ liệu và thử lại !";
                            }
                            tbProduct attpro = db.tbProducts.OrderByDescending(c => c.proId).FirstOrDefault();

                            var xpro = (from p in db.tbProducts orderby p.proId descending select p).Take(1).ToList();
                           
                            xpro[0].userId = Session["uid"].ToString();
                            db.SubmitChanges();
                            pnlErr.Visible = true;
                            ltrErr.Text = "Thêm mới sản phẩm thành công !!";
                        }
                    }
                    #endregion

                    tbProduct objFistNew = db.tbProducts.OrderByDescending(s => s.proId).FirstOrDefault();
                    UpdateAttributes(objFistNew.proId.ToString(), chkTinhnang);

                    Resetcontrol();
                    ResetUploadForm();
                    Response.Redirect("/admin-product/list.aspx");
                }
                else
                {
                    string idpage = "";
                    string NewRelateID = "", strDauPhay = "";
                    int j = 0;
                    if (lstNewRelate.Items.Count > 0)
                    {
                        for (j = 0; j < lstNewRelate.Items.Count; j++)
                        {
                            strDauPhay = j == lstNewRelate.Items.Count - 1 ? "" : ";";
                            NewRelateID += lstNewRelate.Items[j].Text + strDauPhay;
                        }
                    }
                    var objUpdate = db.tbProducts.Where(s => s.proId + "" == proID).ToList();
                    if (objUpdate.Count > 0)
                    {
                        objUpdate[0].proCode = txtMa.Text;
                        objUpdate[0].proName = txtTen.Text;
                        objUpdate[0].proTag = NewRelateID;
                        objUpdate[0].proImage = string.Join(",", arrImage);
                        objUpdate[0].proFiles = "";
                        objUpdate[0].proContent = fckTomtat.Text;
                        objUpdate[0].proDetail = "";
                        objUpdate[0].proDate = datetime;
                        objUpdate[0].proTitle = txtTieude.Text;
                        objUpdate[0].proDescription = txtDesscription.Text;
                        objUpdate[0].proKeyword = txtKeyword.Text;
                        objUpdate[0].proPriority = 0;
                        objUpdate[0].proIndex = Convert.ToInt32(sproIndex);
                        objUpdate[0].proType = 1;
                        objUpdate[0].proActive = Convert.ToInt32(sproActive);
                        objUpdate[0].proOriginalPrice = txtGiagoc.Text.Trim() == "" ? "0" : txtGiagoc.Text;
                        objUpdate[0].proPrice = txtGia.Text.Trim() == "" ? "0" : txtGia.Text;
                        objUpdate[0].proUnitprice = txtLoaitien.Text;
                        objUpdate[0].proPromotions = txtPromotions.Text;
                        objUpdate[0].proWeight = "0";
                        objUpdate[0].proUnits = "";
                        objUpdate[0].proOrd = txtThutu.Text != "" ? Convert.ToInt32(txtThutu.Text) : 1;
                        objUpdate[0].proLang = lang;
                        objUpdate[0].proWarranty = txtproWarranty.Text;
                        objUpdate[0].proVideo = txtVideo.Text;
                        objUpdate[0].proCount = txtCount.Text != "" ? Convert.ToDouble(txtCount.Text) : 0;
                        objUpdate[0].proSpLienQuan = "";
                        objUpdate[0].proUrl = "";
                        objUpdate[0].proKieuspdaban = 0;
                        objUpdate[0].proNoibat = chkNoibat.Checked ? 1 : 0;
                        objUpdate[0].proBanchay = chkSpbanchay.Checked ? 1 : 0;
                        objUpdate[0].proKM = chkKM.Checked ? 1 : 0;
                        if (!string.IsNullOrEmpty(txtTungay.Text + ""))
                        {
                            objUpdate[0].proTungay = convertDate(txtTungay.Text);
                        }
                        if (!string.IsNullOrEmpty(txtDenngay.Text + ""))
                        {
                            objUpdate[0].proDenngay = convertDate(txtDenngay.Text);
                        }
                        objUpdate[0].proNoidungKM = txtPromotions.Text;
                        foreach (ListItem item in rbtStatus.Items)
                        {
                            if (item.Selected)
                            {
                                objUpdate[0].proStatus = Convert.ToInt32(item.Value);
                            }
                        }
                        objUpdate[0].catId = ddlForder.SelectedValue;
                        objUpdate[0].manufacturerId = int.Parse(ddlMan.SelectedValue);
                        objUpdate[0].userId = Session["uid"].ToString();
                        #region Update Page 
                        List<tbPageDATA> curPage = tbPageDB.tbPage_GetByTagName(objUpdate[0].proTagName);
                        if (curPage.Count() > 0)
                        {
                            idpage = curPage[0].pagId;
                            string bfTagname = curPage[0].pagTagName;
                            curPage[0].pagName = txtTen.Text;
                            curPage[0].pagImage = string.Join(",", arrImage);
                            curPage[0].pagType = pageType.PD;
                            var curNewType = db.tbPages.Where(s => s.pagId == int.Parse(ddlForder.SelectedValue)).FirstOrDefault();
                            curPage[0].paglevel = curNewType.paglevel + "00000";
                            curPage[0].pagTitle = txtTieude.Text;
                            curPage[0].pagDescription = txtDesscription.Text;
                            curPage[0].pagKeyword = txtKeyword.Text;
                            curPage[0].pagOrd = "0";
                            curPage[0].pagActive = chkKichhoat.Checked ? "0" : "1";
                            curPage[0].pagLang = lang;
                            curPage[0].pagDetail = "";
                            curPage[0].pagLink = "";
                            curPage[0].pagTarget = "";
                            curPage[0].pagPosition = "0";
                            curPage[0].catId = "0";
                            curPage[0].grnId = "0";
                            curPage[0].pagTagName = "";
                            curPage[0].check1 = "";
                            curPage[0].check2 = "";
                            curPage[0].check3 = "";
                            curPage[0].check4 = "";
                            curPage[0].check5 = "";

                            string tagName = objUpdate[0].proTagName;
                            List<tbPage> hasTagName = db.tbPages.Where(s => s.pagName == txtTen.Text).ToList();
                            if (hasTagName.Count > 1)
                            {
                                curPage[0].pagTagName = MyWeb.Common.StringClass.NameToTag(txtTen.Text) + "-" + objUpdate[0].proId;
                            }
                            else
                            {
                                curPage[0].pagTagName = tagName;
                            }

                            objUpdate[0].proTagName = curPage[0].pagTagName;
                            tbPageDB.tbPage_Update(curPage[0]);

                            #region Update các menu liên kết đến
                            List<tbPageDATA> lstUpdate = tbPageDB.tbPage_GetByAll(lang);
                            lstUpdate = lstUpdate.Where(s => s.pagLink == bfTagname).ToList();
                            if (lstUpdate.Count > 0)
                            {
                                for (int i = 0; i < lstUpdate.Count; i++)
                                {
                                    lstUpdate[i].pagLink = curPage[0].pagTagName;
                                    tbPageDB.tbPage_Update(lstUpdate[i]);
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            tbPageDATA objNewMenu = new tbPageDATA();
                            objNewMenu.pagName = txtTen.Text;
                            objNewMenu.pagImage = "";
                            objNewMenu.pagType = pageType.PD;
                            var curNewType = db.tbPages.Where(s => s.pagId == int.Parse(ddlForder.SelectedValue)).FirstOrDefault();
                            objNewMenu.paglevel = curNewType.paglevel + "00000";
                            objNewMenu.pagTitle = txtTen.Text;
                            objNewMenu.pagDescription = txtDesscription.Text;
                            objNewMenu.pagKeyword = txtKeyword.Text;
                            objNewMenu.pagOrd = "0";
                            objNewMenu.pagActive = chkKichhoat.Checked ? "0" : "1";
                            objNewMenu.pagLang = lang;
                            objNewMenu.pagDetail = "";
                            objNewMenu.pagLink = "";
                            objNewMenu.pagTarget = "";
                            objNewMenu.pagPosition = "0";
                            objNewMenu.catId = "";
                            objNewMenu.grnId = "";
                            objNewMenu.pagTagName = "";
                            objNewMenu.check1 = "";
                            objNewMenu.check2 = "";
                            objNewMenu.check3 = "";
                            objNewMenu.check4 = "";
                            objNewMenu.check5 = "";
                            string tagName = MyWeb.Common.StringClass.NameToTag(txtTen.Text);
                            if (tbPageDB.tbPage_Add(objNewMenu))
                            {
                                List<tbPageDATA> curItem = tbPageDB.tbPage_GetByMax();
                                var hasTagName = db.tbPages.Where(s => s.pagTagName == tagName).FirstOrDefault();
                                curItem[0].pagId = curItem[0].pagId;
                                curItem[0].pagTagName = hasTagName != null ? tagName + "-" + curItem[0].pagId : tagName;
                                tbPageDB.tbPage_Update(curItem[0]);
                                objUpdate[0].proTagName = curItem[0].pagTagName;
                                tbPageDB.tbPage_Update(curItem[0]);
                            }
                        }
                        #endregion

                        if (tbProductDB.tbProduct_Update(objUpdate[0]))
                        {
                            pnlErr.Visible = true;
                            ltrErr.Text = "Cập nhật sản phẩm thành công !!";
                        }

                        UpdateAttributes(objUpdate[0].proId.ToString(), chkTinhnang);
                    }

                    Response.Redirect("/admin-product/list.aspx");

                }
            }
        }

        protected void BindTags()
        {
            IEnumerable<tbTag> list = db.tbTags.Where(s => s.Lang == lang && s.Active == 1 && s.Type == 1).OrderBy(s => s.Ord).ThenByDescending(s => s.Id);

            foreach (tbTag n in list)
            {
                lstNew.Items.Add(new ListItem(n.Name, n.Id.ToString()));
            }
        }

        void BindForder()
        {
            string strForder = "";
            if (_Admin == "0" || _Admin == "100" || _Admin == "1")
            {
                List<tbPageDATA> list = tbPageDB.tbPage_GetByAll(lang);
                list = list.Where(s => s.pagType == pageType.GP).ToList();
                ddlForder.Items.Clear();
                ddlForder.Items.Add(new ListItem("- Nhóm sản phẩm -", "0"));
                for (int i = 0; i < list.Count; i++)
                {
                    strForder = "";
                    for (int j = 1; j < list[i].paglevel.Length / 5; j++)
                    {
                        strForder = strForder + "---";
                    }
                    ddlForder.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].pagId));
                }
                list.Clear();
                list = null;

            }
            else
            {
                List<tbPageDATA> list = tbPageDB.tbPage_GetByTop("", "pagType = '100' and pagId in (select roleidmodule from tbUserRole where roleuserid=" + userid + ")", "paglevel");
                ddlForder.Items.Clear();
                ddlForder.Items.Add(new ListItem("- Nhóm sản phẩm -", "0"));
                for (int i = 0; i < list.Count; i++)
                {
                    strForder = "";
                    for (int j = 1; j < list[i].paglevel.Length / 5; j++)
                    {
                        strForder = strForder + "---";
                    }
                    ddlForder.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].pagId));
                }
                list.Clear();
                list = null;
            }

        }

        protected void BindListMan()
        {
            string strForder = "";
            List<tbPageDATA> list = tbPageDB.tbPage_GetByAll(lang);
            list = list.Where(s => s.pagType == pageType.GM).ToList();
            ddlMan.Items.Clear();
            ddlMan.Items.Add(new ListItem("- Hãng sản xuất -", "0"));
            for (int i = 0; i < list.Count; i++)
            {
                strForder = "";
                for (int j = 1; j < list[i].paglevel.Length / 5; j++)
                {
                    strForder = strForder + "---";
                }
                ddlMan.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].pagId));
            }
            list.Clear();
            list = null;
        }

        void BindAttributes()
        {
            IEnumerable<tbAttribute> list = db.tbAttributes.Where(a => a.Lang == lang).OrderBy(a => a.Level);
            chkTinhnang.Items.Clear();
            if (list.Count() > 0)
            {
                attr = true;
                string str = "";
                foreach (tbAttribute lst in list)
                {
                    str = "";
                    for (int j = 1; j < lst.Level.Length / 5; j++)
                    {
                        str = str + "-----";
                    }
                    ListItem li = new ListItem(str + lst.Name, lst.Id.ToString());
                    chkTinhnang.Items.Add(li);
                    foreach (ListItem listItem in chkTinhnang.Items)
                    {
                        if (listItem.ToString().Contains("-----"))
                            listItem.Enabled = true;
                        else listItem.Enabled = false;
                    }
                }
            }
            else
            {
                attr = false;

            }
        }

        void BindAttChecked(string Id)
        {
            IEnumerable<tbAttribute> list = db.tbAttributes.Where(a => a.Lang == lang).OrderBy(a => a.Level);
            chkTinhnang.Items.Clear();
            if (list.Count() > 0)
            {
                attr = true;
                string str = "";
                foreach (tbAttribute lst in list)
                {
                    str = "";
                    for (int j = 1; j < lst.Level.Length / 5; j++)
                    {
                        str = str + "-----";
                    }
                    ListItem li = new ListItem(str + lst.Name, lst.Id.ToString());
                    if (lst.Level.Length == 10)
                    {
                        IEnumerable<tbAttPro> listCatt = db.tbAttPros.Where(c => c.proId == Int32.Parse(Id) && c.attId == lst.Id);
                        li.Selected = listCatt.Count() > 0;
                    }
                    chkTinhnang.Items.Add(li);
                    foreach (ListItem listItem in chkTinhnang.Items)
                    {
                        if (listItem.ToString().Contains("-----"))
                            listItem.Enabled = true;
                        else listItem.Enabled = false;
                    }
                }
            }
        }

        #region[Validation]
        protected bool Validation()
        {
            if (ddlForder.SelectedValue == "") { ltrErr.Text = "Chưa chọn nhóm sản phẩm !!";  pnlErr.Visible = true; return false; }
            //if (ddlMan.SelectedValue == "") { ltrErr.Text = "Chưa chọn hãng sản xuất !!"; pnlErr.Visible = true; return false; }
            if (txtTen.Text == "") { ltrErr.Text = "Chưa nhập tên sản phẩm !!"; pnlErr.Visible = true; return false; }
            return true;
        }
        #endregion

        protected void btnActive_Click(object sender, EventArgs e)
        {
            int i = 0;
            for (i = 0; i <= lstNewRelate.Items.Count - 1; i++)
            {
                if (lstNewRelate.Items[i].Value == lstNew.SelectedValue)
                {
                    return;
                }
            }
            lstNewRelate.Items.Add(new ListItem(lstNew.SelectedItem.Text, lstNew.SelectedItem.Value));
        }

        protected void btnUnactive_Click(object sender, EventArgs e)
        {
            int i = 0;
            for (i = 0; i <= lstNewRelate.Items.Count - 1; i++)
            {
                if (lstNewRelate.Items[i].Selected == true)
                {
                    lstNewRelate.Items.RemoveAt(lstNewRelate.SelectedIndex);
                    return;
                }
            }
        }

        private void UpdateAttributes(string strID, CheckBoxList cbl)
        {
            List<tbAttPro> listcatatt = db.tbAttPros.Where(c => c.proId == Int32.Parse(strID)).ToList();
            if (listcatatt.Count > 0)
            {
                for (int i = 0; i < listcatatt.Count; i++)
                {
                    CenterDB.Attpro_delete(listcatatt[i].Id.ToString());
                }
            }
            tbAttribute att = new tbAttribute();
            string level = "";
            foreach (ListItem listItem in cbl.Items)
            {
                if (listItem.Selected == true)
                {
                    level = att.Level;
                    tbAttribute attsub = db.tbAttributes.FirstOrDefault(a => a.Id == Int32.Parse(listItem.Value));
                    att = db.tbAttributes.FirstOrDefault(a => a.Level == attsub.Level.Substring(0, 5));
                    CenterDB.Insert_attpro(listItem.Value, strID);
                }
            }
        }

        #region[Resetcontrol]
        void Resetcontrol()
        {
            ddlImage.SelectedValue = "1";
            rbtStatus.SelectedValue = "0";
            txtTen.Text = "";
            txtGiagoc.Text = "";
            txtGia.Text = "";
            txtTieude.Text = "";
            txtDesscription.Text = "";
            txtKeyword.Text = "";
            txtPromotions.Text = "";
            fckTomtat.Text = "";
            txtMa.Text = "";
            txtproWarranty.Text = "";
            chkSanphammoi.Checked = false;
            chkNoibat.Checked = false;
            chkSpbanchay.Checked = false;
            chkKM.Checked = false;
            txtTungay.Text = "";
            txtDenngay.Text = "";
            txtDenngay.Text = "";
            txtPromotions.Text = "";
            lstNewRelate.Items.Clear();
        }
        protected void ResetUploadForm()
        {
            ddlImage.SelectedValue = "1";
            for (int i = 0; i < 10; i++)
            {
                TextBox txtMImage = (TextBox)rptImages.Items[i].FindControl("txtMImage");
                txtMImage.Text = "";
            }
        }
        #endregion

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("/admin-product/list.aspx");
        }

    }
}