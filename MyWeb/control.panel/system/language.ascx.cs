﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.system
{
    public partial class language : System.Web.UI.UserControl
    {
        private string lang = "vi";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (!IsPostBack)
            {
                BindData();
            }
        }

        void BindData()
        {
            var obj = db.tbLanguages.OrderBy(s => s.lanOrder).ToList();
            if (obj.Count() > 0)
            {
                rptFolderList.DataSource = obj;
                rptFolderList.DataBind();
            }
            else
            {
                rptFolderList.DataSource = null;
                rptFolderList.DataBind();
            }
        }

        public static string ShowActive(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }

        public static string ShowActiveClass(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "btn btn-primary btn-xs" : "btn btn-danger btn-xs";
        }

        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Active":
                    tbLanguage obj = db.tbLanguages.FirstOrDefault(s => s.lanId == strID);
                    if (obj.lanActive== true)
                    {
                        obj.lanActive = false;
                    }
                    else { obj.lanActive = true; }
                    db.SubmitChanges();
                    BindData();
                    break;
                case "Edit":
                    db.tbLanguages.ToList().ForEach(s => s.lanDefault = false);
                    db.SubmitChanges();
                    tbLanguage _obj = db.tbLanguages.FirstOrDefault(s => s.lanId == strID);
                    _obj.lanDefault = true;
                    db.SubmitChanges();
                    BindData();
                    break;
            }
        }
    }
}