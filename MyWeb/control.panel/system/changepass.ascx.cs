﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.system
{
    public partial class changepass : System.Web.UI.UserControl
    {
        static int UserID;
        static string Username = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uid"] != null) { UserID = int.Parse(Session["uid"].ToString()); }
            if (Session["user"] != null) { Username = Session["user"].ToString(); }
            txtUserName.Text = Username.ToString();
            txtUserName.ReadOnly = true;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtPass.Text == "")
            {
                pnlErr.Visible = true;
                ltrErr.Text = "Mật khẩu không được trống!";
                return;
            }
            if (txtRePass.Text == "")
            {
                pnlErr.Visible = true;
                ltrErr.Text = "Nhập lại mật khẩu và xác nhận lại !";
                return;
            }
            if (txtPass.Text != txtRePass.Text)
            {
                pnlErr.Visible = true;
                ltrErr.Text = "Xác nhận mật khẩu sai !";
                return;
            }
            List<tbUserDATA> objListUser = tbUserDB.tbUser_GetByID(UserID.ToString());
            if (objListUser.Count > 0)
            {
                objListUser[0].useId = UserID.ToString();
                objListUser[0].usePid = EncryptCode.Encrypt(txtPass.Text);
                if (tbUserDB.tbusers_Changepass(objListUser[0]))
                {
                    pnlErr.Visible = true;
                    ltrErr.Text = "Đổi mật khẩu thành công !";
                    txtPass.Text = "";
                    txtRePass.Text = "";
                }
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {

        }
    }
}