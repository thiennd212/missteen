﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="changepass.ascx.cs" Inherits="MyWeb.control.panel.system.changepass" %>
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="/control.panel/">Trang chủ</a></li>
        <li class="active">Thay đổi mật khẩu</li>
    </ol>
    <h1 class="page-header">Thay đổi mật khẩu</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse" data-sortable-id="table-basic-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">Thay đổi mật khẩu</h4>
                </div>

                <div class="panel-body panel-form">
                    <asp:Panel ID="pnlErr" runat="server" Visible="false">
                        <div class="alert alert-danger fade in" style="border-radius: 0px;">
                            <button class="close" data-dismiss="alert" type="button">
                                <span aria-hidden="true">×</span>
                            </button>
                            <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                        </div>
                    </asp:Panel>
                    <div class="form-horizontal form-bordered">
                        <div class="form-group">
                            <label class="control-label col-md-2">Tên đăng nhập:</label>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtUserName" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Mật khẩu:</label>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtPass" runat="server" class="form-control" TextMode="Password"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2">Xác nhận mật khẩu:</label>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtRePass" runat="server" class="form-control" TextMode="Password"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2"></label>
                            <div class="col-md-7">
                                <asp:LinkButton ID="btnUpdate" runat="server" class="btn btn-primary" OnClick="btnUpdate_Click">Cập nhật</asp:LinkButton>
                                <asp:LinkButton ID="btnReset" runat="server" class="btn btn-danger" OnClick="btnReset_Click">Hủy</asp:LinkButton>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>