﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="listContestant.ascx.cs" Inherits="MyWeb.control.panel.contestant.listContestant" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<script src="../../scripts/ckfinder/ckfinder.js" type="text/javascript"></script>
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="/control.panel/">Trang chủ</a></li>
        <li class="active">Quản lý thí sinh</li>
    </ol>
    <!-- end breadcrumb -->
    <asp:Panel ID="pnlListForder" runat="server" Visible="true">
        <h1 class="page-header">Quản lý thí sinh</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse" data-sortable-id="table-basic-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Danh sách thí sinh</h4>
                    </div>

                    <div class="alert alert-info fade in" id="pnlErr" runat="server" visible="false">
                        <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                        <button class="close" data-dismiss="alert" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <asp:LinkButton ID="btnAddNew" runat="server" class="btn btn-success btn-sm" OnClick="btnAddNew_Click"><i class="fa fa-plus"></i><span>Thêm mới</span></asp:LinkButton>
                                <asp:LinkButton ID="btnDeleteAll" runat="server" class="btn btn-danger btn-sm" OnClick="btnDeleteAll_Click" OnClientClick="javascript:return confirm('Bạn có muốn xóa thư mục đã chọn?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                            </div>

                            <div class="col-sm-4">
                                <asp:TextBox ID="txtSearch" runat="server" class="form-control input-sm" placeholder="Từ khóa tìm kiếm"></asp:TextBox>
                            </div>

                            <div class="col-sm-2">
                                <asp:DropDownList ID="drlForder" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drlForder_SelectedIndexChanged"></asp:DropDownList>
                            </div>

                            <div class="col-sm-1">
                                <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-primary btn-sm" Style="float: right;" OnClick="btnSearch_Click"><i class="fa fa-search"></i><span>Tìm kiếm</span></asp:LinkButton>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                                        <thead>
                                            <tr>
                                                <th width="10">
                                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="False"></asp:CheckBox>
                                                </th>
                                                <th>Số báo danh</th>
                                                <th>Tên thí sinh</th>
                                                <th>Nhóm</th>
                                                <th>Tỉnh thành</th>
                                                <th>Tuổi</th>
                                                <th>Lượt bình chọn</th>
                                                <th width="160">Công cụ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptFolderList" runat="server" OnItemCommand="rptFolderList_ItemCommand">
                                                <ItemTemplate>
                                                    <tr class="even gradeC">
                                                        <td>
                                                            <asp:CheckBox ID="chkBox" CssClass="chkBoxSelect" runat="server"></asp:CheckBox>
                                                            <asp:HiddenField ID="hidCatID" Value='<%#DataBinder.Eval(Container.DataItem, "Id")%>' runat="server" />
                                                            <asp:Label ID="lblID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Id")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <%#Eval("IdentificationNumber").ToString()%>
                                                        </td>
                                                        <td>
                                                            <%#Eval("FullName").ToString()%>
                                                        </td>
                                                        <td>
                                                            <%#GetCategoryName(Eval("IdContestantGroup").ToString())%>    
                                                        </td>
                                                        <td>
                                                            <%#Eval("ContestantPlace").ToString() %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("Age").ToString()%>
                                                        </td>
                                                        <td>
                                                            <%#Eval("TotalVoted").ToString()%>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton class="btn btn-success btn-xs" ID="btnEdit" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Id")%>' CommandName="Edit" ToolTip="Sửa"><i class="fa fa-pencil-square-o"></i>Sửa</asp:LinkButton>
                                                            <asp:LinkButton class="btn btn-danger btn-xs" ID="btnDel" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Id")%>' CommandName="Del" ToolTip="Xóa" OnClientClick="javascript:return confirm('Bạn có muốn xóa?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row dataTables_wrapper">
                            <div class="col-sm-5">
                                <div id="data-table_info" class="dataTables_info" role="status" aria-live="polite">
                                    <asp:Literal ID="ltrStatistic" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div id="data-table_paginate" class="dataTables_paginate paging_simple_numbers">
                                    <ul class="pagination">
                                        <li id="data-table_previous" class="paginate_button previous disabled">
                                            <asp:LinkButton ID="btnPrevious" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Trước</asp:LinkButton>
                                        </li>
                                        <asp:Repeater ID="rptNumberPage" runat="server" OnItemCommand="rptNumberPage_ItemCommand" OnItemDataBound="rptNumberPage_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrLiPage" runat="server"></asp:Literal>
                                                <asp:LinkButton ID="btn" runat="server" CommandArgument='<%# Eval("PageIndex")%>' CommandName="page" Text='<%# Eval("PageText")%> '></asp:LinkButton></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <li id="data-table_next" class="paginate_button next">
                                            <asp:LinkButton ID="btnNext" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Sau</asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAddForder" runat="server" Visible="false">
        <h1 class="page-header">Thêm/sửa thí sinh</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse" data-sortable-id="table-basic-2">
                    <div class="panel panel-inverse panel-with-tabs" data-sortable-id="ui-unlimited-tabs-1">
                        <div class="panel-heading p-0">
                            <div class="panel-heading-btn m-r-10 m-t-10">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            </div>
                            <div class="tab-overflow">
                                <ul class="nav nav-tabs nav-tabs-inverse">
                                    <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-inverse"><i class="fa fa-arrow-left"></i></a></li>
                                    <li class="active"><a href="#info-tab" data-toggle="tab">Thông tin thí sinh</a></li>
                                    <li class=""><a href="#contain-tab" data-toggle="tab">Thông tin khác</a></li>
                                    <li class=""><a href="#img-tab" data-toggle="tab">Ảnh đại diện</a></li>
                                    <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-inverse"><i class="fa fa-arrow-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-content panel-body panel-form">
                            <asp:Panel ID="pnlErr2" runat="server" Visible="false">
                                <div class="alert alert-danger fade in" style="border-radius: 0px;">
                                    <button class="close" data-dismiss="alert" type="button">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <asp:Literal ID="ltrErr2" runat="server"></asp:Literal>
                                </div>
                            </asp:Panel>
                            <div class="tab-pane fade active in" id="info-tab">
                                <div class="form-horizontal form-bordered">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Nhóm thí sinh:</label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCategory" runat="server" class="form-control"></asp:DropDownList>
                                            <asp:HiddenField ID="hidID" runat="server" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Số báo danh:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtIdentificationNumber" runat="server" class="form-control" onkeyup="valid(this,'quotes')"
                                                 onblur="valid(this,'quotes')">0</asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Tên thí sinh:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtName" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Tuổi:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtAge" runat="server" class="form-control" onkeyup="valid(this,'quotes')" 
                                                onblur="valid(this,'quotes')">1</asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Chiều cao:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtHeight" runat="server" class="form-control">0.00</asp:TextBox>
                                            <asp:CompareValidator ControlToValidate="txtHeight" Operator="DataTypeCheck" ID="MileageValidator" 
                                                runat="server" Type="Double" Display="Dynamic" ErrorMessage="Nhập sai định dạng." style="color:red"> </asp:CompareValidator>
                                        </div>
                                        <label class="control-label col-md-2" style="text-align:left">cm</label>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Tỉnh thành:</label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlPlace" runat="server" class="form-control"></asp:DropDownList>
                                            <asp:HiddenField ID="hfIdPlace" runat="server" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Lượt bình chọn:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtVoted" runat="server" class="form-control" onkeyup="valid(this,'quotes')"
                                                 onblur="valid(this,'quotes')" ReadOnly="true">0</asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Hiển thị trang chủ:</label>
                                        <div class="col-md-7">
                                            <asp:CheckBox ID="ckbHome" runat="server" Checked="true" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Hiển thị:</label>
                                        <div class="col-md-7">
                                            <asp:CheckBox ID="ckbActive" runat="server" Checked="true" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contain-tab">
                                <div class="form-horizontal form-bordered">
                                    <asp:UpdatePanel runat="server" ID="upTab">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Thành tích cá nhân:</label>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <CKEditor:CKEditorControl ID="txtPersonalAchievements" runat="server"></CKEditor:CKEditorControl>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Năng khiếu:</label>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <CKEditor:CKEditorControl ID="txtGifted" runat="server"></CKEditor:CKEditorControl>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Sở thích:</label>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <CKEditor:CKEditorControl ID="txtHobby" runat="server"></CKEditor:CKEditorControl>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Châm ngôn sống:</label>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <CKEditor:CKEditorControl ID="txtLifeMotto" runat="server"></CKEditor:CKEditorControl>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Quan điểm:</label>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <CKEditor:CKEditorControl ID="txtPointOfView" runat="server"></CKEditor:CKEditorControl>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="img-tab">
                                <div class="form-horizontal form-bordered">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 1:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage1" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage1.ClientID%>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 2:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage2" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage2.ClientID%>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 3:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage3" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage3.ClientID%>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 4:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage4" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage4.ClientID%>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 5:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage5" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage5.ClientID%>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 6:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage6" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage6.ClientID%>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 7:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage7" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage7.ClientID%>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 8:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage8" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage8.ClientID%>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 9:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage9" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage9.ClientID%>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Ảnh 10:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <asp:TextBox ID="txtImage10" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                    <div class="input-group-btn">
                                                        <button onclick="BrowseServer('<%=txtImage10.ClientID%>','Adv');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal form-bordered">
                                <div class="form-group">
                                    <label class="control-label col-md-2"></label>
                                    <div class="col-md-7">
                                        <asp:LinkButton ID="btnUpdate" runat="server" class="btn btn-primary" OnClick="btnUpdate_Click">Cập nhật</asp:LinkButton>
                                        <asp:LinkButton ID="btnReset" runat="server" class="btn btn-danger" OnClick="btnReset_Click">Hủy</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</div>
