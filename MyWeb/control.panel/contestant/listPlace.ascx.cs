﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.contestant 
{
    public partial class listPlace : System.Web.UI.UserControl 
    {
        string _Admin = "";
        private string lang = "vi";
        int pageSize = 15;
        string userid = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); }
            if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); }
            if (Session["uid"] != null) userid = Session["uid"].ToString();

            if (Request["pages"] != null && !Request["pages"].Equals(""))
            {
                pageSize = int.Parse(Request["pages"].ToString());
            }
            if (!IsPostBack)
            {
                BindForder();
                BindData();
            }
        }

        void BindData()
        {
            List<tbPageDATA> _listForder = tbPageDB.tbPage_GetByAll(lang);
            _listForder = _listForder.Where(s => s.pagType == pageType.ContestantPlace).ToList();

            if (txtSearch.Text != "")
            {
                _listForder = _listForder.Where(s => s.pagName.ToLower().Contains(txtSearch.Text.ToLower())).ToList();
            }

            if (drlForder.SelectedValue != "0")
            {
                _listForder = _listForder.Where(s => s.paglevel.StartsWith(drlForder.SelectedValue)).ToList();
            }

            if (_listForder.Count() > 0)
            {
                recordCount = _listForder.Count();
                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion

                var result = _listForder.Skip(currentPage * pageSize).Take(pageSize);
                rptFolderList.DataSource = result;
                rptFolderList.DataBind();
                BindPaging();
            }
            else
            {
                rptFolderList.DataSource = null;
                rptFolderList.DataBind();
            }
        }
        public string BindCateName(string strID)
        {
            string strReturn = "";
            var obj = db.tbPages.Where(s => s.paglevel == strID.Substring(0, strID.Length - 5)).ToList();
            if (obj.Count > 0)
            {
                strReturn = obj[0].pagName;
            }
            return strReturn;
        }

        void BindForder()
        {
            string strForder = "";
            List<tbPageDATA> list = tbPageDB.tbPage_GetByAll(lang);
            list = list.Where(s => s.pagType == pageType.ContestantPlace).ToList();
            drlForder.Items.Clear();
            drlForder.Items.Add(new ListItem("- Nhóm thí sinh -", "0"));

            drlChuyenmuc.Items.Clear();
            drlChuyenmuc.Items.Add(new ListItem("- Nhóm cha -", ""));
            for (int i = 0; i < list.Count; i++)
            {
                strForder = "";
                for (int j = 1; j < list[i].paglevel.Length / 5; j++)
                {
                    strForder = strForder + "---";
                }
                drlForder.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].paglevel));
                drlChuyenmuc.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].paglevel));
            }
            list.Clear();
            list = null;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            RepeaterItem item = default(RepeaterItem);
            for (int i = 0; i < rptFolderList.Items.Count; i++)
            {
                item = rptFolderList.Items[i];
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    if (((CheckBox)item.FindControl("chkBox")).Checked)
                    {
                        try
                        {
                            HiddenField hidID = (HiddenField)item.FindControl("hidCatID");
                            var itemDel = db.tbPages.Where(u => u.pagId == Convert.ToInt32(hidID.Value)).ToList();
                            if (itemDel.Count() > 0)
                            {
                                string levelss = itemDel[0].paglevel;

                                var pagecap1 = db.tbPages.Where(s => s.paglevel.Substring(0, levelss.Length) == levelss).ToList();
                                if (pagecap1.Count > 0)
                                {
                                    db.tbPages.DeleteAllOnSubmit(pagecap1);
                                }
                                db.tbPages.DeleteAllOnSubmit(itemDel);
                                db.SubmitChanges();
                            }
                        }
                        catch { }
                    }
                }
            }
            pnlErr.Visible = true;
            ltrErr.Text = "Xóa thành công nhóm thí sinh !";
            BindData();
            chkSelectAll.Checked = false;
        }

        protected void drlForder_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Active":
                    tbPageDATA item = tbPageDB.tbPage_GetByID(strID).FirstOrDefault();
                    if (item != null)
                    {
                        item.pagId = strID;
                        item.pagActive = item.pagActive == "1" ? "0" : "1";
                        tbPageDB.tbPage_Update(item);
                    }
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Cập nhật trạng thái thành công !";
                    break;
                case "Del":
                    var itemDel = db.tbPages.Where(u => u.pagId == Convert.ToInt32(strID)).ToList();
                    string levelss = itemDel[0].paglevel;

                    var pagecap1 = db.tbPages.Where(s => s.paglevel.Substring(0, levelss.Length) == levelss).ToList();
                    if (pagecap1.Count > 0)
                    {
                        db.tbPages.DeleteAllOnSubmit(pagecap1);
                        db.SubmitChanges();
                    }

                    tbPageDB.tbPage_Delete(strID);
                    pnlErr.Visible = true;
                    ltrErr.Text = "Xóa thành công nhóm thí sinh !";
                    BindData();

                    break;
                case "Edit":
                    BindForder();
                    tbPageDATA itemEdit = tbPageDB.tbPage_GetByID(strID).FirstOrDefault();
                    hidID.Value = strID;
                    hidLevel.Value = itemEdit.paglevel.Substring(0, itemEdit.paglevel.Length - 5);
                    hidParId.Value = itemEdit.grnId.ToString();
                    hidCurLever.Value = itemEdit.paglevel;
                    txtTenchuyenmuc.Text = itemEdit.pagName;
                    if (itemEdit.paglevel != "")
                    {
                        try
                        {
                            drlChuyenmuc.SelectedValue = itemEdit.paglevel.Substring(0, itemEdit.paglevel.Length - 5);
                        }
                        catch
                        {
                        }

                    }

                    txtThuTu.Text = itemEdit.pagOrd.ToString();
                    chkKichhoat.Checked = itemEdit.pagActive == "1" ? true : false;
                    pnlErr.Visible = false;
                    ltrErr.Text = "";
                    ltrErr2.Text = "";
                    pnlErr2.Visible = false;
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;

                    break;
                case "Add":
                    BindForder();
                    tbPageDATA itemAdd = tbPageDB.tbPage_GetByID(strID).FirstOrDefault();
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    Resetcontrol();
                    hidLevel.Value = itemAdd.paglevel;
                    hidParId.Value = itemAdd.pagId;
                    ltrErr2.Text = "";
                    pnlErr2.Visible = false;
                    pnlErr.Visible = false;
                    ltrErr.Text = "";
                    drlChuyenmuc.SelectedValue = hidLevel.Value;
                    break;
            }
        }

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        protected void txtOrder_TextChanged(object sender, EventArgs e)
        {
            TextBox txtOrder = (TextBox)sender;
            var blbID = (Label)txtOrder.FindControl("lblID");
            List<tbPageDATA> list = tbPageDB.tbPage_GetByID(blbID.Text);
            if (list.Count > 0)
            {
                list[0].pagOrd = txtOrder.Text;
                tbPageDB.tbPage_Update(list[0]);
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật thứ tự thành công !";
        }

        protected string BindView(string name, string treecode)
        {
            string str = "<i class=\"ion-ios-folder fa-2x text-inverse icon-forder\"></i>";
            if (treecode.ToString().Trim().Length > 50)
            {
                str = "<span class='icon-forder' style='margin-left:100px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 45)
            {
                str = "<span class='icon-forder' style='margin-left:90px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 40)
            {
                str = "<span class='icon-forder' style='margin-left:80px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 35)
            {
                str = "<span class='icon-forder' style='margin-left:70px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 30)
            {
                str = "<span class='icon-forder' style='margin-left:60px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 25)
            {
                str = "<span class='icon-forder' style='margin-left:50px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 20)
            {
                str = "<span class='icon-forder' style='margin-left:40px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i><span>";
            }
            else if (treecode.ToString().Trim().Length > 15)
            {
                str = "<span class='icon-forder' style='margin-left:30px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 10)
            {
                str = "<span class='icon-forder' style='margin-left:20px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 5)
            {
                str = "<span class='icon-forder' style='margin-left:10px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else
            {
                str = str;
            }
            return str = str + "&nbsp;" + name;
        }

        protected void txtNameForder_TextChanged(object sender, EventArgs e)
        {
            TextBox txtNameForder = (TextBox)sender;
            var blbID = (Label)txtNameForder.FindControl("lblID");
            List<tbPageDATA> list = tbPageDB.tbPage_GetByID(blbID.Text);
            if (list.Count > 0)
            {
                list[0].pagId = blbID.Text;
                list[0].pagName = txtNameForder.Text;
                tbPageDB.tbPage_Update(list[0]);

            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật tên nhóm thành công !";
        }

        public static string ShowActive(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = false;
            pnlAddForder.Visible = true;
            Resetcontrol();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Validation() == true)
            {
                string scatActive = "";
                string checkleft = "", catIndex = "";
                if (chkKichhoat.Checked == true) { scatActive = "1"; } else { scatActive = "0"; }
                string strID = hidID.Value;
                string sgrnlevel = hidLevel.Value;
                if (drlChuyenmuc.SelectedValue != sgrnlevel)
                    sgrnlevel = drlChuyenmuc.SelectedValue;

                if (strID.Length == 0)
                {
                    tbPageDATA item = new tbPageDATA();
                    item.pagName = txtTenchuyenmuc.Text.Trim();
                    item.pagImage = "";
                    if (sgrnlevel.Length > 0)
                    {
                        item.paglevel = sgrnlevel + "00000";
                    }
                    else
                    {
                        item.paglevel = "00000";
                    }
                    item.pagTitle = "";
                    item.pagDescription = "";
                    item.pagKeyword = "";
                    item.pagType = pageType.ContestantPlace;
                    item.pagOrd = txtThuTu.Text;
                    item.pagActive = scatActive;
                    item.pagLang = lang;
                    item.pagDetail = "";
                    item.pagLink = "";
                    item.pagTarget = "";
                    item.pagPosition = "0";
                    item.catId = "";
                    item.grnId = "0";
                    item.pagTagName = "";
                    item.check1 = catIndex;
                    item.check2 = checkleft;
                    item.check3 = "";
                    item.check4 = "";
                    item.check5 = "";
                    string tagName = MyWeb.Common.StringClass.NameToTag(txtTenchuyenmuc.Text);
                    if (tbPageDB.tbPage_Add(item))
                    {

                        List<tbPageDATA> curItem = tbPageDB.tbPage_GetByMax();
                        tbPage pg = db.tbPages.FirstOrDefault(s => s.pagTagName == tagName && s.pagId != int.Parse(curItem[0].pagId));
                        if (pg != null)
                        {
                            curItem[0].pagTagName = tagName + "-" + curItem[0].pagId;
                        }
                        else
                        {
                            curItem[0].pagTagName = tagName;
                        }
                        tbPageDB.tbPage_Update(curItem[0]);
                        pnlErr.Visible = true;
                        ltrErr.Text = "Thêm mới địa điểm thành công !";
                        BindData();
                        Resetcontrol();
                        hidLevel.Value = "";
                        hidParId.Value = "";
                        pnlListForder.Visible = true;
                        pnlAddForder.Visible = false;
                    }
                }
                else
                {
                    List<tbPageDATA> lst = tbPageDB.tbPage_GetByID(strID);
                    if (lst.Count > 0)
                    {
                        string bfTagname = lst[0].pagTagName;
                        lst[0].pagId = strID;
                        lst[0].pagName = txtTenchuyenmuc.Text.Trim();
                        lst[0].pagImage = "";
                        lst[0].paglevel = sgrnlevel + "00000";
                        lst[0].pagTitle = "";
                        lst[0].pagDescription = "";
                        lst[0].pagKeyword = "";
                        lst[0].pagType = pageType.ContestantPlace;
                        lst[0].pagOrd = txtThuTu.Text;
                        lst[0].pagActive = scatActive;
                        lst[0].pagLang = lang;
                        lst[0].pagDetail = "";
                        lst[0].pagLink = "";
                        lst[0].pagTarget = "";
                        lst[0].pagPosition = "0";
                        lst[0].catId = "";
                        lst[0].grnId = "0";
                        var hasTagName = db.tbPages.Where(s => s.pagTagName == MyWeb.Common.StringClass.NameToTag(txtTenchuyenmuc.Text) && s.pagId != int.Parse(lst[0].pagId)).FirstOrDefault();
                        lst[0].pagTagName = hasTagName != null ? MyWeb.Common.StringClass.NameToTag(txtTenchuyenmuc.Text) + "-" + lst[0].pagId : MyWeb.Common.StringClass.NameToTag(txtTenchuyenmuc.Text);
                        lst[0].check1 = catIndex;
                        lst[0].check2 = checkleft;
                        lst[0].check3 = "";
                        lst[0].check4 = "";
                        lst[0].check5 = "";
                        if (tbPageDB.tbPage_Update(lst[0]))
                        {
                            #region Update các menu liên kết đến
                            List<tbPageDATA> lstUpdate = tbPageDB.tbPage_GetByAll(lang);
                            lstUpdate = lstUpdate.Where(s => s.pagLink == bfTagname).ToList();
                            if (lstUpdate.Count > 0)
                            {
                                for (int i = 0; i < lstUpdate.Count; i++)
                                {
                                    lstUpdate[i].pagLink = lst[0].pagTagName;
                                    tbPageDB.tbPage_Update(lstUpdate[i]);
                                }
                            }
                            #endregion
                            pnlErr.Visible = true;
                            ltrErr.Text = "Cập nhật địa điểm thành công !";
                            Resetcontrol();
                            BindData();
                            hidID.Value = "";
                            hidLevel.Value = "";
                            hidParId.Value = "";
                            pnlListForder.Visible = true;
                            pnlAddForder.Visible = false;
                        }

                    }
                }

            }
        }

        protected bool Validation()
        {
            if (txtTenchuyenmuc.Text == "")
            {
                ltrErr2.Text = "Chưa nhập tên địa điểm !";
                txtTenchuyenmuc.Focus();
                pnlErr2.Visible = true;
                return false;
            }

            if (drlChuyenmuc.SelectedValue != "" && hidCurLever.Value != "")
            {
                if (drlChuyenmuc.SelectedValue.StartsWith(hidCurLever.Value) && drlChuyenmuc.SelectedValue.Length > hidCurLever.Value.Length)
                {
                    ltrErr2.Text = "Nhóm thí sinh không thể là con chính nó !";
                    txtTenchuyenmuc.Focus();
                    pnlErr2.Visible = true;
                    return false;
                }
            }

            return true;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            Session["insert"] = "false";
            hidLevel.Value = "";
            hidParId.Value = "";
            hidID.Value = "";
        }

        void Resetcontrol()
        {
            txtTenchuyenmuc.Text = "";
            chkKichhoat.Checked = false;
            txtThuTu.Text = "1";
            hidID.Value = "";
            hidLevel.Value = "";
            hidParId.Value = "";
        }

        public static string ShowActiveClass(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "btn btn-primary btn-xs" : "btn btn-danger btn-xs";
        }
    }
}