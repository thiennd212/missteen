﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.contestant
{
    public partial class listContestant : System.Web.UI.UserControl
    {
        string _Admin = "";
        private string lang = "vi";
        int pageSize = 15;
        string userid = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); }
            if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); }
            if (Session["uid"] != null) userid = Session["uid"].ToString();

            if (Request["pages"] != null && !Request["pages"].Equals(""))
            {
                pageSize = int.Parse(Request["pages"].ToString());
            }
            if (!IsPostBack)
            {
                BindForder();
                BindData();
            }
        }

        void BindData()
        {
            var _listForder = db.Contestants.Where(s => s.Lang == lang).OrderByDescending(s => s.Id).ToList();

            if (txtSearch.Text != "")
            {
                _listForder = _listForder.Where(s => s.FullName.ToLower().Contains(txtSearch.Text.ToLower())).ToList();
            }

            if (drlForder.SelectedValue != "0")
            {
                _listForder = _listForder.Where(s => s.IdContestantGroup == int.Parse(drlForder.SelectedValue)).ToList();
            }

            if (_listForder.Count() > 0)
            {
                recordCount = _listForder.Count();
                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion

                var result = _listForder.Skip(currentPage * pageSize).Take(pageSize);
                rptFolderList.DataSource = result;
                rptFolderList.DataBind();
                BindPaging();
            }
            else
            {
                rptFolderList.DataSource = null;
                rptFolderList.DataBind();
            }
        }


        void BindForder()
        {
            string strForder = "";
            List<tbPageDATA> list = tbPageDB.tbPage_GetByAll(lang);
            list = list.Where(s => s.pagType == pageType.ContestantGroup).ToList();
            drlForder.Items.Clear();
            drlForder.Items.Add(new ListItem("- Nhóm thí sinh -", "0"));

            ddlCategory.Items.Clear();
            ddlCategory.Items.Add(new ListItem("- Nhóm cha -", "0"));
            for (int i = 0; i < list.Count; i++)
            {
                strForder = "";
                for (int j = 1; j < list[i].paglevel.Length / 5; j++)
                {
                    strForder = strForder + "---";
                }
                drlForder.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].pagId));
                ddlCategory.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].pagId));
            }
            list.Clear();
            list = null;

            // bind data for ddlPlace
            //string strPlace = "";
            //List<tbPageDATA> lstPlace = tbPageDB.tbPage_GetByAll(lang);
            //lstPlace = lstPlace.Where(s => s.pagType == pageType.ContestantPlace).ToList();
            ddlPlace.Items.Clear();
            ddlPlace.Items.Add(new ListItem("- Địa chỉ -", "0"));
            ddlPlace.Items.Add(new ListItem("Cần Thơ", "1"));
            ddlPlace.Items.Add(new ListItem("Đà Nẵng", "2"));
            ddlPlace.Items.Add(new ListItem("Hải Phòng", "3"));
            ddlPlace.Items.Add(new ListItem("Hà Nội", "4"));
            ddlPlace.Items.Add(new ListItem("TP HCM", "5"));
            ddlPlace.Items.Add(new ListItem("An Giang", "6"));
            ddlPlace.Items.Add(new ListItem("Bà Rịa - Vũng Tàu", "7"));
            ddlPlace.Items.Add(new ListItem("Bắc Giang", "8"));
            ddlPlace.Items.Add(new ListItem("Bắc Kạn", "9"));
            ddlPlace.Items.Add(new ListItem("Bạc Liêu", "10"));
            ddlPlace.Items.Add(new ListItem("Bắc Ninh", "11"));
            ddlPlace.Items.Add(new ListItem("Bến Tre", "12"));
            ddlPlace.Items.Add(new ListItem("Bình Định", "13"));
            ddlPlace.Items.Add(new ListItem("Bình Dương", "14"));
            ddlPlace.Items.Add(new ListItem("Bình Phước", "15"));
            ddlPlace.Items.Add(new ListItem("Bình Thuận", "16"));
            ddlPlace.Items.Add(new ListItem("Cà Mau", "17"));
            ddlPlace.Items.Add(new ListItem("Cao Bằng", "18"));
            ddlPlace.Items.Add(new ListItem("Đắk Lắk", "19"));
            ddlPlace.Items.Add(new ListItem("Đắk Nông", "20"));
            ddlPlace.Items.Add(new ListItem("Điện Biên", "21"));
            ddlPlace.Items.Add(new ListItem("Đồng Nai", "22"));
            ddlPlace.Items.Add(new ListItem("Đồng Tháp", "23"));
            ddlPlace.Items.Add(new ListItem("Gia Lai", "24"));
            ddlPlace.Items.Add(new ListItem("Hà Giang", "25"));
            ddlPlace.Items.Add(new ListItem("Hà Nam", "26"));
            ddlPlace.Items.Add(new ListItem("Hà Tĩnh", "27"));
            ddlPlace.Items.Add(new ListItem("Hải Dương", "28"));
            ddlPlace.Items.Add(new ListItem("Hậu Giang", "29"));
            ddlPlace.Items.Add(new ListItem("Hòa Bình", "30"));
            ddlPlace.Items.Add(new ListItem("Hưng Yên", "31"));
            ddlPlace.Items.Add(new ListItem("Khánh Hòa", "32"));
            ddlPlace.Items.Add(new ListItem("Kiên Giang", "33"));
            ddlPlace.Items.Add(new ListItem("Kon Tum", "34"));
            ddlPlace.Items.Add(new ListItem("Lai Châu", "35"));
            ddlPlace.Items.Add(new ListItem("Lâm Đồng", "36"));
            ddlPlace.Items.Add(new ListItem("Lạng Sơn", "37"));
            ddlPlace.Items.Add(new ListItem("Lào Cai", "38"));
            ddlPlace.Items.Add(new ListItem("Long An", "39"));
            ddlPlace.Items.Add(new ListItem("Nam Định", "40"));
            ddlPlace.Items.Add(new ListItem("Nghệ An", "41"));
            ddlPlace.Items.Add(new ListItem("Ninh Bình", "42"));
            ddlPlace.Items.Add(new ListItem("Ninh Thuận", "43"));
            ddlPlace.Items.Add(new ListItem("Phú Thọ", "44"));
            ddlPlace.Items.Add(new ListItem("Quảng Bình", "45"));
            ddlPlace.Items.Add(new ListItem("Quảng Nam", "46"));
            ddlPlace.Items.Add(new ListItem("Quảng Ngãi", "47"));
            ddlPlace.Items.Add(new ListItem("Quảng Ninh", "48"));
            ddlPlace.Items.Add(new ListItem("Quảng Trị", "49"));
            ddlPlace.Items.Add(new ListItem("Sóc Trăng", "50"));
            ddlPlace.Items.Add(new ListItem("Sơn La", "51"));
            ddlPlace.Items.Add(new ListItem("Tây Ninh", "52"));
            ddlPlace.Items.Add(new ListItem("Thái Bình", "53"));
            ddlPlace.Items.Add(new ListItem("Thái Nguyên", "54"));
            ddlPlace.Items.Add(new ListItem("Thanh Hóa", "55"));
            ddlPlace.Items.Add(new ListItem("Thừa Thiên Huế", "56"));
            ddlPlace.Items.Add(new ListItem("Tiền Giang", "57"));
            ddlPlace.Items.Add(new ListItem("Trà Vinh", "58"));
            ddlPlace.Items.Add(new ListItem("Tuyên Quang", "59"));
            ddlPlace.Items.Add(new ListItem("Vĩnh Long", "60"));
            ddlPlace.Items.Add(new ListItem("Vĩnh Phúc", "61"));
            ddlPlace.Items.Add(new ListItem("Yên Bái", "62"));
            ddlPlace.Items.Add(new ListItem("Phú Yên", "63"));

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            RepeaterItem item = default(RepeaterItem);
            for (int i = 0; i < rptFolderList.Items.Count; i++)
            {
                item = rptFolderList.Items[i];
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    if (((CheckBox)item.FindControl("chkBox")).Checked)
                    {
                        try
                        {
                            HiddenField hidID = (HiddenField)item.FindControl("hidCatID");
                            var itemDel = db.tbPages.Where(u => u.pagId == Convert.ToInt32(hidID.Value)).ToList();
                            if (itemDel.Count() > 0)
                            {
                                string levelss = itemDel[0].paglevel;
                                var procap1 = db.Contestants.Where(u => u.IdContestantGroup.Value == int.Parse(hidID.Value)).ToList();
                                if (procap1.Count > 0)
                                {
                                    db.Contestants.DeleteAllOnSubmit(procap1);
                                }

                                var pagecap1 = db.tbPages.Where(s => s.paglevel.Substring(0, levelss.Length) == levelss).ToList();
                                if (pagecap1.Count > 0)
                                {
                                    db.tbPages.DeleteAllOnSubmit(pagecap1);
                                }
                                db.tbPages.DeleteAllOnSubmit(itemDel);
                                db.SubmitChanges();
                            }
                        }
                        catch { }
                    }
                }
            }
            pnlErr.Visible = true;
            ltrErr.Text = "Xóa thành công thí sinh !";
            BindData();
            chkSelectAll.Checked = false;
        }

        protected void drlForder_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Del":
                    var itemDel = db.Contestants.FirstOrDefault(s => s.Id == int.Parse(strID));

                    if (itemDel != null)
                    {
                        db.Contestants.DeleteOnSubmit(itemDel);
                        db.SubmitChanges();
                    }

                    pnlErr.Visible = true;
                    ltrErr.Text = "Xóa thành công thí sinh !";
                    BindData();

                    break;
                case "Edit":
                    var itemEdit = db.Contestants.FirstOrDefault(s => s.Id == int.Parse(strID));

                    if (itemEdit != null)
                    {
                        BindForder();

                        txtIdentificationNumber.Text = itemEdit.IdentificationNumber.ToString() ?? "";
                        txtName.Text = itemEdit.FullName;
                        if (!string.IsNullOrEmpty(itemEdit.IdContestantGroup.ToString()))
                            ddlCategory.SelectedValue = itemEdit.IdContestantGroup.ToString();
                        if (!string.IsNullOrEmpty(itemEdit.ContestantPlace.ToString()))
                            ddlPlace.SelectedItem.Text = itemEdit.ContestantPlace.ToString();
                        txtAge.Text = itemEdit.Age.ToString() ?? "0";
                        txtHeight.Text = itemEdit.Height.ToString() ?? "0.00";
                        txtPersonalAchievements.Text = itemEdit.PersonalAchievements;
                        txtGifted.Text = itemEdit.Gifted;
                        txtHobby.Text = itemEdit.Hobby;
                        txtLifeMotto.Text = itemEdit.LifeMotto;
                        txtPointOfView.Text = itemEdit.PointOfView;

                        txtImage1.Text = itemEdit.Image1;
                        txtImage2.Text = itemEdit.Image2;
                        txtImage3.Text = itemEdit.Image3;
                        txtImage4.Text = itemEdit.Image4;
                        txtImage5.Text = itemEdit.Image5;
                        txtImage6.Text = itemEdit.Image6;
                        txtImage7.Text = itemEdit.Image7;
                        txtImage8.Text = itemEdit.Image8;
                        txtImage9.Text = itemEdit.Image9;
                        txtImage10.Text = itemEdit.Image10;
                        txtVoted.Text = itemEdit.TotalVoted.ToString() ?? "0";
                        if (itemEdit.IsHome != null) ckbHome.Checked = itemEdit.IsHome.Value;
                        ckbActive.Checked = itemEdit.Active == 1;

                    }

                    hidID.Value = strID;
                    pnlErr.Visible = false;
                    ltrErr.Text = "";
                    ltrErr2.Text = "";
                    pnlErr2.Visible = false;
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;

                    break;
                case "Add":
                    BindForder();
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    Resetcontrol();
                    ltrErr2.Text = "";
                    pnlErr2.Visible = false;
                    pnlErr.Visible = false;
                    ltrErr.Text = "";
                    break;
            }
        }

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }


        public static string ShowActive(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = false;
            pnlAddForder.Visible = true;
            Resetcontrol();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Validation() == true)
            {
                Contestant itemContestant = new Contestant();
                tbPage itemPage = new tbPage();
                var curId = hidID.Value;

                if (curId.Length > 0)
                {
                    itemContestant = db.Contestants.FirstOrDefault(s => s.Id == int.Parse(curId));
                    if (itemContestant != null)
                    {
                        itemPage = db.tbPages.FirstOrDefault(s => s.pagTagName == itemContestant.TagName);
                    }
                }

                itemContestant.IdContestantGroup = int.Parse(ddlCategory.SelectedValue);
                itemContestant.ContestantPlace = ddlPlace.SelectedItem.Text;
                itemContestant.FullName = txtName.Text.Trim();
                itemContestant.IdentificationNumber = int.Parse(txtIdentificationNumber.Text);
                itemContestant.Age = int.Parse(txtAge.Text);
                itemContestant.Height = Convert.ToDecimal(txtHeight.Text);
                itemContestant.PersonalAchievements = txtPersonalAchievements.Text;
                itemContestant.Gifted = txtGifted.Text;
                itemContestant.Hobby = txtHobby.Text;
                itemContestant.LifeMotto = txtLifeMotto.Text;
                itemContestant.PointOfView = txtPointOfView.Text;
                itemContestant.Image1 = txtImage1.Text;
                itemContestant.Image2 = txtImage2.Text;
                itemContestant.Image3 = txtImage3.Text;
                itemContestant.Image4 = txtImage4.Text;
                itemContestant.Image5 = txtImage5.Text;
                itemContestant.Image6 = txtImage6.Text;
                itemContestant.Image7 = txtImage7.Text;
                itemContestant.Image8 = txtImage8.Text;
                itemContestant.Image9 = txtImage9.Text;
                itemContestant.Image10 = txtImage10.Text;
                itemContestant.IsHome = ckbHome.Checked;
                itemContestant.Lang = lang;
                itemContestant.Active = ckbActive.Checked ? 1 : 0;
                itemContestant.TotalVoted = int.Parse(txtVoted.Text);
                var year = DateTime.Now.Year;
                itemContestant.BirthYear = year - int.Parse(txtAge.Text);

                string tag = txtName.Text + "-" + txtIdentificationNumber.Text + "";
                string tagName = MyWeb.Common.StringClass.NameToTag(tag);

                tbPage hasTagName = null;
                if (itemPage != null)
                {
                    itemPage.pagType = int.Parse(pageType.ContestantDetail);
                    itemPage.paglevel = "9999900000";
                    itemPage.pagTitle = txtName.Text;
                    itemPage.pagTagName = tagName;
                    hasTagName = db.tbPages.FirstOrDefault(s => s.pagTagName == tagName && s.pagId != itemPage.pagId);
                }

                if (curId.Length == 0)
                {
                    if (itemPage != null)
                        db.tbPages.InsertOnSubmit(itemPage);
                    db.SubmitChanges();

                    if (hasTagName != null)
                    {
                        var lastPage = db.tbPages.OrderByDescending(s => s.pagId).FirstOrDefault();

                        if (lastPage != null)
                        {
                            lastPage.pagTagName = tagName + "-" + lastPage.pagId;
                            itemContestant.TagName = lastPage.pagTagName;
                        }
                    }
                    else
                    {
                        var lastPage = db.tbPages.OrderByDescending(s => s.pagId).FirstOrDefault();

                        if (lastPage != null)
                        {
                            lastPage.pagTagName = tagName + "-" + lastPage.pagId;
                        }

                        itemContestant.TagName = tagName;

                    }

                    db.Contestants.InsertOnSubmit(itemContestant);
                    db.SubmitChanges();

                }
                else
                {
                    if (hasTagName != null)
                    {
                        if (itemPage != null)
                            itemPage.pagTagName = tagName + "-" + itemPage.pagId;
                        itemContestant.TagName = itemPage.pagTagName;
                    }
                    else
                    {
                        if (itemPage != null)
                            itemPage.pagTagName = tagName;
                        itemContestant.TagName = tagName;
                    }

                    db.SubmitChanges();
                }



                pnlErr.Visible = true;
                ltrErr.Text = curId.Length == 0 ? "Thêm mới thí sinh thành công" : "Cập nhật thí sinh thành công !";
                Resetcontrol();
                BindData();
                hidID.Value = "";
                hfIdPlace.Value = "";
                pnlListForder.Visible = true;
                pnlAddForder.Visible = false;
            }
        }

        protected bool Validation()
        {
            if (ddlCategory.SelectedValue == "0")
            {
                ltrErr2.Text = "Chưa chọn nhóm thí sinh !";
                ddlCategory.Focus();
                pnlErr2.Visible = true;
                return false;
            }
            else if (txtName.Text == "")
            {
                ltrErr2.Text = "Chưa nhập tên thí sinh !";
                txtName.Focus();
                pnlErr2.Visible = true;
                return false;
            }

            return true;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            Session["insert"] = "false";
            hidID.Value = "";
        }

        void Resetcontrol()
        {
            txtIdentificationNumber.Text = "";
            txtName.Text = "";
            txtAge.Text = "0";
            txtHeight.Text = "0.00";
            txtPersonalAchievements.Text = "";
            txtGifted.Text = "";
            txtHobby.Text = "";
            txtLifeMotto.Text = "";
            txtPointOfView.Text = "";
            txtVoted.Text = "0";
            ckbHome.Checked = false;
            ckbActive.Checked = false;
            txtImage1.Text = "";
            txtImage2.Text = "";
            txtImage3.Text = "";
            txtImage4.Text = "";
            txtImage5.Text = "";
            txtImage6.Text = "";
            txtImage7.Text = "";
            txtImage8.Text = "";
            txtImage9.Text = "";
            txtImage10.Text = "";
            hidID.Value = "";
            hfIdPlace.Value = "";
        }

        public static string ShowActiveClass(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "btn btn-primary btn-xs" : "btn btn-danger btn-xs";
        }

        protected string GetCategoryName(string id)
        {
            var item = db.tbPages.FirstOrDefault(s => s.pagId == int.Parse(id));

            return item != null ? item.pagName : "";
        }

    }
}