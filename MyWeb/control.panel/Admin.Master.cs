﻿using MyWeb.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        string _lang = "vi";
        HttpCookie myCookie = new HttpCookie("interface");
        HttpCookie _interface = null;
        public string strFaceValue = "";
        string[] _strAuthorized = null;
        string _strUserRole = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            checkCodeService.checkCodeWebsiteSoapClient svCheck = new checkCodeService.checkCodeWebsiteSoapClient();
            string webUrl = Request.RawUrl;
            string webIP = GetIPAddress();
            int check = 0;
            if (Session["checkCode"] == null)
            {
                check = svCheck.checkLicenseWebsite(webUrl, webIP);
            }
            else
            {
                check = Convert.ToInt32(Session["checkCode"].ToString());
            }
            if (check == 2)
            {
                Response.Redirect("/error404.html");
            }
            else if (check == 3)
            {
                Response.Redirect("/error404.html");
            }
            else
            {
                #region Load data
                _interface = Request.Cookies["interface"];
                if (_interface == null)
                {
                    strFaceValue = "MENU_LEFT";
                }
                else
                {
                    strFaceValue = _interface.Value.ToString();
                }

                //PHAN QUYỀN
                try
                {
                    var strUrl = Request.Url.AbsolutePath;
                    if (Session["UserRole"] != null) _strUserRole = Session["UserRole"].ToString();
                    if (_strUserRole == "2")
                    {
                        if (!(strUrl.Contains("control.panel") || strUrl.Contains("myacount") || strUrl.Contains("changepass")))
                        {
                            var objMenu = db.tbModules.FirstOrDefault(s => s.active == true && s.link == strUrl);
                            if (objMenu != null)
                            {
                                if (Session["strAuthorized"] != null) _strAuthorized = Session["strAuthorized"].ToString().Split(';').ToArray();
                                if (!(_strAuthorized.Contains(objMenu.treecode)))
                                {
                                    Response.Redirect("/admincp/");
                                }
                            }
                            else
                            {
                                Response.Redirect("/admincp/");
                            }
                        }
                    }
                }
                catch
                {
                    _strAuthorized = null;
                    Response.Redirect("/admincp/");
                }


                if (!IsPostBack)
                {
                    if (Session["p"] == null)
                    {
                        Response.Redirect("/admincp/");
                    }
                    AdminPannel.Action = Request.RawUrl;
                    _lang = MyWeb.Global.GetLangAdm();
                    if (MyWeb.Common.GlobalClass.CurrentUserNameLogon == null || MyWeb.Common.GlobalClass.CurrentUserNameLogon == "")
                    {

                    }
                    else
                    {
                        try
                        {
                            if (Session["user"] == null || Session["user"].ToString() == "")
                            {
                                MyWeb.Common.CookieClass cookie = new MyWeb.Common.CookieClass();
                                string sessionCount = cookie.GetCookie("SessionCount");
                                sessionCount = !String.IsNullOrEmpty(sessionCount) && sessionCount != "" ? sessionCount : "0";
                                List<tbUserDATA> objtbUserAdmin = tbUserDB.tbUser_GetByID(MyWeb.Common.GlobalClass.CurrentUserNameLogon);
                                if (objtbUserAdmin.Count > 0)
                                {
                                    dataAccessDataContext db = new dataAccessDataContext();
                                    Session["ctrname"] = "";
                                    Session["p"] = objtbUserAdmin[0].useLevel.ToString();
                                    MyWeb.Common.GlobalClass.CurrentUserName = objtbUserAdmin[0].useId.ToString();
                                    Session["uid"] = objtbUserAdmin[0].useId.ToString();
                                    Session["user"] = objtbUserAdmin[0].useUid;
                                    Session["hoten"] = objtbUserAdmin[0].useName;
                                    Session["admin"] = objtbUserAdmin[0].useAdmin;
                                    Session["mail"] = objtbUserAdmin[0].useMail;
                                    Session["Lang"] = MyWeb.Global.GetLang();
                                    Session["lanname"] = db.tbLanguages.FirstOrDefault(s => s.lanId.Trim().ToLower() == Session["Lang"].ToString()).lanName;
                                    Session["IsAuthorized"] = true;
                                    Session["UserRole"] = "Admin";
                                }
                            }
                        }
                        catch
                        {
                            Session["id"] = null;
                            Session["uid"] = null;
                            Session["p"] = null;
                            Session["c"] = null;
                            Session["IsAuthorized"] = false;
                            Session["UserRole"] = null;
                            MyWeb.Common.GlobalClass.CurrentUserName = "";
                            Session.Abandon();
                            Response.Redirect("/admincp/");
                        }
                    }
                }
                #endregion
            }
        }

        public string GetIPAddress()
        {
            //IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            //IPAddress ipAddress = ipHostInfo.AddressList[0];
            //return ipAddress.ToString();
            return Request.ServerVariables["LOCAL_ADDR"];
        }

        protected void ddlMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlMenu.SelectedValue == "1")
            {
                myCookie.Value = "MENU_TOP";
                myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);
                Response.Redirect("/control.panel/");
            }
            else if (ddlMenu.SelectedValue == "2")
            {
                myCookie.Value = "MENU_LEFT";
                myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);
                Response.Redirect("/control.panel/");
            }
            else
            {
                myCookie.Value = "MENU_LEFT";
                myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);
                Response.Redirect("/control.panel/");
            }
        }
    }
}