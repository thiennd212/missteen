﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.search
{
    public partial class listSearch : System.Web.UI.UserControl
    {
        private string lang = "vi";
        int pageSize = 15;
        string strWhere = "";
        string strOrderBy = "";
        string strKeyword = "";
        dataAccessDataContext db = new dataAccessDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["pages"] != null && !Request["pages"].Equals(""))
            {
                pageSize = int.Parse(Request["pages"].ToString());
            }

            lang = MyWeb.Global.GetLangAdm();
            if (!IsPostBack)
            {
                if (Session["txtSearchAdmin"] != null && Session["txtSearchAdmin"].ToString() != "")
                    strKeyword = Session["txtSearchAdmin"].ToString().TrimEnd().TrimStart();
                BindData();
            }
        }

        void BindData()
        {
            List<tbProductDATA> _result = new List<tbProductDATA>();

            strWhere += " AND LOWER(proLang) = '" + lang + "' ";

            strOrderBy = "proId DESC";
            if (Common.StringClass.Check(strKeyword))
            {
                strWhere += " AND (LOWER(proId) LIKE N'%" + strKeyword.ToLower() + "%' OR LOWER(proCode) LIKE N'%" + strKeyword.ToLower() + "%' OR LOWER(proName) LIKE N'%" + strKeyword.ToLower() + "%' OR LOWER(proPrice) LIKE N'%" + strKeyword.ToLower() + "%' )";
            }

            int intCount = 0;
            _result = tbProductDB.getProductListByAdmin(currentPage + 1, ref intCount, pageSize, 0, strWhere, strOrderBy);
            if (_result.Count() > 0)
            {
                recordCount = (int)intCount;
                rptProductList.DataSource = _result.ToList();
                rptProductList.DataBind();
                BindPaging();

                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion
            }
            else
            {
                rptProductList.DataSource = null;
                rptProductList.DataBind();
            }
        }

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        protected string BindImages(string strPart)
        {
            string str = "";
            try
            {
                str = "<img style=\"width:50px;\" src=\"" + strPart.Split(',')[0].Replace("uploads", "uploads/_thumbs") + "\" />";
            }
            catch (Exception)
            { }
            return str;
        }

        public string BindCateName(string strID)
        {
            string strReturn = "";
            var obj = db.tbPages.Where(u => u.pagId == Convert.ToInt32(strID)).ToList();
            if (obj.Count > 0)
            {
                strReturn = obj[0].pagName;
            }
            return strReturn;
        }

        public static string BindNumber(string strNumber)
        {
            Double giatri = 0;
            if (strNumber.ToString().Trim().Length > 0)
            {
                giatri = Double.Parse(strNumber.ToString());
                strNumber = giatri.ToString();
                string dp = display_so(strNumber);
                string p1 = "", p2 = "";
                if (dp.IndexOf(".") > -1)
                {
                    string[] s = dp.Split(Convert.ToChar("."));
                    p1 = s[0];
                    p2 = s[1];
                }
                else
                {
                    p1 = dp;
                }
                if (p1.Length > 3)
                {
                    dp = "";
                    int count = 0;
                    for (int i = p1.Length - 1; i > -1; i--)
                    {
                        count++;
                        dp = p1[i].ToString() + dp;
                        if (i < p1.Length - 1 && count % 3 == 0 && i > 0)
                        {
                            dp = "," + dp;
                        }


                    }
                    if (p2.Length > 0)
                        dp = dp + "." + p2;
                }

                return dp;
            }
            else
            {
                return "0";
            }
        }

        static string display_so(string so)
        {
            string pr = "";
            if (so.Length > 5)
            {
                string re = "";
                if (so.IndexOf(".") > -1)
                {
                    string[] s = so.Split(Convert.ToChar("."));
                    re = s[0];
                    if (s[1].Length > 2)
                    {
                        re += "." + s[1].Substring(0, 2);
                    }
                    else
                    {
                        re += "." + s[1];
                    }
                }
                else
                {
                    return so;
                }

                return re;
            }
            else
            {
                return so;
            }
            return so;
        }

        protected void rptProductList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {

                case "Edit":
                    Response.Redirect("/admin-product/add.aspx?idpro=" + strID);
                    break;
                case "Active":
                    tbProduct product = db.tbProducts.FirstOrDefault(s => s.proId == Int32.Parse(strID));
                    if (product.proActive == 0)
                    {
                        product.proActive = 1;
                    }
                    else { product.proActive = 0; }
                    db.SubmitChanges();
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Cập nhật thành công!!";
                    break;
                case "DEL":
                    string tagName = db.tbProducts.Where(s => s.proId == int.Parse(strID)).FirstOrDefault().proTagName;
                    tbPage pg = db.tbPages.Where(s => s.pagTagName == tagName).FirstOrDefault();
                    if (pg != null)
                    {
                        tbPageDB.tbPage_Delete(pg.pagId.ToString());
                    }
                    var comm = db.tbComments.Where(u => u.comProId == Convert.ToInt32(strID)).ToList();
                    if (comm.Count > 0)
                    {
                        for (int i = 0; i < comm.Count; i++)
                        {
                            db.tbComments.DeleteOnSubmit(comm[i]);
                            db.SubmitChanges();
                        }
                    }
                    tbProductDB.tbProduct_Delete(strID);
                    DeleteCenter(strID);
                    pnlErr.Visible = true;
                    ltrErr.Text = "Xóa thành công sản phẩm !!";
                    BindData();
                    break;
            }
        }

        public static string ShowActive(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }

        public static string ShowActiveClass(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "btn btn-primary btn-xs" : "btn btn-danger btn-xs";
        }

        private void DeleteCenter(string _strCatID)
        {
            IEnumerable<tbCatAtt> listcatatt = db.tbCatAtts.Where(c => c.catId == Int32.Parse(_strCatID));
            if (listcatatt.Count() > 0)
            {
                db.tbCatAtts.DeleteAllOnSubmit(listcatatt);
                db.SubmitChanges();
            }
        }
    }
}