﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="homePage.ascx.cs" Inherits="MyWeb.control.panel.website.homePage" %>

<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Trang chủ</a></li>
        <li class="active">Quản lý chung</li>
    </ol>
    <!-- end breadcrumb -->
    <h1 class="page-header">Quản lý chung</h1>
    <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-blue">
                <div class="stats-icon"><i class="fa fa-tags"></i></div>
                <div class="stats-info">
                    <h4>TỔNG SỐ SẢN PHẨM</h4>
                    <p>
                        <asp:Literal ID="ltrNumberPro" runat="server"></asp:Literal>
                    </p>
                </div>
                <div class="stats-link">
                    <a href="/admin-product/list.aspx">Xem chi tiết <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
            </div>
        </div>
        <!-- end col-3 -->

        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-green">
                <div class="stats-icon"><i class="fa fa-desktop"></i></div>
                <div class="stats-info">
                    <h4>SỐ BÀI VIẾT</h4>
                    <p>
                        <asp:Literal ID="ltrNews" runat="server"></asp:Literal>
                    </p>
                </div>
                <div class="stats-link">
                    <a href="/admin-news/list.aspx">Xem chi tiết <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
            </div>
        </div>
        <!-- end col-3 -->

        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-purple">
                <div class="stats-icon"><i class="fa fa-gift"></i></div>
                <div class="stats-info">
                    <h4>EMAIL KHUYẾN MẠI</h4>
                    <p>
                        <asp:Literal ID="ltrNumberEmail" runat="server"></asp:Literal>
                    </p>
                </div>
                <div class="stats-link">
                    <a href="/admin-promotion/list.aspx">Xem chi tiết <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
            </div>
        </div>
        <!-- end col-3 -->

        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-red">
                <div class="stats-icon"><i class="fa fa-shopping-cart"></i></div>
                <div class="stats-info">
                    <h4>ĐƠN HÀNG</h4>
                    <p>
                        <asp:Literal ID="ltrNumberOrder" runat="server"></asp:Literal>
                    </p>
                </div>
                <div class="stats-link">
                    <a href="/admin-order/list.aspx">Xem chi tiết <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
            </div>
        </div>
        <!-- end col-3 -->
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-inverse panel-with-tabs" data-sortable-id="ui-unlimited-tabs-1">
                <div class="panel-heading p-0">
                    <div class="panel-heading-btn m-r-10 m-t-10">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    </div>
                    <div class="tab-overflow">
                        <ul class="nav nav-tabs nav-tabs-inverse">
                            <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-inverse"><i class="fa fa-arrow-left"></i></a></li>
                            <li class="active"><a href="#latest-product" data-toggle="tab">Sản phẩm mới</a></li>
                            <li class=""><a href="#latest-news" data-toggle="tab">Tin mới</a></li>
                            <li class=""><a href="#latest-order" data-toggle="tab">Đơn hàng mới</a></li>
                            <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-inverse"><i class="fa fa-arrow-right"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="latest-product">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tên sản phẩm</th>
                                        <th>Thư mục</th>
                                        <th>Giá bán</th>
                                        <th></th>
                                        <th width="160">Công cụ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptProductList" runat="server" OnItemCommand="rptProductList_ItemCommand">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblorder" runat="server"></asp:Label></td>
                                                <td><%#DataBinder.Eval(Container.DataItem, "proName").ToString()%></td>
                                                <td><%#BindCateName(DataBinder.Eval(Container.DataItem, "catId").ToString())%></td>
                                                <td><%#BindNumber(DataBinder.Eval(Container.DataItem, "proPrice").ToString())%> đ</td>
                                                <td>
                                                    <asp:LinkButton ID="btnActive" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"proId")%>' CommandName="Active" class='<%#ShowActiveClass(DataBinder.Eval(Container.DataItem, "proActive").ToString())%>' ToolTip="Kích hoạt"><%#ShowActive(DataBinder.Eval(Container.DataItem, "proActive").ToString())%></asp:LinkButton>
                                                </td>
                                                <td>
                                                    <a class="btn btn-success btn-xs" href='/admin-product/add.aspx?idpro=<%#DataBinder.Eval(Container.DataItem,"proId")%>' title="Làm mới"><i class="fa fa-pencil-square-o"></i>Sửa</a>
                                                    <asp:LinkButton ID="btnDel" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"proId")%>' CommandName="DEL" OnClientClick="javascript:return confirm('Bạn có muốn xóa?');" class="btn btn-danger btn-xs" ToolTip="Xóa sản phẩm"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="latest-news">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tiêu đề</th>
                                        <th>Thư mục</th>
                                        <th></th>
                                        <th width="160">Công cụ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptNewsList" runat="server" OnItemCommand="rptNewsList_ItemCommand">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblorder" runat="server"></asp:Label></td>
                                                <td><%#Eval("newName").ToString()%></td>
                                                <td><%#BindCateNameNews(DataBinder.Eval(Container.DataItem, "grnID").ToString())%></td>
                                                <td>
                                                    <asp:LinkButton ID="btnActive" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"newid")%>' CommandName="Active" class='<%#ShowActiveClassNews(DataBinder.Eval(Container.DataItem, "newActive").ToString())%>' ToolTip="Kích hoạt"><%#ShowActiveNews(DataBinder.Eval(Container.DataItem, "newActive").ToString())%></asp:LinkButton>
                                                </td>
                                                <td>
                                                    <a class="btn btn-success btn-xs" href='/admin-news/list.aspx?id=<%#DataBinder.Eval(Container.DataItem,"newid")%>' title="Làm mới"><i class="fa fa-pencil-square-o"></i>Sửa</a>
                                                    <asp:LinkButton class="btn btn-danger btn-xs" ID="btnDel" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"newid")%>' CommandName="Del" ToolTip="Xóa" OnClientClick="javascript:return confirm('Bạn có muốn xóa?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="latest-order">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="10">
                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="False"></asp:CheckBox>
                                        </th>
                                        <th style="min-width:100px;">Tên KH</th>
                                        <th width="100">Điện thoại</th>
                                        <th width="180">Email</th>
                                        <th width="100">Ngày lập</th>
                                        <th width="100">Tổng tiền</th>
                                        <th width="120">Tình trạng</th>
                                        <th width="100">Công cụ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptFolderList" runat="server" OnItemCommand="rptFolderList_ItemCommand">
                                        <ItemTemplate>
                                            <tr class="even gradeC">
                                                <td>
                                                    <asp:CheckBox ID="chkBox" CssClass="chkBoxSelect" runat="server"></asp:CheckBox>
                                                    <asp:Label ID="lblID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"id")%>'></asp:Label>
                                                </td>
                                                <td>
                                                    <%#DataBinder.Eval(Container.DataItem, "name")%>
                                                </td>
                                                <td>
                                                    <%#DataBinder.Eval(Container.DataItem, "phone")%>
                                                </td>
                                                <td>
                                                    <%#DataBinder.Eval(Container.DataItem, "mail")%>
                                                </td>
                                                <td><%#FormatDate(DataBinder.Eval(Container.DataItem, "createdate").ToString())%></td>
                                                <td><%#NumberFormat(DataBinder.Eval(Container.DataItem, "totalmoney").ToString())%> vnđ</td>
                                                <td>
                                                    <%#Actives(DataBinder.Eval(Container.DataItem, "status").ToString())%>
                                                </td>
                                                <td>
                                                    <asp:LinkButton class="btn btn-danger btn-xs" ID="btnDel" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"id")%>' CommandName="Del" ToolTip="Xóa" OnClientClick="javascript:return confirm('Bạn có muốn xóa?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col-8 -->

        <div class="col-md-4">
            <div class="panel panel-inverse" data-sortable-id="index-2">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>

                    </div>
                    <h4 class="panel-title">Lịch</h4>
                </div>
                <div class="panel-body">
                    <div id="datepicker-inline" class="datepicker-full-width">
                        <div></div>
                    </div>
                </div>
            </div>
            <!-- end panel -->

        </div>
        <!-- end col-4 -->

    </div>
</div>
