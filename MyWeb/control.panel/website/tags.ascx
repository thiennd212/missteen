﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="tags.ascx.cs" Inherits="MyWeb.control.panel.website.tags" %>

<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="/control.panel/">Trang chủ</a></li>
        <li><a href="/admin-library/list.aspx">Thư viện</a></li>
        <li class="active">Quản lý thẻ tag</li>
    </ol>

    <asp:Panel ID="pnlListForder" runat="server" Visible="true">
        <h1 class="page-header">Quản lý thẻ tag</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse" data-sortable-id="table-basic-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Danh sách thẻ tag</h4>
                    </div>

                    <div class="alert alert-info fade in" id="pnlErr" runat="server" visible="false">
                        <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                        <button class="close" data-dismiss="alert" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-10">
                                <asp:LinkButton ID="btnAddNew" runat="server" class="btn btn-success btn-sm" OnClick="btnAddNew_Click"><i class="fa fa-plus"></i><span>Thêm mới</span></asp:LinkButton>
                                <asp:LinkButton ID="btnDeleteAll" runat="server" class="btn btn-danger btn-sm" OnClick="btnDeleteAll_Click" OnClientClick="javascript:return confirm('Bạn có muốn xóa thẻ tags đã chọn?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                            </div>

                            <div class="col-sm-2">
                                <asp:DropDownList ID="drlFind" runat="server" class="form-control input-sm" AutoPostBack="true" OnSelectedIndexChanged="drlFind_SelectedIndexChanged">
                                    <asp:ListItem Value="0"> - Loại thẻ tags - </asp:ListItem>
                                    <asp:ListItem Text="Loại sản phẩm" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Loại tin tức" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                                        <thead>
                                            <tr>
                                                <th width="10">
                                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="False"></asp:CheckBox>
                                                </th>
                                                <th>Tên thẻ tag</th>
                                                <th width="200">Tiêu đề</th>
                                                <th width="150">Mô tả</th>
                                                <th width="100">Từ khóa</th>
                                                <th width="60">Thứ tự</th>
                                                <th width="20"></th>
                                                <th width="100">Công cụ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptFolderList" runat="server" OnItemCommand="rptFolderList_ItemCommand">
                                                <ItemTemplate>
                                                    <tr class="even gradeC">
                                                        <td>
                                                            <asp:CheckBox ID="chkBox" CssClass="chkBoxSelect" runat="server"></asp:CheckBox>
                                                            <asp:HiddenField ID="hidCatID" Value='<%#DataBinder.Eval(Container.DataItem, "Id")%>' runat="server" />
                                                            <asp:Label ID="lblID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Id")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtNameForder" runat="server" Text='<%#Eval("Name").ToString()%>' AutoPostBack="true" OnTextChanged="txtNameForder_TextChanged" class="form-control input-sm"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtTit" runat="server" Text=' <%#DataBinder.Eval(Container.DataItem, "Title")%>' OnTextChanged="txtTit_TextChanged" AutoPostBack="true" class="form-control input-sm"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtDes" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Description")%>' OnTextChanged="txtDes_TextChanged" AutoPostBack="true" class="form-control input-sm"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "Keyword")%>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtNumberOrder" runat="server" Text='<%#Eval("Ord").ToString()%>' AutoPostBack="true" OnTextChanged="txtNumberOrder_TextChanged" class="form-control input-sm" onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="btnActive" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Id")%>' CommandName="Active" class="btn btn-primary btn-xs" ToolTip="Kích hoạt"><%#ShowActive(DataBinder.Eval(Container.DataItem, "Active").ToString())%></asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton class="btn btn-success btn-xs" ID="btnEdit" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Id")%>' CommandName="Edit" ToolTip="Sửa"><i class="fa fa-pencil-square-o"></i>Sửa</asp:LinkButton>
                                                            <asp:LinkButton class="btn btn-danger btn-xs" ID="btnDel" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Id")%>' CommandName="Del" ToolTip="Xóa" OnClientClick="javascript:return confirm('Bạn có muốn xóa?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                        <div class="row dataTables_wrapper">
                            <div class="col-sm-5">
                                <div id="data-table_info" class="dataTables_info" role="status" aria-live="polite">
                                    <asp:Literal ID="ltrStatistic" runat="server"></asp:Literal>
                                </div>
                            </div>

                            <div class="col-sm-7">
                                <div id="data-table_paginate" class="dataTables_paginate paging_simple_numbers">
                                    <ul class="pagination">
                                        <li id="data-table_previous" class="paginate_button previous disabled">
                                            <asp:LinkButton ID="btnPrevious" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Trước</asp:LinkButton>
                                        </li>
                                        <asp:Repeater ID="rptNumberPage" runat="server" OnItemCommand="rptNumberPage_ItemCommand" OnItemDataBound="rptNumberPage_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrLiPage" runat="server"></asp:Literal>
                                                <asp:LinkButton ID="btn" runat="server" CommandArgument='<%# Eval("PageIndex") %>' CommandName="page" Text='<%# Eval("PageText") %> '></asp:LinkButton></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <li id="data-table_next" class="paginate_button next">
                                            <asp:LinkButton ID="btnNext" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Sau</asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>




                    </div>

                </div>

            </div>

        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAddForder" runat="server" Visible="false">

        <h1 class="page-header">Thêm/sửa thẻ tag</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse" data-sortable-id="table-basic-2">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Thêm/sửa thẻ tag</h4>
                    </div>

                    <div class="panel-body panel-form">
                        <asp:Panel ID="pnlErr2" runat="server" Visible="false">
                            <div class="alert alert-danger fade in" style="border-radius: 0px;">
                                <button class="close" data-dismiss="alert" type="button">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <asp:Literal ID="ltrErr2" runat="server"></asp:Literal>
                            </div>
                        </asp:Panel>

                        <div class="form-horizontal form-bordered">
                            <div class="form-group">
                                <label class="control-label col-md-2">Thuộc loại:</label>
                                <div class="col-md-7">
                                    <asp:DropDownList runat="server" ID="drlType" class="form-control">
                                        <asp:ListItem Text="Loại sản phẩm" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Loại tin tức" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hidID" runat="server" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Tên thẻ tags:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtName" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Tag url:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtUrl" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Meta title:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtTitle" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Meta description:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtDescription" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Meta keyword:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtKeyword" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Thứ tự:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtThuTu" runat="server" class="form-control" onkeyup="valid(this,'quotes')" onblur="valid(this,'quotes')">1</asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Trang chủ:</label>
                                <div class="col-md-7">
                                    <asp:CheckBox ID="chkIndex" runat="server" Checked="true" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Hiển thị:</label>
                                <div class="col-md-7">
                                    <asp:CheckBox ID="chkActive" runat="server" Checked="true" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2"></label>
                                <div class="col-md-7">
                                    <asp:LinkButton ID="btnUpdate" runat="server" class="btn btn-primary" OnClick="btnUpdate_Click">Cập nhật</asp:LinkButton>
                                    <asp:LinkButton ID="btnReset" runat="server" class="btn btn-danger" OnClick="btnReset_Click">Hủy</asp:LinkButton>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>


    </asp:Panel>
</div>
