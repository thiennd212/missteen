﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="listOrder.ascx.cs" Inherits="MyWeb.control.panel.website.listOrder" %>

<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="/control.panel/">Trang chủ</a></li>
        <li class="active">Quản lý đơn hàng</li>
    </ol>
    <!-- end breadcrumb -->
    <asp:Panel ID="pnlListForder" runat="server" Visible="true">
        <h1 class="page-header">Quản lý đơn hàng</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse" data-sortable-id="table-basic-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Danh sách đơn hàng</h4>
                    </div>

                    <div class="alert alert-info fade in" id="pnlErr" runat="server" visible="false">
                        <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                        <button class="close" data-dismiss="alert" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-5">
                                <asp:TextBox ID="txtSearch" runat="server" class="form-control input-sm" placeholder="Tên / số điện thoại / email khách hàng "></asp:TextBox>
                            </div>

                            <div class="col-sm-2">
                                <asp:TextBox ID="txtStartDate" runat="server" class="form-control input-sm datepicker-autoClose" placeholder="Từ ngày"></asp:TextBox>
                            </div>

                            <div class="col-sm-2">
                                <asp:TextBox ID="txtEndDate" runat="server" class="form-control input-sm datepicker-autoClose" placeholder="Đến ngày"></asp:TextBox>
                            </div>

                            <div class="col-sm-2">
                                <asp:DropDownList ID="drlSortBy" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drlSortBy_SelectedIndexChanged">
                                    <asp:ListItem Value="0">- Sắp xếp theo -</asp:ListItem>
                                    <asp:ListItem Value="name">Tên A -> Z </asp:ListItem>
                                    <asp:ListItem Value="namedesc">Tên Z -> A</asp:ListItem>
                                    <asp:ListItem Value="price">Tên Giá tăng dần</asp:ListItem>
                                    <asp:ListItem Value="pricedesc">Tên Giá giảm dần</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="col-sm-1">
                                <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-primary btn-sm" Style="float: right;" OnClick="btnSearch_Click"><i class="fa fa-search"></i><span>Tìm kiếm</span></asp:LinkButton>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                                        <thead>
                                            <tr>
                                                <th width="10">
                                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="False"></asp:CheckBox>
                                                </th>
                                                <th>Tên khách hàng</th>
                                                <th width="100">Điện thoại</th>
                                                <th width="180">Email</th>
                                                <th width="100">Ngày lập</th>
                                                <th width="100">Tổng tiền</th>
                                                <th width="120">Tình trạng</th>
                                                <th width="100">Công cụ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptFolderList" runat="server" OnItemCommand="rptFolderList_ItemCommand">
                                                <ItemTemplate>
                                                    <tr class="even gradeC">
                                                        <td>
                                                            <asp:CheckBox ID="chkBox" CssClass="chkBoxSelect" runat="server"></asp:CheckBox>
                                                            <asp:Label ID="lblID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"id")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "name")%>
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "phone")%>
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "mail")%>
                                                        </td>
                                                        <td><%#FormatDate(DataBinder.Eval(Container.DataItem, "createdate").ToString())%></td>
                                                        <td><%#NumberFormat(DataBinder.Eval(Container.DataItem, "totalmoney").ToString())%> vnđ</td>
                                                        <td>
                                                            <%#Actives(DataBinder.Eval(Container.DataItem, "status").ToString())%>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton class="btn btn-success btn-xs" ID="btnEdit" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"id")%>' CommandName="Edit" ToolTip="Xem đơn hàng"><i class="fa fa-pencil-square-o"></i>Xem</asp:LinkButton>
                                                            <asp:LinkButton class="btn btn-danger btn-xs" ID="btnDel" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"id")%>' CommandName="Del" ToolTip="Xóa" OnClientClick="javascript:return confirm('Bạn có muốn xóa?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                        <div class="row dataTables_wrapper">
                            <div class="col-sm-5">
                                <div id="data-table_info" class="dataTables_info" role="status" aria-live="polite">
                                    <asp:Literal ID="ltrStatistic" runat="server"></asp:Literal>
                                </div>
                            </div>

                            <div class="col-sm-7">
                                <div id="data-table_paginate" class="dataTables_paginate paging_simple_numbers">
                                    <ul class="pagination">
                                        <li id="data-table_previous" class="paginate_button previous disabled">
                                            <asp:LinkButton ID="btnPrevious" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Trước</asp:LinkButton>
                                        </li>
                                        <asp:Repeater ID="rptNumberPage" runat="server" OnItemCommand="rptNumberPage_ItemCommand" OnItemDataBound="rptNumberPage_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrLiPage" runat="server"></asp:Literal>
                                                <asp:LinkButton ID="btn" runat="server" CommandArgument='<%# Eval("PageIndex") %>' CommandName="page" Text='<%# Eval("PageText") %> '></asp:LinkButton></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <li id="data-table_next" class="paginate_button next">
                                            <asp:LinkButton ID="btnNext" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Sau</asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>



                </div>
            </div>

        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAddForder" runat="server" Visible="false">
        <h1 class="page-header">Chi tiết đơn hàng</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse panel-with-tabs" data-sortable-id="ui-unlimited-tabs-1">
                    <div class="panel-heading p-0">
                        <div class="panel-heading-btn m-r-10 m-t-10">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        </div>
                        <div class="tab-overflow">
                            <ul class="nav nav-tabs nav-tabs-inverse">
                                <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-inverse"><i class="fa fa-arrow-left"></i></a></li>
                                <li class="active"><a href="#info-tab" data-toggle="tab">Thông tin đơn hàng</a></li>
                                <li class=""><a href="#contain-tab" data-toggle="tab">Sản phẩm</a></li>
                                <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-inverse"><i class="fa fa-arrow-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-content panel-body panel-form">
                        <asp:Panel ID="pnlErr2" runat="server" Visible="false">
                            <div class="alert alert-danger fade in" style="border-radius: 0px;">
                                <button class="close" data-dismiss="alert" type="button">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <asp:Literal ID="ltrErr2" runat="server"></asp:Literal>
                            </div>
                        </asp:Panel>
                        <div class="tab-pane fade active in" id="info-tab">
                            <div class="form-horizontal form-bordered">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Tên khách hàng:</label>
                                    <div class="col-md-7">
                                        <asp:HiddenField ID="hidID" runat="server" />
                                        <asp:TextBox ID="lblTenKH" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Địa chỉ:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="lblDiachi" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Điện thoại:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="lblDienthoai" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Điện thoại cố định:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="lblDienthoaicodinh" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Hình thức thanh toán:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="lblHinhthucthanhtoan" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Nội dung yêu cầu:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="lblNoiduingYC" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Tổng số tiền:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="lblSotien" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-2">Trạng thái:</label>
                                    <div class="col-md-7">
                                        <asp:DropDownList ID="drlStutus" runat="server" class="form-control">
                                            <asp:ListItem Value="0">- Tình trạng đơn hàng -</asp:ListItem>
                                            <asp:ListItem Value="1">Mới gửi</asp:ListItem>
                                            <asp:ListItem Value="5">Đang xử lý</asp:ListItem>
                                            <asp:ListItem Value="10">Thành công</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="tab-pane fade" id="contain-tab">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="panel-body">
                                        <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Tên sản phẩm</th>
                                                    <th width="100">Đơn giá</th>
                                                    <th width="180">Số lượng</th>
                                                    <th width="100">Tổng tiền</th>
                                                    <th width="100">Ngày mua</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptProList" runat="server">
                                                    <ItemTemplate>
                                                        <td>
                                                            <asp:Label ID="lblorder" runat="server"></asp:Label></td>
                                                        <td><%#DataBinder.Eval(Container.DataItem, "proname")%></td>
                                                        <td><%#DataBinder.Eval(Container.DataItem, "price")%> VNĐ</td>
                                                        <td><%#DataBinder.Eval(Container.DataItem, "number")%></td>
                                                        <td><%#NumberFormat(DataBinder.Eval(Container.DataItem, "money").ToString())%> VNĐ</td>
                                                        <td><%#FormatDate(DataBinder.Eval(Container.DataItem, "createdate").ToString())%></td>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal form-bordered">
                            <div class="form-group" style="border-top: 1px solid #eee;">
                                <label class="control-label col-md-2"></label>
                                <div class="col-md-7">
                                    <asp:LinkButton ID="btnUpdate" runat="server" class="btn btn-primary" OnClick="btnUpdate_Click">Cập nhật</asp:LinkButton>
                                    <asp:LinkButton ID="btnReset" runat="server" class="btn btn-danger" OnClick="btnReset_Click">Hủy</asp:LinkButton>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </asp:Panel>
</div>
