﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.website
{
    public partial class listBookNow : System.Web.UI.UserControl
    {
        private string lang = "vi";
        int pageSize = 15;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (!IsPostBack)
            {
                BindData();
            }
        }

        void BindData()
        {            
            var _listForder = db.tbContacts.Where(s=>s.conType==9 || s.conType==10).ToList();            
            if (_listForder.Any())
            {
                recordCount = _listForder.Count();
                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion
                
                var result = _listForder.Skip(currentPage * pageSize).Take(pageSize);
                rptFolderList.DataSource = result;
                rptFolderList.DataBind();
                BindPaging();
            }
            else
            {
                rptFolderList.DataSource = null;
                rptFolderList.DataBind();
            }
        }

        #region Page

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        #endregion

        protected void drlTrangthai_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected string FormatDate(object date)
        {
            if (date != null)
            {
                if (date.ToString().Trim().Length > 0)
                {
                    if (DateTime.Parse(date.ToString()).Year != 1900)
                    {
                        DateTime dNgay = Convert.ToDateTime(date.ToString());
                        return ((DateTime)dNgay).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        protected string Actives(string actives)
        {
            string Chuoi = "";
            if (actives == "0")
            {
                Chuoi = "Chưa xem";
            }
            else
            {
                Chuoi = "Đã xem";
            }
            return Chuoi;
        }

        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Edit":
                    var list = db.tbContacts.FirstOrDefault(s=> s.conId == int.Parse(strID));
                    txtTennguoiLH.Text = list.conName;
                    txtDienthoaicodinh.Text = list.conPositions;
                    txtDiachi.Text = list.conAddress;
                    txtDienthoai.Text = list.conTel;
                    txtFax.Text = list.confax;
                    txtMail.Text = list.conMail;
                    txtNoidung.Text = list.conDetail;
                    if (list.conMail.Length > 0)
                    {
                        lblFIledinhkem.Text = "";
                    }
                    else
                    {
                        lblFIledinhkem.Text = "<a href=\"" + list.conMail.ToString() + "\">" + list.conMail.ToString() + "</a>";
                    }
                    txtSoNguoi.Text = list.numberPeople.ToString();
                    txtThoiGian.Text = list.bookDate;
                    txtNgay.Text = FormatDate(list.conDate).ToString();                    
                    list.conActive = 1;
                    db.SubmitChanges();
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    break;
                case "Del":
                    tbContactDB.tbContact_Delete(strID);
                    BindData();
                    break;
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            Session["insert"] = "false";
            hidID.Value = "";
        }

        protected void Btn_Export_Click(object sender, EventArgs e)
        {
            string now = String.Format("{0:yyMMddHHmm}", DateTime.Now);
            //ExportToExcel(BindDatatable(), "C:\\Danh_Sach_Dat_Ban_" + now + ".xls");
            Response.ClearContent();

            Response.Buffer = true;

            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Danh_Sach_Dat_Ban_" + now + ".xls"));

            Response.ContentType = "application/ms-excel";

            //Encoding UTF-8
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

            StringWriter stringWriter = new StringWriter();

            HtmlTextWriter textWriter = new HtmlTextWriter(stringWriter);

            GridView excel = new GridView();
            excel.DataSource = BindDatatable();
            excel.DataBind();
            excel.RenderControl(textWriter);
            Response.Write(stringWriter.ToString());
            Response.End();  
        }

        protected DataTable BindDatatable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Tên người đặt bàn", typeof(string));
            dt.Columns.Add("Điện thoại", typeof(string));
            dt.Columns.Add("Email", typeof(string));
            dt.Columns.Add("Ngày đặt bàn", typeof(string));
            dt.Columns.Add("Số người", typeof(string));
            dt.Columns.Add("Nội dung yêu cầu bổ sung", typeof(string));
            try
            {
                var _listForder = db.tbContacts.Where(s => s.conType == 9 || s.conType == 10).ToList();
                if (_listForder.Any())
                {
                    recordCount = _listForder.Count();
                    #region Statistic
                    int fResult = currentPage * pageSize + 1;
                    int tResult = (currentPage + 1) * pageSize;
                    tResult = tResult > recordCount ? recordCount : tResult;                    
                    #endregion

                    var result = _listForder.Skip(currentPage * pageSize).Take(pageSize);
                    foreach (var item in result)
                    {
                        dt.Rows.Add(item.conName, item.conTel, item.conMail, FormatDate(item.conDate), item.numberPeople.ToString(), item.conDetail);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

        private void ExportToExcel(DataTable table, string filePath)
        {
            StreamWriter sw = new StreamWriter(filePath, false);
            sw.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            sw.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            sw.Write("<BR><BR><BR>");
            sw.Write("<Table border='1' bgColor='#ffffff' borderColor='#000000' cellSpacing='0' cellPadding='0' style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
            int columnscount = table.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {
                sw.Write("<Td>");
                sw.Write("<B>");
                sw.Write(table.Columns[j].ToString());
                sw.Write("</B>");
                sw.Write("</Td>");
            }
            sw.Write("</TR>");
            foreach (DataRow row in table.Rows)
            {
                sw.Write("<TR>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    sw.Write("<Td>");
                    sw.Write(row[i].ToString());
                    sw.Write("</Td>");
                }
                sw.Write("</TR>");
            }
            sw.Write("</Table>");
            sw.Write("</font>");
            sw.Close();
        }
    }
}