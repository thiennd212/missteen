﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.control
{
    public partial class ucLoadControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string action = Request["action"] != null ? Request["action"].ToString() : "";
                string type = Request["type"] != null ? Request["type"].ToString() : "";
                if (action == "setting")
                {
                    if (type == "configuration")
                    {
                        Controls.Add(LoadControl("~/control.panel/system/configuration.ascx"));
                    }
                    else if (type == "payment")
                    {
                        Controls.Add(LoadControl("~/control.panel/system/payment.ascx"));
                    }
                    else if (type == "myacount")
                    {
                        Controls.Add(LoadControl("~/control.panel/system/myacount.ascx"));
                    }
                    else if (type == "myacount")
                    {
                        Controls.Add(LoadControl("~/control.panel/system/myacount.ascx"));
                    }
                    else if (type == "language")
                    {
                        Controls.Add(LoadControl("~/control.panel/system/language.ascx"));
                    }
                    else if (type == "changepass")
                    {
                        Controls.Add(LoadControl("~/control.panel/system/changepass.ascx"));
                    }
                    else if (type == "listAcount")
                    {
                        Controls.Add(LoadControl("~/control.panel/system/listAcount.ascx"));
                    }
                }
                else if (action == "product")
                {
                    if (type == "list")
                    {
                        Controls.Add(LoadControl("~/control.panel/producs/producsList.ascx"));
                    }
                    else if (type == "add")
                    {
                        Controls.Add(LoadControl("~/control.panel/producs/producsAdd.ascx"));
                    }
                    else if (type == "category")
                    {
                        Controls.Add(LoadControl("~/control.panel/producs/producsCategory.ascx"));
                    }
                    else if (type == "attributes")
                    {
                        Controls.Add(LoadControl("~/control.panel/producs/producsAttributes.ascx"));
                    }
                    else if (type == "manufacturer")
                    {
                        Controls.Add(LoadControl("~/control.panel/producs/producsManufacturer.ascx"));
                    }
                }
                else if (action == "news")
                {
                    if (type == "category")
                    {
                        Controls.Add(LoadControl("~/control.panel/news/newsCategory.ascx"));
                    }
                    else if (type == "list")
                    {
                        Controls.Add(LoadControl("~/control.panel/news/newsList.ascx"));
                    }
                }
                else if (action == "library")
                {
                    if (type == "category")
                    {
                        Controls.Add(LoadControl("~/control.panel/library/libraryCategory.ascx"));
                    }
                    else if (type == "list")
                    {
                        Controls.Add(LoadControl("~/control.panel/library/libraryList.ascx"));
                    }
                }
                else if (action == "advertise")
                {
                    if (type == "file")
                    {
                        Controls.Add(LoadControl("~/control.panel/advertise/advertiseFile.ascx"));
                    }
                    else if (type == "list")
                    {
                        Controls.Add(LoadControl("~/control.panel/advertise/advertiseList.ascx"));
                    }
                }
                else if (action == "website")
                {
                    if (type == "listmenu")
                    {
                        Controls.Add(LoadControl("~/control.panel/website/listPage.ascx"));
                    }
                    else if (type == "cateonline")
                    {
                        Controls.Add(LoadControl("~/control.panel/website/cateOnline.ascx"));
                    }
                    else if (type == "online")
                    {
                        Controls.Add(LoadControl("~/control.panel/website/online.ascx"));
                    }
                    else if (type == "contact")
                    {
                        Controls.Add(LoadControl("~/control.panel/website/contact.ascx"));
                    }
                    else if (type == "contactcustomer")
                    {
                        Controls.Add(LoadControl("~/control.panel/website/contactCustomer.ascx"));
                    }                        
                    else if (type == "booknow")
                    {
                        Controls.Add(LoadControl("~/control.panel/website/listBookNow.ascx"));
                    }                        
                    else if (type == "comment")
                    {
                        Controls.Add(LoadControl("~/control.panel/website/comment.ascx"));
                    }
                    else if (type == "tags")
                    {
                        Controls.Add(LoadControl("~/control.panel/website/tags.ascx"));
                    }
                }
                else if (action == "client")
                {
                    if (type == "list")
                    {
                        Controls.Add(LoadControl("~/control.panel/client/listClient.ascx"));
                    }
                    else if (type == "promotion")
                    {
                        Controls.Add(LoadControl("~/control.panel/client/listPromotion.ascx"));
                    }
                }
                else if (action == "order")
                {
                    Controls.Add(LoadControl("~/control.panel/website/listOrder.ascx"));
                }
                else if (action == "search")
                {
                    Controls.Add(LoadControl("~/control.panel/search/listSearch.ascx"));
                }
                else if (action == "tour")
                {
                    if(type == "category")
                    {
                        Controls.Add(LoadControl("~/control.panel/tour/listCategory.ascx"));
                    }
                    else if(type == "topic")
                    {
                        Controls.Add(LoadControl("~/control.panel/tour/listTopic.ascx"));
                    }
                    else if (type == "place")
                    {
                        Controls.Add(LoadControl("~/control.panel/tour/listPlace.ascx"));
                    }
                    else if (type == "tour")
                    {
                        Controls.Add(LoadControl("~/control.panel/tour/listTour.ascx"));
                    }
                    else if (type == "booktour")
                    {
                        Controls.Add(LoadControl("~/control.panel/tour/listBookTour.ascx"));
                    }
                }
                else if (action == "contestant")
                {
                    if (type == "group")
                    {
                        Controls.Add(LoadControl("~/control.panel/contestant/listContestantGroup.ascx"));
                    }
                    else if (type == "contestant")
                    {
                        Controls.Add(LoadControl("~/control.panel/contestant/listContestant.ascx"));
                    }
                    else if (type == "place")
                    {
                        Controls.Add(LoadControl("~/control.panel/contestant/listPlace.ascx"));
                    }
                }
                else
                {
                    Controls.Add(LoadControl("~/control.panel/website/homePage.ascx"));
                }
            }
            catch
            {
                Response.Redirect("/admincp/");
            }
        }
    }
}