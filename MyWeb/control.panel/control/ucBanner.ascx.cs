﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.control
{
    public partial class ucBanner : System.Web.UI.UserControl
    {
        static string lang = "vi";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            string _strName = "";
            if (Session["hoten"] != null)
            {
                _strName = Session["hoten"].ToString();
                ltrUserName.Text = _strName;
            }
            if (!IsPostBack)
            {
                txtSearch.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSearch.ClientID + "')");

                BindData();
            }
        }

        void BindData()
        {
            //
            string strWhere = "";
            strWhere = " AND shop.lang ='" + lang + "'";
            var _obj = tbCustomersDB.tbCustomers_WhereByLinq(strWhere);
            if (_obj.Count() > 0)
            {
                ltrNumberOrder.Text = " (" + _obj.Count() + ")";

                _obj = _obj.Take(5).ToList();
                rptOrderList.DataSource = _obj;
                rptOrderList.DataBind();

                for (int i = 0; i < _obj.Count(); i++)
                {
                    Label lblorder = (Label)rptOrderList.Items[i].FindControl("lblorder");
                    int k = (i + 1);
                    lblorder.Text = k.ToString();
                }
            }


            //
            strWhere = " AND shop.lang ='" + lang + "' AND (status IS NULL OR status=1)";
            var _objNew = tbCustomersDB.tbCustomers_WhereByLinq(strWhere);
            if (_objNew.Count() > 0)
            {
                ltrOrderNew.Text = _objNew.Count().ToString();
            }
            else
            {
                ltrOrderNew.Text = "0";
            }
        }

        protected string FormatDate(object date)
        {
            if (date.ToString().Trim().Length > 0)
            {
                if (DateTime.Parse(date.ToString()).Year != 1900)
                {
                    DateTime dNgay = Convert.ToDateTime(date.ToString());
                    return ((DateTime)dNgay).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            try
            {
                string newUser = MyWeb.Common.GlobalClass.CurrentUserName.Replace(Session["user"].ToString().Trim().ToLower(), string.Empty);
                MyWeb.Common.GlobalClass.CurrentUserName = newUser;
                Session["id"] = string.Empty;
                Session["uid"] = string.Empty;
                Session["p"] = string.Empty;
                Session["c"] = string.Empty;
                Session["IsAuthorized"] = string.Empty;
                Session["UserRole"] = string.Empty;
                Session.Abandon();
            }
            catch
            {
                Session["id"] = string.Empty;
                Session["uid"] = string.Empty;
                Session["p"] = string.Empty;
                Session["c"] = string.Empty;
                Session["IsAuthorized"] = string.Empty;
                Session["UserRole"] = string.Empty;
                MyWeb.Common.GlobalClass.CurrentUserName = "";
                Session.Abandon();
            }
            Response.Redirect("/admincp/");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            var strSearch = txtSearch.Text;
            Session["txtSearchAdmin"] = txtSearch.Text;
            Response.Redirect("/admin-search/list.aspx");
        }
    }
}